import { SET_ENTITY } from "@/store/mutation-types"
import CrudService from "@/services/crud.service";

export default {
	namespaced: true,
	state: {
		indexTableCols: [
			{
				text: "#",
				value: "index",
				width: "50",
				active: true,
				sortable: false,
				align: "center",
			},
			{
				text: "components.execution-duration",
				value: "document.deadline",
				width: "150",
				active: true,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: "ui-countdown",
				detail: true,
				order: 3
			},
			// {
			// 	text: "document-flow.views.registration.state",
			// 	value: "is_read",
			// 	width: "135",
			// 	active: true,
			// 	sortable: false
			// },
			{
				text: "priority",
				value: "document.priority",
				width: "150",
				active: true,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: "ui-priority",
				detail: true,
				order: 6
			},
			{
				text: "components.registration-number",
				value: "document.register_number",
				active: true,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: null,
				detail: true,
				order: 4
			},
			{
				text: "components.document-type",
				value: "document.doc_type.name",
				active: true,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: null,
				detail: true,
				order: 1
			},
			{
				text: "document-flow.views.boxes.journal",
				value: "document.journal.name",
				active: true,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: null,
				detail: true,
				order: 12
			},
			{
				text: "control-tasks.create-task.deadline",
				value: "deadline",
				active: true,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: null,
				detail: true,
				order: 12
			},
			// {
			// 	text: "document-flow.views.boxes.receipt-date",
			// 	value: "assignment.receipt_date",
			// 	active: true,
			// 	sortable: false,
			// 	cols: {
			// 		"md": "4"
			// 	},
			// 	component: "ui-date-text",
			// 	detail: true,
			// 	order: 13
			// },
			{
				text: "components.outgoing-number",
				value: "document.outgoing_number",
				active: false,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: null,
				detail: true,
				order: 7
			},
			{
				text: "document-flow.components.registration-date",
				value: "document.register_date",
				active: true,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: "ui-date-text",
				detail: true,
				order: 5
			},
			{
				text: "document-flow.components.outgoing-date",
				value: "document.outgoing_date",
				active: false,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: "ui-date-text",
				detail: true,
				order: 8
			},
			{
				text: "document-flow.views.registration.author",
				value: "document.created_by",
				active: true,
				sortable: false,
				width: "125",
				cols: {
					"md": "4"
				},
				component: "ui-avatar",
				detail: true,
				order: 2
			},
			{
				text: "document-flow.components.correspondent",
				value: "document.correspondent.organization_name",
				active: true,
				sortable: false,
				width: "300",
				cols: {
					"md": "12"
				},
				component: null,
				detail: true,
				order: 17
			},
			{
				text: "document-flow.views.boxes.content",
				value: "document.description",
				active: false,
				sortable: false,
				width: "300",
				cols: {
					"md": "12"
				},
				component: null,
				detail: true,
				order: 18
			},
			// {
			// 	text: "components.status",
			// 	value: "status.name",
			// 	width: "140",
			// 	sortable: false,
			// 	active: true,
			// 	cols: {
			// 		"md": "4"
			// 	},
			// 	component: "ui-status",
			// 	detail: true,
			// 	order: 3
			// },
		],
		indexTableFilters: {
			search: null,
			priority: null,
			is_read: null,
			has_deadline: null,
			type: null,
			is_agreed: null,
			has_resolution: null,
			status: null,
			statusList: null,
			condition: null,
			journal: null,
			doc_type: null,
			doc_type_name: null,
			register_number: null,
			register_start_date: null,
			register_end_date: null,
			outgoing_number: null,
			outgoing_start_date: null,
			outgoing_end_date: null,
			correspondent: null,
			correspondent_name: null,
			controller: null,
			__correspondent: null,
			__doc_type: null,
			__register_date: null,
			__outgoing_date: null,
			review_user: null,
			review_user_name: null,
			__review_user: null,
			page: 1,
			page_size: 15
		},
		createdResolutionDetailModelLoader: true,
		createdResolutionDetailModel: {},
		resolutionPerformersList: [],
	},
	/**
	 *
	 **/
	actions: {
		/*
		*
		* */
		async fetchCreatedResolutionById({ commit, rootState }, payload) {
			if(payload.loader) {
				commit(SET_ENTITY, {
					module: "documentFlowBoxesCreatedResolutions",
					entity: "createdResolutionDetailModelLoader",
					value: true
				}, {root: true})
			}

			await CrudService.getList(`v2/docflow/${rootState.auth.currentUser.filial}/assignment-by-self/${payload.id}/`)
			.then((data) => {
				commit(SET_ENTITY, {
					module: "documentFlowBoxesCreatedResolutions",
					entity: "createdResolutionDetailModel",
					value: data
				}, {root: true})

				setTimeout(() => {
					commit(SET_ENTITY, {
						module: "documentFlowBoxesCreatedResolutions",
						entity: "createdResolutionDetailModelLoader",
						value: false
					}, {root: true})
				}, 1000)
			})
		},
		/*
		* Получить созданную резолюцию
		* */
		async fetchGetCreatedResolution({ commit, rootState, state }, payload) {
			await CrudService.getList(`v2/docflow/${rootState.auth.currentUser.filial}/assignment/${payload.id}/`)
			.then((data) => {
				state.createdResolutionDetailModel.assignees = {
					assignment: data.assignment,
					check_id: data.check_id,
					content: data.content,
					deadline: data.deadline,
					id: data.id,
					parent: data.parent,
					review: data.review,
					is_verified: data.is_verified,
					type: data.type
				}
			})
		},
		/*
		* Удалить созданную резолюцию
		* */
		async fetchDeleteCreatedResolution({ commit, rootState, state, dispatch }, payload) {
			await this.$axios.delete(`v2/docflow/${rootState.auth.currentUser.filial}/assignment/${payload.id}/`, {
				params: {
					comment: payload.comment
				}
			})
			.then(() => {

			})
		},
		/*
		* Изменить созданную резолюцию
		* */
		async fetchUpdateCreatedResolution({ commit, rootState, state, dispatch }, payload) {
			await this.$axios.put(
				`v2/docflow/${rootState.auth.currentUser.filial}/assignment/${payload.id}/`,
				{ ...payload.model }
			)
			.then(({ data }) => {
				dispatch(
					"fetchCreatedResolutionById",
					{ id: payload.documentId, loader: false }
				)

				dispatch(
					"treeView/fetchTreeUsersList",
					{ treeId: payload.treeId },
					{ root: true }
				)
			})
		},
		/*
		* Заполняем нужные поля колонки таблицы если пользователь перезагрузил браузер
		* */
		setActiveIndexTableCols({ commit }, payload) {
			commit(SET_ENTITY, { module: "documentFlowBoxesCreatedResolutions", entity: "indexTableCols", value: payload }, { root: true })
		},
		/*
		*
		* */
		resetIndexTableCols({ state }) {
			state.indexTableCols = [
				{
					text: "#",
					value: "index",
					width: "50",
					active: true,
					sortable: false,
					align: "center",
				},
				// {
				// 	text: "document-flow.views.registration.state",
				// 	value: "is_read",
				// 	width: "135",
				// 	active: true,
				// 	sortable: false
				// },
				{
					text: "components.execution-duration",
					value: "document.deadline",
					width: "150",
					active: true,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: "ui-countdown",
					detail: true,
					order: 3
				},
				{
					text: "priority",
					value: "document.priority",
					width: "150",
					active: true,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: "ui-priority",
					detail: true,
					order: 6
				},
				{
					text: "components.registration-number",
					value: "document.register_number",
					active: true,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: null,
					detail: true,
					order: 4
				},
				{
					text: "components.document-type",
					value: "document.doc_type.name",
					active: true,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: null,
					detail: true,
					order: 1
				},
				{
					text: "document-flow.views.boxes.journal",
					value: "document.journal.name",
					active: true,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: null,
					detail: true,
					order: 12
				},
				{
					text: "control-tasks.create-task.deadline",
					value: "deadline",
					active: true,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: null,
					detail: true,
					order: 12
				},
				// {
				// 	text: "document-flow.views.boxes.receipt-date",
				// 	value: "assignment.receipt_date",
				// 	active: true,
				// 	sortable: false,
				// 	cols: {
				// 		"md": "4"
				// 	},
				// 	component: "ui-date-text",
				// 	detail: true,
				// 	order: 13
				// },
				{
					text: "components.outgoing-number",
					value: "document.outgoing_number",
					active: false,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: null,
					detail: true,
					order: 7
				},
				{
					text: "document-flow.components.registration-date",
					value: "document.register_date",
					active: true,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: "ui-date-text",
					detail: true,
					order: 5
				},
				{
					text: "document-flow.components.outgoing-date",
					value: "document.outgoing_date",
					active: false,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: "ui-date-text",
					detail: true,
					order: 8
				},
				{
					text: "document-flow.views.registration.author",
					value: "document.created_by",
					active: true,
					sortable: false,
					width: "125",
					cols: {
						"md": "4"
					},
					component: "ui-avatar",
					detail: true,
					order: 2
				},
				{
					text: "document-flow.components.correspondent",
					value: "document.correspondent.organization_name",
					active: true,
					sortable: false,
					width: "300",
					cols: {
						"md": "12"
					},
					component: null,
					detail: true,
					order: 17
				},
				{
					text: "document-flow.views.boxes.content",
					value: "document.description",
					active: false,
					sortable: false,
					width: "300",
					cols: {
						"md": "12"
					},
					component: null,
					detail: true,
					order: 18
				},
				// {
				// 	text: "components.status",
				// 	value: "status.name",
				// 	width: "140",
				// 	sortable: false,
				// 	active: true,
				// 	cols: {
				// 		"md": "4"
				// 	},
				// 	component: "ui-status",
				// 	detail: true,
				// 	order: 3
				// },
			]
		},
		/*
		*
		* */
		async resetIndexTableFilters({ state }) {
			state.indexTableFilters = {
				search: null,
				priority: null,
				is_read: null,
				has_deadline: null,
				type: null,
				is_agreed: null,
				has_resolution: null,
				status: null,
				statusList: null,
				condition: null,
				journal: null,
				doc_type: null,
				doc_type_name: null,
				register_number: null,
				register_start_date: null,
				register_end_date: null,
				outgoing_number: null,
				outgoing_start_date: null,
				outgoing_end_date: null,
				correspondent: null,
				correspondent_name: null,
				controller: null,
				__correspondent: null,
				__doc_type: null,
				__register_date: null,
				__outgoing_date: null,
				review_user: null,
				review_user_name: null,
				__review_user: null,
				page: 1,
				page_size: 15
			}
		},
		/*
		* Заполняем нужные поля фильтра если пользователь перезагрузил браузер
		* */
		setActiveIndexTableFilters({ commit, state }, payload) {
			Object.entries(payload).forEach(item => {
				const [key, value] = item

				if(value && (typeof value === "boolean" || Array.isArray(value))) {
					state.indexTableFilters[key] = value
				}

				if(!(value && (typeof value === "boolean"))) {
					state.indexTableFilters[key] = value
				}

				if(value && (typeof value === "number" || typeof value === "string")) {
					state.indexTableFilters[key] = Number(value) ? Number(value) : value
				}

				if(value === "true" || value === "false") {
					state.indexTableFilters[key] = value === "true"
				}
			})

			Object.entries(state.indexTableFilters).forEach(item => {
				const [key, value] = item

				if(value && value.length && key === "statusList") {
					state.indexTableFilters.statusList = state.indexTableFilters.statusList.map(item => parseInt(item))
				}

				if(value && key === "doc_type_name") {
					state.indexTableFilters.__doc_type = {
						id: state.indexTableFilters.doc_type,
						name: state.indexTableFilters.doc_type_name,
						type: "doc_type"
					}
				}

				if(value && key === "register_start_date") {
					state.indexTableFilters.__register_date = [state.indexTableFilters.register_start_date, state.indexTableFilters.register_end_date]
				}

				if(value && key === "outgoing_start_date") {
					state.indexTableFilters.__outgoing_date = [state.indexTableFilters.outgoing_start_date, state.indexTableFilters.outgoing_end_date]
				}

				if(value && key === "correspondent_name") {
					state.indexTableFilters.__correspondent = {
						id: state.indexTableFilters.correspondent,
						organization_name: state.indexTableFilters.correspondent_name
					}
				}

				if(value && key === "review_user_name") {
					state.indexTableFilters.__review_user = {
						id: state.indexTableFilters.review_user,
						full_name: state.indexTableFilters.review_user_name
					}
				}
			})
		}
	},
}
