import { format } from "date-fns"
import { SET_ENTITY } from "@/store/mutation-types"

let day = new Date()
export default {
  namespaced: true,
  state: {
    indexTableCols: [
      {
        text: "#",
        value: "index",
        width: "50",
        active: true,
        sortable: false,
        align: "center",
      },
      {
        text: "control-tasks.create-task.document-type",
        value: "doc_type.name",
        active: true,
        sortable: false,
      },
      {
        text: "components.registration-number",
        value: "register_number",
        active: true,
        sortable: false,
      },
      {
        text: "document-flow.components.registration-date",
        value: "register_date",
        active: true,
        sortable: false,
      },
      {
        text: "document-flow.views.boxes.branch",
        value: "sender",
        active: true,
        width: "300",
        sortable: false,
      },
      {
        text: "components.author",
        value: "created_by",
        active: true,
        sortable: false,
      },
      {
        text: "components.status",
        value: "status.name",
        width: "140",
        active: true,
        sortable: false,
      },
    ],
    indexTableFilters: {
      search: null,
      author: null,
      author_full_name: null,
      filial: null,
      priority: null,
      is_read: null,
      has_deadline: null,
      type: null,
      is_agreed: null,
      has_resolution: null,
      signer: null,
      status: null,
      statusList: null,
      condition: null,
      journal: null,
      department: null,
      doc_type: null,
      doc_type_name: null,
      register_number: null,
      register_start_date: format(Date.now() - (day.getDate() - 1) * 24 * 60 * 60 * 1000, "yyyy-MM-dd"),
      register_end_date: format(day, "yyyy-MM-dd"),
      outgoing_number: null,
      outgoing_start_date: null,
      outgoing_end_date: null,
      correspondent: null,
      correspondent_name: null,
      review_user: null,
      full_name: null,
      __author: null,
      __correspondent: null,
      __doc_type: null,
      __register_date: format(Date.now() - (day.getDate() - 1) * 24 * 60 * 60 * 1000, "yyyy-MM-dd"),
      __outgoing_date: null,
      __review_user: null,
      __signer: null,
      page: 1,
      page_size: 15
    }
  },
  actions: {
    setActiveIndexTableCols({ commit }, payload) {
      commit(SET_ENTITY, { module: "documentFlowRegistrationFilialDocument", entity: "indexTableCols", value: payload }, { root: true })
    },
    /**/
    resetIndexTableCols({ state }) {
      state.indexTableCols = [
        {
          text: "#",
          value: "index",
          width: "50",
          active: true,
          sortable: false,
          align: "center",
        },
        {
          text: "control-tasks.create-task.document-type",
          value: "doc_type.name",
          active: true,
          sortable: false,
        },
        {
          text: "components.registration-number",
          value: "register_number",
          active: true,
          sortable: false,
        },
        {
          text: "document-flow.components.registration-date",
          value: "register_date",
          active: true,
          sortable: false,
        },
        {
          text: "document-flow.views.boxes.branch",
          value: "sender",
          active: true,
          width: "300",
          sortable: false,
        },
        {
          text: "components.author",
          value: "created_by",
          active: true,
          sortable: false,
        },
        {
          text: "components.status",
          value: "status.name",
          width: "140",
          active: true,
          sortable: false,
        },
      ]
    },
    /**/
    async resetIndexTableFilters({ state, dispatch }) {
      state.indexTableFilters = {
        search: null,
        author: null,
        author_full_name: null,
        filial: null,
        priority: null,
        is_read: null,
        has_deadline: null,
        type: null,
        is_agreed: null,
        has_resolution: null,
        signer: null,
        status: null,
        statusList: null,
        condition: null,
        journal: null,
        department: null,
        doc_type: null,
        doc_type_name: null,
        register_number: null,
        register_start_date: format(Date.now() - (day.getDate() - 1) * 24 * 60 * 60 * 1000, "yyyy-MM-dd"),
        register_end_date: format(day, "yyyy-MM-dd"),
        outgoing_number: null,
        outgoing_start_date: null,
        outgoing_end_date: null,
        correspondent: null,
        correspondent_name: null,
        review_user: null,
        full_name: null,
        __author: null,
        __correspondent: null,
        __doc_type: null,
        __register_date: format(Date.now() - (day.getDate() - 1) * 24 * 60 * 60 * 1000, "yyyy-MM-dd"),
        __outgoing_date: null,
        __review_user: null,
        __signer: null,
        page: 1,
        page_size: 15
      }

      dispatch("setActiveIndexTableFilters", state.indexTableFilters)
    },
    /**/
    setActiveIndexTableFilters({ commit, state }, payload) {
      Object.entries(payload).forEach(item => {
        const [key, value] = item

        if(value && (typeof value === "boolean" || Array.isArray(value))) {
          state.indexTableFilters[key] = value
        }

        if(value && (typeof value === "number" || typeof value === "string")) {
          state.indexTableFilters[key] = Number(value) ? Number(value) : value
        }

        if(value === "true" || value === "false") {
          state.indexTableFilters[key] = value === "true"
        }
      })

      Object.entries(state.indexTableFilters).forEach(item => {
        const [key, value] = item

        if(value && value.length && key === "statusList") {
          state.indexTableFilters.statusList = state.indexTableFilters.statusList.map(item => parseInt(item))
        }

        if(value && key === "doc_type_name") {
          state.indexTableFilters.__doc_type = {
            id: state.indexTableFilters.doc_type,
            name: state.indexTableFilters.doc_type_name,
            type: "doc_type"
          }
        }

        if(value && key === "register_start_date") {
          state.indexTableFilters.__register_date = [state.indexTableFilters.register_start_date, state.indexTableFilters.register_end_date]
        }

        if(value && key === "outgoing_start_date") {
          state.indexTableFilters.__outgoing_date = [state.indexTableFilters.outgoing_start_date, state.indexTableFilters.outgoing_end_date]
        }

        if(value && key === "correspondent_name") {
          state.indexTableFilters.__correspondent = {
            id: state.indexTableFilters.correspondent,
            organization_name: state.indexTableFilters.correspondent_name
          }
        }

        if (value && key === "review_user") {
          state.indexTableFilters.__review_user = {
            id: state.indexTableFilters.review_user,
            full_name: state.indexTableFilters.full_name
          }
        }
      })
    }
  }
}
