import router from "@/router";
import { getRefreshToken, getToken, removeToken, saveAccessToken } from "@/services/jwt.service";
import Socket from "@/services/socket.service";
import store from "@/store";
import Axios from "axios";
import Vue from "vue";

Axios.interceptors.request.use(
  (config) => {
    config.headers = {
      Authorization: getToken() ? "Token " + getToken() : "",
      Accept: "application/json"
    };

    return config;
    },
    (error) => Promise.reject(error)
);

let refreshToken = false
let subscribers = []

const HTTP_AUTH = Axios.create({
  baseURL: process.env.VUE_APP_BASE_URL
});

HTTP_AUTH.interceptors.request.use(
  async (config) => config,
  (error) => Promise.reject(error)
);

HTTP_AUTH.interceptors.response.use(
  response => response,
  error => Promise.reject(error.response)
)

Axios.interceptors.response.use(
  (response) => response,
  (error) => {
    const originalRequest = error.config
    const language = localStorage.getItem('lang') || 'uz';
    // Если нет токена
    if(!getToken()) {
      router.push({ name: "AuthLoginWithPhone" }).catch(() => {})
    }

    if(getToken() === '[object Object]') {
      removeToken()

      router.push({ name: "AuthLoginWithPhone" }).catch(() => {})
      return false;
    }

    // Незарегистрированные пользователи
    if (error.response.status === 401) {
      // Не верный логин или пароль
      if(error.response.data.detail === "Логин или пароль неверны") {
        const loginError = language === 'uz' ? "Login yoki parol noto'g'ri" : "Логин или пароль неверный"
        Vue.$toast.error(loginError, { timeout: 3000 })
        return false;
      }

      // Инвалид токен
      if(error.response.data.message.code === "token_not_valid" && !refreshToken) {
        refreshToken = true

        HTTP_AUTH.post(`/api/token/refresh/`, {
          refresh: getRefreshToken()
        })
        .then(({ data }) => {
          refreshToken = false
          onRefreshed(data.access)

          saveAccessToken(data.access)

          setTimeout(() => {
            let socket = new Socket()

            store.dispatch("socket/initUserHandShakeSocket", socket.init(process.env.VUE_APP_SOCKET_URL, getToken()), { root: true })
            .then(r  => {
              store.getters["socket/getUserHandShakeSocket"].onOpen({
                command: "user_handshake"
              })

              store.getters["socket/getUserHandShakeSocket"].onMessageReceive()
            })
          }, 1500)
        })
        .catch(() => {
          removeToken()

          router.push({ name: "AuthLayout" })
          .then(() => {
            window.location.reload()
          })
          .catch(() => {})
        })
      }

      return new Promise((resolve) => {
        subscribeTokenRefresh(access_token => {
          originalRequest.headers.Authorization = "Token " + access_token
          resolve(Axios(originalRequest))
        })
      })
    }

    // Ошибки сервера
    if(error.response.status === 500) {
      const error=language === 'uz'?'Server xatoligi':'Ошибка сервера'
      Vue.$toast.error(error)
    }
    else if(error.response.status === 502) {
      const error=language === 'uz'?'Xajmi katta ma\'lumotlar so\'raldi!':'Запрошены огромные данные!'
      Vue.$toast.error(error)
    }
    /*
    * data: ["text"]
    * */
    if(Array.isArray(error.response.data)) {
      error.response.data.forEach(item => {
        Vue.$toast.error(item, { timeout: 3000 })
      })

      return false
    }
    /*
    * Если data объект
    * */
    if(error.response.data instanceof Object && error.response.data.__proto__.constructor === Object) {
      // Если есть ключ detail
      if(error.response.data.hasOwnProperty("detail")) {
        let detail = error.response.data.detail

        // Если detail объект
        if(detail instanceof Object && detail.__proto__.constructor === Object) {
          Vue.$toast.error(Object.values(detail)[0], { timeout: 3000 })
        }
        // Если detail {string}
        else {
          Vue.$toast.error(detail, { timeout: 3000 })
        }
      }
      else {
        let entries = Object.entries(error.response.data)

        entries.forEach(entry => {
          let [ key, value ] = entry

          Vue.$toast.error(`${ (key[0].toUpperCase() + key.slice(1)).replace(/[_.-]/gi, " ") }: ${ Array.isArray(value) ? value.join(' ') : value }`,
            { timeout: 3000 }
          )
        })
      }
    }

    return Promise.reject(error.response);
  }
);

function subscribeTokenRefresh(cb) {
  subscribers.push(cb);
}

function onRefreshed(access_token) {
  subscribers = subscribers.filter(callback => callback(access_token))
}

subscribers = [];

Axios.defaults.baseURL = process.env.VUE_APP_BASE_URL
Vue.prototype.$http = Axios;

export default Axios;
