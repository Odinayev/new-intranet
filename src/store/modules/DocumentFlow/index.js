import { SET_ENTITY } from "@/store/mutation-types"

export default {
	namespaced: true,
	state: {
		treeUsers: {}
	},
	/*
	*
	* */
	getters: {

	},
	/*
	*
	* */
	mutations: {

	},
	/*
	*
	* */
	actions: {
		/*
		* Заполняем нужные поля колонки таблицы если пользователь перезагрузил браузер
		* */
		setActiveIndexTableCols({ commit }, payload) {
			commit(SET_ENTITY, { module: payload.module, entity: "indexTableCols", value: payload.columns }, { root: true })
		},
	},
}
