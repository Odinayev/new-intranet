import { SET_ENTITY } from "@/store/mutation-types"
import CrudService from "@/services/crud.service"

export default {
  namespaced: true,
  state: {
    departmentList: [],
    departmentUsersList: [],
    isPhoneBookLoading: false,
  },
  /*
  *
  * */
  getters: {
    getDepartmentList: (state) => state.departmentList,
    isPhoneBookLoading: (state) => state.isPhoneBookLoading,
    departmentUsersList: (state) => state.departmentUsersList,
  },
  mutations: {
    "PUSH_LIST"(state, { entity, value }) {
      state[entity].push(...value)
    },
    /*
    *
    * */
    "CLEAR_LIST"(state, { entity }) {
      state[entity] = []
    },
    /**/
    SET_PHONE_BOOK_LOADER(state, payload){
      state.isPhoneBookLoading = payload
    }
  },
  /*
  *
  * */
  actions: {
    /*
    *
    * */
    fetchDepartmentList({ commit }, payload) {
      commit("SET_PHONE_BOOK_LOADER", true)
      CrudService.getList(`/departments/`, {
        filial: payload,
        condition: "A",
        page_size: 50
      })
        .then((data) => {
          commit(SET_ENTITY, {
            module: "phoneBookModule",
            entity: "departmentList",
            value: data.results.filter(item => item.condition === 'A'),
          }, { root: true })
        })
    },
    /*
    *
    * */
    fetchDepartmentSelected({ commit, dispatch }, payload) {
      // Отчистим массив перед тем как заполнить
      dispatch("clearDepartmentUserList")

      dispatch("fetchDepartmentUsers", payload)

      if(payload.sub_departments.length > 0) {
        payload.sub_departments.forEach(item => {
          setTimeout(() => {
            dispatch('fetchDepartmentUsers', item)
          }, 75)

          if(item.sub_departments.length > 0) {
            item.sub_departments.forEach(subItem => {
              setTimeout(() => {
                dispatch('fetchDepartmentUsers', subItem)
              }, 150)
            })
          }
        })
      }
    },
    /*
    *
    * */
    fetchDepartmentUsers({ commit, dispatch }, payload) {
      commit("SET_PHONE_BOOK_LOADER", true)
      CrudService.getList('/phonebook/', {
        dept_code: payload.code,
        filial: payload.mfo,
        search: payload.search,
        ordering: "-order_by",
        page_size: 50
      })
        .then((data) => {
          commit("PUSH_LIST", {
            entity: "departmentUsersList",
            value: data.results
          });
        })
        .finally(() => {
          commit("SET_PHONE_BOOK_LOADER", false)
        })
    },
    /*
    *
    * */
    updateDepartmentUserCallNumber({ commit, dispatch, state }, payload) {
      return new Promise((resolve, reject) => {
        this.$axios.put(`/phonebook/${payload.id}/`, {
          cisco: payload.cisco
        })
          .then(({ data }) => {
            // Отчистим массив перед тем как заполнить
            dispatch('clearDepartmentUserList')

            dispatch('fetchDepartmentSelected', state.subDepartmentsSelected)

            resolve(data)
          })
      })
    },
    /*
    *
    * */
    clearDepartmentUserList({ commit }) {
      commit("CLEAR_LIST", { entity: "departmentUsersList" })
    }
  }
}
