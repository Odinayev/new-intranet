export default {
  namespaced: true,
  state: {
    directionsTableCols: [
      {
        text: "№",
        value: "index",
        width: "50",
        active: true,
        sortable: false,
      },
      {
        text: "Направления",
        i18n: "directions",
        value: "name",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Автор",
        i18n: "components.author",
        value: "created_by",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Дата создана",
        i18n: "created-date",
        value: "created_date",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Состояние",
        i18n: "components.state",
        value: "is_active",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Кому",
        i18n: "document-flow.views.send-document.to-whom",
        value: "to_where",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Действие",
        i18n: "action",
        value: "action",
        width: "50",
        active: true,
        sortable: false,
      }
    ],
    directionsTableFilters: {
      page: 1,
      page_size: 15,
    }
  },
  actions: {
    actionCreateDirection({ commit }, body) {
      return new Promise((resolve, reject) => {
        this.$axios.post(`/audit/directions/`, body)
          .then(res => {
            resolve(res)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    /** **/
    actionUpdateDirection({ commit }, { id, body }){
      return new Promise((resolve, reject) => {
        this.$axios.put(`/audit/directions/${id}/`, body)
          .then(res => {
            resolve(res)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    /** **/
    actionDeleteDirection({ commit }, id){
      return new Promise((resolve, reject) => {
        this.$axios.delete(`/audit/directions/${id}/`)
          .then(res => {
            resolve(res)
          })
          .catch(err => {
            reject(err)
          })
      })
    }
  }
}
