const Equipments = [
  {
    path: "settings",
    name: "MainSettings",
    meta: {
      isAuthRequired: true
    },
    component: () => import(/* webpackChunkName: "MainSettings" */ "@/modules/Equipments/views"),
    redirect: { name: "SettingsEquipments" },
    children: [
      {
        path: "equipments",
        name: "SettingsEquipments",
        meta: {
          isAuthRequired: true
        },
        component: () => import(/* webpackChunkName: "SettingsEquipments" */ "@/modules/Equipments/views/Home")
      },
      // Corespondent
      {
        path: "corespondent",
        name: "SettingsCorespondent",
        meta: {
          isAuthRequired: true
        },
        component: () => import(/* webpackChunkName: "SettingsCorespondent" */ "@/modules/Equipments/views/AddCorespondent")
      },
      {
        path: "corespondent/:id",
        name: "SettingsCorespondentUpdate",
        meta: {
          isAuthRequired: true
        },
        component: () => import(/* webpackChunkName: "SettingsCorespondentUpdate" */ "@/modules/Equipments/views/AddCorespondent/update")
      },
      {
        path: "corespondent/create",
        name: "SettingsCorespondentCreate",
        meta: {
          isAuthRequired: true
        },
        component: () => import(/* webpackChunkName: "SettingsCorespondent" */ "@/modules/Equipments/views/AddCorespondent/create")
      },
      // /Corespondent

      // Create template
      {
        path: "create-template",
        name: "CreateTemplate",
        meta: {
          isAuthRequired: true
        },
        component: () => import(/* webpackChunkName: "CreateTemplate" */ "@/modules/Equipments/views/CreateTemplate")
      },
      {
        path: "create-template/show/:id",
        name: "CreateTemplateUpdate",
        meta: {
          isAuthRequired: true
        },
        component: () => import(/* webpackChunkName: "CreateTemplateUpdate" */ "@/modules/Equipments/views/CreateTemplate/update")
      },
      {
        path: "create-template/create",
        name: "CreateTemplateCreate",
        meta: {
          isAuthRequired: true
        },
        component: () => import(/* webpackChunkName: "CreateTemplateCreate" */ "@/modules/Equipments/views/CreateTemplate/create")
      },
      // /Create template

      // Resolution Content
      {
        path: "resolution-content",
        name: "ResolutionContent",
        meta: {
          isAuthRequired: true
        },
        component: () => import(/* webpackChunkName: "ResolutionContent" */ "@/modules/Equipments/views/ResolutionContent")
      },
      {
        path: "resolution-content/:id",
        name: "ResolutionContentUpdate",
        meta: {
          isAuthRequired: true
        },
        component: () => import(/* webpackChunkName: "ResolutionContentUpdate" */ "@/modules/Equipments/views/ResolutionContent/update")
      },
      {
        path: "resolution-content/create",
        name: "ResolutionContentCreate",
        meta: {
          isAuthRequired: true
        },
        component: () => import(/* webpackChunkName: "SettingsCorespondent" */ "@/modules/Equipments/views/ResolutionContent/create")
      },
      // /Resolution Content

      // Группа сотрудников
      {
        path: "group-staff",
        name: "GroupStaff",
        meta: {
          isAuthRequired: true
        },
        component: () => import(/* webpackChunkName: "GroupStaff" */ "@/modules/Equipments/views/GroupStaff")
      },
      {
        path: "group-staff/create",
        name: "GroupStaffCreate",
        meta: {
          isAuthRequired: true
        },
        component: () => import(/* webpackChunkName: "GroupStaffCreate" */ "@/modules/Equipments/views/GroupStaff/create")
      },
      {
        path: "group-staff/update/:id",
        name: "GroupStaffUpdate",
        meta: {
          isAuthRequired: true
        },
        component: () => import(/* webpackChunkName: "GroupStaffUpdate" */ "@/modules/Equipments/views/GroupStaff/update")
      },
      // /Группа сотрудников

      // Add news
      {
        path: "add-news",
        name: "AddNews",
        meta: {
          isAuthRequired: true
        },
        component: () => import(/* webpackChunkName: "AddNews" */ "@/modules/Equipments/views/AddNews")
      },
      {
        path: "add-news/:id",
        name: "AddNewsUpdate",
        meta: {
          isAuthRequired: true
        },
        component: () => import(/* webpackChunkName: "AddNewsUpdate" */ "@/modules/Equipments/views/AddNews/update")
      },
      {
        path: "add-news/create",
        name: "AddNewsCreate",
        meta: {
          isAuthRequired: true
        },
        component: () => import(/* webpackChunkName: "AddNewsCreate" */ "@/modules/Equipments/views/AddNews/create")
      },
      // /Add news

      // History changes
      {
        path: "history",
        name: "HistoryChanges",
        meta: {
          isAuthRequired: true
        },
        component: () => import(/* webpackChunkName: "HistoryChanges" */ "@/modules/Equipments/views/HistoryChanges"),
        children: [
          {
            path: ":id",
            name: "HistoryChangesID",
            meta: {
              isAuthRequired: true
            },
            component: () => import(/* webpackChunkName: "HistoryChangesID" */ "@/modules/Equipments/views/HistoryChanges/components/HistoryItem")
          },
        ]
      },
      {
        path: "create/history",
        name: "HistoryChangesCreate",
        meta: {
          isAuthRequired: true
        },
        component: () => import(/* webpackChunkName: "HistoryChangesCreate" */ "@/modules/Equipments/views/HistoryChanges/Create")
      },
      {
        path: "update/history/:id",
        name: "HistoryChangesUpdate",
        meta: {
          isAuthRequired: true
        },
        component: () => import(/* webpackChunkName: "HistoryChangesUpdate" */ "@/modules/Equipments/views/HistoryChanges/Update")
      },
      // /History changes
      {
        path: "failed-letters",
        name: "FailedLetters",
        meta: {
          isAuthRequired: true
        },
        component: () => import(/* webpackChunkName: "AddNews" */ "@/modules/Equipments/views/FailedLetters")
      },
    ]
  }
]
export default Equipments
