const BankStructure = [
	{
		path: "bank-structure",
		name: "BankStructure",
		component: () => import("@/modules/BankStructure/Structure"),
	},
]

export default BankStructure
