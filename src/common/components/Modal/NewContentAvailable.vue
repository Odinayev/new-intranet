<script>
import { mapGetters } from "vuex";
export default {
	name: "NewContentAvailable",
	computed: {
		...mapGetters({
			isNewContentAvailable: "socket/isNewContentAvailable",
		})
	},
	methods: {
		reloadContent() {
			window.location.reload()
		}
	}
}
</script>

<template>
	<ui-dialog
		v-model="isNewContentAvailable.isNew"
		title="new-update"
		content-height="400px"
		success-text="update"
		cancel-text="postpone"
		persistent
		@emit:close="(_value) => isNewContentAvailable.isNew = _value"
		@emit:cancel-button="(_value) => isNewContentAvailable.isNew = _value"
		@emit:success-button="reloadContent"
	>
		<template #content>
			<v-alert
				color="var(--secondary-1)"
				dark
				class="mb-5 px-4 py-3"
			>
	      <span class="d-block primary-text font-500 small-2">
		      {{ $t("new-update-text") }}
	      </span>
				<span class="d-block primary-text font-500 small-2 mt-1">
		      {{ $t("new-update-text-2") }}
	      </span>
			</v-alert>

			<div class="d-flex align-center justify-center mt-10 mb-8">
				<div class="text-center">
					<v-icon size="72">{{ "$vuetify.icons.PartyIcon" }}</v-icon>
					<h2 class="mt-4">{{ $t("new-content-available") }}</h2>
				</div>
			</div>

			<h4 class="mb-2">{{ isNewContentAvailable.title }}</h4>
			<ol class="pl-4">
				<li v-for="(item, index) in isNewContentAvailable.content" :key="index" class="mb-1">
					{{ item.name }}
				</li>
			</ol>
		</template>
	</ui-dialog>
</template>

<style scoped>

</style>
