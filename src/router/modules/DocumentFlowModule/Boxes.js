
const Boxes = [
	{
		path: "boxes",
		name: "BoxesIndex",
		meta: {
			breadcrumb: true,
			title: "Ящики",
			isAuthRequired: true,
		},
		component: () => import(/* webpackChunkName: "BoxesIndex" */ "@/modules/DocumentFlow/views/BoxesNew"),
		redirect: { name: "ReviewIndex" },
		children: [
			// На рассмотрение
			{
				path: "review",
				name: "ReviewIndex",
				meta: {
					breadcrumb: true,
					title: "На рассмотрение",
					isAuthRequired: true,
				},
				component: () => import(/* webpackChunkName: "ReviewIndex" */ "@/modules/DocumentFlow/views/BoxesNew/Review"),
			},
			// /На рассмотрение

			// Входящие
			{
				path: "inbox",
				name: "InboxIndex",
				meta: {
					breadcrumb: true,
					title: "Входящие",
					isAuthRequired: true,
				},
				component: () => import(/* webpackChunkName: "InboxIndex" */ "@/modules/DocumentFlow/views/BoxesNew/Inbox"),
			},
			// /Входящие

			// На подпись
			{
				path: "sign",
				name: "SignIndex",
				meta: {
					breadcrumb: true,
					title: "На подпись",
					isAuthRequired: true,
				},
				component: () => import(/* webpackChunkName: "SignIndex" */ "@/modules/DocumentFlow/views/BoxesNew/Sign"),
			},
			// /На подпись

			// На согласование
			{
				path: "approval",
				name: "ApprovalIndex",
				meta: {
					breadcrumb: true,
					title: "На согласование",
					isAuthRequired: true,
				},
				component: () => import(/* webpackChunkName: "ApprovalIndex" */ "@/modules/DocumentFlow/views/BoxesNew/Approval"),
			},
			// /На согласование

			// Созданные поручение
			{
				path: "created-resolutions",
				name: "CreatedResolutionsIndex",
				meta: {
					breadcrumb: true,
					title: "Созданные поручение",
					isAuthRequired: true,
				},
				component: () => import(/* webpackChunkName: "CreatedResolutionsIndex" */ "@/modules/DocumentFlow/views/BoxesNew/CreatedResolutions"),
			},
			// /Созданные поручение

			// На контроль
			{
				path: "to-control",
				name: "ControlIndex",
				meta: {
					breadcrumb: true,
					title: "На контроль",
					isAuthRequired: true,
				},
				component: () => import(/* webpackChunkName: "ControlIndex" */ "@/modules/DocumentFlow/views/BoxesNew/Control"),
			},
			// /На контроль

			// Утвердить
			{
				path: "confirmation",
				name: "ConfirmationIndex",
				meta: {
					breadcrumb: true,
					title: "Утвердить",
					isAuthRequired: true,
				},
				component: () => import(/* webpackChunkName: "ConfirmationIndex" */ "@/modules/DocumentFlow/views/BoxesNew/Confirmation"),
			},
			// /Утвердить

			// General Control
			{
				path: "general-control",
				name: "GeneralControlIndex",
				meta: {
					breadcrumb: true,
					title: "Генеральный контроль",
					isAuthRequired: true,
				},
				component: () => import(/* webpackChunkName: "ReviewIndex" */ "@/modules/DocumentFlow/views/BoxesNew/GeneralControl"),
			},
			// /General Control
		]
	},
	// На рассмотрение (Detail)
	{
		path: "boxes/review/show/:id/:type",
		name: "ReviewDetail",
		component: () => import("@/modules/DocumentFlow/views/BoxesNew/Review/show.vue")
	},
	// /На рассмотрение (Detail)

	// Входящий (Detail)
	{
		path: "boxes/inbox/show/:id",
		name: "InboxDetail",
		component: () => import("@/modules/DocumentFlow/views/BoxesNew/Inbox/show.vue")
	},
	// /Входящий (Detail)

	// Входящий (Detail)
	{
		path: "boxes/control/show/:id",
		name: "ControlDetail",
		component: () => import("@/modules/DocumentFlow/views/BoxesNew/Control/show.vue")
	},
	// /Входящий (Detail)

	// Созданные поручение (Detail)
	{
		path: "boxes/created-resolutions/show/:id",
		name: "CreatedResolutionsDetail",
		component: () => import("@/modules/DocumentFlow/views/BoxesNew/CreatedResolutions/show.vue")
	},
	// /Созданные поручение (Detail)

	// Утвердить (Detail)
	{
		path: "boxes/confirmation/show/:id",
		name: "ConfirmationDetail",
		component: () => import("@/modules/DocumentFlow/views/BoxesNew/Confirmation/show.vue")
	},
	// /Утвердить (Detail)

	// General Control (Detail)
	{
		path: "boxes/general-control/show/:id/:type",
		name: "GeneralControlDetail",
		component: () => import("@/modules/DocumentFlow/views/BoxesNew/GeneralControl/show.vue")
	},
	// /General Control (Detail)
]

export default Boxes
