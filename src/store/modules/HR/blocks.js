export default {
  namespaced: true,
  state: {
    blockTableCols: [
      {
        text: "№",
        value: "index",
        width: "50",
        active: true,
        sortable: false,
      },
      {
        text: "Названия",
        i18n: "components.base.name",
        value: "name",
        width: "730",
        active: true,
        sortable: false,
      },
      {
        text: "Дата создана",
        i18n: "created-date",
        value: "created_date",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Действие",
        i18n: "action",
        value: "action",
        width: "50",
        active: true,
        sortable: false,
      }
    ],
    blockTableFilters: {
      page: 1,
      page_size: 15
    }
  }
}