import Vue from "vue";

import UiDataTable from "@/common/components/UI/DataTable.vue";
import UiPagination from "@/common/components/UI/Pagination.vue";
import UiStatus from "@/common/components/UI/Status.vue";
import UiPriority from "@/common/components/UI/Priority.vue";
import UiAvatarGroup from "@/common/components/UI/AvatarGroup.vue";
import UiDialog from "@/common/components/UI/Dialog.vue";
import UiLabelText from "@/common/components/UI/LabelText.vue";
import UiTabs from "@/common/components/UI/Tabs.vue";
import UiDrawer from "@/common/components/UI/Drawer.vue";

Vue.component("UiDataTable", UiDataTable)
Vue.component("UiPagination", UiPagination)
Vue.component("UiStatus", UiStatus)
Vue.component("UiPriority", UiPriority)
Vue.component("UiAvatarGroup", UiAvatarGroup)
Vue.component("UiDialog", UiDialog)
Vue.component("UiLabelText", UiLabelText)
Vue.component("UiTabs", UiTabs)
Vue.component("UiDrawer", UiDrawer)


import AppCardWrapper from "@/common/components/base/AppCardWrapper.vue"

import AppAutocomplete from "@/common/components/base/AppAutocomplete"
import AppAvatar from "@/common/components/base/AppAvatar"
import AppAvatarGroup from "@/common/components/base/AppAvatarGroup"
import AppButton from "@/common/components/base/AppButton"
import AppColumnChart from "@/common/components/base/AppColumnChart"
import AppDatePicker from "@/common/components/base/AppDatePicker"
import AppLoader from "@/common/components/base/AppLoader"
import AppPagination from "@/common/components/base/AppPagination"
import AppSkeleton from "@/common/components/base/AppSkeleton"
import AppTable from "@/common/components/base/AppTable"
import AppTooltip from "@/common/components/base/AppTooltip"
import AppUserCard from "@/common/components/base/AppUserCard"
import AppWrapper from "@/common/components/base/AppWrapper"
import Autocomplete from "@/common/components/base/Autocomplete"
import AppFileUpload from "../common/components/base/AppFileUpload"
import AppChipGroup from "@/common/components/base/AppChipGroup"
import AppEditor from "../common/components/base/AppEditor"
import AppMenu from "../common/components/base/AppMenu"
import AppCombobox from "../common/components/base/AppCombobox"
import BaseDataTable from "@/common/componentsV3/UI/BaseDataTable.vue"
import BaseIconify from "@/common/componentsV3/UI/BaseIconify.vue"
import BaseAutoComplete from "@/common/componentsV3/UI/BaseAutoComplete.vue"
import BaseModal from "@/common/componentsV3/UI/BaseModal.vue"
import BaseTextField from "@/common/componentsV3/UI/BaseTextField.vue"
import BaseSwitch from "@/common/componentsV3/UI/BaseSwitch.vue"
import BaseCheckbox from "@/common/componentsV3/UI/BaseCheckbox.vue"
import BaseDatePicker from "@/common/componentsV3/UI/BaseDatePicker.vue"
import BaseStatus from "@/common/componentsV3/UI/BaseStatus.vue"
import BaseFileUpload from "@/common/componentsV3/UI/BaseFileUpload.vue"
import BaseTextArea from "@/common/componentsV3/UI/BaseTextArea.vue"
import BaseSelect from "@/common/componentsV3/UI/BaseSelect.vue"
import BaseTab from "@/common/componentsV3/UI/BaseTab.vue"
import BaseButtonTab from "@/common/componentsV3/UI/BaseButtonTab.vue"
import BaseRadioGroup from "@/common/componentsV3/UI/BaseRadioGroup.vue"

Vue.component("AppCardWrapper", AppCardWrapper)

Vue.component("AppAutocomplete", AppAutocomplete)
Vue.component("AppAvatar", AppAvatar)
Vue.component("AppAvatarGroup", AppAvatarGroup)
Vue.component("AppButton", AppButton)
Vue.component("AppColumnChart", AppColumnChart)
Vue.component("AppDatePicker", AppDatePicker)
Vue.component("AppLoader", AppLoader)
Vue.component("AppPagination", AppPagination)
Vue.component("AppSkeleton", AppSkeleton)
Vue.component("AppTable", AppTable)
Vue.component("AppTooltip", AppTooltip)
Vue.component("AppUserCard", AppUserCard)
Vue.component("AppWrapper", AppWrapper)
Vue.component("Autocomplete", Autocomplete)
Vue.component("AppFileUpload", AppFileUpload)
Vue.component("AppChipGroup", AppChipGroup)
Vue.component("AppEditor", AppEditor)
Vue.component("AppMenu", AppMenu)
Vue.component("AppCombobox", AppCombobox)
Vue.component("BaseDataTable", BaseDataTable)
Vue.component("BaseIconify", BaseIconify)
Vue.component("BaseAutoComplete", BaseAutoComplete)
Vue.component("BaseModal", BaseModal)
Vue.component("BaseTextField", BaseTextField)
Vue.component("BaseSwitch", BaseSwitch)
Vue.component("BaseCheckbox", BaseCheckbox)
Vue.component("BaseDatePicker", BaseDatePicker)
Vue.component("BaseStatus", BaseStatus)
Vue.component("BaseFileUpload", BaseFileUpload)
Vue.component("BaseTextArea", BaseTextArea)
Vue.component("BaseSelect", BaseSelect)
Vue.component("BaseTab", BaseTab)
Vue.component("BaseButtonTab", BaseButtonTab)
Vue.component("BaseRadioGroup", BaseRadioGroup)
