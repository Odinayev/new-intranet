import Boxes from "./Boxes"
import Registration from "./Registration";
import SendDocument from "./SendDocument";
import XDFU from "./Xdfu";

const DocumentFlowModule = [
	{
		path: "document-flow",
		name: "DocumentFlowModule",
		meta: {
			isAuthRequired: true,
		},
		component: () => import(/* webpackChunkName: "DocumentFlow" */ "@/modules/DocumentFlow/views"),
		redirect: { name: "BoxesIndex" },
		children: [
			...Boxes,
			...Registration,
			...SendDocument,
			...XDFU
		]
	}
]

export default DocumentFlowModule
