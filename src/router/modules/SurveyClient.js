const SurveyClient = [
	{
		path: "survey-client/start",
		name: "SurveyClient",
		component: () => import("@/modules/SurveyClient"),
	},
	{
		path: "survey-client/completed",
		name: "SurveyClientCompleted",
		component: () => import("@/modules/SurveyClient/SurveyClientCompleted"),
	},
	{
		path: "survey-client/completed/:id",
		name: "SurveyClientView",
		component: () => import("@/modules/SurveyClient/SurveyClientView"),
	},
	{
		path: "survey-client/list",
		name: "SurveyClientList",
		component: () => import("@/modules/SurveyClient/SurveyClientList"),
	},
	{
		path: "survey-client/list/:id",
		name: "SurveyClientEdit",
		component: () => import("@/modules/SurveyClient/SurveyClientEdit"),
	},
]
export default SurveyClient
