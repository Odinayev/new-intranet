const XDFU = [
  {
    path: "xdfu",
    name: "XdfuIndex",
    meta: {
      breadcrumb: true,
      title: "XDFU",
      isAuthRequired: true,
    },
    component: () => import(/* webpackChunkName: "BoxesIndex" */ "@/modules/DocumentFlow/views/Xdfu"),
    redirect: { name: "XdfuReviewIndex" },
    children: [
      // На рассмотрение
      {
        path: "review",
        name: "XdfuReviewIndex",
        meta: {
          breadcrumb: true,
          title: "На рассмотрение",
          isAuthRequired: true,
        },
        component: () => import(/* webpackChunkName: "ReviewIndex" */ "@/modules/DocumentFlow/views/Xdfu/Review"),
      },
      // /На рассмотрение

      // Входящие
      {
        path: "inbox",
        name: "XdfuInboxIndex",
        meta: {
          breadcrumb: true,
          title: "Входящие",
          isAuthRequired: true,
        },
        component: () => import(/* webpackChunkName: "InboxIndex" */ "@/modules/DocumentFlow/views/Xdfu/Inbox"),
      },
      // /Входящие

      // General Control
      {
        path: "general-control",
        name: "XdfuGeneralControlIndex",
        meta: {
          breadcrumb: true,
          title: "Генеральный контроль",
          isAuthRequired: true,
        },
        component: () => import(/* webpackChunkName: "InboxIndex" */ "@/modules/DocumentFlow/views/Xdfu/GeneralControl"),
      },
      // /General Control
    ]
  },

  // На рассмотрение (Detail)
  {
    path: "xdfu/review/show/:id/:type",
    name: "XdfuReviewDetail",
    component: () => import("@/modules/DocumentFlow/views/BoxesNew/Review/show.vue")
  },
  // /На рассмотрение (Detail)

  // Входящий (Detail)
  {
    path: "xdfu/inbox/show/:id",
    name: "XdfuInboxDetail",
    component: () => import("@/modules/DocumentFlow/views/BoxesNew/Inbox/show.vue")
  },
  // /Входящий (Detail)
]

export default XDFU
