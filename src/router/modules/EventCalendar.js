
const eventCalendar = [
	{
		path: "event-calendar",
		name: "MainEventCalendar",
		component: () => import(/* webpackChunkName: "EventCalendar" */ "@/modules/EventCalendar")
	},
	{
		path: "event-calendar/:date",
		name: "MainEventCalendarDate",
		component: () => import(/* webpackChunkName: "EventCalendar" */ "@/modules/EventCalendar")
	}
]

export default eventCalendar
