
export default {
	assignmentList: [
		{
			title: "Поручение",
			value: "assignment"
		},
		{
			title: "Контрольный пункт",
			value: "control_point"
		},
		{
			title: "Для сведения",
			value: "for_information"
		},
	]
}
