import { SET_ENTITY } from '@/store/mutation-types'
import CrudService from '@/services/crud.service'
import { SIGN_TYPE } from '@/common/enums/notificationTypes'

export default {
	namespaced: true,
	state: {
		indexTableCols: [
			{
				text: "#",
				value: "index",
				width: "50",
				active: true,
				sortable: false,
				align: "center",
			},
			{
				text: "components.execution-duration",
				value: "incoming_doc.deadline",
				width: "150",
				active: true,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: "ui-countdown",
				detail: true,
				order: 3
			},
			{
				text: "user-profile.views.name",
				value: "incoming_doc.title",
				active: true,
				sortable: false,
				width: "125"
			},
			{
				text: "document-flow.views.registration.state",
				value: "read_time",
				width: "135",
				active: false,
				sortable: false
			},
			{
				text: "priority",
				value: "incoming_doc.priority",
				width: "150",
				active: true,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: "ui-priority",
				detail: true,
				order: 6
			},
			{
				text: "components.registration-number",
				value: "incoming_doc.register_number",
				active: true,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: null,
				detail: true,
				order: 5
			},
			{
				text: "components.document-type",
				value: "incoming_doc.doc_type.name",
				active: true,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: null,
				detail: true,
				order: 1
			},
			{
				text: "document-flow.views.boxes.journal",
				value: "incoming_doc.journal.name",
				active: true,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: null,
				detail: true,
				order: 2
			},
			{
				text: "components.outgoing-number",
				value: "incoming_doc.outgoing_number",
				active: false,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: null,
				detail: true,
				order: 7
			},
			{
				text: "document-flow.components.registration-date",
				value: "incoming_doc.register_date",
				active: true,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: "ui-date-text",
				detail: true,
				order: 5,
			},
			{
				text: "document-flow.components.outgoing-date",
				value: "incoming_doc.outgoing_date",
				active: false,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: "ui-date-text",
				detail: true,
				order: 8
			},
			{
				text: "document-flow.components.correspondent",
				value: "incoming_doc.correspondent.organization_name",
				width: "350",
				active: true,
				sortable: false,
				cols: {
					"md": "12"
				},
				component: null,
				detail: true,
				order: 9
			},
			{
				text: "document-flow.components.for-consideration",
				value: "reviewers",
				active: true,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: "ui-avatar-group",
				detail: true,
				order: 4
			},
			{
				text: "document-flow.views.boxes.content",
				value: "incoming_doc.description",
				active: false,
				sortable: false,
				width: "300",
				cols: {
					"md": "12"
				},
				component: null,
				detail: true,
				order: 10
			},
			{
				text: "components.status",
				value: "status.name",
				width: "140",
				sortable: false,
				active: true,
				cols: {
					"md": "4"
				},
				component: "ui-status",
				detail: true,
				order: 3
			},
		],
		indexTableFilters: {
			search: null,
			priority: null,
			is_read: null,
			has_deadline: null,
			type: null,
			is_agreed: null,
			has_resolution: null,
			status: null,
			statusList: null,
			condition: null,
			journal: null,
			doc_type: null,
			doc_type_name: null,
			register_number: null,
			register_start_date: null,
			register_end_date: null,
			outgoing_number: null,
			outgoing_start_date: null,
			outgoing_end_date: null,
			correspondent: null,
			correspondent_name: null,
			is_verified: null,
			__correspondent: null,
			__doc_type: null,
			__register_date: null,
			__outgoing_date: null,
			review_user: null,
			review_user_name: null,
			__review_user: null,
			page: 1,
			page_size: 15
		},
		reviewDetailModelLoader: true,
		reviewDetailModel: null,
	},
	/**
	*
	**/
	actions: {
		async fetchReviewById({ commit, rootState }, payload) {
			if(payload.loader) {
				commit(SET_ENTITY, {
					module: "documentFlowBoxesReview",
					entity: "reviewDetailModelLoader",
					value: true
				}, {root: true})
			}

			await CrudService.getList(`v2/docflow/${rootState.auth.currentUser.filial}/users-review/${payload.id}/`, { is_secret: payload.is_secret })
			.then((data) => {
				commit(SET_ENTITY, {
					module: "documentFlowBoxesReview",
					entity: "reviewDetailModel",
					value: data
				}, {root: true})

				setTimeout(() => {
					commit(SET_ENTITY, {
						module: "documentFlowBoxesReview",
						entity: "reviewDetailModelLoader",
						value: false
					}, {root: true})
				}, 1000)
			})
		},
		/*
		* Изменить статус документа (На рассмотрение)
		* */
		async fetchUpdateReviewStatus({ commit, rootState, state, dispatch }, payload) {
			await this.$axios.put(`/v2/docflow/${rootState.auth.currentUser.filial}/users-review/${payload.documentId}/?is_secret=${payload.is_secret}`, {
				status: payload.status,
				comment: payload.comment
			})
			.then(({ data }) => {
				state.reviewDetailModel.status = data.status

				dispatch(
					"treeView/fetchTreeUsersList",
					{ treeId: state.reviewDetailModel.incoming_doc.id },
					{ root: true }
				)
			})
		},
		/*
		*  Ознакомиться с документом
		* */
		async fetchAcquaintWithDocument({ commit, rootState, state, dispatch }, payload) {
			await this.$axios.put(`/v2/docflow/${rootState.auth.currentUser.filial}/review/${payload.documentId}/acquaint/`)
			.then(({ data }) => {
				state.reviewDetailModel.read_time = data.read_time

				dispatch(
					"treeView/fetchTreeUsersList",
					{ treeId: state.reviewDetailModel.incoming_doc.id },
					{ root: true }
				)
			})
		},
		/*
		* Создать резолюцию (Создать фишку)
		* */
		async fetchCreateResolution({ commit, rootState, state, dispatch }, payload) {
			await this.$axios.post(`v2/docflow/${rootState.auth.currentUser.filial}/assignment/`, { ...payload.model })
			.then(({ data }) => {
				state.reviewDetailModel.has_resolution = true
				state.reviewDetailModel.assignees = {
					assignment: data.assignment,
					controllers: data.controllers,
					check_id: data.check_id,
					content: data.content,
					deadline: data.deadline,
					id: data.id,
					is_verified: data.is_verified,
					review: data.review,
					type: data.type
				}

				dispatch(
					"treeView/fetchTreeUsersList",
					{ treeId: payload.treeId },
					{ root: true }
				)
			})
		},
		/*
		* Изменить резолюцию (Изменить фишку)
		* */
		async fetchUpdateResolution({ commit, rootState, state, dispatch }, payload) {
			await this.$axios.put(
				`v2/docflow/${rootState.auth.currentUser.filial}/assignment/${payload.id}/`,
				{ ...payload.model }
			)
			.then(({ data }) => {
				state.reviewDetailModel.assignees = {
					assignment: data.assignment,
					controllers: data.controllers,
					check_id: data.check_id,
					content: data.content,
					deadline: data.deadline,
					id: data.id,
					is_verified: data.is_verified,
					review: data.review,
					type: data.type,
				}

				dispatch(
					"treeView/fetchTreeUsersList",
					{ treeId: payload.treeId },
					{ root: true }
				)
			})
		},
		/*
		* Удалить резолюцию (Удалить фишку)
		* */
		async fetchDeleteResolution({ commit, rootState, state, dispatch }, payload) {
			await this.$axios.delete(
				`v2/docflow/${rootState.auth.currentUser.filial}/assignment/${payload.id}/`, {
					params: {
						comment: payload.comment
					}
				}
			)
			.then(({ data }) => {
				state.reviewDetailModel.has_resolution = false
				state.reviewDetailModel.assignees = {}

				dispatch(
					"treeView/fetchTreeUsersList",
					{ treeId: state.reviewDetailModel.incoming_doc.id },
					{ root: true }
				)
			})
		},
		/*
		* Отменить подпись
		* */
		async fetchCancelVerifySign({ commit, rootState, state, dispatch }, payload) {
			await this.$axios.put(
				`v2/docflow/${rootState.auth.currentUser.filial}/assignment/${payload.id}/verify-or-cancel-assignment/`,
			{ ...payload.model }
			)

			dispatch("fetchReviewById", { id: state.reviewDetailModel.id, loader: false, is_secret: payload.is_secret })
		},
		/*
		* Подписать документ или удалить подпись
		* */
		async fetchVerifySign({ commit, rootState, state, dispatch }, payload) {
			// await this.$axios.put(
			// 	`v2/docflow/${rootState.auth.currentUser.filial}/assignment/${payload.id}/verify-or-cancel-assignment/`,
			// { ...payload.model }
			// )
			await this.$axios.put(`/docflow/${rootState.auth.currentUser.filial}/assigment/verify_assignment/`, payload.model)
			.then(({ data }) => {
				switch(payload.model.type) {
					case SIGN_TYPE.SIGN:
						state.reviewDetailModel.assignees.is_verified = true
						break;
					case SIGN_TYPE.CANCEL:
						state.reviewDetailModel.assignees.is_verified = false
						break;
					default:
						console.log("default case")
				}

				dispatch("fetchReviewById", { id: state.reviewDetailModel.id, loader: false, is_secret: payload.is_secret })
			})
		},
		/*
		* Принять к сведению
		* */
		async fetchReviewExecutionMark({ commit, rootState, state, dispatch }, payload) {
			await this.$axios.put(`/v2/docflow/${rootState.auth.currentUser.filial}/users-review/${payload.id}/?is_secret=${payload.is_secret}`, {
				comment: payload.text,
				status: 4
			})
			.then(({ data }) => {
				dispatch(
					"fetchReviewById",
					{ id: state.reviewDetailModel.id, loader: false, is_secret: payload.is_secret }
				)

				dispatch(
					"treeView/fetchTreeUsersList",
					{ treeId: state.reviewDetailModel.incoming_doc.id },
					{ root: true }
				)
			})
		},
		/*
		* TODO: Удалить подпись (Это функция временно)
		* */
		async fetchCancelAssignDocument({ commit, rootState, state, dispatch }, payload) {
			await this.$axios.put(`/docflow/${rootState.auth.currentUser.filial}/assigment/cancel_assignment/`, payload.model)
			.then(() => {
				switch(payload.model.type) {
					case SIGN_TYPE.SIGN:
						state.reviewDetailModel.assignees.is_verified = true
						break;
					case SIGN_TYPE.CANCEL:
						state.reviewDetailModel.assignees.is_verified = false
						break;
					default:
						console.log("default case")
				}

				dispatch("fetchReviewById", { id: state.reviewDetailModel.id, loader: false })
			})
		},
		/*
		* Заполняем нужные поля колонки таблицы если пользователь перезагрузил браузер
		* */
		setActiveIndexTableCols({ commit }, payload) {
			commit(SET_ENTITY, { module: "documentFlowBoxesReview", entity: "indexTableCols", value: payload }, { root: true })
		},
		/*
		*
		* */
		resetIndexTableCols({ state }) {
			state.indexTableCols = [
				{
					text: "#",
					value: "index",
					width: "50",
					active: true,
					sortable: false,
					align: "center",
				},
				{
					text: "components.execution-duration",
					value: "incoming_doc.deadline",
					width: "150",
					active: true,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: "ui-countdown",
					detail: true,
					order: 3
				},
				{
					text: "user-profile.views.name",
					value: "incoming_doc.title",
					active: true,
					sortable: false,
					width: "125"
				},
				{
					text: "document-flow.views.registration.state",
					value: "read_time",
					width: "135",
					active: false,
					sortable: false
				},
				{
					text: "priority",
					value: "incoming_doc.priority",
					width: "150",
					active: true,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: "ui-priority",
					detail: true,
					order: 6
				},
				{
					text: "components.registration-number",
					value: "incoming_doc.register_number",
					active: true,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: null,
					detail: true,
					order: 5
				},
				{
					text: "components.document-type",
					value: "incoming_doc.doc_type.name",
					active: true,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: null,
					detail: true,
					order: 1
				},
				{
					text: "document-flow.views.boxes.journal",
					value: "incoming_doc.journal.name",
					active: true,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: null,
					detail: true,
					order: 2
				},
				{
					text: "components.outgoing-number",
					value: "incoming_doc.outgoing_number",
					active: false,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: null,
					detail: true,
					order: 7
				},
				{
					text: "document-flow.components.registration-date",
					value: "incoming_doc.register_date",
					active: true,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: "ui-date-text",
					detail: true,
					order: 5,
				},
				{
					text: "document-flow.components.outgoing-date",
					value: "incoming_doc.outgoing_date",
					active: false,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: "ui-date-text",
					detail: true,
					order: 8
				},
				{
					text: "document-flow.components.correspondent",
					value: "incoming_doc.correspondent.organization_name",
					width: "350",
					active: true,
					sortable: false,
					cols: {
						"md": "12"
					},
					component: null,
					detail: true,
					order: 9
				},
				{
					text: "document-flow.components.for-consideration",
					value: "reviewers",
					active: true,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: "ui-avatar-group",
					detail: true,
					order: 4
				},
				{
					text: "document-flow.views.boxes.content",
					value: "incoming_doc.description",
					active: false,
					sortable: false,
					width: "300",
					cols: {
						"md": "12"
					},
					component: null,
					detail: true,
					order: 10
				},
				{
					text: "components.status",
					value: "status.name",
					width: "140",
					sortable: false,
					active: true,
					cols: {
						"md": "4"
					},
					component: "ui-status",
					detail: true,
					order: 3
				},
			]
		},
		/*
		*
		* */
		async resetIndexTableFilters({ state }) {
			state.indexTableFilters = {
				search: null,
				priority: null,
				is_read: null,
				has_deadline: null,
				type: null,
				is_agreed: null,
				has_resolution: null,
				status: null,
				statusList: null,
				condition: null,
				journal: null,
				doc_type: null,
				doc_type_name: null,
				register_number: null,
				register_start_date: null,
				register_end_date: null,
				outgoing_number: null,
				outgoing_start_date: null,
				outgoing_end_date: null,
				correspondent: null,
				correspondent_name: null,
				is_verified: null,
				__correspondent: null,
				__doc_type: null,
				__register_date: null,
				__outgoing_date: null,
				review_user: null,
				review_user_name: null,
				__review_user: null,
				page: 1,
				page_size: 15
			}
		},
		/*
		* Заполняем нужные поля фильтра если пользователь перезагрузил браузер
		* */
		setActiveIndexTableFilters({ commit, state }, payload) {
			Object.entries(payload).forEach(item => {
				const [key, value] = item

				if(value && (typeof value === "boolean" || Array.isArray(value))) {
					state.indexTableFilters[key] = value
				}

				if(!(value && (typeof value === "boolean"))) {
					state.indexTableFilters[key] = value
				}

				if(value && (typeof value === "number" || typeof value === "string")) {
					state.indexTableFilters[key] = Number(value) ? Number(value) : value
				}

				if(value === "true" || value === "false") {
					state.indexTableFilters[key] = value === "true"
				}
			})

			Object.entries(state.indexTableFilters).forEach(item => {
				const [key, value] = item

				if(value && value.length && key === "statusList") {
					state.indexTableFilters.statusList = state.indexTableFilters.statusList.map(item => parseInt(item))
				}

				if(value && key === "doc_type_name") {
					state.indexTableFilters.__doc_type = {
						id: state.indexTableFilters.doc_type,
						name: state.indexTableFilters.doc_type_name,
						type: "doc_type"
					}
				}

				if(value && key === "register_start_date") {
					state.indexTableFilters.__register_date = [state.indexTableFilters.register_start_date, state.indexTableFilters.register_end_date]
				}

				if(value && key === "outgoing_start_date") {
					state.indexTableFilters.__outgoing_date = [state.indexTableFilters.outgoing_start_date, state.indexTableFilters.outgoing_end_date]
				}

				if(value && key === "correspondent_name") {
					state.indexTableFilters.__correspondent = {
						id: state.indexTableFilters.correspondent,
						organization_name: state.indexTableFilters.correspondent_name
					}
				}

				if(value && key === "review_user_name") {
					state.indexTableFilters.__review_user = {
						id: state.indexTableFilters.review_user,
						full_name: state.indexTableFilters.review_user_name
					}
				}
			})
		},
	},
}
