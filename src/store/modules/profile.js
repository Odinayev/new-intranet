import CrudService from "@/services/crud.service"
import { SET_ENTITY } from "@/store/mutation-types"

export default {
  state: {
    profileBarMenu: [
      {
        name: "Профиль",
        routerLink: "UserProfile"
      },
      {
        name: "Техника и оборудования",
        routerLink: "UserTechniques"
      },
      {
        name: "Пароль и безопасность",
        routerLink: "UserPasswordAndSecurity"
      },
      {
        name: "Личные документы",
        routerLink: "UserPersonalDocuments"
      }
    ],
    techniquesList: [],
    userPasscode: null,
    salaryList: [],
    salaryStatisticList: [],
    userSendPhoneNumber: null
  },
  getters: {
    getProfileBarMenu:(state) => state.profileBarMenu,
    getTechniquesList:(state) => state.techniquesList,
    getUserPasscode:(state) => state.userPasscode,
    getSalaryList:(state) => state.salaryList,
    getSalaryStatisticList:(state) => state.salaryStatisticList,
    getUserSendPhoneNumber:(state) => state.userSendPhoneNumber,
  },
  mutations: {
    [SET_ENTITY](state, { entity, value }) {
      state[entity] = value
    },
    /*
    *
    * */
    "SET_SALARY_STATISTIC"(state, payload) {
      if(payload.length) {
        let monthNotWorked = payload.length && payload[0].month_value > 1 ? payload[0].month_value - 1 : 0

        state.salaryStatisticList = [...new Array(monthNotWorked).fill(" "), ...payload.map(item => item.salary)]
      } else {
        state.salaryStatisticList = [...new Array(12).fill(" ")]
      }
    }
  },
  actions: {
    fetchTechniquesList(ctx, payload) {
      ctx.commit("LOADING", true)
      this.$axios.get(`/${ctx.rootState.auth.currentUser.filial}/user/equipments/`, {
        params: {
          page_size: 2000
        }
      })
        .then(({data}) => {
          ctx.commit(
            SET_ENTITY, {
              entity: "techniquesList",
              value: data.results
            }
          );
        })
        .finally(() => {
          ctx.commit("LOADING", false)
        })
    },
    /*
    *
    * */
    fetchGetUserSalaryList({ commit }, payload) {
      return new Promise((resolve, reject) => {
        CrudService.getList(`/user/my/salary/`, {
          passcode: payload.passcode,
          date: payload.date
        })
          .then((data) => {
            console.log(data);

            commit(SET_ENTITY, { entity: "userPasscode", value: payload.passcode })
            commit(SET_ENTITY, { entity: "salaryList", value: data })

            resolve(data)
          })
          .catch(() => {
            reject()
          })
      })
    },
    /*
    *
    * */
    fetchGetUserSalaryStatisticList({ commit }, payload) {
      return new Promise((resolve, reject) => {
        CrudService.getList(`/user/my/salary_statistics/`, {
          passcode: payload.passcode,
          year: payload.year
        })
          .then((data) => {
            commit("SET_SALARY_STATISTIC", data.results)
            resolve(data)
          })
          .catch(() => {
            reject()
          })
      })
    },
    /*
    *
    * */
    fetchSendCode({ commit }, payload) {
      return new Promise((resolve, reject) => {
        CrudService.post(`/user/send_code/`, payload)
          .then(() => {
            commit(SET_ENTITY, { entity: "userSendPhoneNumber", value: payload.phone_number })

            resolve()
          })
          .catch(() => {
            reject()
          })
      })
    },
    updateProfileBarMenu({commit}, payload) {
      commit(SET_ENTITY, { entity: "profileBarMenu", value: payload })
    }
  }
}
