import Vue from "vue";
import VueQuillEditor from 'vue-quill-editor';

import 'quill/dist/quill.bubble.css'; // for bubble theme
import 'quill/dist/quill.core.css'; // import styles
import 'quill/dist/quill.snow.css'; // for snow theme
const placeholder = localStorage.getItem('lang') === 'uz'?'Matn kiriting':'Введите сообщение...'
Vue.use(VueQuillEditor, {
  modules: {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'],
      [{ 'header': 1 }, { 'header': 2 }],
      [{ 'size': ['small', false, 'large', 'huge'] }],
      [{ 'color': [] }, { 'background': [] }],
      [{ 'align': [] }],
    ],
  },
  placeholder
})
