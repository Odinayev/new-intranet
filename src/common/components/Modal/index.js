export { default as ChangeStatusModal } from "./ChangeStatus.vue"
export { default as DeleteModal } from "./Delete.vue"
export { default as ShowFileModal } from "./ShowFile.vue"
export { default as NewContentAvailableModal } from "./NewContentAvailable.vue"
export { default as WarningModal } from "./WarningModal.vue"
