const ControlTasks = [
	{
		path: "control-tasks",
		name: "ControlTasks",
		redirect: { name: "Tasks" },
	},
	{
		path: "control-tasks/tasks",
		name: "Tasks",
		meta: {
			isAuthRequired: true,
		},
		component: () =>
			import(
				/* webpackChunkName: "DocumentFlow" */ "@/modules/ControlTasks/Tasks/index"
			),
	},
	{
		path: "control-tasks/tasks/:id",
		name: "EditTask",
		meta: {
			isAuthRequired: true,
		},
		component: () =>
			import(
				/* webpackChunkName: "DocumentFlow" */ "@/modules/ControlTasks/EditTask/index"
			),
	},
	{
		path: "control-tasks/create",
		name: "CreateTask",
		meta: {
			isAuthRequired: true,
		},
		component: () =>
			import(
				/* webpackChunkName: "DocumentFlow" */ "@/modules/ControlTasks/CreateTask/index"
			),
	},
	{
		path: "control-tasks/reports",
		name: "Reports",
		meta: {
			isAuthRequired: true,
		},
		component: () =>
			import(
				/* webpackChunkName: "DocumentFlow" */ "@/modules/ControlTasks/Reports/index"
			),
	},
];
export default ControlTasks;
