export default {
	INFO: 'info',
	SUCCESS: 'success',
	ERROR: 'error',
	WARNING: 'warning'
};

export const colorsList = {
	PRIMARY: "var(--primary-1)",
	SUCCESS: "var(--primary-2)",
	WARNING: "var(--primary-3)",
	ERROR: "var(--primary-4)",
}

export const STATUS_CODES = {
	TODO: 1,
	IN_PROGRESS: 2,
	ON_HOLD: 3,
	DONE: 4,
	FOR_SIGNATURE: 5,
	CANCEL: 6,
	ON_REVIEW: 7,
	OVERDUE_PERFORMANCE: 8,
	NOT_PERFORMED: 9
}

export const JOURNAL_TYPES = {
	INCOMING: 1,
	INNER: 3,
	OUTGOING: 4,
	APPEALS: 5,
	ORDER_AND_PROTOCOL: 6,
	APPLICATION: 7,
	CALL_CENTER_APPEALS: 2
}

export const FORM_TYPES = {
	CREATE: "CREATE",
	UPDATE: "UPDATE",
	EDIT: "EDIT",
	SHOW: "SHOW"
}
export const FILE_EXTENSIONS = {
	FILE: "FILE",
	RESOLUTION: "RESOLUTION",
	VIEWER: "VIEWER"
}
export const SIGN_TYPE = {
	SIGN: "SIGN",
	CANCEL: "CANCEL"
}
export const COMMENT_TYPES = {
	DOC_FLOW: "doc_flow_comment",
	TERM_TASK: "term_task_comment",
	FOR_REG: "for_reg_comment",
	COMPOSE: "doc_flow_compose"
}
export const COMMENT_ACTIONS = {
	EDIT: "edit",
	REPLY: "reply",
}
