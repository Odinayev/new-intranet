import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import ru from 'vuetify/lib/locale/ru';

// Custom svg icons
import ClipboardPaste from "@/common/components/svg/ClipboardPaste";
import CogsIcon from "@/common/components/svg/CogsIcon";
import DirectDocument from "@/common/components/svg/DirectDocument";
import FileExport from "@/common/components/svg/FileExport";
import FileImport from "@/common/components/svg/FileImport";
import FileInvoice from "@/common/components/svg/FileInvoice";
import Hourglass from "@/common/components/svg/Hourglass";
import MailBlock from "@/common/components/svg/MailBlock";
import MailRead from "@/common/components/svg/MailRead";
import Timer from "@/common/components/svg/Timer";
import User from "@/common/components/svg/User";
import Users from "@/common/components/svg/Users";
import DocumentNormalIcon from "@/common/components/svg/DocumentNormalIcon"
import ClipboardTextIcon from "@/common/components/svg/ClipboardTextIcon"
import DirectInboxIcon from "@/common/components/svg/DirectInboxIcon"
import DirectSendIcon from "@/common/components/svg/DirectSendIcon"
import DirectNormalIcon from "@/common/components/svg/DirectNormalIcon"
import DirectIcon from "@/common/components/svg/DirectIcon"
import DirectNotifyIcon from "@/common/components/svg/DirectNotifyIcon"
import VerifyIcon from "@/common/components/svg/VerifyIcon"
import CakeIcon from "@/common/components/svg/CakeIcon"
import CrownIcon from "@/common/components/svg/CrownIcon"
import ArchiveIcon from "@/common/components/svg/ArchiveIcon"
import PartyIcon from "@/common/components/svg/PartyIcon"
import LinkIcon from "@/common/components/svg/LinkIcon"
import BookSavedIcon from "@/common/components/svg/BookSavedIcon"
import SaveIcon from "@/common/components/svg/SaveIcon"
import SaveAddIcon from "@/common/components/svg/SaveAddIcon"
import MessageIcon from "@/common/components/svg/MessageIcon"
import CalendarIcon from "@/common/components/svg/CalendarIcon"
import TickSquareIcon from "@/common/components/svg/TickSquareIcon"
import TaskSquareIcon from "@/common/components/svg/TaskSquareIcon"
import MinusSquareIcon from "@/common/components/svg/MinusSquareIcon"
import EditIcon from "@/common/components/svg/EditIcon"
import RotateLeftIcon from "@/common/components/svg/RotateLeftIcon"
import CallCallingIcon from "@/common/components/svg/CallCallingIcon"
import ArrowRightIcon from "@/common/components/svg/ArrowRightIcon"
import ArrowLeftIcon from "@/common/components/svg/ArrowLeftIcon"
import FilterIcon from "@/common/components/svg/FilterIcon"
import SliderHorizontalIcon from "@/common/components/svg/SliderHorizontalIcon"
import SmsStarIcon from "../common/components/svg/SmsStarIcon";
import SmsNotificationIcon from "../common/components/svg/SmsNotificationIcon";
import DocumentTextIcon from "@/common/components/svg/DocumentTextIcon.vue"
import UserCircleIcon from "@/common/components/svg/UserCircleIcon.vue"
import ReplyIcon from "@/common/components/svg/ReplyIcon.vue"
import TrashIcon from "@/common/components/svg/TrashIcon.vue"
import PenIcon from "@/common/components/svg/PenIcon.vue"
import FolderDownload from "@/common/components/svg/FolderDownload.vue";
import Menu from "@/common/components/svg/Menu.vue";
import FlashCircle from "@/common/components/svg/FlashCircle.vue";
import Mouse from "@/common/components/svg/Mouse.vue";
import Edit from "@/common/components/svg/Edit.vue";
import MinusCircle from "@/common/components/svg/MinusCircle.vue";
import Eye from "@/common/components/svg/Eye.vue";
import CalendarRemove from "@/common/components/svg/CalendarRemove.vue";
import CalendarWeek from "@/common/components/svg/CalendarWeek.vue";
import Calendar from "@/common/components/svg/Calendar.vue";
import ArrowRight from "@/common/components/svg/ArrowRight.vue";
import UsersV2 from "@/common/components/svg/UsersV2.vue";
import UserV2 from "@/common/components/svg/UserV2.vue";
import EmptyV2 from "@/common/components/svg/EmptyV2.vue";
import Monotone from "@/common/components/svg/Monotone.vue";
import MonotoneFilled from "@/common/components/svg/MonotoneFilled.vue";
import UserCheck from "@/common/components/svg/UserCheck.vue";
import LinkV2 from "@/common/components/svg/LinkV2.vue";
import ExternalLink from "@/common/components/svg/ExternalLink.vue";
import CheckCircleIcon from "@/common/components/svg/CheckCircleIcon.vue";
import CarIcon from "@/common/components/svg/CarIcon.vue";
import LocationIcon from "@/common/components/svg/LocationIcon.vue";
import TickIcon from "@/common/components/svg/TickIcon.vue";
import AvatarUser from "@/common/components/svg/AvatarUser.vue";
import AvatarUserSmall from "@/common/components/svg/AvatarUserSmall.vue";
import WarningIcon from "@/common/components/svg/WarningIcon.vue";
import CheckBoxCircleChecked from "@/common/components/svg/CheckBoxCircleChecked.vue";
import RadioCircleCheckedIcon from "@/common/components/svg/RadioCircleCheckedIcon.vue";
import CheckBoxCircleBlank from "@/common/components/svg/CheckBoxCircleBlank.vue";
import NoteSearch from "@/common/components/svg/NoteSearch.vue";
import BuildingIcon from "@/common/components/svg/BuildingIcon.vue";
import NotebookIcon from "@/common/components/svg/NotebookIcon.vue"
import TelegramBoldIcon from "@/common/components/svg/TelegramBoldIcon.vue"
import CursorDefault from "@/common/components/svg/CursorDefault.vue"
import CursorPrimary from "@/common/components/svg/CursorPrimary.vue"
import ChartDefault from "@/common/components/svg/ChartDefault.vue"
import ChartPrimary from "@/common/components/svg/ChartPrimary.vue"
import {
  LayoutIcon,
  ColumnsIcon,
  BarChartIcon,
  BookIcon,
  BookOpenIcon,
  BoxIcon,
  DollarSignIcon,
  ClipboardIcon,
  CornerUpLeftIcon,
  FileTextIcon,
  GridIcon,
  HardDriveIcon,
  HomeIcon,
  InboxIcon,
  ListIcon,
  MailIcon,
  PhoneIcon,
  PlusCircleIcon,
  PlusIcon,
  SearchIcon,
  SendIcon,
  SettingsIcon,
  TabletIcon,
  UsersIcon,
  XIcon
} from "vue-feather-icons";

Vue.use(Vuetify);

export default new Vuetify({
  breakpoint: {
    thresholds: {
      xs: 600,
      sm: 1024,
      md: 1366,
      lg: 1574,
      xl: 1924
    }
  },
  theme: {
    options: {
      customProperties: true,
    },
    themes: {
      light: {
        primary: "#5964D1",
        'primary-500': "#5562E5",
        success: "#00d084",
        warning: "#FFAB00",
        orange: "#FF5630",
        danger: "#f60000",
        error: "#f60000",
        'greyscale-100': "#F6F8FA",
        'success-300': "#39C286",
        'critic-50': "#FFEEF2",
        'info-500': "#1177FF",
      },
      dark: {
        primary: "#0092df",
        success: "#00d084",
        warning: "#FFAB00",
        orange: "#FF5630",
        danger: "#f60000",
        error: "#f60000",
      },
    },
  },
  lang: {
    locales: {ru},
    current: 'ru',
  },
  icons: {
    values: {
      DocumentNormalIcon: {
        component: DocumentNormalIcon
      },
      DocumentTextIcon: {
        component: DocumentTextIcon
      },
      ClipboardTextIcon: {
        component: ClipboardTextIcon
      },
      DirectInboxIcon: {
        component: DirectInboxIcon
      },
      DirectSendIcon: {
        component: DirectSendIcon
      },
      DirectNormalIcon: {
        component: DirectNormalIcon
      },
      UserCircleIcon: {
        component: UserCircleIcon
      },
      DirectIcon: {
        component: DirectIcon
      },
      DirectNotifyIcon: {
        component: DirectNotifyIcon
      },
      VerifyIcon: {
        component: VerifyIcon
      },
      CakeIcon: {
        component: CakeIcon
      },
      CrownIcon: {
        component: CrownIcon
      },
      ArchiveIcon: {
        component: ArchiveIcon
      },
      PartyIcon: {
        component: PartyIcon
      },
      LinkIcon: {
        component: LinkIcon
      },
      BookSavedIcon: {
        component: BookSavedIcon
      },
      SaveIcon: {
        component: SaveIcon
      },
      SaveAddIcon: {
        component: SaveAddIcon
      },
      MessageIcon: {
        component: MessageIcon
      },
      CalendarIcon: {
        component: CalendarIcon
      },
      TickSquareIcon: {
        component: TickSquareIcon
      },
      TaskSquareIcon: {
        component: TaskSquareIcon
      },
      MinusSquareIcon: {
        component: MinusSquareIcon
      },
      EditIcon: {
        component: EditIcon
      },
      RotateLeftIcon: {
        component: RotateLeftIcon
      },
      CallCallingIcon: {
        component: CallCallingIcon
      },
      ArrowRightIcon: {
        component: ArrowRightIcon
      },
      ArrowLeftIcon: {
        component: ArrowLeftIcon
      },
      LayoutIcon: {
        component: LayoutIcon
      },
      ColumnsIcon: {
        component: ColumnsIcon
      },
      PlusIcon: {
        component: PlusIcon
      },
      TabletIcon: {
        component: TabletIcon
      },
      fileExport: {
        component: FileExport
      },
      fileInvoice: {
        component: FileInvoice
      },
      fileImport: {
        component: FileImport
      },
      clipboardPaste: {
        component: ClipboardPaste
      },
      cogsIcon: {
        component: CogsIcon
      },
      FileTextIcon: {
        component: FileTextIcon
      },
      HomeIcon: {
        component: HomeIcon
      },
      ClipboardIcon: {
        component: ClipboardIcon
      },
      MailIcon: {
        component: MailIcon
      },
      GridIcon: {
        component: GridIcon
      },
      ListIcon: {
        component: ListIcon
      },
      PlusCircleIcon: {
        component: PlusCircleIcon
      },
      FilterIcon: {
        component: FilterIcon
      },
      BoxIcon: {
        component: BoxIcon
      },
      SendIcon: {
        component: SendIcon
      },
      XIcon: {
        component: XIcon
      },
      PhoneIcon: {
        component: PhoneIcon
      },
      SettingsIcon: {
        component: SettingsIcon
      },
      HardDriveIcon: {
        component: HardDriveIcon
      },
      BookIcon: {
        component: BookIcon
      },
      BookOpenIcon: {
        component: BookOpenIcon
      },
      UsersIcon: {
        component: UsersIcon
      },
      MailBlock: {
        component: MailBlock
      },
      MailRead: {
        component: MailRead
      },
      DirectDocument: {
        component: DirectDocument
      },
      Timer: {
        component: Timer
      },
      Hourglass: {
        component: Hourglass
      },
      User: {
        component: User
      },
      Users: {
        component: Users
      },
      CornerUpLeftIcon: {
        component: CornerUpLeftIcon
      },
      BarChartIcon: {
        component: BarChartIcon
      },
      SearchIcon: {
        component: SearchIcon
      },
      InboxIcon: {
        component: InboxIcon
      },
      DollarSignIcon: {
        component: DollarSignIcon
      },
      SliderHorizontalIcon: {
        component: SliderHorizontalIcon
      },
      SmsStarIcon: {
        component: SmsStarIcon
      },
      SmsNotificationIcon: {
        component: SmsNotificationIcon
      },
      ReplyIcon: {
        component: ReplyIcon
      },
      TrashIcon: {
        component: TrashIcon
      },
      PenIcon: {
        component: PenIcon
      },
      FolderDownload: {
        component: FolderDownload
      },
      Menu: {
        component: Menu
      },
      FlashCircle: {
        component: FlashCircle
      },
      Mouse: {
        component: Mouse
      },
      Edit: {
        component: Edit
      },
      MinusCircle: {
        component: MinusCircle
      },
      Eye: {
        component: Eye
      },
      CalendarRemove: {
        component: CalendarRemove
      },
      CalendarWeek: {
        component: CalendarWeek
      },
      Calendar: {
        component: Calendar
      },
      ArrowRight: {
        component: ArrowRight
      },
      UsersV2: {
        component: UsersV2
      },
      UserV2: {
        component: UserV2
      },
      EmptyV2: {
        component: EmptyV2
      },
      Monotone: {
        component: Monotone
      },
      MonotoneFilled: {
        component: MonotoneFilled
      },
      UserCheck: {
        component: UserCheck
      },
      LinkV2: {
        component: LinkV2
      },
      ExternalLink: {
        component: ExternalLink
      },
      CheckCircleIcon: {
        component: CheckCircleIcon
      },
      CarIcon: {
        component: CarIcon
      },
      LocationIcon: {
        component: LocationIcon
      },
      TickIcon: {
        component: TickIcon
      },
      AvatarUser: {
        component: AvatarUser
      },
      AvatarUserSmall: {
        component: AvatarUserSmall
      },
      WarningIcon: {
        component: WarningIcon
      },
      CheckBoxCircleCheckedIcon: {
        component: CheckBoxCircleChecked
      },
      CheckBoxCircleBlankIcon: {
        component: CheckBoxCircleBlank
      },
      NoteSearch: {
        component: NoteSearch
      },
      BuildingIcon: {
        component: BuildingIcon
      },
      NotebookIcon: {
        component: NotebookIcon
      },
      TelegramBoldIcon: {
          component: TelegramBoldIcon
      },
      RadioCircleCheckedIcon: {
        component: RadioCircleCheckedIcon
      },
      CursorDefault: {
        component: CursorDefault
      },
      CursorPrimary: {
        component: CursorPrimary
      },
      ChartDefault: {
        component: ChartDefault
      },
      ChartPrimary: {
        component: ChartPrimary
      }
    }
  },
});
