import { SET_ENTITY } from '@/store/mutation-types'
import CrudService from '@/services/crud.service'
import { resetModel } from '@/common/helpers'
import { format } from 'date-fns'
import { uz } from 'date-fns/locale'

let model = {
	last_name: null,
	first_name: null,
	father_name: null,
	birth_date: null,
	phone: null,
	country: null,
	__country: null,
	region: null,
	__region: null,
	district: null,
	__district: null,
	address: null,
	branch: null,
	__branch: null,
	department: null,
	__department: null,
	position: null,
	__position: null,
	pinfl: null,
	__pinfl: null
}
let templateModel = {
	candidate: null,
	title: null,
	requires_employer_signature: true,
	document_info: {
		region: 'Toshkent shahar',
		signer: null,
		__signer: [],
		__approver: [],
		branch: null,
		__branches: null,
		department: null,
		__departments: null,
		sub_department: null,
		__sub_departments: null,
		division: {},
		__division: null,
		position: null,
		employee: null,
		passport_info: null,
		address: null,
		additional_details: {},
	},
	signers: []
}

export default {
	namespaced: true,
	state: {
		indexTableCols: [
			{
				text: "#",
				value: "index",
				width: "50",
				active: true,
				sortable: false,
				align: "center",
			},
			{
				text: "Имя кандидата",
				value: "last_name",
				width: "265",
				active: true,
				sortable: false,
			},
			{
				text: "Департамент",
				value: "department",
				width: "235",
				active: true,
				sortable: false,
			},
			{
				text: "Позиция",
				value: "position",
				width: "215",
				active: true,
				sortable: false,
			},
			{
				text: "Документы кандидата",
				value: "candidate_documents",
				width: "250",
				active: true,
				sortable: false,
			},
			// {
			// 	text: "Дата загрузки",
			// 	value: "upload_date",
			// 	width: "250",
			// 	active: true,
			// 	sortable: false,
			// },
			// {
			// 	text: "Кому",
			// 	value: "whom",
			// 	width: "250",
			// 	active: true,
			// 	sortable: false,
			// },
			{
				text: "Действие",
				value: "actions",
				width: "100",
				active: true,
				sortable: false,
			}
		],
		indexTableFilters: {
			...model,
			page: 1,
			page_size: 15
		},
		formModel: Object.assign({}, model),
		createButtonLoading: false,
		listLoader: true,
		sidebarTabList: [
			{
				value: 'employment_contract'
			},
			{
				value: 'employment_order'
			},
			{
				value: 'personal_data'
			},
			{
				value: 'job_application'
			},
			{
				value: 'obligation'
			},
			{
				value: 'risk_obligation'
			},
			{
				value: 'financial_responsibility'
			},
			{
				value: 'full_financial_responsibility'
			}
		],
		activeSidebarTab: {
			prev: null,
			current: null,
			next: null
		},
		stepsList: [],
		singingDocuments: [],
		contentModel: {
			...templateModel,
			document_info: Object.assign({
				start_date: '',
				period: null,
				trial_period: null,
				work_time: null,
				register_number: null,
				register_date: '',
			}, templateModel.document_info)
		},
		orderModel: {
			...templateModel,
			document_info: Object.assign({
				start_date: '',
				register_date: '',
				register_number: null,
				additional_details: {
					shb: null,
					text: null
				}
			}, templateModel.document_info)
		},
		personalDataModel: {
			...templateModel,
			document_info: Object.assign({
				start_date: '',
				register_date: '',
				additional_details: {
					passport: null,
					stir: null,
					table_number: null
				}
			}, templateModel.document_info)
		},
		jobApplication: {
			...templateModel,
			document_info: {
				...templateModel.document_info,
				start_date: '',
				register_date: '',
				additional_details: {
					heading_1: '“Asakabank” AJ Boshqaruvi',
					heading_2: 'Raisi v.b K.A.Tulyaganovga',
					heading_3: null,
					heading_4: null,
					heading_5: null,
					formatted_start_date: null,
				}
			}
		},
		obligationModel: {
			...templateModel,
			document_info: Object.assign({
				start_date: '',
				register_date: '',
			}, templateModel.document_info)
		},
		riskObligationModel: {
			...templateModel,
			document_info: Object.assign({
				start_date: '',
				register_date: '',
			}, templateModel.document_info)
		},
		financialResponsibilityModel: {
			...templateModel,
			document_info: {
				...templateModel.document_info,
				start_date: '',
				register_date: '',
			}
		},
		fullFinancialResponsibilityModel: {
			...templateModel,
			document_info: {
				...templateModel.document_info,
				start_date: '',
				register_date: '',
				additional_details: {
					chairman_register_date: '',
					chairman_register_number: '',
					pinfl: null,
					stir: null
				}
			}
		}
	},
	getters: {
		isStepFinished: state => ({ step }) => state.stepsList.some(item => item.document.type === step),
		isStepAgreed: state => ({ step }) => {
			let current = state.stepsList.find(item => item.document.type === step)

			return current && current.document.is_employer_agreed
		},
		isStepFullySinged: state => ({ step }) => {
			let current = state.stepsList.find(item => item.document.type === step)

			return current.document.is_candidate_signed && current.document.is_employer_signed
		},
		getEmployer: state => state.contentModel.signers.find(employer => employer.type === 'employer'),
		getSigners: state => ({ model }) => model.signers.filter(item => item.type === 'signer'),
	},
	/**
	 *
	 **/
	actions: {
		async fetchHiringCreate({ commit, rootState, state }) {
			let model = {
				...state.formModel,
				country: state.formModel.__country.id,
				branch: state.formModel.__branch.id,
				department: state.formModel.__department.id,
				district: state.formModel.__district.id,
				region: state.formModel.__region.id,
				position: state.formModel.__position.id,
				pinfl: state.formModel.__pinfl.replace(/ /g, ''),
				phone: state.formModel.phone.replace(/[^0-9]/g, ''),
			}

			commit(SET_ENTITY, {
				module: "hrHiring",
				entity: "createButtonLoading",
				value: true
			}, { root: true })

			commit(SET_ENTITY, {
				module: "hrHiring",
				entity: "listLoader",
				value: true
			}, { root: true })

			try {
				await CrudService.post(`hr/candidates/`, model)
					.then((data) => {
						resetModel(state.formModel)
					})

				return Promise.resolve()
			}
			catch (e) {
				return Promise.reject()
			}
			finally {
				setTimeout(() => {
					commit(SET_ENTITY, {
						module: "hrHiring",
						entity: "createButtonLoading",
						value: false
					}, { root: true })

					commit(SET_ENTITY, {
						module: "hrHiring",
						entity: "listLoader",
						value: false
					}, { root: true })
				}, 500)
			}
		},
		/*
		*
		* */
		async fetchHiringById({ commit, state }, payload) {
			await CrudService.getList(`hr/candidates/${payload.id}/`)
				.then((data) => {
					state.formModel = data
					state.formModel.__country = data.country
					state.formModel.__region = data.region
					state.formModel.__district = data.district
					state.formModel.__branch = data.branch
					state.formModel.__department = data.department
					state.formModel.__position = data.position
					state.formModel.__pinfl = data.pinfl
				})
		},
		/*
		*
		* */
		async fetchHiringUpdate({ commit, state }, payload) {
			let model = {
				...state.formModel,
				country: state.formModel.__country.id,
				branch: state.formModel.__branch.id,
				department: state.formModel.__department.id,
				district: state.formModel.__district.id,
				region: state.formModel.__region.id,
				position: state.formModel.__position.id,
				pinfl: state.formModel.__pinfl.replace(/ /g, ''),
				phone: state.formModel.phone.replace(/[^0-9]/g, ''),
			}

			commit(SET_ENTITY, {
				module: "hrHiring",
				entity: "createButtonLoading",
				value: true
			}, { root: true })

			commit(SET_ENTITY, {
				module: "hrHiring",
				entity: "listLoader",
				value: true
			}, { root: true })

			try {
				this.$axios.put(`hr/candidates/${payload.id}/`, model)
					.then((data) => {

					})

				return Promise.resolve()
			}
			catch (e) {
				return Promise.reject()
			}
			finally {
				setTimeout(() => {
					commit(SET_ENTITY, {
						module: "hrHiring",
						entity: "createButtonLoading",
						value: false
					}, { root: true })

					commit(SET_ENTITY, {
						module: "hrHiring",
						entity: "listLoader",
						value: false
					}, { root: true })
				}, 500)
			}
		},
		/*
		*
		* */
		async fetchHiringDelete({ commit, state }, payload) {
			commit(SET_ENTITY, {
				module: "hrHiring",
				entity: "createButtonLoading",
				value: true
			}, { root: true })

			commit(SET_ENTITY, {
				module: "hrHiring",
				entity: "listLoader",
				value: true
			}, { root: true })

			try {
				this.$axios.delete(`hr/candidates/${payload.id}/`)
					.then((data) => {

					})

				return Promise.resolve()
			}
			catch (e) {
				return Promise.reject()
			}
			finally {
				setTimeout(() => {
					commit(SET_ENTITY, {
						module: "hrHiring",
						entity: "createButtonLoading",
						value: false
					}, { root: true })

					commit(SET_ENTITY, {
						module: "hrHiring",
						entity: "listLoader",
						value: false
					}, { root: true })
				}, 500)
			}
		},
		/*
		*
		* */
		async fetchHiringNextStep({ commit, state }, { type, title }) {
			let model = {}
			let approvers = []
			let candidate = state.contentModel.signers.find(candidate => candidate.type === 'candidate')

			switch (type) {
				case 'employment_contract':
					console.log('employment_contract')
					model = {
						...state.contentModel,
						candidate: state.formModel.id,
						type,
						title
					}
					break;
				case 'employment_order':
					console.log('employment_order')
					if(state.orderModel.document_info.__approver.length) {
						approvers = state.orderModel.document_info.__approver.map(approver => {
							return {
								signer_id: approver.id,
								type: 'signer',
								first_name: approver.first_name,
								last_name: approver.last_name,
								father_name: approver.father_name,
								full_name: approver.full_name
							}
						})
					}

					model = {
						...state.orderModel,
						candidate: state.formModel.id,
						type,
						title,
						signers: [
							{
								...state.contentModel.document_info.signer,
								signer_id: state.contentModel.document_info.signer.id,
								type: 'employer',
							},
							...approvers,
							{
								father_name: candidate.father_name,
								first_name: candidate.first_name,
								full_name: candidate.full_name,
								last_name: candidate.last_name,
								signer_id: candidate.signer_id,
								type: 'candidate'
							}
						],
						document_info: {
							...state.orderModel.document_info,
							// start_date: format(new Date(state.orderModel.document_info.start_date), 'dd.MM.yyyy'),
							// register_date: format(new Date(state.orderModel.document_info.register_date), 'dd.MM.yyyy'),
							department: state.contentModel.document_info.department,
							sub_department: state.contentModel.document_info.sub_department,
							division: state.contentModel.document_info.division,
							position: state.contentModel.document_info.position
						}
					}
					break;
				case 'personal_data':
					console.log('personal_data')
					model = {
						...state.personalDataModel,
						candidate: state.formModel.id,
						type,
						title,
						signers: [
							// {
							// 	...state.contentModel.document_info.signer,
							// 	signer_id: state.contentModel.document_info.signer.id,
							// 	type: 'employer',
							// },
							{
								father_name: candidate.father_name,
								first_name: candidate.first_name,
								full_name: candidate.full_name,
								last_name: candidate.last_name,
								signer_id: candidate.signer_id,
								type: 'candidate'
							}
						],
						document_info: {
							...state.personalDataModel.document_info,
							address: state.contentModel.document_info.address,
							region: state.contentModel.document_info.region,
							// start_date: format(new Date(state.personalDataModel.document_info.start_date), 'dd.MM.yyyy'),
							// register_date: format(new Date(state.personalDataModel.document_info.register_date), 'dd.MM.yyyy'),
						}
					}
					break;
				case 'job_application':
					console.log('job_application')
					model = {
						...state.jobApplication,
						candidate: state.formModel.id,
						type,
						title,
						document_info: {
							...state.jobApplication.document_info,
							// start_date: format(new Date(state.jobApplication.document_info.start_date), 'dd.MM.yyyy'),
							// register_date: format(new Date(state.jobApplication.document_info.register_date), 'dd.MM.yyyy'),
							department: state.contentModel.document_info.department,
							sub_department: state.contentModel.document_info.sub_department,
							division: state.contentModel.document_info.division,
							position: state.contentModel.document_info.position,
							additional_details: {
								...state.jobApplication.document_info.additional_details,
								formatted_start_date: `${format(new Date(state.jobApplication.document_info.start_date), 'yyyy', { locale: uz })} yil ${format(new Date(state.jobApplication.document_info.start_date), 'dd', { locale: uz })} ${format(new Date(state.jobApplication.document_info.start_date), 'MMMM', { locale: uz }).toLocaleLowerCase()}dan`
							}
						}
					}
					break;
				case 'obligation':
					console.log('obligation')
					model = {
						...state.obligationModel,
						candidate: state.formModel.id,
						type,
						title,
						signers: [
							{
								father_name: candidate.father_name,
								first_name: candidate.first_name,
								full_name: candidate.full_name,
								last_name: candidate.last_name,
								signer_id: candidate.signer_id,
								type: 'candidate'
							}
						],
						document_info: {
							...state.obligationModel.document_info,
							// start_date: format(new Date(state.obligationModel.document_info.start_date), 'dd.MM.yyyy'),
							// register_date: format(new Date(state.obligationModel.document_info.register_date), 'dd.MM.yyyy'),
							department: state.contentModel.document_info.department,
							position: state.contentModel.document_info.position
						}
					}
					break;
				case 'risk_obligation':
					console.log('risk_obligation')
					model = {
						...state.riskObligationModel,
						candidate: state.formModel.id,
						type,
						title,
						signers: [
							{
								father_name: candidate.father_name,
								first_name: candidate.first_name,
								full_name: candidate.full_name,
								last_name: candidate.last_name,
								signer_id: candidate.signer_id,
								type: 'candidate'
							}
						],
						document_info: {
							...state.riskObligationModel.document_info,
							// start_date: format(new Date(state.riskObligationModel.document_info.start_date), 'dd.MM.yyyy'),
							// register_date: format(new Date(state.riskObligationModel.document_info.register_date), 'dd.MM.yyyy'),
							department: state.contentModel.document_info.department,
							position: state.contentModel.document_info.position
						}
					}
					break;
				case 'financial_responsibility':
					console.log('financial_responsibility')
					model = {
						...state.financialResponsibilityModel,
						candidate: state.formModel.id,
						type,
						title,
						signers: [
							{
								...state.contentModel.document_info.signer,
								signer_id: state.contentModel.document_info.signer.id,
								type: 'employer',
							},
							{
								father_name: candidate.father_name,
								first_name: candidate.first_name,
								full_name: candidate.full_name,
								last_name: candidate.last_name,
								signer_id: candidate.signer_id,
								type: 'candidate'
							}
						],
						document_info: {
							...state.financialResponsibilityModel.document_info,
							// start_date: format(new Date(state.financialResponsibilityModel.document_info.start_date), 'dd.MM.yyyy'),
							// register_date: format(new Date(state.financialResponsibilityModel.document_info.register_date), 'dd.MM.yyyy'),
							department: state.contentModel.document_info.department,
							address: state.contentModel.document_info.address,
							additional_details: {
								contract_number: state.contentModel.document_info.additional_details.contract_number
							}
						}
					}
					break;
				default: // full_financial_responsibility
					console.log('full_financial_responsibility')
					model = {
						...state.fullFinancialResponsibilityModel,
						candidate: state.formModel.id,
						type,
						title,
						signers: [
							{
								...state.contentModel.document_info.signer,
								signer_id: state.contentModel.document_info.signer.id,
								type: 'employer',
							},
							{
								father_name: candidate.father_name,
								first_name: candidate.first_name,
								full_name: candidate.full_name,
								last_name: candidate.last_name,
								signer_id: candidate.signer_id,
								type: 'candidate'
							}
						],
						document_info: {
							...state.fullFinancialResponsibilityModel.document_info,
							// start_date: format(new Date(state.fullFinancialResponsibilityModel.document_info.start_date), 'dd.MM.yyyy'),
							// register_date: format(new Date(state.fullFinancialResponsibilityModel.document_info.register_date), 'dd.MM.yyyy'),
							department: state.contentModel.document_info.department,
							sub_department: state.contentModel.document_info.sub_department,
							division: state.contentModel.document_info.division,
							position: state.contentModel.document_info.position,
							address: state.contentModel.document_info.address,
							additional_details: {
								...state.fullFinancialResponsibilityModel.document_info.additional_details,
								pinfl: state.formModel.pinfl,
								stir: state.personalDataModel.document_info.additional_details.stir,
								contract_number: state.contentModel.document_info.additional_details.contract_number
							}
						}
					}
			}

			try {
				return await this.$axios.post(`hr/signing-documents/`, model)
					.then((data) => {
					})
			}
			catch (e) {
				return Promise.reject()
			}
			finally {

			}
		},
		/*
		*
		* */
		async fetchUpdateCandidateSigningDocument({ commit, state }, { type, title, id }) {
			let model = {
				...state.contentModel,
				candidate: state.formModel.id,
				type,
				title,
			}

			console.log('id', type, title, id);
			console.log('state.contentModel', state.contentModel);
			console.log('model', model);

			// try {
			// 	return await this.$axios.put(`hr/signing-documents/${id}/`, state.contentModel)
			// 		.then((data) => {
			// 		})
			// }
			// catch (e) {
			// 	return Promise.reject()
			// }
			// finally {
			//
			// }
		},
		/*
		*
		* */
		async fetchCandidateSigningDocument({ commit, state }, { candidate, title }) {
			let { results } = await CrudService.getList(`hr/signing-documents/`, {
				candidate,
				title
			})

			commit(SET_ENTITY, {
				module: 'hrHiring',
				entity: 'singingDocuments',
				value: results
			}, { root: true })

			return Promise.resolve(results.length ? results[0] : null)
		},
		/*
		*
		* */
		async fetchEmploymentContractDocument({ commit, state, dispatch }, { candidate, title }) {
			let model = await dispatch('fetchCandidateSigningDocument', { candidate, title })

			if(model) {
				state.contentModel = {
					...model,
					document_info: {
						...model.document_info,
						// start_date: `${year}-${month}-${day}`,
						// register_date: `${registerYear}-${registerMonth}-${registerDay}`,
						__branches: model.document_info.branch,
						__departments: model.document_info.department,
						__division: model.document_info.division,
						__sub_departments: model.document_info.sub_department,
						__signer: model.document_info.signer
					}
				}

				state.orderModel = {
					...state.orderModel,
					document_info: {
						...state.orderModel.document_info,
						start_date: model.document_info.start_date,
						register_date: model.document_info.register_date
					}
				}

				state.personalDataModel = {
					...state.personalDataModel,
					document_info: {
						...state.personalDataModel.document_info,
						start_date: model.document_info.start_date,
						register_date: model.document_info.register_date
					}
				}

				state.jobApplication = {
					...state.jobApplication,
					document_info: {
						...state.jobApplication.document_info,
						start_date: model.document_info.start_date,
						register_date: model.document_info.register_date
					}
				}

				state.obligationModel = {
					...state.obligationModel,
					document_info: {
						...state.obligationModel.document_info,
						start_date: model.document_info.start_date,
						register_date: model.document_info.register_date
					}
				}

				state.riskObligationModel = {
					...state.riskObligationModel,
					document_info: {
						...state.riskObligationModel.document_info,
						start_date: model.document_info.start_date,
						register_date: model.document_info.register_date
					}
				}

				state.financialResponsibilityModel = {
					...state.financialResponsibilityModel,
					document_info: {
						...state.financialResponsibilityModel.document_info,
						start_date: model.document_info.start_date,
						register_date: model.document_info.register_date
					}
				}

				state.fullFinancialResponsibilityModel = {
					...state.fullFinancialResponsibilityModel,
					document_info: {
						...state.fullFinancialResponsibilityModel.document_info,
						start_date: model.document_info.start_date,
						register_date: model.document_info.register_date
					}
				}
			}
		},
		/*
		*
		* */
		async fetchEmploymentOrderDocument({ commit, state, dispatch }, { candidate, title }) {
			let model = await dispatch('fetchCandidateSigningDocument', { candidate, title })

			if(model) {
				state.orderModel = {
					...model,
					document_info: {
						...model.document_info,
						__branches: model.document_info.branch,
						__departments: model.document_info.department,
						__division: model.document_info.division,
						__sub_departments: model.document_info.sub_department,
						__signer: model.document_info.signer,
						__approver: model.signers.filter(approver => approver.type === 'signer')
					}
				}
			}
		},
		/*
		*
		* */
		async fetchPersonalDataDocument({ commit, state, dispatch }, { candidate, title }) {
			let model = await dispatch('fetchCandidateSigningDocument', { candidate, title })

			if(model) {
				state.personalDataModel = {
					...model
				}
			}
		},
		/*
		*
		* */
		async fetchJobApplicationDocument({ commit, state, dispatch }, { candidate, title }) {
			let model = await dispatch('fetchCandidateSigningDocument', { candidate, title })

			if(model) {
				state.jobApplication = {
					...model
				}
			}
		},
		/*
		*
		* */
		async fetchObligationDocument({ commit, state, dispatch }, { candidate, title }) {
			let model = await dispatch('fetchCandidateSigningDocument', { candidate, title })

			if(model) {
				state.obligationModel = {
					...model
				}
			}
		},
		/*
		*
		* */
		async fetchRiskObligationDocument({ commit, state, dispatch }, { candidate, title }) {
			let model = await dispatch('fetchCandidateSigningDocument', { candidate, title })

			if(model) {
				state.riskObligationModel = {
					...model
				}
			}
		},
		/*
		*
		* */
		async fetchFinancialResponsibilityDocument({ commit, state, dispatch }, { candidate, title }) {
			let model = await dispatch('fetchCandidateSigningDocument', { candidate, title })

			if(model) {
				state.financialResponsibilityModel = {
					...model
				}
			}
		},
		/*
		*
		* */
		async fetchFullFinancialResponsibilityDocument({ commit, state, dispatch }, { candidate, title }) {
			let model = await dispatch('fetchCandidateSigningDocument', { candidate, title })

			if(model) {
				state.fullFinancialResponsibilityModel = {
					...model
				}
			}
		},
		/*
		*
		* */
		async fetchSigningSteps({ commit, state }, { candidate, signerId }) {
			await CrudService.getList(`hr/signing-steps/`, {
				candidate,
				signer_id: signerId
			})
				.then((data) => {
					commit(SET_ENTITY, {
						module: 'hrHiring',
						entity: 'stepsList',
						value: data.results
					}, { root: true })
				})
		},
		/*
		*
		* */
		async fetchSigningAgree({ commit, state }, { stepId, signerId }) {
			try {
				return await this.$axios.put(`hr/signing-steps/${stepId}/move-next-step/`, {
					signer: signerId,
				})
					.then((data) => {

					})
			}
			catch (e) {
				return Promise.reject()
			}
			finally {

			}
		},
		/*
		*
		* */
		async fetchSignAllDocuments({ commit, state, dispatch }, model) {
			try {
				await this.$axios.put(`hr/signing-documents/sign/`, model)
					.then((data) => {

					})
				return Promise.resolve()
			}
			catch (e) {
				return Promise.reject()
			}
			finally {
			}
		},
		/*
		*
		* */
		handleSidebarItem({ commit, state, dispatch }, { index }) {
			let prev = state.sidebarTabList[index - 1]?.value ?? null
			let current = state.sidebarTabList[index].value
			let next = state.sidebarTabList[index + 1]?.value ?? null

			dispatch('setActiveSidebarTab', { prev, current, next })
			this.$router.replace({
				query: { prev, current, next }
			}).catch(() => {})
		},
		/*
		*
		* */
		handleSignerSidebarItem({ commit, state, dispatch }, { index }) {
			let prev = state.stepsList[index - 1]?.document.type ?? null
			let current = state.stepsList[index].document.type
			let next = state.stepsList[index + 1]?.document.type ?? null

			dispatch('setActiveSidebarTab', { prev, current, next })
			this.$router.replace({
				query: { prev, current, next }
			}).catch(() => {})
		},
		/*
		*
		* */
		setActiveSidebarTab({ commit }, payload) {
			commit(SET_ENTITY, {
				module: "hrHiring",
				entity: "activeSidebarTab",
				value: {
					prev: payload.prev,
					current: payload.current,
					next: payload.next
				}
			}, { root: true })
		},
		/*
		*
		* */
		setActiveSignerDocument({ commit, state }, { current }) {
			let active = state.singingDocuments.find(({ type }) => type === current)
			state.formModel = active.signers.find(({ type }) => type === 'candidate')
			state.contentModel = {
				...active,
				document_info: {
					...active.document_info,
					__departments: active.document_info.department,
					__sub_departments: active.document_info.sub_department,
					__division: active.document_info.division,
					__branches: active.document_info.branch,
					signer: active.signers.find(({ type }) => type === 'employer'),
					__signer: active.signers.find(({ type }) => type === 'employer')
				}
			}

			switch (current) {
				case 'employment_order':
					state.orderModel = {
						...active,
						document_info: {
							...active.document_info,
							__approver: active.signers.filter(({ type }) => type === 'signer')
						}
					}
					break;
				case 'personal_data':
					state.personalDataModel = {
						...active
					}
					break;
				case 'job_application':
					state.jobApplication = {
						...active
					}
					break;
				case 'obligation':
					state.obligationModel = {
						...active
					}
					break;
				case 'risk_obligation':
					state.riskObligationModel = {
						...active
					}
					break;
				case 'financial_responsibility':
					state.financialResponsibilityModel = {
						...active
					}
					break;
				default:
					state.fullFinancialResponsibilityModel = {
						...active
					}
			}
		}
	},
}
