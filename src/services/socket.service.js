let socket = null

export default class Socket {
	#store;
	constructor(store) {
		this.#store = store
		this.socket = null
		this.a = null
	}

	init(url, token) {
		socket = new WebSocket(`${url}?token=${token}`)
		this.socket = socket

		this.randomNumber()

		return this
	}

	randomNumber() {
		return this.a = Math.random()
	}

	onOpen(json) {
		socket.addEventListener("open", () => {
			socket.send(JSON.stringify(json))
		})
	}

	onCloseCurrent(json) {
		socket.addEventListener("close", () => {
			socket.send(JSON.stringify(json))
		})
	}

	onMessageSend(json) {
		socket.send(JSON.stringify(json))
	}

	onMessageReceive() {
		socket.addEventListener("message", (event) => {
			const data = JSON.parse(event.data)

			console.log(data);

			switch (data.type) {
				case "new_message":
					this.#store.dispatch("chatModule/mergeConversationUserMessages", data);
					this.#store.dispatch("chatModule/fetchConversationUsersList");
					this.#store.dispatch("chatModule/fetchConversationGroupList");
					this.#store.dispatch("chatModule/fetchGetBot");
					this.#store.dispatch("chatModule/fetchToggleScrollHeight");
					this.#store.dispatch("fetchUnreadCountList");
					// this.#store.dispatch("fetchNewMessageNotification", data);
					break;
				case "new_chat_message":
					this.#store.dispatch("chatModule/fetchConversationUsersList");
					this.#store.dispatch("chatModule/fetchConversationGroupList");
					this.#store.dispatch("chatModule/fetchGetBot");
					this.#store.dispatch("fetchNotification", { ...data, type: "new_message" });
					this.#store.dispatch("fetchUnreadCountList");
					this.#store.dispatch("fetchNewMessageNotification", data);
					break;
				case "status_changed":
					this.#store.dispatch("documentFlowModule/fetchIncomingDocumentUpdate", data)
					break;
				case "document_acquainted":
					this.#store.dispatch("documentFlowModule/fetchUpdateAcquainted", data)
					break;
				case "review_document_acquainted":
					this.#store.dispatch("documentFlowModule/fetchReviewUpdateAcquainted", data)
					break;
				case "document_uploaded_by_scanner":
					this.#store.dispatch("documentFlowModule/fetchScannedDocument", data)
					break;
				case "document_performed":
					this.#store.dispatch("documentFlowModule/fetchDocumentPerformed", data)
					break;
				case "notification":
					this.#store.dispatch("fetchNotification", data)
					break;
				case "unread_notification_count":
					this.#store.dispatch("fetchNotificationListUnreadCount", data)
					break;
				case "message_read":
					this.#store.dispatch("chatModule/fetchSetMessageReadModel", data.content);
					this.#store.dispatch("fetchUnreadCountList");
					break;
				case "delete_message":
					this.#store.dispatch("chatModule/fetchDeleteMessageFromSocket", data.content)
					break;
				case "edit_message":
					this.#store.dispatch("chatModule/fetchEditMessageFromSocket", data.content)
					break;
				case "new_update_available":
					this.#store.dispatch("socket/newContentAvailable", data.content)
					break;
				case "generation_failed":
					this.#store.dispatch("equipmentModule/fetchGenerationFailedData", data)
					break;
				default:
					1
			}
		})
	}

	onCloseChannel() {
		socket.close()
	}
}
