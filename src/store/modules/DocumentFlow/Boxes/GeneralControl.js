import { SET_ENTITY } from "@/store/mutation-types"
import CrudService from "@/services/crud.service"
import { SIGN_TYPE } from "@/common/enums/notificationTypes";

export default {
  namespaced: true,
  state: {
    indexTableCols: [
      {
        text: "#",
        value: "index",
        width: "50",
        active: true,
        sortable: false,
        align: "center",
      },
      {
        text: "components.execution-duration",
        value: "incoming_doc.deadline",
        width: "150",
        active: true,
        sortable: false,
        cols: {
          "md": "4"
        },
        component: "ui-countdown",
        detail: true,
        order: 3
      },
      {
        text: "user-profile.views.name",
        value: "incoming_doc.title",
        active: true,
        sortable: false,
        width: "125"
      },
      {
        text: "document-flow.views.registration.state",
        value: "read_time",
        width: "135",
        active: false,
        sortable: false
      },
      {
        text: "priority",
        value: "incoming_doc.priority",
        width: "150",
        active: true,
        sortable: false,
        cols: {
          "md": "4"
        },
        component: "ui-priority",
        detail: true,
        order: 6
      },
      {
        text: "components.registration-number",
        value: "incoming_doc.register_number",
        active: true,
        sortable: false,
        cols: {
          "md": "4"
        },
        component: null,
        detail: true,
        order: 5
      },
      {
        text: "components.document-type",
        value: "incoming_doc.doc_type.name",
        active: true,
        sortable: false,
        cols: {
          "md": "4"
        },
        component: null,
        detail: true,
        order: 1
      },
      {
        text: "document-flow.views.boxes.journal",
        value: "incoming_doc.journal.name",
        active: true,
        sortable: false,
        cols: {
          "md": "4"
        },
        component: null,
        detail: true,
        order: 2
      },
      {
        text: "components.outgoing-number",
        value: "incoming_doc.outgoing_number",
        active: false,
        sortable: false,
        cols: {
          "md": "4"
        },
        component: null,
        detail: true,
        order: 7
      },
      {
        text: "document-flow.components.registration-date",
        value: "incoming_doc.register_date",
        active: true,
        sortable: false,
        cols: {
          "md": "4"
        },
        component: "ui-date-text",
        detail: true,
        order: 5,
      },
      {
        text: "document-flow.components.outgoing-date",
        value: "incoming_doc.outgoing_date",
        active: false,
        sortable: false,
        cols: {
          "md": "4"
        },
        component: "ui-date-text",
        detail: true,
        order: 8
      },
      {
        text: "document-flow.components.correspondent",
        value: "incoming_doc.correspondent.organization_name",
        width: "350",
        active: true,
        sortable: false,
        cols: {
          "md": "12"
        },
        component: null,
        detail: true,
        order: 9
      },
      {
        text: "document-flow.components.for-consideration",
        value: "reviewers",
        active: true,
        sortable: false,
        cols: {
          "md": "4"
        },
        component: "ui-avatar-group",
        detail: true,
        order: 4
      },
      {
        text: "document-flow.views.boxes.content",
        value: "incoming_doc.description",
        active: false,
        sortable: false,
        width: "300",
        cols: {
          "md": "12"
        },
        component: null,
        detail: true,
        order: 10
      },
      {
        text: "components.status",
        value: "status.name",
        width: "140",
        sortable: false,
        active: true,
        cols: {
          "md": "4"
        },
        component: "ui-status",
        detail: true,
        order: 3
      },
    ],
    indexTableFilters: {
      search: null,
      priority: null,
      is_read: null,
      has_deadline: null,
      type: null,
      is_agreed: null,
      has_resolution: null,
      status: null,
      statusList: null,
      condition: null,
      journal: null,
      doc_type: null,
      doc_type_name: null,
      register_number: null,
      register_start_date: null,
      register_end_date: null,
      outgoing_number: null,
      outgoing_start_date: null,
      outgoing_end_date: null,
      correspondent: null,
      correspondent_name: null,
      is_verified: null,
      __correspondent: null,
      __doc_type: null,
      __register_date: null,
      __outgoing_date: null,
      review_user: null,
      review_user_name: null,
      __review_user: null,
      page: 1,
      page_size: 15
    },
    generalControlDetailModelLoader: true,
    generalControlDetailModel: null,
  },

  actions: {
    async fetchGeneralControlById({ commit, rootState }, payload) {
      if(payload.loader) {
        commit(SET_ENTITY, {
          module: "documentFlowBoxesGeneralControl",
          entity: "generalControlDetailModelLoader",
          value: true
        }, {root: true})
      }

      await CrudService.getList(`v2/docflow/${rootState.auth.currentUser.filial}/users-review/${payload.id}/`)
        .then((data) => {
          commit(SET_ENTITY, {
            module: "documentFlowBoxesGeneralControl",
            entity: "generalControlDetailModel",
            value: data
          }, {root: true})

          setTimeout(() => {
            commit(SET_ENTITY, {
              module: "documentFlowBoxesGeneralControl",
              entity: "generalControlDetailModelLoader",
              value: false
            }, {root: true})
          }, 1000)
        })
    },
    /** **/
    async fetchAcquaintWithDocument({ commit, rootState, state, dispatch }, payload) {
      await this.$axios.put(`/v2/docflow/${rootState.auth.currentUser.filial}/review/${payload.documentId}/acquaint/`)
        .then(({ data }) => {
          state.generalControlDetailModel.read_time = data.read_time

          dispatch(
            "treeView/fetchTreeUsersList",
            { treeId: state.generalControlDetailModel.incoming_doc.id },
            { root: true }
          )
        })
    },
    /** **/
    async fetchReviewExecutionMark({ commit, rootState, state, dispatch }, payload) {
      await this.$axios.put(`/v2/docflow/${rootState.auth.currentUser.filial}/users-review/${payload.id}/`, {
        comment: payload.text,
        status: 4
      })
        .then(({ data }) => {
          dispatch(
            "fetchGeneralControlById",
            { id: state.generalControlDetailModel.id, loader: false }
          )

          dispatch(
            "treeView/fetchTreeUsersList",
            { treeId: state.generalControlDetailModel.incoming_doc.id },
            { root: true }
          )
        })
    },
    /** **/
    setActiveIndexTableCols({ commit }, payload) {
      commit(SET_ENTITY, { module: "documentFlowBoxesGeneralControl", entity: "indexTableCols", value: payload }, { root: true })
    },
    /** **/
    resetIndexTableCols({ state }) {
      state.indexTableCols = [
        {
          text: "#",
          value: "index",
          width: "50",
          active: true,
          sortable: false,
          align: "center",
        },
        {
          text: "components.execution-duration",
          value: "incoming_doc.deadline",
          width: "150",
          active: true,
          sortable: false,
          cols: {
            "md": "4"
          },
          component: "ui-countdown",
          detail: true,
          order: 3
        },
        {
          text: "user-profile.views.name",
          value: "incoming_doc.title",
          active: true,
          sortable: false,
          width: "125"
        },
        {
          text: "document-flow.views.registration.state",
          value: "read_time",
          width: "135",
          active: false,
          sortable: false
        },
        {
          text: "priority",
          value: "incoming_doc.priority",
          width: "150",
          active: true,
          sortable: false,
          cols: {
            "md": "4"
          },
          component: "ui-priority",
          detail: true,
          order: 6
        },
        {
          text: "components.registration-number",
          value: "incoming_doc.register_number",
          active: true,
          sortable: false,
          cols: {
            "md": "4"
          },
          component: null,
          detail: true,
          order: 5
        },
        {
          text: "components.document-type",
          value: "incoming_doc.doc_type.name",
          active: true,
          sortable: false,
          cols: {
            "md": "4"
          },
          component: null,
          detail: true,
          order: 1
        },
        {
          text: "document-flow.views.boxes.journal",
          value: "incoming_doc.journal.name",
          active: true,
          sortable: false,
          cols: {
            "md": "4"
          },
          component: null,
          detail: true,
          order: 2
        },
        {
          text: "components.outgoing-number",
          value: "incoming_doc.outgoing_number",
          active: false,
          sortable: false,
          cols: {
            "md": "4"
          },
          component: null,
          detail: true,
          order: 7
        },
        {
          text: "document-flow.components.registration-date",
          value: "incoming_doc.register_date",
          active: true,
          sortable: false,
          cols: {
            "md": "4"
          },
          component: "ui-date-text",
          detail: true,
          order: 5,
        },
        {
          text: "document-flow.components.outgoing-date",
          value: "incoming_doc.outgoing_date",
          active: false,
          sortable: false,
          cols: {
            "md": "4"
          },
          component: "ui-date-text",
          detail: true,
          order: 8
        },
        {
          text: "document-flow.components.correspondent",
          value: "incoming_doc.correspondent.organization_name",
          width: "350",
          active: true,
          sortable: false,
          cols: {
            "md": "12"
          },
          component: null,
          detail: true,
          order: 9
        },
        {
          text: "document-flow.components.for-consideration",
          value: "reviewers",
          active: true,
          sortable: false,
          cols: {
            "md": "4"
          },
          component: "ui-avatar-group",
          detail: true,
          order: 4
        },
        {
          text: "document-flow.views.boxes.content",
          value: "incoming_doc.description",
          active: false,
          sortable: false,
          width: "300",
          cols: {
            "md": "12"
          },
          component: null,
          detail: true,
          order: 10
        },
        {
          text: "components.status",
          value: "status.name",
          width: "140",
          sortable: false,
          active: true,
          cols: {
            "md": "4"
          },
          component: "ui-status",
          detail: true,
          order: 3
        },
      ]
    },
    /** **/
    async resetIndexTableFilters({ state }) {
      state.indexTableFilters = {
        search: null,
        priority: null,
        is_read: null,
        has_deadline: null,
        type: null,
        is_agreed: null,
        has_resolution: null,
        status: null,
        statusList: null,
        condition: null,
        journal: null,
        doc_type: null,
        doc_type_name: null,
        register_number: null,
        register_start_date: null,
        register_end_date: null,
        outgoing_number: null,
        outgoing_start_date: null,
        outgoing_end_date: null,
        correspondent: null,
        correspondent_name: null,
        is_verified: null,
        __correspondent: null,
        __doc_type: null,
        __register_date: null,
        __outgoing_date: null,
        review_user: null,
        review_user_name: null,
        __review_user: null,
        page: 1,
        page_size: 15
      }
    },
    /** **/
    setActiveIndexTableFilters({ commit, state }, payload) {
      Object.entries(payload).forEach(item => {
        const [key, value] = item

        if(value && (typeof value === "boolean" || Array.isArray(value))) {
          state.indexTableFilters[key] = value
        }

        if(!(value && (typeof value === "boolean"))) {
          state.indexTableFilters[key] = value
        }

        if(value && (typeof value === "number" || typeof value === "string")) {
          state.indexTableFilters[key] = Number(value) ? Number(value) : value
        }

        if(value === "true" || value === "false") {
          state.indexTableFilters[key] = value === "true"
        }
      })

      Object.entries(state.indexTableFilters).forEach(item => {
        const [key, value] = item

        if(value && value.length && key === "statusList") {
          state.indexTableFilters.statusList = state.indexTableFilters.statusList.map(item => parseInt(item))
        }

        if(value && key === "doc_type_name") {
          state.indexTableFilters.__doc_type = {
            id: state.indexTableFilters.doc_type,
            name: state.indexTableFilters.doc_type_name,
            type: "doc_type"
          }
        }

        if(value && key === "register_start_date") {
          state.indexTableFilters.__register_date = [state.indexTableFilters.register_start_date, state.indexTableFilters.register_end_date]
        }

        if(value && key === "outgoing_start_date") {
          state.indexTableFilters.__outgoing_date = [state.indexTableFilters.outgoing_start_date, state.indexTableFilters.outgoing_end_date]
        }

        if(value && key === "correspondent_name") {
          state.indexTableFilters.__correspondent = {
            id: state.indexTableFilters.correspondent,
            organization_name: state.indexTableFilters.correspondent_name
          }
        }

        if(value && key === "review_user_name") {
          state.indexTableFilters.__review_user = {
            id: state.indexTableFilters.review_user,
            full_name: state.indexTableFilters.review_user_name
          }
        }
      })
    },
  }

}
