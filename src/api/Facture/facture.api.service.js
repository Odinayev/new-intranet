import axios from "axios";

export const fetchDeleteFacture = (id) => {
  return new Promise((resolve, reject) => {
    axios.post(`/facture/document/delete/`, {
      document_id: id
    })
      .then((res) => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      })
  })
}

export const fetchDeleteFacturePermanently = (id) => {
  return new Promise((resolve, reject) => {
    axios.post(`/facture/document/full_delete/`, {
      document_id: id
    })
      .then((res) => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      })
  })
}

export const getReceiverDetails = (inn) => {
  return new Promise ((resolve, reject) => {
    axios.get(`/facture/company_basic_details/`, {
      params: {
        company_inn: inn
      }
    })
      .then(({data}) => {
        resolve(data);
      })
      .catch(err => {
        reject(err);
      })
  })
}

export const fetchCreatePowerOfAttorneyRoaming = (model) => {
  return new Promise((resolve, reject) => {
    axios.post(`/facture/powers_of_attorney/create/`, model)
      .then(res => {
        resolve(res.data);
      })
      .catch(err => {
        reject(err)
      })
  })
}

export const fetchSendToAgreement = ({ id, users }) => {
  return new Promise((resolve, reject) => {
    axios.post(`/facture/agreement/send/${id}/`, {
      users
    })
      .then(({ data }) => {
        resolve(data);
      })
      .catch(err => {
        reject(err)
      })
  })
}
