export default {
	namespaced: true,
	state: {
		userHandShakeSocket: null,
		chatSocket: null,
		newContentAvailable: {
			isNew: false,
			content: [],
			title: null
		},
	},
	/*
	*
	* */
	getters: {
		getUserHandShakeSocket: (state) => state.userHandShakeSocket,
		getChatSocket: (state) => state.chatSocket,
		isNewContentAvailable: (state) => state.newContentAvailable,
	},
	/*
	*
	* */
	mutations: {
		"SET_USER_HANDSHAKE_SOCKET"(state, payload) {
			state.userHandShakeSocket = payload
		},
		/*
		*
		* */
		"SET_CHAT_SOCKET"(state, payload) {
			state.chatSocket = payload
		},
		/*
		*
		* */
		"SET_NEW_CONTENT_AVAILABLE"(state, payload) {
			state.newContentAvailable.title = payload.title
			state.newContentAvailable.content = payload.change_logs
			state.newContentAvailable.isNew = true
		},
	},
	/*
	*
	* */
	actions: {
		initUserHandShakeSocket({ commit }, payload) {
			commit("SET_USER_HANDSHAKE_SOCKET", payload)
		},
		/*
		*
		* */
		chatSocket({ commit, dispatch, state }, payload) {
			commit("SET_CHAT_SOCKET", payload)
		},
		/*
		*
		* */
		newContentAvailable({ commit }, payload) {
			commit("SET_NEW_CONTENT_AVAILABLE", payload)
		}
	},
}
