import Vue from "vue";
import {capitalize, upperCase} from "lodash";
import {differenceInDays, isSameDay, parseISO} from "date-fns";
import {diffWords} from "diff";
import axios from "../plugins/axios";
import {AUDIT_PERFORMER_TYPES} from "./constants";

export function findUserById(array, targetId) {
  console.log("ARRAY", array)
  for (const item of array) {
    if (item.user.id === targetId) {
      return item;
    }

    if (item.children) {
      const result = findUserById(item.children, targetId);
      if (result) {
        return result;
      }
    }
  }

  return null;
}

export function getValueByPath(obj, path) {
  // Разбиваем путь на отдельные ключи
  const keys = path.split('.');

  // Используем метод reduce для последовательного доступа к вложенным свойствам объекта
  return keys.reduce((acc, key) => {
    return acc ? acc[key] : null;
  }, obj);
}

export function combineKeysDocumentDetail(cols, model) {
  return cols
    .filter(col => col.detail)
    .sort((prevCol, nextCol) => prevCol.order - nextCol.order)
    .map(item => {
      return {
        key: item.text,
        value: getValueByPath(model, item.value),
        cols: item.cols,
        component: item.component
      }
    })
}

export function getItemRow(id, parentArray) {
  return findNodeWithId(id, parentArray)
}

function findNodeWithId(id, rootArr) {
  for (let el of rootArr) {
    if (el.id === id) {
      return el
    }
    if (el.replies && el.replies.length > 0) {
      const idFoundInChildren = findNodeWithId(id, el.replies)
      if (idFoundInChildren !== null) {
        return idFoundInChildren
      }
    }
  }
  return null
}

export function lazyLoadComponent({componentFactory, loading, loadingData}) {
  let resolveComponent;

  return () => ({
    // We return a promise to resolve a
    // component eventually.
    component: new Promise((resolve) => {
      resolveComponent = resolve;
    }),
    loading: {
      mounted() {
        // We immediately load the component if
        // `IntersectionObserver` is not supported.
        if (!('IntersectionObserver' in window)) {
          componentFactory().then(resolveComponent);
          return;
        }

        const observer = new IntersectionObserver((entries) => {
          // Use `intersectionRatio` because of Edge 15's
          // lack of support for `isIntersecting`.
          // See: https://github.com/w3c/IntersectionObserver/issues/211
          if (entries[0].intersectionRatio <= 0) return;

          // Cleanup the observer when it's not
          // needed anymore.
          observer.unobserve(this.$el);
          // The `componentFactory()` resolves
          // to the result of a dynamic `import()`
          // which is passed to the `resolveComponent()`
          // function.
          componentFactory().then(resolveComponent);
        });
        // We observe the root `$el` of the
        // mounted loading component to detect
        // when it becomes visible.
        observer.observe(this.$el);
      },
      // Here we render the the component passed
      // to this function via the `loading` parameter.
      render(createElement) {
        return createElement(loading, loadingData);
      },
    },
  });
}

/*
*
* */
export function selectFromList(array, context) {
  let list = []

  array.forEach(item => {
    list.push(context[item - 1])
  })

  return list
}

/*
*
* */
export function setVH() {
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty(`--vh`, `${vh}px`);
}

/*
*
* */
export function prettyBytes(num, precision = 3, addSpace = true) {
  const UNITS = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
  if (Math.abs(num) < 1) return num + (addSpace ? ' ' : '') + UNITS[0];
  const exponent = Math.min(
    Math.floor(Math.log10(num < 0 ? -num : num) / 3),
    UNITS.length - 1
  );
  const n = Number(
    ((num < 0 ? -num : num) / 1000 ** exponent).toPrecision(precision)
  );
  return (num < 0 ? '-' : '') + n + (addSpace ? ' ' : '') + UNITS[exponent];
};

/**/
export function toMonthName(monthNumber, language) {
  const date = new Date();
  date.setMonth(monthNumber);

  return date.toLocaleString(language, {
    month: 'long',
  });
};

/**/
export function toMonthNameUzb(monthNumber) {
  switch (Number(monthNumber)) {
    case 1:
      return "yanvar";
    case 2:
      return "fevral";
    case 3:
      return "mart";
    case 4:
      return "aprel";
    case 5:
      return "may";
    case 6:
      return "iyun";
    case 7:
      return "iyul";
    case 8:
      return "avgust";
    case 9:
      return "sentabr";
    case 10:
      return "oktabr";
    case 11:
      return "noyabr";
    case 12:
      return "dekabr";
    default:
      return "Noto'g'ri sana"
  }
}

/*
*
* */
export const currentDate = (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().substr(0, 10)

/*
*
* */
export const generateDayHours = (interval, language = window.navigator.language) => {
  const ranges = [];
  const date = new Date();
  const format = {
    hour: 'numeric',
    minute: 'numeric',
  };

  for (let minutes = 480; minutes < 24 * 60 - 300; minutes = minutes + interval) {
    date.setHours(0);
    date.setMinutes(minutes);
    ranges.push({
      id: minutes,
      time: date.toLocaleTimeString(language, format)
    });
  }

  return ranges;
}

/*
*
* */
export const clearUnderscoresFromString = (string) => string.replace(/[_.-]/gi, " ")

/*
*
* */
export const stringCapitalize = (string) => capitalize(clearUnderscoresFromString(string))

/*
*
* */
export const stringUppercase = (string) => upperCase(clearUnderscoresFromString(string))

/*
*
* */
export const createUUIDv4 = () => {
  return 'xxxxxxxx'.replace(/[xy]/g, c => {
    const r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
};

/*
*
* */
export const base64toBlob = (b64Data, contentType = '', sliceSize = 512) => {
  const byteCharacters = atob(b64Data);
  const byteArrays = [];

  for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    const slice = byteCharacters.slice(offset, offset + sliceSize);

    const byteNumbers = new Array(slice.length);
    for (let i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }

    const byteArray = new Uint8Array(byteNumbers);
    byteArrays.push(byteArray);
  }

  return new Blob(byteArrays, {type: contentType});
}

/*
*
* */
export const textDifferent = (item, textNode) => {
  let diff = diffWords(item.old_text, item.new_text)
  let fragment = document.createDocumentFragment();

  for (let i = 0; i < diff.length; i++) {
    if (diff[i].added && diff[i + 1] && diff[i + 1].removed) {
      let swap = diff[i];
      diff[i] = diff[i + 1];
      diff[i + 1] = swap;
    }

    let node;

    if (diff[i].removed) {
      node = document.createElement('del');
      node.appendChild(document.createTextNode(diff[i].value));
    } else if (diff[i].added) {
      node = document.createElement('ins');
      node.appendChild(document.createTextNode(diff[i].value));
    } else {
      node = document.createTextNode(diff[i].value);
    }
    fragment.appendChild(node);
  }

  textNode.forEach(item => {
    item.textContent = ""
    item.appendChild(fragment)
  })
}

export const getObjectWithoutKeyNull = (obj) => {
	let newObj = {};
	for (const [key, value] of Object.entries(obj)){
		if (obj.hasOwnProperty(key)){
			if (obj[key] != null && obj[key] !== ""){
				newObj[key] = value;
			}
		}
	}
	return newObj;
}

export function resetModel(model, excludes = []){
	for (const [key, value] of Object.entries(model)){
		if (model.hasOwnProperty(key) && !excludes.includes(key)){
			if (Array.isArray(model[key])){
				model[key] = [];
			}
			else if (Object.prototype.toString.call(model[key]) === '[object Object]'){
				model[key] = {};
			}
			else {
				model[key] = null;
			}
		}
	}
}

export function factureAdjustItems(invoiceServices) {
  return new Promise((resolve, reject) => {
    if (invoiceServices.length) {
      let array = [];

      axios.get(`/facture/product_catalog/list/`)
        .then(({data}) => {
          invoiceServices.forEach(item => {
            let productSelected = data.find(x => x.classCode === item.catalog.code);
            array.push({
              vat_temp: {
                vat_rate_temp: item.vatRate === '0.12' ? '12%' : item.vatRate === '0' ? '0' : 'Без НДС',
                vat_value_temp: item.vatAmountDisplay,
              },
              productSelected,
              measurementSelected: productSelected.packageNames.find(x => x.code === item.measurementCode),
              // end temporary objects
              item_number: "1",
              description: productSelected.name,
              volume: item.quantity,
              unit_price: item.pricePerItem,
              subtotal: item.deliveryCost,
              vat: {
                vat_rate: item.vatRate === '0.12' ? '12%' : item.vatRate === '0' ? '0' : 'Без НДС',
                vat_value: item.vatAmountDisplay
              },
              subtotal_with_taxes: item.deliveryCostWithTaxes,
              measurement_unit: item.measurement,
              excise: {
                excise_rate: "",
                excise_value: ""
              },
              catalog: {
                code: productSelected.classCode,
                name: productSelected.className
              },
              lgota_id: "",
              barcode: item.barcode
            })
            resolve(array)
          })
        })
    }
  })
}

export function getFactureSignDetails(documentId) {
  return new Promise((resolve, reject) => {
    axios.post(`/facture/sign/get_documents_hash_codes/`, {
      documents: [documentId]
    })
      .then(({data}) => {
        let roamingJsonContent64 = Base64.encode(data.Documents[0].RoamingJsonContent);
        resolve({roamingJsonContent64})
      })
  })
}

export function create_pkcs7_64(uid, keyId, roamingJsonContent64, comment) {
  return new Promise((resolve, reject) => {
    CAPIWS.callFunction({plugin: "pkcs7", name: "create_pkcs7", arguments: [roamingJsonContent64, keyId, 'no']}, (event, data) => {
      if (data.success === true){
				axios.get(`/facture/sign/get_timestamp/`, {
					params: {
						signature_hex: data.signature_hex
					}
				})
					.then((res) => {
						if (res.data.Success === true){
							CAPIWS.callFunction({plugin:"pkcs7", name:"attach_timestamp_token_pkcs7", arguments:[data.pkcs7_64, data.signer_serial_number, res.data.TimeStamp]},function(event, response){
								if (response.success === true){
                  axios.post(`/facture/sign/document/`, {
                    unique_id: uid,
                    certificate_serial: response.signer_serial_number,
                    signed_content: "",
                    roaming_signed_content: response.pkcs7_64,
                    comment: comment
                  })
                    .then(signResponse => {
                      resolve(signResponse);
                    })
                    .catch(err => {
                      reject(err);
                    })
                }else {
                  Vue.$toast.error("Что-то пошло не так. Пожалуйста, попробуйте еще раз!")
                }
							})
						}else {
              Vue.$toast.error("Что-то пошло не так. Пожалуйста, попробуйте еще раз!")
            }
					})
			}else {
        Vue.$toast.error("Пароль неверный!")
      }
    })

  })
}

export function returnCondition(condition) {
	if (condition === 'vacation'){
		return 'Mehnat ta\'tilida'
	}else if (condition === 'business_trip') {
		return 'Xizmat safarida'
	}else if (condition === 'unpaid_leave') {
		return 'O\'z hisobidan ta\'tilda'
	}else if (condition === 'sick') {
		return 'Kasallik varaqasida'
	}else {
		return null
	}
}

export const adjustSignersForJointProtocol = (signed_by) => {
	let temp_finance_chairman = signed_by.find(item => item.position === 'chairman_finance');
	let finance_chairman = temp_finance_chairman;
	if (temp_finance_chairman && temp_finance_chairman.hasOwnProperty('user')) {
		finance_chairman = {...temp_finance_chairman, ...temp_finance_chairman.user}
	}

	let temp_monitoring_chairman = signed_by.find(item => item.position === 'chairman_monitoring');
	let monitoring_chairman = temp_monitoring_chairman;
	if (temp_monitoring_chairman && temp_monitoring_chairman.hasOwnProperty('user')) {
		monitoring_chairman = {...temp_monitoring_chairman, ...temp_monitoring_chairman.user}
	}

	let temp_finance_signers = signed_by.filter(item => item.position === 'board_members_finance');
	let finance_signers = []
	temp_finance_signers.forEach(item => {
		if (item && item.hasOwnProperty('user')){
			finance_signers.push({ ...item.user, ...item, temp_condition: item.condition ? item.condition : null });
		}else {
			finance_signers.push(item)
		}
	})

	let temp_monitoring_signers = signed_by.filter(item => item.position === 'board_members_monitoring');
	let monitoring_signers = []
	temp_monitoring_signers.forEach(item => {
		if (item && item.hasOwnProperty('user')){
			monitoring_signers.push({ ...item.user, ...item, temp_condition: item.condition ? item.condition : null });
		}else {
			monitoring_signers.push(item)
		}
	})

	return { finance_chairman, monitoring_chairman, finance_signers, monitoring_signers }
}

export const returnShortMonthName = (date, locale = 'ru') => {
  const monthNamesUz  = ["Yan", "Fev", "Mar", "Apr", "May", "Iyun", "Iyul", "Avg", "Sen", "Okt", "Noy", "Dek"]
  const monthNamesRu  = ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"]

  const month = new Date(date).getMonth();
  return locale === "ru" ? monthNamesRu[month] : monthNamesUz[month]
}

export const returnDateDifferenceFromNow = (date, locale = "ru") => {
  const diffInDays = differenceInDays(parseISO(date), new Date())

  if (locale === "ru"){
    if (diffInDays <= 1) {
      return `остался ${diffInDays} день`
    } else if (diffInDays > 1 && diffInDays < 5) {
      return `осталось ${diffInDays} дня`
    } else {
      return `осталось ${diffInDays} дней`
    }
  } else {
    return `${diffInDays} kun qoldi`
  }
}
export const returnDateDifference = (date, locale = "ru") => {
  return differenceInDays(parseISO(date), new Date())
}
export const returnPosition = (item) => {
  if (item.hasOwnProperty('user')){
    return item.user.position
  }else {
    return item.position
  }
}

export const returnShortFullName = (item) => {
  if (item.hasOwnProperty('user')){
    return `${returnFirstCharacter(item.user.first_name)} ${returnFirstCharacter(item?.user?.father_name ? item.user.father_name : null)} ${item.user.last_name}`
  }else {
    return `${returnFirstCharacter(item.first_name)} ${returnFirstCharacter(item?.father_name ? item.father_name : null)} ${item.last_name}`
  }
}

export const returnFirstCharacter = (name) => {
  if (name){
    const firstTwoCharacters =  name.slice(0, 2).toLowerCase();
    if (firstTwoCharacters === 'sh' || firstTwoCharacters === 'ch' || firstTwoCharacters === "o'" || firstTwoCharacters === "g'"){
      return name.slice(0, 2) + '.'
    }else {
      return name.slice(0, 1) + '.'
    }
  }else {
    return ""
  }
}

export const returnIsSameDay = (date) => {
  return isSameDay(parseISO(date), new Date());
}

export const returnPriorityText = (value) => {
  return value === 'high' ? `Javob xati talab etiladi` : `Ma'lumot uchun`
}

export const getXdfuItem = () => {
  const key = 'xdfu'
  const itemStr = localStorage.getItem(key);
  if (!itemStr) {
    return null;
  }

  const item = JSON.parse(itemStr);
  const now = new Date();

  // Compare the expiry time with the current time
  if (now.getTime() > item.expiry) {
    // If the item has expired, remove it from storage
    localStorage.removeItem(key);
    return null;
  }
  return item.value;
}

export const checkItem = () => {
  return getXdfuItem();
}

export const adjustUserObjectToAutocomplete = (items = [], multiple = true, userId = null) => {
  if (items && items.length && multiple) {
    const userIds = items.map(item => item.hasOwnProperty('user') ? item.user.id : item.id).join(',')
    return new Promise((resolve, reject) => {
      axios.get(`/user/search/`, { params: { ids: userIds } })
        .then(res => {
          resolve(res.data.results)
        })
        .catch(err => {
          reject(err)
        })
    })
  } else if (!multiple && userId) {
    return new Promise((resolve, reject) => {
      axios.get(`/user/search/`, { params: { ids: userId } })
        .then(res => {
          if (res && res.status === 200 && res.data.results && res.data.results.length) {
            resolve(res.data.results[0])
          }
        })
        .catch(err => {
          reject(err)
        })
    })
  }
}
export const findNewParticipant = async (participants, tasks) => {
  // Use map to transform firstArray, checking for matching ids in secondArray
  const filteredArray = await participants
      .map(item => {
        // Check if any performer in secondArray has the same id as the item in firstArray
        const match = tasks.some(secondItem => secondItem.performer.id === item.id);
        // If no match is found, return the item, otherwise return null
        return match ? null : item;
      })
      .filter(item => item !== null); // Filter out null values

  // If the filtered array is empty, return null, otherwise return the array
  return filteredArray.length > 0 ? filteredArray : null;
}

export const findRealParticipants = async (participants, tasks) => {
  const firstArrayIds = new Set(participants.map(item => item.id));

  // Filter the secondArray to only include objects where performer.id exists in firstArray
  return tasks.filter(secondItem =>
      firstArrayIds.has(secondItem.performer.id) || secondItem.performer_type === AUDIT_PERFORMER_TYPES.GROUP_HEAD
  );
}

export const deepEqual = (obj1, obj2) => {
  if (obj1 === obj2) return true; // check reference equality first

  if (typeof obj1 !== 'object' || typeof obj2 !== 'object' || obj1 === null || obj2 === null) {
    return false; // return false if not objects or if one of them is null
  }

  const keys1 = Object.keys(obj1);
  const keys2 = Object.keys(obj2);

  if (keys1.length !== keys2.length) return false; // check if they have the same number of properties

  for (let key of keys1) {
    if (!keys2.includes(key) || !deepEqual(obj1[key], obj2[key])) {
      return false; // check if both have the same keys and values
    }
  }

  return true;
}

export const adjustObjectToArray = (api = '', items = [], multiple = true, id = null) => {
  if (api && items.length && multiple) {
    const ids = items.map(item => item.id).join(',')
    return new Promise((resolve, reject) => {
      axios.get(`/${api}/`, {
        params: {ids}
      })
        .then(res => {
          if (res && res.status === 200) {
            resolve(res.data.results)
          }
        })
    })
  }
  else if (api && multiple === false && id){
    return new Promise((resolve, reject) => {
      axios.get(`/${api}/${id}/`)
        .then(res => {
          if (res && res.status === 200) {
            console.log(res)
          }
        })
    })
  }
}

export const validateNumber = (event) => {
  if (!/^[0-9]$/.test(event.key) && event.key !== 'Backspace') {
    event.preventDefault()
  }
}

export const validateOnPaste = (event) => {
  const pastedText = event.clipboardData.getData('Text')
  if (!/^\d+$/.test(pastedText)) {
    event.preventDefault()
  }
}

export const returnCurrentMonthStartDate = () => {
  const year = new Date().getFullYear()
  const month = new Date().getMonth() + 1
  const monthString = month < 10 ? `0${month}` : month
  return `${year}-${monthString}-01`
}

export const returnCurrentDate = () => {
  const year = new Date().getFullYear()
  const month = new Date().getMonth() + 1
  const monthString = month < 10 ? `0${month}` : month
  const date = new Date().getDate()
  const dateString = date < 10 ? `0${date}` : date
  return `${year}-${monthString}-${dateString}`
}

export const removeUnderscoreKeys = (obj) => {
  if (typeof obj !== 'object' || obj === null) {
    return obj
  }

  const newObj = Array.isArray(obj) ? [] : {}

  for (const key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      if (!key.startsWith('__')) {
        newObj[key] = removeUnderscoreKeys(obj[key])
      }
    }
  }
  return newObj
}


Vue.mixin({
  methods: {
    getFirstCharacter: val => {
      if (val) {
        const firstTwoCharacters = val.slice(0, 2).toLowerCase();
        if (firstTwoCharacters === 'sh' || firstTwoCharacters === 'ch') {
          return val.slice(0, 2) + '.'
        } else {
          return val.slice(0, 1) + '.'
        }
      } else {
        return ""
      }
    }
  }
})
