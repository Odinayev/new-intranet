

export const DOCUMENT_TEXT_LIST = [
	{
		id: 1,
		text: "Входящие",
		icon: "$vuetify.icons.fileImport",
		colors: {
			primary: "var(--primary-1)",
			secondary: "var(--secondary-1)",
			transparent: "var(--transparent-1)"
		}
	},
	{
		id: 2,
		text: "Внутренние",
		icon: "$vuetify.icons.fileInvoice",
		colors: {
			primary: "var(--primary-2)",
			secondary: "var(--secondary-2)",
			transparent: "var(--transparent-2)"
		}
	},
	{
		id: 3,
		text: "Исходящие",
		icon: "$vuetify.icons.fileExport",
		colors: {
			primary: "var(--primary-3)",
			secondary: "var(--secondary-3)",
			transparent: "var(--transparent-3)"
		}
	},
	{
		id: 4,
		text: "Обращения",
		icon: null,
		colors: {
			primary: "var(--primary-4)",
			secondary: "var(--secondary-4)",
			transparent: "var(--transparent-4)"
		}
	},
	{
		id: 5,
		text: "Приказы и распоряжения. Протоколы",
		icon: null,
		colors: {

		}
	},
	{
		id: 6,
		text: "Контрольные документы",
		icon: null,
		colors: {

		}
	},
	{
		id: 7,
		text: "Пришедшие на регистрацию",
		icon: null,
		colors: {

		}
	},
	{
		id: 8,
		text: "Органайзер",
		icon: null,
		colors: {

		}
	},
	{
		id: 9,
		text: "Из вышестоящего органа",
		icon: null,
		colors: {

		}
	},
	{
		id: 10,
		text: "Согласование",
		icon: null,
		colors: {

		}
	}
]

export const TABLE_HEAD_TEXT_LIST = [
	{
		id: 1,
		title: "#",
		width: "75px"
	},
	{
		id: 2,
		title: "Рег. номер",
		width: "150px",
		value: "registrationNumber"
	},
	{
		id: 3,
		title: "Рег. дата",
		width: "150px",
		value: "registrationDate"
	},
	{
		id: 4,
		title: "Корреспондент",
		width: "200px",
		value: "Correspondent"
	},
	{
		id: 5,
		title: "Исх. номер",
		width: "150px",
		value: "originalNumber"
	},
	{
		id: 6,
		title: "Исх. дата",
		width: "150px",
		value: "originalDate"
	},
	{
		id: 7,
		title: "Содержание",
		width: "250px",
		value: "content"
	},
	{
		id: 8,
		title: "Файлы",
		width: "150px",
		value: "files"
	},
	{
		id: 9,
		title: "На рассмотрение",
		width: "150px",
		value: "forConsideration"
	},
	{
		id: 10,
		title: "Резолюция",
		width: "150px",
		value: "resolution"
	},
	{
		id: 11,
		title: "Ответ. исполнитель",
		width: "175px",
		value: "responsibleExecutor"
	},
	{
		id: 12,
		title: "Исполнители",
		width: "150px",
		value: "performers"
	},
	{
		id: 13,
		title: "Причина возврата",
		width: "150px",
		value: "reasonForReturn"
	},
	{
		id: 14,
		title: "Срок исполнителя",
		width: "150px",
		value: "executorTerm"
	},
	{
		id: 15,
		title: "Тип",
		width: "150px",
		value: "type"
	},
	{
		id: 16,
		title: "Кому",
		width: "150px",
		value: "whom"
	},
	{
		id: 17,
		title: "Дата рассылки",
		width: "150px",
		value: "dispatchDate"
	},
	{
		id: 18,
		title: "Срок ответа",
		width: "150px",
		value: "responseTime"
	},
	{
		id: 19,
		title: "Действие",
		width: "75px",
		value: "actions"
	},
	{
		id: 20,
		title: "Уровень",
		width: "100px",
		value: "level"
	},
	{
		id: 21,
		title: "Статус",
		width: "125px",
		value: "status"
	},
	{
		id: 22,
		title: "Имя файла",
		width: "200px",
		value: "fileName"
	},
	{
		id: 23,
		title: "Дата и время",
		width: "125px",
		value: "dateTime"
	},
	{
		id: 24,
		title: "Имя сотрудника",
		width: "250px",
		value: "employeeName"
	},
	{
		id: 25,
		title: "Срок исполнения",
		width: "200px",
		value: "periodExecution"
	},
	{
		id: 26,
		title: "Назначенные",
		width: "200px",
		value: "appointed"
	},
	{
		id: 27,
		title: "Отв. исп.",
		width: "75px",
		value: "responsibleExecutorCheck"
	},
	{
		id: 28,
		title: "Создатель",
		width: "125px",
		value: "createdBy"
	},
	{
		id: 29,
		title: "Отв. за контр.",
		width: "100px",
		value: "controlPoint"
	},
	{
		id: 30,
		title: "Вид документа",
		width: "150px",
		value: "documentType"
	},
	{
		id: 31,
		title: "Подписант",
		width: "100px",
		value: "signer"
	},
	{
		id: 32,
		title: "Автор",
		width: "100px",
		value: "author"
	},
	{
		id: 33,
		title: "Подразделение",
		width: "150px",
		value: "filial"
	},
	{
		id: 34,
		title: "Ответ. исп.",
		width: "100px",
		value: "responsibleExecutor2"
	},
]

export const REFERENCE_DOC_TYPE = "doc_type"

export const REFERENCE_CORRESPONDENT = "correspondent"

export const MONTH_LIST = [
	"January",
	"February",
	"March",
	"April",
	"May",
	"June",
	"July",
	"August",
	"September",
	"October",
	"November",
	"December"
]

export const REVIEW_COLUMN = "REVIEW_COLUMN"
export const REVIEW_FILTER = "REVIEW_FILTER"
export const XDFU_REVIEW_COLUMN = "XDFU_REVIEW_COLUMN"
export const XDFU_REVIEW_FILTER = "XDFU_REVIEW_FILTER"
export const XDFU_GENERAL_CONTROL_COLUMN = "XDFU_GENERAL_CONTROL_COLUMN"
export const XDFU_GENERAL_CONTROL_FILTER = "XDFU_GENERAL_CONTROL_FILTER"
export const INBOX_COLUMN = "INBOX_COLUMN"
export const INBOX_FILTER = "INBOX_FILTER"
export const XDFU_INBOX_COLUMN = "XDFU_INBOX_COLUMN"
export const XDFU_INBOX_FILTER = "XDFU_INBOX_FILTER"
export const SIGN_COLUMN = "SIGN_COLUMN"
export const SIGN_FILTER = "SIGN_FILTER"
export const APPROVAL_COLUMN = "APPROVAL_COLUMN"
export const APPROVAL_FILTER = "APPROVAL_FILTER"
export const CREATED_RESOLUTIONS_COLUMN = "CREATED_RESOLUTIONS_COLUMN"
export const CREATED_RESOLUTIONS_FILTER = "CREATED_RESOLUTIONS_FILTER"
export const CONTROL_COLUMN = "CONTROL_COLUMN"
export const CONTROL_FILTER = "CONTROL_FILTER"
export const INCOMING_COLUMN = "INCOMING_COLUMN"
export const INCOMING_FILTER = "INCOMING_FILTER"
export const SECRET_DOCUMENTS_COLUMN = "SECRET_DOCUMENTS_COLUMN"
export const SECRET_DOCUMENTS_FILTER = "SECRET_DOCUMENTS_FILTER"
export const INNER_COLUMN = "INNER_COLUMN"
export const INNER_FILTER = "INNER_FILTER"
export const OUTGOING_COLUMN = "OUTGOING_COLUMN"
export const OUTGOING_FILTER = "OUTGOING_FILTER"
export const APPEAL_COLUMN = "APPEAL_COLUMN"
export const APPEAL_FILTER = "APPEAL_FILTER"
export const CALL_CENTER_APPEAL_COLUMN = "CALL_CENTER_APPEAL_COLUMN"
export const CALL_CENTER_APPEAL_FILTER = "CALL_CENTER_APPEAL_FILTER"
export const ORDER_AND_PROTOCOL_COLUMN = "ORDER_AND_PROTOCOL_COLUMN"
export const ORDER_AND_PROTOCOL_FILTER = "ORDER_AND_PROTOCOL_FILTER"
export const FILIAL_DOCUMENT_COLUMN = "FILIAL_DOCUMENT_COLUMN"
export const FILIAL_DOCUMENT_FILTER = "FILIAL_DOCUMENT_FILTER"
export const APPLICATION_COLUMN = "APPLICATION_COLUMN"
export const APPLICATION_FILTER = "APPLICATION_FILTER"
export const SEND_DOCUMENT_COLUMN = "SEND_DOCUMENT_COLUMN"
export const SEND_DOCUMENT_FILTER = "SEND_DOCUMENT_FILTER"
export const AUDIT_CONFIG_GROUP_COLUMN = "AUDIT_CONFIG_GROUP_COLUMN"
export const AUDIT_CONFIG_GROUP_FILTER = "AUDIT_CONFIG_GROUP_FILTER"
export const AUDIT_CHECKUP_DETAIL_COLUMN = "AUDIT_CHECKUP_DETAIL_COLUMN"
export const AUDIT_CHECKUP_DETAIL_FILTER = "AUDIT_CHECKUP_DETAIL_FILTER"
export const SURVEY_COLUMN = "SURVEY_COLUMN"
export const SURVEY_FILTER = "SURVEY_FILTER"
export const SURVEY_BLOCK_COLUMN = "SURVEY_BLOCK_COLUMN"
export const SURVEY_BLOCK_FILTER = "SURVEY_BLOCK_FILTER"
export const AUDIT_DIRECTIONS_COLUMN = "AUDIT_DIRECTIONS_COLUMN"
export const AUDIT_DIRECTIONS_FILTER = "AUDIT_DIRECTIONS_FILTER"
export const AUDIT_CHECKUP_COLUMN = "AUDIT_CHECKUP_COLUMN"
export const AUDIT_CHECKUP_FILTER = "AUDIT_CHECKUP_FILTER"
export const CONFIRMATION_COLUMN = "CONFIRMATION_COLUMN"
export const CONFIRMATION_FILTER = "CONFIRMATION_FILTER"
export const GENERAL_CONTROL_COLUMN = "GENERAL_CONTROL_COLUMN"
export const GENERAL_CONTROL_FILTER = "GENERAL_CONTROL_FILTER"
export const HR_HIRING_COLUMN = "HR_HIRING_COLUMN"
export const HR_HIRING_FILTER = "HR_HIRING_FILTER"
export const RESOLUTION_TYPES = {
	ASSIGNMENT: "assignment",
	CONTROL_POINT: "control_point",
	FOR_INFORMATION: "for_information",
}
export const RESOLUTION_TYPES_LIST = [
	{
		title: "Поручение",
		value: RESOLUTION_TYPES.ASSIGNMENT
	},
	{
		title: "Контрольный пункт",
		value: RESOLUTION_TYPES.CONTROL_POINT
	},
	{
		title: "Для сведения",
		value: RESOLUTION_TYPES.FOR_INFORMATION
	},
]
export const ROLE_TYPES = {
	ADMIN: "admin",
	ASSISTANT: "assistant"
}

export const DEP_NAME_NOT_VISIBLE_USER_IDS = [10013, 5646, 10362, 5326, 5322, 5398, 5798, 5397]
export const AUDIT_CHECKUP_TYPES = {
	DEPARTMENT: "department",
	FILIAL: "filial",
	PROCESS: "process"
}
export const AUDIT_PERFORMER_TYPES = {
	GROUP_HEAD: "group_head",
	GROUP_MEMBER: "group_member"
}
export const AUDIT_RESULT_TYPES = {
	FLAW: "flaw",
	SUCCESS: "success",
	ANALYSIS: "analysis"
}
export const AUDIT_USER_TYPES = {
	AUDITOR: "auditor",
	RESPONSIBLE: "responsible"
}

export const STATUS_TYPES = {
	PENDING: "pending",
	SENT: "sent",
	FORWARDED: "forwarded",
	REJECTED: "rejected",
	IN_PROGRESS: "in_progress",
	ACCEPTED: "accepted",
	CLOSED: "closed",
	PUBLISHED: 'published',
	NOT_PUBLISHED: 'not_published'
}
