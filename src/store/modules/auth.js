import { removeToken, saveToken, getRefreshToken, saveAccessToken, removeAccessToken } from "@/services/jwt.service";
import { SET_ENTITY } from "@/store/mutation-types";

export default {
	namespaced: true,
	state: {
		userLoggedIn: false,
		currentUser: {}
	},
	/*
	*
	* */
	getters: {
		getCurrentUser: (state) => state.currentUser,
		getUserLoggedIn: (state) => state.userLoggedIn
	},
	/*
	*
	* */
	mutations: {
		SET_CURRENT_USER(state, payload) {
			state.currentUser = payload
		},
	},
	/*
	*
	* */
	actions: {
		/*
		*
		* */
		login({ commit, rootState, dispatch }, payload) {
			return new Promise((resolve, reject) => {
				this.$axios.post(`/login/`, payload)
					.then(({ data }) => {
						// commit('SET_CURRENT_USER', data)
						commit(SET_ENTITY, {
							module: "auth",
							entity: "userLoggedIn",
							value: true
						}, {root: true})

						saveToken(data.access, data.refresh)

						dispatch("fetchCurrentUser")
						.then(user => {
							resolve(user)
						})
					})
					.catch((error) => {
						reject(error)
					})
			})
		},
		/*
		*
		* */
		loginWithAD({ commit, rootState, dispatch }, payload) {
			return new Promise((resolve, reject) => {
				this.$axios.post(`/ldap_login/`, payload)
				.then(({ data }) => {
					commit(SET_ENTITY, {
						module: "auth",
						entity: "userLoggedIn",
						value: true
					}, {root: true})

					saveToken(data.access, data.refresh)

					dispatch("fetchCurrentUser")
					.then(user => {
						resolve(user)
					})
				})
				.catch((error) => {
					reject(error)
				})
			})
		},
		/*
		*
		* */
		logOut({ commit, rootGetters }) {
			return new Promise((resolve, reject) => {
				this.$axios.post(`/api/token/blacklist/`, {
					refresh: getRefreshToken()
				})
				.then(() => {
					// Закрываем соединение с сокетом
					if (rootGetters["socket/getChatSocket"]) {
						rootGetters["socket/getChatSocket"].socket.close()
					}
					rootGetters["socket/getUserHandShakeSocket"].socket.close()

					// commit('SET_CURRENT_USER', {})
					commit(SET_ENTITY, {
						module: "auth",
						entity: "userLoggedIn",
						value: false
					}, {root: true})

					removeToken()

					resolve()
				})
				.catch(() => {
					removeToken()
					reject()
				})
			})
		},

		/*
		* Get current user
		* */
		fetchCurrentUser({ commit, rootState }) {
			return new Promise((resolve) => {
				this.$axios.get("/user/my/profile/")
					.then(({ data }) => {
						commit('SET_CURRENT_USER', data)
						commit(SET_ENTITY, {
							module: "auth",
							entity: "userLoggedIn",
							value: true
						}, {root: true})

						resolve(data)
					})
			})
		},
		/*
		*
		* */
		fetchRefreshToken({ commit, dispatch }, payload) {
			return new Promise((resolve, reject) => {
				this.$axios.post(`/api/token/refresh/`, {
					refresh: getRefreshToken()
				})
				.then(({ data }) => {
					removeAccessToken()

					commit(SET_ENTITY, {
						module: "auth",
						entity: "userLoggedIn",
						value: true
					}, {root: true})

					if(data.hasOwnProperty("access")) {
						saveAccessToken(data.access)
					}

					dispatch("fetchCurrentUser")

					resolve()
				})
				.catch(() => {
					reject()
				})
			})
		},
		/*
		*
		* */
		fetchResetUserInfo({ commit }) {
			commit('SET_CURRENT_USER', {})
		}
	},
}
