import {SET_ENTITY} from "@/store/mutation-types";
import CrudService from "@/services/crud.service";
import { saveAs } from 'file-saver'
import { format, set } from "date-fns";

export default {
  namespaced: true,
  state: {
    isListLoading: false,
    assignment: null,
    createdResolutionList: null,
    isDocumentAssigned: {},
    headerFilters: {
      // Входящие
      priority: null,
      status: null,
      journal: null,
      doc_type: null,
      type: null,
      deadline_start: null,
      deadline_end: null,
      correspondent: null,
      controller: null,
      // На рассмотрение
      register_start_date: new Date().getFullYear() + '-' + (Number(new Date().getMonth()) + 1) + '-01',
      register_end_date: null,
      register_number: null,
      outgoing_start_date: null,
      outgoing_end_date: null,
      outgoing_number: null,
      approver: null,
      author: null,
      signer: null,
      department: null,
      condition: null,
      search: null,
      review_user: null,
      // Для Зам.пред
      is_for_signature: "True",
      is_verified: "False",
      __resolutionAssign: false,
      is_signed: null,
      compose__register_number: null,
      is_agreed: null,
      is_read: null,
      has_deadline: null
    },
    isHeaderFilterSaved: false,
    baseDocumentFilter: {
      condition: null,
      search: null,
    },
    correspondentList: [],
    docTypeList: [],
    docTypeCorrespondList: [],
    journalList: [],
    newJournalList: [
      {
        id: 1,
        name: "Входящий",
        router_link: "DocumentFlowIncomingRegistrationFormCreate",
        router_link_list: "IncomingDocumentTableListType",
        role: ["admin", "chancellery", "assistant", "chancellery_admin", "filial_chancellery", "call_center"],
        count: null,
      },
      {
        id: 3,
        name: "Внутренние",
        router_link: "DocumentFlowInnerRegistrationFormCreate",
        router_link_list: "DocumentFlowInnerDocumentsList",
        role: ["admin", "department_employee", "chancellery", "hr_manager", "filial_hr_manager", "assistant", "chancellery_admin", "filial_chancellery"],
        count: null,
      },
      {
        id: 4,
        name: "Исходящие",
        router_link: "DocumentFlowOutgoingRegistrationFormCreate",
        router_link_list: "DocumentFlowOutgoingDocumentsList",
        role: ["admin", "chancellery", "assistant", "chancellery_admin", "filial_chancellery"],
        count: null,
      },
      {
        id: 5,
        name: "Обращение",
        router_link: "DocumentFlowAppealRegistrationFormCreate",
        router_link_list: "DocumentFlowAppealsList",
        role: ["admin", "chancellery", "assistant", "chancellery_admin", "filial_chancellery", "call_center"],
        count: null,
      },
      {
        id: 6,
        name: "Приказы и распоряжения",
        router_link: "DocumentFlowOrdersProtocolsRegistrationFormCreate",
        router_link_list: "DocumentFlowOrdersProtocolsList",
        role: ["admin", "chancellery", "hr_manager", "filial_hr_manager", "assistant", "chancellery_admin", "filial_chancellery", "deputy_chair", "credit_committee"],
        count: null,
      },
      {
        id: 88,
        name: "Входящие от филиалов",
        router_link: "DocumentFlowIncomingRegistrationFormCreate",
        router_link_list: "DocumentFlowFilialDocumentsList",
        role: ["admin", "filial_chancellery", "chancellery", "assistant", "chancellery_admin"],
        count: null,
      },
      {
        id: 7,
        name: "Заявления",
        router_link: "DocumentFlowIncomingRegistrationFormCreate",
        router_link_list: "DocumentFlowApplicationsList",
        role: ["admin", "hr_manager", "filial_hr_manager"]
      },
      {
        id: 8,
        name: "XDFU",
        router_link: "DocumentFlowSecretDocumentsRegistrationFormCreate",
        router_link_list: "DocumentFlowSecretDocumentsList",
        role: ["admin", "xdfu_manager"]
      },
    ],
    applicantList: [
      {
        name: "Физическое лицо",
        value: "physics"
      },
      {
        name: "Юридическое лицо",
        value: "corporate"
      }
    ],
    incomingDocumentList: [],
    reviewList: [],
    reviewListToDo: [],
    reviewListInProgress: [],
    reviewListOnHold: [],
    reviewListDone: [],
    reviewUsersList: [],
    reviewGetOne: {},
    attachDocumentsList: [],
    attachDocumentListID: [],
    eventsList: [],
    usersBelowList: [],
    assignmentList: [],
    assignmentListToDo: [],
    departmentList: [],
    innerDocumentsList: [],
    applicationsList: [],
    outgoingDocumentsList: [],
    appealList: [],
    orderProtocolList: [],
    filialDocumentsList: [],
    assignmentGetOne: {},
    assignedUsersTreeList: [],
    assignmentUsersList: [],
    inboxList: [],
    inboxListToDo: [],
    inboxListInProgress: [],
    inboxListOnHold: [],
    inboxListDone: [],
    inboxListCreateBy: [],
    reviewGetOneInner: {},
    reviewGetOneOutgoing: {},
    reviewGetOneAppeal: {},
    reviewGetOneAppealCallCenter: {},
    reviewGetOneOrdersProtocols: {},
    reviewGetOneFilialDocument: {},
    reviewGetOneApplication: {},
    reviewGetOneSecretDocuments: {},
    documentResolution: {},
    approvalList: [],
    signsList: [],
    filialList: [],
    attachedDocuments: [],
    executionStatisticsListIncoming: [],
    executionStatisticsListReview: [],
    currentUserInTree: null,
    composeModel: {},

    // Loaders
    userTreeLoading: true,

    // Mock-states
    boxesDropdownList: [
      // {
      //   title: "На согласование",
      //   value: "approval",
      //   link: "BoxesApproval",
      //   count: null
      // },
      {
        title: "На рассмотрение",
        value: "review",
        link: "BoxesReview",
        count: null
      },
      // {
      //   title: "Поручения",
      //   value: "assignment",
      //   link: "BoxesAssignment"
      // },
      // {
      //   title: "Контрольные пункты",
      //   value: "control_point",
      //   link: "BoxesControlPoint"
      // },
      // {
      //   title: "Для сведения",
      //   value: "for_information",
      //   link: "BoxesForInformation"
      // },
      {
        title: "Входящие",
        value: "inbox",
        link: "InboxTableType",
        count: null
      },
      // {
      //   title: "Внутренние",
      //   value: "internal",
      //   link: "InternalTableType"
      // },
      // {
      //   title: "Приказы и распоряжения, Протоколы",
      //   value: "order_and_instructions",
      //   link: "OrderTableType"
      // }
    ],
    documentsTypeList: [
      {
        name: "Поручение",
        value: "assignment"
      },
      {
        name: "Контрольный пункт",
        value: "control_point"
      }
    ],
    boxDropdownSelected: "review",
    defaultSignersList: [
      // {
      //   user: 5325,
      //   is_invited: false,
      //   full_name: "Нодирбек Сайдуллаев Нарзуллаевич"
      // },
      {
        user: 5326,
        is_invited: false,
        full_name: "Закиров Суръат Уткурович"
      },
      {
        user: 5324,
        is_invited: false,
        full_name: "Абдуллаев Бобохон Рустамович"
      },
      {
        user: 5323,
        is_invited: false,
        full_name: "Шарипов Бахромходжа Шухратович"
      },
      {
        user: 5322,
        is_invited: false,
        full_name: "Мамаджанов Бунёд Бахтиёрович"
      },
      {
        user: 5398,
        is_invited: false,
        full_name: "Талибджанов Саъдулла Зиёдуллаевич"
      },
      {
        user: 5646,
        is_invited: false,
        full_name: "Умаров Сардорбек Мамашарифович"
      },
      {
        user: 5480,
        is_invited: false,
        full_name: "Давлатов Алишер Сапарович"
      },
      {
        user: 5327,
        is_invited: false,
        full_name: "Пулатов Навфал Хайдарович"
      },
      // {
      //   user: 5582,
      //   is_invited: false,
      //   full_name: "Алишер Асатуллаев Асатулла ўғли"
      // },
      {
        user: 6090,
        is_invited: false,
        full_name: "Бекназаров Аброр Мухиддинович"
      },
      // {
      //   user: 6081,
      //   is_invited: false,
      //   full_name: "Бегматов Абдулатиф Абдумуталович"
      // }
    ],
    searchedDocumentList: [],
    activeComment: null,

    // SendDocument
    leadershipRequestModel: {
      type: null,
      register_number: null,
      register_date: null,
      short_description: null,
      text: null,
      files: [],
      journal_id: 3,
      signed_by: null,
      agreed_by: null,
      signer_obj: null,
      sender: null,
      evaluation: null,
      goal: null,
      is_for_payment: false,
      sub_type: null,
      employees: [],
      __leadership: null,
      __agreedList: [],
      __signersList: [],
      __files: [],
      __assistant: [],
      __forwardDocumentSelect: "head_office",
      __status: "draft",
      __isDraftCreated: false,
      priority: 'low',
      __staffHead: null
    },
    // Rotation create (JSON field)
    rotationRequestModel: {
      text: `
        <table style="width: 100%; border-collapse: collapse" border="1">
          <tbody>
            <tr>
              <td style="width: 49.1678%; padding: 4px"><strong>So'rovnoma sanasi / Raqami</strong></td>
              <td style="width: 49.1678%; padding: 4px">&nbsp;</td>
            </tr>
            <tr>
              <td style="width: 49.1678%; padding: 4px"><strong>Departament/Filial</strong></td>
              <td style="width: 49.1678%; padding: 4px">&nbsp;</td>
            </tr>
            <tr>
              <td style="width: 49.1678%; padding: 4px"><strong>Rotatsiya turi</strong></td>
              <td style="width: 49.1678%; padding: 4px">&nbsp;</td>
            </tr>
            <tr>
              <td style="width: 49.1678%; padding: 4px"><strong>Nomzodning F.I.SH</strong></td>
              <td style="width: 49.1678%; padding: 4px">&nbsp;</td>
            </tr>
            <tr>
              <td style="width: 49.1678%; padding: 4px"><strong>Amaldagi Departamant/Bo'lim</strong></td>
              <td style="width: 49.1678%; padding: 4px">&nbsp;</td>
            </tr>
            <tr>
              <td style="width: 49.1678%; padding: 4px"><strong>Egallab turgan lavozimi</strong></td>
              <td style="width: 49.1678%; padding: 4px">&nbsp;</td>
            </tr>
            <tr>
              <td style="width: 49.1678%; padding: 4px"><strong>Egallab turgan lavozim muddati</strong></td>
              <td style="width: 49.1678%; padding: 4px">&nbsp;</td>
            </tr>
            <tr>
              <td style="width: 49.1678%; padding: 4px"><strong>Taklif etilayotgan Departament/Bo'lim</strong></td>
              <td style="width: 49.1678%; padding: 4px">&nbsp;</td>
            </tr>
            <tr>
              <td style="width: 49.1678%; padding: 4px"><strong>Rotatsiyadan keyingi lavozim</strong></td>
              <td style="width: 49.1678%; padding: 4px">&nbsp;</td>
            </tr>
            <tr>
              <td style="width: 49.1678%; padding: 4px"><strong>Shtat birligi</strong></td>
              <td style="width: 49.1678%; padding: 4px">&nbsp;</td>
            </tr>
            <tr>
              <td style="width: 49.1678%; padding: 4px"><strong>Rotatsiya sababi: Asos:</strong></td>
              <td style="width: 49.1678%; padding: 4px">&nbsp;</td>
            </tr>
          </tbody>
        </table>

        <p>Qo'shimcha xabar ...</p>
      `,
      rotation_signers: {
        type: {
          title: "Вертикальный",
          value: "vertical"
        },
        executive: [],
        invited: []
      }
    }
  },
  /*
  *
  * */
  getters: {
    getCreatedResolutionList: (state) => state.createdResolutionList,
    getIsDocumentAssigned: (state) => state.isDocumentAssigned,
    getHeaderFilters: (state) => state.headerFilters,
    getIsHeaderFilterSaved: (state) => state.isHeaderFilterSaved,
    getBaseDocumentFilter: (state) => state.baseDocumentFilter,
    getCorrespondent: (state) => state.correspondentList,
    getDocTypeList: (state) => state.docTypeList,
    getDocTypeCorrespondList: (state) => state.docTypeCorrespondList,
    getJournalList: (state) => state.journalList,
    getNewJournalList: (state) => state.newJournalList,
    getApplicantList: (state) => state.applicantList,
    getIncomingDocumentList: (state) => state.incomingDocumentList,
    getAppealList: (state) => state.appealList,
    getOrderProtocolList: (state) => state.orderProtocolList,
    getInnerDocumentsList: (state) => state.innerDocumentsList,
    getFilialDocumentsList: (state) => state.filialDocumentsList,
    getOutgoingDocumentsList: (state) => state.outgoingDocumentsList,
    getIncomingStatusTodo: (state) => state.incomingDocumentList.filter(item => item.status.name === "TO DO"),
    getIncomingStatusInProgress: (state) => state.incomingDocumentList.filter(item => item.status.name === "IN PROGRESS"),
    getIncomingStatusOnHold: (state) => state.incomingDocumentList.filter(item => item.status.name === "ON HOLD"),
    getIncomingStatusDone: (state) => state.incomingDocumentList.filter(item => item.status.name === "DONE"),
    getReviewList: (state) => state.reviewList,
    getReviewListToDo: (state) => state.reviewListToDo,
    getReviewListInProgress: (state) => state.reviewListInProgress,
    getReviewListOnHold: (state) => state.reviewListOnHold,
    getReviewListDone: (state) => state.reviewListDone,
    getReviewUsersList: (state) => state.reviewUsersList,
    getReviewGetOne: (state) => state.reviewGetOne,
    getAttachDocumentsList: (state) => state.attachDocumentsList,
    getAttachDocumentListID: (state) => state.attachDocumentListID,
    getEventsList: (state) => state.eventsList,
    getUsersBelowList: (state) => state.usersBelowList,
    getAssignmentList: (state) => state.assignmentList,
    getAssignmentListToDo: (state) => state.assignmentListToDo,
    getAssignmentGetOne: (state) => state.assignmentGetOne,
    getAssignmentStatusTodo: (state) => state.assignmentList.filter(item => item.__status.title === "TO DO"),
    getAssignmentStatusInProgress: (state) => state.assignmentList.filter(item => item.__status.title === "IN PROGRESS"),
    getAssignmentStatusOnHold: (state) => state.assignmentList.filter(item => item.__status.title === "ON HOLD"),
    getAssignmentStatusDone: (state) => state.assignmentList.filter(item => item.__status.title === "DONE"),
    getAssignedUsersTreeList: (state) => state.assignedUsersTreeList,
    getAssignmentUsersList: (state) => state.assignmentUsersList,
    getDepartmentList: (state) => state.departmentList,
    getInboxList: (state) => state.inboxList,
    getInboxListToDo: (state) => state.inboxListToDo,
    getInboxListInProgress: (state) => state.inboxListInProgress,
    getInboxListOnHold: (state) => state.inboxListOnHold,
    getInboxListDone: (state) => state.inboxListDone,
    getInboxListCreateBy: (state) => state.inboxListCreateBy,
    getReviewGetOneInner: (state) => state.reviewGetOneInner,
    getReviewGetOneOutgoing: (state) => state.reviewGetOneOutgoing,
    getReviewGetOneAppeal: (state) => state.reviewGetOneAppeal,
    getReviewGetOneAppealCallCenter: (state) => state.reviewGetOneAppealCallCenter,
    getReviewGetOneOrdersProtocols: (state) => state.reviewGetOneOrdersProtocols,
    getReviewGetOneSecretDocuments: (state) => state.reviewGetOneSecretDocuments,
    getDocumentResolution: (state) => state.documentResolution,
    getApprovalList: (state) => state.approvalList,
    getSignsList: (state) => state.signsList,
    getReviewGetOneFilialDocument: (state) => state.reviewGetOneFilialDocument,
    getReviewGetOneApplication: (state) => state.reviewGetOneApplication,
    getFilialList: (state) => state.filialList,
    getAttachedDocuments: (state) => state.attachedDocuments,
    getSearchedDocumentList: (state) => state.searchedDocumentList,
    getExecutionStatisticsListIncoming: (state) => state.executionStatisticsListIncoming,
    getExecutionStatisticsListReview: (state) => state.executionStatisticsListReview,
    getCurrentUserInTree: (state) => state.currentUserInTree,
    getComposeModel: (state) => state.composeModel,

    // Loaders
    getUserTreeLoading: (state) => state.userTreeLoading,

    // Mock-state getters
    getBoxDropdownList: (state) => state.boxesDropdownList,
    getBoxDropdownSelected: (state) => state.boxDropdownSelected,
    getDocumentsTypeList: (state) => state.documentsTypeList,
    getDefaultSignersList: (state) => state.defaultSignersList,
    getIsListLoading: (state) => state.isListLoading,
    getActiveComment: (state) => state.activeComment,
    getApplicationsList: (state) => state.applicationsList,
  },
  /*
  *
  * */
  mutations: {
    /**/
    "SET_ACTIVE_COMMENT"(state, payload){
      state.activeComment = payload
    },
    /*
    *
    * */
    "UPDATE_INCOMING_DOCUMENTS"(state, payload) {
      let index = state.incomingDocumentList.findIndex(item => item.id === payload.id)

      state.incomingDocumentList.splice(index, 1)

      state.incomingDocumentList.push(payload)
    },
    /*
    *
    * */
    "SET_ATTACH_DOCUMENT_LIST_ID"(state, payload) {
      if (payload.incoming_doc !== undefined){
        state.attachDocumentListID.push({
          document: payload.document,
          incoming_doc: Number(payload.incoming_doc)
        })
      }else {
        state.attachDocumentListID.push({
          document: payload.document,
        })
      }
    },
    "CLEAR_ATTACHED_DOCUMENTS_ID"(state){
      state.attachDocumentListID = []
      state.attachedDocuments = []
    },
    /*
    *
    * */
    "ADD_BLOCK"(state, payload) {
      state.inboxList.push(payload)
    },
    /*
    *
    * */
    "PUSH_LIST"(state, {entity, value}) {
      state[entity].push(...value)
    },
    /*
    *
    * */
    "UPDATE_STATUS_DOCUMENT_LIST"(state, payload) {
      let index = state.assignmentList.findIndex(item => item.id === payload.id)

      state.assignmentList.splice(index, 1)

      state.assignmentList.push(payload)
    },
    /*
    *
    * */
    "UPDATE_STATUS"(state, {fromState, toState, value}) {
      let index = fromState.findIndex(item => item.id === value.id)

      fromState.splice(index, 1)
      state[toState].push(value)
    },
    /*
    *
    * */
    "CLEAR_DOCUMENT_LIST"(state) {
      state.reviewListToDo = []
      state.reviewListInProgress = []
      state.reviewListOnHold = []
      state.reviewListDone = []

      state.inboxListToDo = []
      state.inboxListInProgress = []
      state.inboxListOnHold = []
      state.inboxListDone = []
    },
    /*
    *
    * */
    "CLEAR_DOCUMENT_ASSIGN"(state) {
      state.documentResolution = {}
      state.isDocumentAssigned = {}
      state.createdResolutionList = null
    },
    /*
    *
    * */
    "CLEAR_HEADER_FILTER"(state) {
      for (let key in state.headerFilters) {
        if(key === 'is_for_signature' || key === 'is_verified' || key === '__resolutionAssign') {
          continue
        }

        state.headerFilters[key] = null
      }
    },
    /**/
    "MERGE_ATTACHED_DOCUMENTS"(state, payload){
      state.attachedDocuments.push(payload)
    },
    /*
    *
    * */
    "SET_ENTITIES"(state, payload) {
      payload.forEach(item => {
        const [key, value] = Object.entries(item).flat(1)

        state.leadershipRequestModel[key] = value
      })
    },
    /**/
    "SET_ASSIGNMENT"(state, payload) {
      state.reviewGetOne = payload
    },
    /**/
    "SET_ASSIGNMENT_INNER"(state, payload) {
      state.reviewGetOneInner = payload
    },
    /**/
    "SET_ASSIGNMENT_OUTGOING"(state, payload) {
      state.reviewGetOneOutgoing = payload
    },
    /**/
    "SET_ASSIGNMENT_APPEAL"(state, payload) {
      state.reviewGetOneAppeal = payload
    },
    /**/
    "SET_ASSIGNMENT_ORDER_PROTOCOL"(state, payload) {
      state.reviewGetOneOrdersProtocols = payload
    },
    /**/
    "SET_ASSIGNMENT_APPLICATION"(state, payload) {
      state.reviewGetOneApplication = payload
    },
    /**/
    "SET_ASSIGNMENT_SECRET_DOCUMENTS"(state, payload) {
      state.reviewGetOneSecretDocuments = payload
    },
    /*
    *
    * */
    "CLEAR_LEADERSHIP_STATE"(state) {
      state.leadershipRequestModel = {
        type: null,
        register_number: null,
        register_date: null,
        short_description: null,
        text: null,
        goal: null,
        files: [],
        journal_id: 3,
        signed_by: null,
        signer_obj: null,
        sender: null,
        is_for_payment: false,
        __leadership: null,
        __agreedList: [],
        __signersList: [],
        __files: [],
        __forwardDocumentSelect: "head_office"
      }
    },
    /**/
    "SET_COMPOSE_MODEL"(state, payload){
      state.composeModel = payload
    }
  },
  /*
  *
  * */
  actions: {
    /*
    * Инициализация начальных запросов
    * */
    init({dispatch}) {
      dispatch("fetchCorrespondentList", {})
      dispatch("fetchDocTypeList", {})
    },
    /*
    *
    * */
    fetchSetCurrentUserInTree({ commit, rootState }, payload) {
      if(rootState.auth.currentUser.id === payload.user.id) {
        commit(SET_ENTITY, {
          module: "documentFlowModule",
          entity: "currentUserInTree",
          value: payload
        }, {root: true})
      }
    },
    /*
    *
    * */
    fetchDepartmentList({commit, rootState}) {
      setTimeout(() => {
        this.$axios.get(`/departments/department_without_children/`, {
          params: {
            filial: rootState.auth.currentUser.filial,
            condition: "A",
            page_size: 50
          }
        })
          .then(({data}) => {
            commit(SET_ENTITY, {
              module: "documentFlowModule",
              entity: "departmentList",
              value: data.results
            }, {root: true})
          })
      }, 200)
    },
    /*
    * Получать лист корреспондентов
    * */
    fetchCorrespondentList({commit, rootState}, {search}) {
      CrudService.getList(`/action/correspondent/`, {search})
        .then((data) => {
          commit(SET_ENTITY, {
            module: "documentFlowModule",
            entity: "correspondentList",
            value: data.results
          }, {root: true})
        })
    },
    /*
    *
    * */
    fetchCorrespondentById({commit, rootState}, payload) {
      return new Promise((resolve, reject) => {
        CrudService.getList(`/action/correspondent/${payload.id}`)
        .then((data) => {
          resolve(data)
        })
      })
    },
    /*
    *
    * */
    fetchDocTypeList({commit, rootState}, {search}) {
      CrudService.getList(`/docflow/${rootState.auth.currentUser.filial}/doc_type/`, {search})
        .then((data) => {
          commit(SET_ENTITY, {
            module: "documentFlowModule",
            entity: "docTypeList",
            value: data.results
          }, {root: true})
        })
    },
    /**/
    fetchDocTypeCorrespondList({commit, rootState}, payload) {
      commit(SET_ENTITY, { entity: "isLoading", value: true }, { root: true })
      CrudService.getList(`/docflow/${rootState.auth.currentUser.filial}/reference/`, {...payload})
        .then((data) => {
          commit(SET_ENTITY, {
            module: "documentFlowModule",
            entity: "docTypeCorrespondList",
            value: data.results
          }, {root: true});

          commit(SET_ENTITY, { entity: "isLoading", value: false }, { root: true })
        })
    },
    /*
    *
    * */
    fetchDocTypeCorrespondListById({ commit, rootState }, payload) {
      return new Promise((resolve, reject) => {
        CrudService.getList(`/docflow/${rootState.auth.currentUser.filial}/reference/${payload.id}/`, {
          ...payload.params
        })
        .then((data) => {
          resolve(data)
        })
      })
    },
    /*
    * Получать лист журналов
    * */
    fetchJournalList({commit, rootState}) {
      CrudService.getList(`/docflow/${rootState.auth.currentUser.filial}/journal/`)
        .then((data) => {
          commit(SET_ENTITY, {
            module: "documentFlowModule",
            entity: "journalList",
            value: data.results
          }, {root: true})
        })
    },
    /*
    * Регистрация входящего документа
    * */
    fetchRegisterIncomingDocument({commit, rootState}, payload) {
      return new Promise((resolve, reject) => {
        this.$axios.post(`/docflow/${rootState.auth.currentUser.filial}/incoming/`, payload)
          .then(({data}) => {
            resolve(data)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    /** **/
    fetchRegisterSecretDocuments({commit, rootState}, payload) {
      return new Promise((resolve, reject) => {
        this.$axios.post(`/docflow/${rootState.auth.currentUser.filial}/secret-documents/`, payload)
          .then(({data}) => {
            resolve(data)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    /*
    *
    * */
    fetchRegisterInnerDocument({commit, rootState}, payload) {
      return new Promise((resolve) => {
        this.$axios.post(`/docflow/${rootState.auth.currentUser.filial}/inner_doc/`, payload)
          .then(({data}) => {
            resolve(data)
          })
      })
    },
    /**/
    fetchRegisterOutgoingDocument({commit, rootState}, payload) {
      return new Promise((resolve) => {
        this.$axios.post(`/docflow/${rootState.auth.currentUser.filial}/outgoing/`, payload)
          .then(({data}) => {
            resolve(data)
          })
      })
    },
    /**/
    fetchRegisterAppeals({commit, rootState}, payload) {
      return new Promise((resolve) => {
        this.$axios.post(`/docflow/${rootState.auth.currentUser.filial}/appeals/`, payload)
          .then(({data}) => {
            resolve(data)
            console.log(data)
          })
      })
    },
    /*
    * Получить лист входящего документа
    * */
    fetchIncomingDocumentList({commit, rootState, rootGetters, dispatch}, payload) {
      console.log("PAYLOAD", payload);
      commit(SET_ENTITY, { module: "documentFlowModule", entity: "isListLoading", value: true }, { root: true })
      CrudService.getList(`/docflow/${rootState.auth.currentUser.filial}/incoming/`, {
        ...payload,
        page_size: rootState.paginationOptions.page_size,
        journal: 1

      })
        .then((data) => {
          let model = data.results.map(item => Object.assign(item, {
            __priorityText: rootState.priorityList.find(priority => priority.value === item.priority).title,
            __status: item.status ? rootState.statusList.find(status => status.title === item.status.name) : {}
          }))

          commit(SET_ENTITY, {
            module: "documentFlowModule",
            entity: "incomingDocumentList",
            value: model
          }, {root: true})

          /*
          * Заполняем массив с нужными полями
          * Это нужно для просмотра входящих документов в календарном виде
          * */
          commit(SET_ENTITY,
            {
              module: "documentFlowModule",
              entity: "eventsList",
              value: model.map(item => {
                return {
                  ...item,
                  name: `${item.correspondent.name}`,
                  start: new Date(item.register_date).toISOString().replace(/T.*$/, ''),
                  color: item.priority === "high" ? "var(--secondary-4)" : item.priority === "critical" ? "var(--secondary-4)" : item.priority === "medium" ? "var(--secondary-3)" : "var(--secondary-2)",
                  textColor: item.priority === "high" ? "var(--primary-4)" : item.priority === "critical" ? "var(--primary-4)" : item.priority === "medium" ? "var(--primary-3)" : "var(--primary-2)",
                }
              })
            },
            {root: true}
          )
          dispatch("fetchPagination", data.count, {root: true})
          commit(SET_ENTITY, { module: "documentFlowModule", entity: "isListLoading", value: false }, { root: true })
        })
    },
    /**/
    fetchInnerDocumentList({commit, dispatch, rootState, rootGetters}, payload) {
      commit(SET_ENTITY, { module: "documentFlowModule", entity: "isListLoading", value: true }, { root: true })
      CrudService.getList(`/docflow/${rootState.auth.currentUser.filial}/inner_doc/`, {
        ...payload,
        journal: 3,
        page_size: rootState.paginationOptions.page_size,
      })
        .then((data) => {
          commit(SET_ENTITY, {
            module: "documentFlowModule",
            entity: "innerDocumentsList",
            value: data.results
          }, {root: true});

          dispatch("fetchPagination", data.count, {root: true})
          commit(SET_ENTITY, { module: "documentFlowModule", entity: "isListLoading", value: false }, { root: true })
        })
    },
    /**/
    fetchOutgoingDocumentList({commit, dispatch, rootState, rootGetters}, payload) {
      commit(SET_ENTITY, { module: "documentFlowModule", entity: "isListLoading", value: true }, { root: true })
      CrudService.getList(`/docflow/${rootState.auth.currentUser.filial}/outgoing/`, {
        ...payload,
        page_size: rootState.paginationOptions.page_size,
        journal: 4
      })
        .then((data) => {
          commit(SET_ENTITY, {
            module: "documentFlowModule",
            entity: "outgoingDocumentsList",
            value: data.results
          }, {root: true});

          dispatch("fetchPagination", data.count, {root: true})
          commit(SET_ENTITY, { module: "documentFlowModule", entity: "isListLoading", value: false }, { root: true })
        })
    },

    /**/
    fetchAppealList({commit, dispatch, rootState, rootGetters}, payload) {
      commit(SET_ENTITY, { module: "documentFlowModule", entity: "isListLoading", value: true }, { root: true })

      CrudService.getList(`/docflow/${rootState.auth.currentUser.filial}/appeals/`, {
        ...payload,
        page_size: rootState.paginationOptions.page_size,
        journal: 5
      })
        .then((data) => {
          commit(SET_ENTITY, {
            module: "documentFlowModule",
            entity: "appealList",
            value: data.results
          }, {root: true});

          dispatch("fetchPagination", data.count, {root: true})
          commit(SET_ENTITY, { module: "documentFlowModule", entity: "isListLoading", value: false }, { root: true })
        })
    },
    /*
    * Изменить статус входящего документа
    * */
    fetchChangeStatusIncomingDocument({commit, rootState, dispatch}, {
      id,
      item,
      ref,
      statusUrl,
      documentId,
      columnState
    }) {
      let model = null
      commit(SET_ENTITY, {entity: "isLoading", value: true}, {root: true})

      // console.log(id, item, ref, statusUrl, documentId)

      switch (statusUrl) {
        case "incoming":
          model = {
            ...item,
            doc_type: item.doc_type.id,
            journal: item.journal.id,
            status: id,
            review_users: undefined,
            correspondent: item.correspondent.id
          }

          this.$axios.put(`/docflow/${rootState.auth.currentUser.filial}/incoming/${item.id}/`, model)
            .then(({data}) => {
              // dispatch("fetchIncomingDocumentList")

              // Скрываем ивент диалог
              dispatch("gridCalendar/toggleEventDescriptionModal", false, {root: true})
              // Очистим поле селект
              ref.reset()
            })
          break;
        case "assignment":
          model = {
            status: id,
            assignment: item.assignment.find(i => i.user.id === rootState.auth.currentUser.id).id
          }

          this.$axios.put(`/docflow/${rootState.auth.currentUser.filial}/assigment_users/${model.assignment}/`, model)
            .then(({data}) => {
              let model = {
                ...data,
                __priorityText: rootState.priorityList.find(priority => priority.value === data.document.priority).title,
                __status: rootState.statusList[data.assignment.find(i => i.user.id === rootState.auth.currentUser.id).status.id - 1],
              }

              console.log(model);

              commit("UPDATE_STATUS_DOCUMENT_LIST", model)
              commit(SET_ENTITY, {entity: "isLoading", value: false}, {root: true})
            })
          break;
        case "review":
          this.$axios.put(`/docflow/${rootState.auth.currentUser.filial}/user_reviews/${documentId}/`, {
            status: id
          })
            .then(({data}) => {
              let model = {
                ...data,
                review_users: data.review_users.map(user => ({user: {...user}})),
                __priorityText: rootState.priorityList.find(priority => priority.value === data.incoming_doc.priority).title,
                __status: rootState.statusList.find(status => status.title === data.status.name)
              }

              switch (id) {
                case 1:
                  commit("UPDATE_STATUS", {
                    fromState: columnState,
                    toState: "reviewListToDo",
                    value: model
                  })
                  console.log('to do');
                  break;
                case 2:
                  commit("UPDATE_STATUS", {
                    fromState: columnState,
                    toState: "reviewListInProgress",
                    value: model
                  })
                  console.log('in progress');
                  break;
                case 3:
                  commit("UPDATE_STATUS", {
                    fromState: columnState,
                    toState: "reviewListOnHold",
                    value: model
                  })
                  console.log('on hold');
                  break;
                default:
                  commit("UPDATE_STATUS", {
                    fromState: columnState,
                    toState: "reviewListDone",
                    value: model
                  })
                  console.log('done');
              }

              commit(SET_ENTITY, {entity: "isLoading", value: false}, {root: true})
            })
          console.log('review')
          break;
        case "inbox":
          model = {
            status: id,
            assignment: item.item.assignment.find(i => i.user.id === rootState.auth.currentUser.id).id
          }

          this.$axios.put(`/docflow/${rootState.auth.currentUser.filial}/assigment_users/${model.assignment}/`, model)
            .then(({data}) => {
              let model = {
                ...data,
                __priorityText: rootState.priorityList.find(priority => priority.value === data.document.priority).title,
                __status: rootState.statusList[data.assignment.find(i => i.user.id === rootState.auth.currentUser.id).status.id - 1],
              }

              switch (id) {
                case 1:
                  commit("UPDATE_STATUS", {
                    fromState: columnState,
                    toState: "inboxListToDo",
                    value: model
                  })
                  break;
                case 2:
                  commit("UPDATE_STATUS", {
                    fromState: columnState,
                    toState: "inboxListInProgress",
                    value: model
                  })
                  break;
                case 3:
                  commit("UPDATE_STATUS", {
                    fromState: columnState,
                    toState: "inboxListOnHold",
                    value: model
                  })
                  break;
                default:
                  commit("UPDATE_STATUS", {
                    fromState: columnState,
                    toState: "inboxListDone",
                    value: model
                  })
              }

              commit(SET_ENTITY, {entity: "isLoading", value: false}, {root: true})
            })
          break;
        default:
          console.log('def')
      }
    },
    /*
    * Обновить лист входящего документа
    * после изменение статуса на странице "Ящики" в GridListType.vue
    * */
    fetchIncomingDocumentUpdate({commit, rootState}, payload) {
      return new Promise((resolve, reject) => {
        let model = {
          ...payload,
          correspondent: payload.correspondent.id,
          doc_type: payload.doc_type.id,
          journal: payload.journal.id,
          status: payload.status.id,
          register_date: `${payload.register_date} ${new Date().getHours()}:${new Date().getMinutes()}`,
          outgoing_date: `${payload.outgoing_date} ${new Date().getHours()}:${new Date().getMinutes()}`,
          deadline: payload.deadline ? `${payload.deadline}T18:00:00+05:00` : null,
          review_users: [],
          documents: []
        }

        this.$axios.put(`/docflow/${rootState.auth.currentUser.filial}/incoming/${payload.id}/`, model)
          .then(({ data }) => {
            resolve(data)
          })
      })
      // let model = {
      //   ...content,
      //   __priorityText: rootState.priorityList.find(priority => priority.value === content.priority).title,
      //   __status: rootState.statusList.find(status => status.title === content.status.name)
      // }
      //
      // commit("UPDATE_INCOMING_DOCUMENTS", model)
      // commit(SET_ENTITY, {entity: "isLoading", value: false}, {root: true})
    },
    fetchSecretDocumentsUpdate({commit, rootState}, payload) {
      return new Promise((resolve, reject) => {
        let model = {
          ...payload,
          correspondent: payload.correspondent.id,
          doc_type: payload.doc_type.id,
          journal: payload.journal.id,
          status: payload.status.id,
          register_date: `${payload.register_date} ${new Date().getHours()}:${new Date().getMinutes()}`,
          outgoing_date: `${payload.outgoing_date} ${new Date().getHours()}:${new Date().getMinutes()}`,
          deadline: payload.deadline ? `${payload.deadline}T18:00:00+05:00` : null,
          review_users: [],
          documents: []
        }

        this.$axios.put(`/docflow/${rootState.auth.currentUser.filial}/secret-documents/${payload.id}/`, model)
          .then(({ data }) => {
            resolve(data)
          })
      })
    },
    /*
    * Получить входящий регистрационный документ по id
    * */
    fetchIncomingDocumentById({commit, rootState, dispatch}, payload) {
      commit("SET_DETAIL_LOADER", true, { root: true });
      CrudService.getList(`/docflow/${rootState.auth.currentUser.filial}/incoming/${payload}/`)
        .then((data) => {
          let model = {
            ...data,
            __userSelected: data.review_users && data.review_users.map(item => item.user.full_name),
            __currentReviewUser:
              data.review_users &&
              data.review_users.find(
                user => user.user.id === rootState.auth.currentUser.id ||
                  user.user.assistants && user.user.assistants.length &&
                  user.user.assistants.includes(rootState.auth.currentUser.id)
              ),
            register_date: new Date(data.register_date).toISOString().replace(/T.*$/, ''),
            outgoing_date: new Date(data.outgoing_date).toISOString().replace(/T.*$/, ''),
            deadline: data.deadline ? new Date(data.deadline).toISOString().replace(/T.*$/, '') : null,
            __deadline: data.deadline ? new Date(data.deadline) : null,
            send_date: data.register_date,
            delivery_type: data.delivery_type ? data.delivery_type.id : null,
            __delivery_type: data.delivery_type
          }

          commit(SET_ENTITY, {
            module: "documentFlowModule",
            entity: "reviewGetOne",
            value: model
          }, {root: true});

          dispatch(
            "treeView/fetchTreeUsersList",
            { treeId: data.id },
            { root: true }
          );

          if (data && data.review_users && data.review_users.length){
            dispatch("documentFlowModule/fetchAssignmentList", { id: data.review_users[0].id }, { root: true })
          }

          commit("SET_DETAIL_LOADER", false, { root: true });
        })
    },
    /** **/
    fetchSecretDocumentsById({commit, rootState, dispatch}, payload) {
      commit("SET_DETAIL_LOADER", true, { root: true });
      CrudService.getList(`/docflow/${rootState.auth.currentUser.filial}/secret-documents/${payload}/`)
        .then((data) => {
          let model = {
            ...data,
            __userSelected: data.review_users && data.review_users.map(item => item.user.full_name),
            __currentReviewUser:
              data.review_users &&
              data.review_users.find(
                user => user.user.id === rootState.auth.currentUser.id ||
                  user.user.assistants && user.user.assistants.length &&
                  user.user.assistants.includes(rootState.auth.currentUser.id)
              ),
            register_date: new Date(data.register_date).toISOString().replace(/T.*$/, ''),
            outgoing_date: new Date(data.outgoing_date).toISOString().replace(/T.*$/, ''),
            deadline: data.deadline ? new Date(data.deadline).toISOString().replace(/T.*$/, '') : null,
            __deadline: data.deadline ? new Date(data.deadline) : null,
            send_date: data.register_date,
            delivery_type: data.delivery_type ? data.delivery_type.id : null,
            __delivery_type: data.delivery_type
          }

          commit(SET_ENTITY, {
            module: "documentFlowModule",
            entity: "reviewGetOneSecretDocuments",
            value: model
          }, {root: true});

          dispatch(
            "treeView/fetchTreeUsersList",
            { treeId: data.id },
            { root: true }
          );

          if (data && data.review_users && data.review_users.length){
            dispatch("documentFlowModule/fetchAssignmentList", { id: data.review_users[0].id }, { root: true })
          }

          commit("SET_DETAIL_LOADER", false, { root: true });
        })
    },
    /*
    * Получить входящий регистрационный документ по id м по типу (Обращение => type = 5)
    * */
    fetchBoxDocumentDetails({commit, rootState}, payload) {
      CrudService.getList(`/docflow/${rootState.auth.currentUser.filial}/document_detail/${payload}/`)
        .then((data) => {
          let model = {
            ...data,
            __userSelected: data.review_users ? data.review_users.map(item => item.user.full_name) : [],
            __currentReviewUser: data.review_users && data.review_users.find(user => user.user.id === rootState.auth.currentUser.id || user.user.assistants && user.user.assistants.includes(rootState.auth.currentUser.id)),
            register_date: data.register_date ? new Date(data.register_date).toISOString().replace(/T.*$/, '') : "",
            outgoing_date: data.outgoing_date ? new Date(data.outgoing_date).toISOString().replace(/T.*$/, '')  : "",
            description: data.text ? data.text : ""
          }

          commit(SET_ENTITY, {
            module: "documentFlowModule",
            entity: "reviewGetOne",
            value: model
          }, {root: true})
        })
    },
    /*
    * Получить список review пользователей на
    * входящий регистрационный документ по id
    * */
    fetchReviewUsersList({commit, rootState}, payload) {
      CrudService.getList(`/docflow/${rootState.auth.currentUser.filial}/for_review/`, {
        incoming_doc: payload,
        page_size: 30
      })
        .then((data) => {
          commit(SET_ENTITY, {
            module: "documentFlowModule",
            entity: "reviewUsersList",
            value: data.results
          }, {root: true})
        })
    },
    /*
    *
    * */
    fetchChangeReviewUser({ commit, rootState }, payload) {
      return new Promise((resolve, reject) => {
        this.$axios.put(`/docflow/${rootState.auth.currentUser.filial}/for_review/${payload.id}/change_review_user/`, {
          user: payload.userId
        })
          .then(() => {
            resolve()
          })
      })
    },
    /*
    * Добавить в список нового review пользователя на
    * входящий регистрационный документ по id
    * */
    fetchSetNewReviewUser({commit, rootState, dispatch}, {model, userId, isController, incomingDocId, isSecret}) {
      CrudService.post(`/docflow/${rootState.auth.currentUser.filial}/for_review/`, isController ? {
        user: userId,
        is_controller: isController ? isController : null,
        incoming_doc: incomingDocId
      } : isSecret ? {
          user: userId,
          incoming_doc: incomingDocId,
          is_secret: true
        } :
        {
        user: userId,
        incoming_doc: incomingDocId
      })
        .then(() => {
          dispatch("fetchReviewUsersList", incomingDocId)
          dispatch("fetchAssignedUsersTree", {treeId: model.id})
          setTimeout(() => {
            dispatch("action/fetchActivitiesList", {obj: model.document_unique_id}, {root: true})
          }, 3000)
        })
    },
    /*
    * Удалить review пользователя из списка
    * */
    fetchDeleteReviewUserFromList({commit, rootState, dispatch}, {model, userId, incomingDocId}) {
      return new Promise((resolve) => {
        CrudService.delete(`/docflow/${rootState.auth.currentUser.filial}/for_review/${userId}/`)
          .then(() => {
            dispatch("fetchReviewUsersList", incomingDocId)
            dispatch("fetchAssignedUsersTree", {treeId: model.id})

            resolve()
          })
      })
    },
    /*
    * Загрузить файлы на входящий регистрационный документ
    * в поле "Прикрепить файл"
    * */
    fetchAttachFiles({commit, rootState, dispatch}, {files, incomingDocId}) {
      return new Promise((resolve, reject) => {
        for (let i = 0; i < files.length; i++) {
          let formData = new FormData()
          formData.append("file", files[i])

          let progressValue = 0

          this.$axios
            .post('/upload/', formData, {
              onUploadProgress: function (progressEvent) {
                progressValue = parseInt( Math.round((progressEvent.loaded / progressEvent.total) * 100))
                if (progressValue <= 100){
                  commit(SET_ENTITY, {
                    entity: "uploadPercentage",
                    value: progressValue - 1
                  }, { root: true })
                }
              }.bind(this)
            })
            .then(({data}) => {
              if (data){
                commit(SET_ENTITY, {
                  entity: "uploadPercentage",
                  value: progressValue
                }, { root: true })
                commit("SET_ATTACH_DOCUMENT_LIST_ID", {document: data.id, incoming_doc: incomingDocId});
                commit("MERGE_ATTACHED_DOCUMENTS", data);
                resolve(data)
              }

              // this.$axios.post(`/docflow/${rootState.auth.currentUser.filial}/files/`, {
              // 	document: data.id,
              // 	incoming_doc: incomingDocId
              // })
              // 	.then(({ data }) => {
              // 		if(!!incomingDocId) {
              // 			dispatch("fetchAttachFilesList", { incomingDocId })
              // 		}
              // 		else {
              // 			commit("SET_ATTACH_DOCUMENT_LIST_ID", data.id)
              // 		}
              // 	})
            })
            .catch(err => {
              reject(err)
            })
        }
      })
    },
    /*
    * Получить лист прикрепленные файлы
    * на входящий регистрационный документ
    * */
    async fetchAttachFilesList({commit, rootState}, {incomingDocId}) {
      commit("SET_PDF_LOADER", true, { root: true })
      await CrudService.getList(`/docflow/${rootState.auth.currentUser.filial}/files/`, {
        incoming_doc: incomingDocId,
        page_size: 150
      })
        .then((data) => {
          commit(SET_ENTITY, {
            module: "documentFlowModule",
            entity: "attachDocumentsList",
            value: data.results
          }, {root: true})
        });
      commit("SET_PDF_LOADER", false, { root: true })
    },
    /**/
    fetchAttachFilesUpload({commit, rootState, dispatch, state}, payload) {
      this.$axios.post(`/docflow/${rootState.auth.currentUser.filial}/files/`, payload)
        .then(() => {
          dispatch("fetchAttachFilesList", {incomingDocId: payload[0].incoming_doc})
          state.attachDocumentListID = [];
        })
      // let newDocument = {
      //   document: document[document.length - 1].document,
      //   incoming_doc: incoming_doc
      // };

      // this.$axios.post(`/docflow/${rootState.auth.currentUser.filial}/files/`, payload[payload.length - 1])
      //   .then(() => {
      //     dispatch("fetchAttachFilesList", {incomingDocId: payload[payload.length - 1].incoming_doc})
      //   })
    },
    /*
    * Удалить файл из списка прикрепленные файлы
    * */
    async fetchDeleteFileFromAttachFile({commit, rootState, dispatch}, {fileId, incomingDocId}) {
      await CrudService.delete(`/docflow/${rootState.auth.currentUser.filial}/files/${fileId}/`)
        .then(() => {
          dispatch("fetchAttachFilesList", {incomingDocId})
        })
    },
    /*
    * Поиск пользователей которые
    * ниже своей позиции по должности
    * */
    fetchUsersBelow({commit, rootState}, {search}) {
      return new Promise((resolve) => {
        CrudService.getList(`/${rootState.auth.currentUser.filial}/user/lists/by_category/`, {
          search
        })
          .then((data) => {
            commit(SET_ENTITY, {
              module: "documentFlowModule",
              entity: "usersBelowList",
              value: data.results
            }, {root: true})

            resolve(data.results)
          })
      })
    },
    /*
    * Прикрепить пользователей к поручению
    * */
    fetchAssignUsersToResolution({commit, rootState, dispatch}, {model, treeId}) {
      return new Promise((resolve) => {
        CrudService.post(`/docflow/${rootState.auth.currentUser.filial}/assigment/`, model)
          .then((data) => {
            dispatch("fetchAssignedUsersTree", {treeId})

            resolve()
          })
      })
    },
    /*
    *
    * */
    fetchAssignmentUpdateById({commit, rootState, dispatch}, payload) {
      return new Promise((resolve, reject) => {
        this.$axios.put(`/docflow/${rootState.auth.currentUser.filial}/assigment/${payload.id}/`, payload.model, {
          params: {
            is_project_resolution: "True",
          }
        })
          .then((data) => {
            resolve()
          })
      })
    },
    /*
    *
    * */
   /* fetchAssigmentList({commit, rootState, dispatch}, payload) {
      CrudService.getList(`/docflow/${rootState.auth.currentUser.filial}/assigment/`, {
        ...payload,
        // status: 1,
        page_size: rootState.paginationOptions.page_size
      })
        .then((data) => {
          let model = data.results.map(item => Object.assign(item, {
            __priorityText: rootState.priorityList.find(priority => priority.value === item.document.priority).title,
            __responsibleUser: item.assignment.filter(assignment => assignment.is_responsible),
            // __status: rootState.statusList[item.assignment.find(i => i.user.id === rootState.auth.currentUser.id).status.id - 1]
          }))

          // let test = model.filter(item => item.__status.title === "TO DO")

          switch (payload.status) {
            case 1:
              console.log(1);
              break;
            default:
              commit(SET_ENTITY, {
                module: "documentFlowModule",
                entity: "assignmentList",
                value: model
              }, {root: true})
          }

          /!*
          * Заполняем массив с нужными полями
          * Это нужно для просмотра входящих документов в календарном виде
          * *!/
          commit(SET_ENTITY,
            {
              module: "documentFlowModule",
              entity: "eventsList",
              value: model.map(item => {
                return {
                  ...item,
                  name: `${item.document.correspondent.name}`,
                  start: new Date(item.document.register_date).toISOString().replace(/T.*$/, ''),
                  color: item.document.priority === "high" ? "var(--secondary-4)" : item.document.priority === "critical" ? "var(--secondary-4)" : item.document.priority === "medium" ? "var(--secondary-3)" : "var(--secondary-2)",
                  textColor: item.document.priority === "high" ? "var(--primary-4)" : item.document.priority === "critical" ? "var(--primary-4)" : item.document.priority === "medium" ? "var(--primary-3)" : "var(--primary-2)",
                }
              })
            },
            {root: true}
          )

          dispatch("fetchPagination", data.count, {root: true})
        })
    },*/

    /**/
    fetchAssignmentList({ state, commit, rootState }, payload){
      return new Promise((resolve, reject) => {
        this.$axios.get(`/docflow/${rootState.auth.currentUser.filial}/assigment/`, {
          params: {
            review: payload.id,
            is_project_resolution: 'True'
          }
        })
          .then(({ data }) => {
            if (data && data.results.length){
              commit(SET_ENTITY, {
                module: "documentFlowModule",
                entity: "assignment",
                value: data.results[0]
              }, {root: true});

              resolve(data.results[0])
            }else {
              commit(SET_ENTITY, {
                module: "documentFlowModule",
                entity: "assignment",
                value: null
              }, {root: true});
            }
          })
      })
    },
    /*
    *
    * */
    fetchAssigmentById({commit, rootState}, {type, id}) {
      CrudService.getList(`/docflow/${rootState.auth.currentUser.filial}/assigment/${id}/`, {
        type
      })
        .then((data) => {
          let model = {
            ...data,
            correspondent: data.document && data.document.correspondent ? data.document.correspondent : {},
            register_date: data.document && data.document.register_date ? new Date(data.document.register_date).toISOString().replace(/T.*$/, '') : "",
            register_number: data.document &&  data.document.register_number ? data.document.register_number : "",
            outgoing_date: data.document &&  data.document.outgoing_date ? new Date(data.document.outgoing_date).toISOString().replace(/T.*$/, '') : "",
            outgoing_number: data.document &&  data.document.outgoing_number ? data.document.outgoing_number : "",
            priority: data.document && data.document.priority ? data.document.priority : {},
            status: data.document && data.document.status ? data.document.status : {},
            journal: data.document && data.document.journal ? data.document.journal : {},
            __userSelected: data.document && data.document.review_users ? data.document.review_users.map(item => item.user.full_name) : [],
            __id: data.document && data.document.id,
            send_date: data.document && data.document.register_date,
            document_unique_id: data.document.id,
            base_status: data.status ? data.status : {}
          }

          commit(SET_ENTITY, {
            module: "documentFlowModule",
            entity: "assignmentGetOne",
            value: model
          }, {root: true})
        })
    },
    /*
    *
    * */
    fetchAssignmentUpdate({commit, rootState, dispatch}, payload) {
      return new Promise((resolve, reject) => {
        this.$axios.put(`/docflow/${rootState.auth.currentUser.filial}/assigment_multiple_update/`, payload.model)
          .then(({data}) => {
            console.log('Фишка изменен')
            dispatch("fetchAssignedUsersTree", {treeId: rootState.documentFlowModule.reviewGetOne.id})

            resolve()
          })
      })
    },
    /*
    *
    * */
    fetchAssignmentToAssign({ commit, rootState, dispatch }, payload) {
      return new Promise((resolve, reject) => {
        this.$axios.put(`/docflow/${rootState.auth.currentUser.filial}/assigment/send_assignment_for_signature/`, payload.model)
          .then(() => {
            resolve()
          })
      })
    },
    /*
    *
    * */
    fetchAssignDocument({ commit, rootState, dispatch }, payload) {
      return new Promise((resolve, reject) => {
        this.$axios.put(`/docflow/${rootState.auth.currentUser.filial}/assigment/verify_assignment/`, payload.model)
          .then(() => {
            resolve()
          })
      })
    },
    /*
    *
    * */
    fetchCancelAssignDocument({ commit, rootState }, payload) {
      return new Promise((resolve, reject) => {
        this.$axios.put(`/docflow/${rootState.auth.currentUser.filial}/assigment/cancel_assignment/`, payload)
          .then(() => {
            resolve()
          })
      })
    },
    /*
    * Получить список назначенных пользователей виде дерево
    * */
    fetchAssignedUsersTree({ commit, rootState, dispatch }, { treeId }) {
      commit(SET_ENTITY, {
        module: "documentFlowModule",
        entity: "userTreeLoading",
        value: true
      }, { root: true })

      return new Promise((resolve, reject) => {
        CrudService.getList(`/docflow/${rootState.auth.currentUser.filial}/incoming_doc_tree/${treeId}/`)
          .then((data) => {

            let model = []

            for (let user of data.review_users) {
              /*
              * For multiple blocks add
              * */
              let assignment = user.assignees ? user.assignees.map(item => item.assignment) : []

              /*
              * For single block add
              * */
              // let assignment = user.assignees ? user.assignees[0].assignment : []


              // // сбор всех родителей (чей родитель равен null)
              // let parents = user.assignees ? user.assignees.filter(x => x.parent === null) : []
              //
              // // сбор всех детей (чей родитель не равен null)
              // let children = user.assignees ? user.assignees.filter(x => x.parent !== null) : []
              //
              // for (let x of children) {
              //   let index = parents.findIndex(obj => obj.id === x.parent);
              //   parents.splice(index + 1, 0, x);
              // }

              model.push({
                creator: user.user,
                creator_read_time: user.read_time,
                children: assignment,
                // children: parents,
                created_date: user.created_date,
                review: user.id,
                status: user.status
              })
            }

            console.log('TREE_MODEL', model);

            commit(SET_ENTITY, {
              module: "documentFlowModule",
              entity: "assignedUsersTreeList",
              value: model
            }, {root: true})

            commit(SET_ENTITY, {
              module: "documentFlowModule",
              entity: "userTreeLoading",
              value: false
            }, { root: true })

            resolve()
          })
      })
    },
    /*
    * Ознакомится с документом
    * */
    fetchGetAcquainted({commit, rootState, dispatch}, {assignmentId, userId}) {
      return new Promise((resolve, reject) => {
        this.$axios.put(`/docflow/${rootState.auth.currentUser.filial}/assigment_users/${assignmentId}/get_acquainted/`)
          .then(({data}) => {
            resolve()
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    /*
    *
    * */
    fetchReviewGetAcquainted({ commit, rootState, dispatch }, { reviewId }) {
      return new Promise((resolve, reject) => {
        this.$axios.put(`/docflow/${rootState.auth.currentUser.filial}/for_review/${reviewId}/get_acquainted/`)
          .then(() => {
            resolve()
          })
          .catch(() => {
            reject()
          })
      })
    },
    /*
    *
    * */
    fetchUpdateAcquainted({ commit, rootState, rootGetters, dispatch }, { content }) {
      let assignmentGetOneModel = rootGetters["documentFlowModule/getAssignmentGetOne"]
      let currentUser = rootGetters["auth/getCurrentUser"]

      assignmentGetOneModel = {
        ...assignmentGetOneModel,
        assignment: assignmentGetOneModel?.assignment?.map(item => {
          return {
            ...item,
            is_read: item.user.id === currentUser.id ? content.is_read : false,
            read_time: item.user.id === currentUser.id ? content.read_time : null,
          }
        })
      }

      commit(SET_ENTITY, {
        module: "documentFlowModule",
        entity: "assignmentGetOne",
        value: assignmentGetOneModel
      }, {root: true})

      if (assignmentGetOneModel && assignmentGetOneModel.document && assignmentGetOneModel.document.id){
        dispatch("fetchAssignedUsersTree", { treeId: assignmentGetOneModel.document.id })
      }
    },
    /*
    *
    * */
    fetchReviewUpdateAcquainted({ commit, rootState, rootGetters, dispatch }, { content }) {
      let reviewGetOne = rootGetters["documentFlowModule/getReviewGetOne"]
      let currentUser = rootGetters["auth/getCurrentUser"]

      reviewGetOne = {
        ...reviewGetOne,
        review_users: reviewGetOne.review_users.map(item => {
          return {
            ...item,
            read_time: item.user.id === currentUser.id || (item.user.assistants && item.user.assistants.includes(currentUser.id)) ? content.read_time : null,
          }
        })
      }

      commit(SET_ENTITY, {
        module: "documentFlowModule",
        entity: "reviewGetOne",
        value: reviewGetOne
      }, {root: true})

      dispatch("fetchAssignedUsersTree", { treeId: reviewGetOne.id })
    },
    /*
    * Отметка об исполнение документа
    * */
    fetchCompletionNote({commit, rootState, rootGetters, dispatch}, {note, assignmentId}) {
      return new Promise((resolve, reject) => {
        this.$axios.put(`/docflow/${rootState.auth.currentUser.filial}/assigment_users/${assignmentId}/perform_assignment/`, note)
          .then(({data}) => {
            resolve()
          })
      })
    },
    /*
    *
    * */
    fetchDocumentPerformed({commit, rootState, rootGetters, dispatch}, { content }) {
      let assignmentGetOneModel = rootGetters["documentFlowModule/getAssignmentGetOne"]
      let currentUser = rootGetters["auth/getCurrentUser"]

      assignmentGetOneModel = {
        ...assignmentGetOneModel,
        assignment: assignmentGetOneModel?.assignment?.map(item => {
          return {
            ...item,
            is_read: item.user.id === currentUser.id ? content.is_performed : false,
            read_time: item.user.id === currentUser.id ? content.performance_date : null,
          }
        })
      }

      commit(SET_ENTITY, {
        module: "documentFlowModule",
        entity: "assignmentGetOne",
        value: assignmentGetOneModel
      }, {root: true})

      if(assignmentGetOneModel && assignmentGetOneModel.document && assignmentGetOneModel.document.id){
        dispatch("fetchAssignedUsersTree", { treeId: assignmentGetOneModel.document.id })
      }
    },
    /*
    *
    * */
    fetchAssignmentUsersPost({commit, rootState, dispatch}, payload) {
      return new Promise((resolve, reject) => {
        CrudService.post(`/docflow/${rootState.auth.currentUser.filial}/assigment_users/`, payload.model)
          .then((data) => {
            dispatch("fetchAssignedUsersTree", { treeId: payload.treeId })
            resolve(data)
          })
      })
    },
    /*
    *
    * */
    fetchChangeResponsibleUser({ commit, rootState }, payload) {
      return new Promise((resolve, reject) => {
        this.$axios.put(`/docflow/${rootState.auth.currentUser.filial}/assigment_users/${payload.id}/`, payload.model)
          .then(() => {
            resolve()
          })
      })
    },
    /*
    *
    * */
    fetchTakeOutOfControl({ commit, rootState }, payload) {
      return new Promise((resolve, reject) => {
        this.$axios.put(`/docflow/${rootState.auth.currentUser.filial}/assigment/${payload.id}/take_out_of_control/`, payload.model)
          .then(() => {
            resolve()
          })
      })
    },
    /*
    *
    * */
    fetchAssignmentUsersList({commit, rootState, dispatch}, payload) {
      return new Promise((resolve, reject) => {
        CrudService.getList(`/docflow/${rootState.auth.currentUser.filial}/assigment_users/`, {
          ...payload.params
        })
          .then((data) => {
            commit(SET_ENTITY, {
              module: "documentFlowModule",
              entity: "assignmentUsersList",
              value: data.results
            }, {root: true})

            resolve()
          })
      })
    },
    /*
    *
    * */
    fetchAssignmentUserDelete({commit, rootState, dispatch}, payload) {
      return new Promise((resolve, reject) => {
        CrudService.delete(`/docflow/${rootState.auth.currentUser.filial}/assigment_users/${payload.id}`)
          .then((data) => {
            dispatch("fetchAssignedUsersTree", { treeId: payload.treeId })
            resolve()
          })
      })
    },
    /*
    *
    * */
    fetchGetAssignmentUserById({ commit, rootState, dispatch }, payload) {
      CrudService.getList(`/docflow/${rootState.auth.currentUser.filial}/assigment_users/${payload.id}/`)
        .then((data) => {
          console.log(data)
        })
    },
    /*
    * Получить список "На рассмотрение"
    * */
    fetchReviewList({commit, rootState, dispatch}, payload) {
      return new Promise((resolve, reject) => {
        CrudService.getList(`/docflow/${rootState.auth.currentUser.filial}/user_reviews/`, {
          page_size: rootState.paginationOptions.page_size,
          ...payload
        })
          .then((data) => {
            let model = data.results.map(item => Object.assign(item, {
              review_users: item.review_users.map(user => ({user: {...user}})),
              __priorityText: item.incoming_doc.priority ? rootState.priorityList.find(priority => priority.value === item.incoming_doc.priority).title : "Нет уровня",
              __status: item.status ? rootState.statusList.find(status => status.title === item.status.name) : {}
            }))

            resolve({
              model,
              count: data.count
            })
          })
          .catch((error) => {
            reject(error)
          })
      })
    },
    /*
    * Получить список всех "На рассмотрение"
    * */
    fetchReviewAllList({commit, rootState, dispatch}, payload) {
      console.log(payload);

      commit(SET_ENTITY, { entity: "isLoading", value: true }, { root: true })

      dispatch("fetchReviewList", payload).then((data) => {
        commit(SET_ENTITY, {
          module: "documentFlowModule",
          entity: "reviewList",
          value: data.model
        }, {root: true})

        dispatch("fetchPagination", data.count, {root: true})

        commit(SET_ENTITY, { entity: "isLoading", value: false }, { root: true })
      })
    },
    /*
    * Получить список всех "На рассмотрение" по статусу "TO DO"
    * */
    fetchReviewListStatusToDo({commit, rootState, dispatch}, payload) {
      dispatch("fetchReviewList", {status: 1, ...payload}).then((data) => {
        commit("PUSH_LIST", {
          entity: "reviewListToDo",
          value: data.model
        })
      })
    },
    /*
    * Получить список всех "На рассмотрение" по статусу "IN PROGRESS"
    * */
    fetchReviewListStatusInProgress({commit, rootState, dispatch}, payload) {
      dispatch("fetchReviewList", {status: 2, ...payload}).then((data) => {
        commit("PUSH_LIST", {
          entity: "reviewListInProgress",
          value: data.model
        })
      })
    },
    /*
    * Получить список всех "На рассмотрение" по статусу "ON HOLD"
    * */
    fetchReviewListStatusOnHold({commit, rootState, dispatch}, payload) {
      dispatch("fetchReviewList", {status: 3, ...payload}).then((data) => {
        commit("PUSH_LIST", {
          entity: "reviewListOnHold",
          value: data.model
        })
      })
    },
    /*
    * Получить список всех "На рассмотрение" по статусу "DONE"
    * */
    fetchReviewListStatusDone({commit, rootState, dispatch}, payload) {
      dispatch("fetchReviewList", {status: 4, ...payload}).then((data) => {
        commit("PUSH_LIST", {
          entity: "reviewListDone",
          value: data.model
        })
      })
    },
    /*
    *
    * */
    fetchInboxListCreateBy({commit, rootState, dispatch}, payload) {
      commit(SET_ENTITY, { entity: "isLoading", value: true }, { root: true })

      CrudService.getList(`/docflow/${rootState.auth.currentUser.filial}/assigment/`, {
        page_size: rootState.paginationOptions.page_size,
        created_by: rootState.auth.currentUser.id,
        ...payload
      })
        .then((data) => {
          let model = data.results.map(item => Object.assign(item, {
            __priorityText: item.document.priority ? rootState.priorityList.find(priority => priority.value === item.document.priority).title : "Нет уровня",
          }))

          commit(SET_ENTITY, {
            module: "documentFlowModule",
            entity: "inboxListCreateBy",
            value: model
          }, {root: true})

          dispatch("fetchPagination", data.count, {root: true})

          commit(SET_ENTITY, { entity: "isLoading", value: false }, { root: true })
        })
    },
    /*
    * Получить список "Входящий" => "Assignment"
    * */
    fetchInboxList({commit, rootState, dispatch}, payload) {
      return new Promise((resolve, reject) => {
        CrudService.getList(`/docflow/${rootState.auth.currentUser.filial}/assigment/`, {
          page_size: rootState.paginationOptions.page_size,
          ordering: "assignment__is_read",
          ...payload
        })
          .then((data) => {
            let model = data.results.map(item => Object.assign(item, {
              __priorityText: item.document && item.document.priority ? rootState.priorityList.find(priority => priority.value === item.document.priority).title : "Нет уровня",
              __status: item.status ? rootState.statusList.find(status => status.title === item.status.name) : null,
              __deadline: item.assignment.find(assignment => assignment.user.id === rootState.auth.currentUser.id)
            }))

            /*
            * Заполняем массив с нужными полями
            * Это нужно для просмотра входящих документов в календарном виде
            * */
            let eventList = model.map(item => {
              if(item.document) {
                return {
                  ...item,
                  name: item.document && item.document.correspondent && item.document.correspondent.organization_name,
                  start: item.__deadline && item.__deadline.deadline ? item.__deadline.deadline : "0000-00-00",
                  color: item.document && item.document.priority === "high" ? "var(--secondary-4)" : item.document.priority === "critical" ? "var(--secondary-4)" : item.document.priority === "medium" ? "var(--secondary-3)" : "var(--secondary-2)",
                  textColor: item.document && item.document.priority === "high" ? "var(--primary-4)" : item.document.priority === "critical" ? "var(--primary-4)" : item.document.priority === "medium" ? "var(--primary-3)" : "var(--primary-2)",
                }
              }
            })

            commit(SET_ENTITY,
              {
                module: "gridCalendar",
                entity: "mergedEventsList",
                value: eventList
              },
              {root: true}
            )

            resolve({
              model,
              count: data.count
            })
          })
          .catch((error) => {
            reject(error)
          })
      })
    },
    /*
    *  Получить список всех "Входящий" => "Assignment"
    * */
    fetchInboxListAll({commit, rootState, dispatch}, payload) {
      commit(SET_ENTITY, { entity: "isLoading", value: true }, { root: true })

      dispatch("fetchInboxList", payload).then((data) => {
        commit(SET_ENTITY, {
          module: "documentFlowModule",
          entity: "inboxList",
          value: data.model
        }, {root: true})

        if(payload.hasOwnProperty("review")) {
          console.log(data.model);

          commit(SET_ENTITY, {
            module: "documentFlowModule",
            entity: "createdResolutionList",
            value: data.model
          }, {root: true})

          commit(SET_ENTITY, {
            module: "documentFlowModule",
            entity: "isDocumentAssigned",
            value: data.model.length ? data.model[0] : {}
          }, {root: true})
        }

        dispatch("fetchPagination", data.count, {root: true})

        commit(SET_ENTITY, { entity: "isLoading", value: false }, { root: true })
      })
    },
    /*
    * Получить список "Входящий" => "Assignment" => "TODO"
    * */
    fetchInboxListToDo({commit, rootState, dispatch}, payload) {
      dispatch("fetchInboxList", {status: 1, ...payload}).then((data) => {
        commit("PUSH_LIST", {
          entity: "inboxListToDo",
          value: data.model
        })
      })
    },
    /*
    * Получить список "Входящий" => "Assignment" => "InProgress"
    * */
    fetchInboxListInProgress({commit, rootState, dispatch}, payload) {
      dispatch("fetchInboxList", {status: 2, ...payload}).then((data) => {
        commit("PUSH_LIST", {
          entity: "inboxListInProgress",
          value: data.model
        })
      })
    },
    /*
    * Получить список "Входящий" => "Assignment" => "ON HOLD"
    * */
    fetchInboxListOnHold({commit, rootState, dispatch}, payload) {
      dispatch("fetchInboxList", {status: 3, ...payload}).then((data) => {
        commit("PUSH_LIST", {
          entity: "inboxListOnHold",
          value: data.model
        })
      })
    },
    /*
    * Получить список "Входящий" => "Assignment" => "DONE"
    * */
    fetchInboxListDone({commit, rootState, dispatch}, payload) {
      dispatch("fetchInboxList", {status: 4, ...payload}).then((data) => {
        commit("PUSH_LIST", {
          entity: "inboxListDone",
          value: data.model
        })
      })
    },
    /*
    *
    * */
    fetchDocumentResolution({ commit, dispatch }, payload) {
      commit(SET_ENTITY, {
        module: "documentFlowModule",
        entity: "documentResolution",
        value: payload
      }, { root: true })

      dispatch("fetchInboxListAll", payload)
    },
    /*
    *
    * */
    fetchInnerDocumentById({commit, dispatch, rootState}, payload) {
      commit("SET_DETAIL_LOADER", true, { root: true });
      this.$axios.get(`/docflow/${rootState.auth.currentUser.filial}/inner_doc/${payload}/`)
        .then(({data}) => {
          let model = {
            ...data,
            send_date: data.register_date,
            register_date: new Date(data.register_date).toISOString().replace(/T.*$/, ''),
            document_unique_id: data.id,
            deadline: data.deadline ? new Date(data.deadline).toISOString().replace(/T.*$/, '') : null,
            __deadline: data.deadline ? new Date(data.deadline) : null,
            __currentReviewUser: data.review_users && data.review_users.find(user => user.user.id === rootState.auth.currentUser.id || user.user.assistants && user.user.assistants.includes(rootState.auth.currentUser.id)),
          }
          commit(SET_ENTITY, {
            module: "documentFlowModule",
            entity: "reviewGetOneInner",
            value: model
          }, { root: true });

          commit(SET_ENTITY, {
            module: "documentFlowModule",
            entity: "reviewGetOne",
            value: model
          }, {root: true});

          dispatch(
            "treeView/fetchTreeUsersList",
            { treeId: data.id },
            { root: true }
          );

          if (data && data.review_users && data.review_users.length){
            dispatch("documentFlowModule/fetchAssignmentList", { id: data.review_users[0].id }, { root: true })
          }

          commit("SET_DETAIL_LOADER", false, { root: true });
        });
    },
    /*
    *
    * */
    fetchBoxesOutgoingDocumentById({commit, dispatch, rootState}, payload) {
      commit("SET_DETAIL_LOADER", true, { root: true });
      this.$axios.get(`/docflow/${rootState.auth.currentUser.filial}/inner_doc/${payload}/`)
      .then(({data}) => {
        console.log(data)
        let model = {
          ...data,
          send_date: data.register_date,
          register_date: new Date(data.register_date).toISOString().replace(/T.*$/, ''),
          document_unique_id: data.id,
          deadline: new Date(data.deadline).toISOString().replace(/T.*$/, ''),
          __currentReviewUser: data.review_users && data.review_users.find(user => user.user.id === rootState.auth.currentUser.id || user.user.assistants && user.user.assistants.includes(rootState.auth.currentUser.id)),
        }
        commit(SET_ENTITY, {
          module: "documentFlowModule",
          entity: "reviewGetOneInner",
          value: model
        }, { root: true });

        commit(SET_ENTITY, {
          module: "documentFlowModule",
          entity: "reviewGetOne",
          value: model
        }, {root: true});

        commit("SET_DETAIL_LOADER", false, { root: true });
      });
    },
    /*
    *
    * */
    fetchApprovalList({ commit, dispatch, rootState }, payload) {
      commit(SET_ENTITY, { entity: "isLoading", value: true }, { root: true })
      CrudService.getList(`/docflow/${rootState.auth.currentUser.filial}/for_agreement/`, {
        page_size: rootState.paginationOptions.page_size,
        ...payload
      })
        .then((data) => {
          let model = data.results.map(item => {
            return {
              ...item,
              ...item.compose,
              __id: item.id
            }
          })

          commit(SET_ENTITY, {
            module: "documentFlowModule",
            entity: "approvalList",
            value: model
          }, { root: true });

          dispatch("fetchPagination", data.count, {root: true})
        })
        .finally(() => {
          commit(SET_ENTITY, { entity: "isLoading", value: false }, { root: true })
        })
    },
    /*
    *
    * */
    fetchSignsList({ commit, dispatch, rootState }, payload) {
      CrudService.getList(`/docflow/${rootState.auth.currentUser.filial}/for_signature/`, {
        page_size: rootState.paginationOptions.page_size,
        ...payload
      })
        .then((data) => {
          commit(SET_ENTITY, {
            module: "documentFlowModule",
            entity: "signsList",
            value: data.results
          }, { root: true });

          dispatch("fetchPagination", data.count, {root: true})
        })
    },
    /*
    *
    * */
    fetchGetHeaderFilterByType({ commit, rootState, state, dispatch }, { type, query }) {
      return new  Promise((resolve, reject) => {
        CrudService.getList(`/action/filter/${type}/`)
        .then((data) => {
          if(data.status === "fail") {
            resolve({ fail: true })

            return;
          }

          Object.entries(data.filter_query).forEach(item => {
            const [key, value] = item

            if(key === "page") {
              return
            }

            if(value || (typeof value === "boolean")) {
              rootState.headerFilter.filter[key] = value
              rootState.headerFilter.filteredState[key] = value
              rootState.headerFilter.filterFakeKeys[key] = value
            }
          })

          commit(SET_ENTITY, {
            module: "documentFlowModule",
            entity: "isHeaderFilterSaved",
            value: true
          }, { root: true });

          resolve(data.filter_query)
        })
        .catch((err) => {
          commit(SET_ENTITY, {
            module: "documentFlowModule",
            entity: "isHeaderFilterSaved",
            value: false
          }, { root: true });
        })
      })
    },
    /*
    *
    * */
    fetchSaveHeaderFilter({ commit, rootState }, { type, filterQuery }) {
      return new Promise((resolve, reject) => {
        CrudService.post(`/action/filter/`, {
          type,
          filter_query: filterQuery
        })
          .then((data) => {
            Object.entries(data.filter_query).forEach(item => {
              const [key, value] = item

              if(key === "page") {
                return
              }

              if(value || (typeof value === "boolean")) {
                rootState.headerFilter.filter[key] = value
              }
            })

            commit(SET_ENTITY, {
              module: "documentFlowModule",
              entity: "isHeaderFilterSaved",
              value: true
            }, { root: true });

            resolve()
          })
      })
    },
    /*
    *
    * */
    fetchUpdateHeaderFilter({ commit, rootState }, { type, filterQuery }) {
      return new Promise((resolve, reject) => {
        this.$axios.put(`/action/filter/${type}`, {
          type,
          filter_query: filterQuery
        })
          .then(({ data }) => {
            Object.entries(data.filter_query).forEach(item => {
              const [key, value] = item

              if(key === "page") {
                return
              }

              if(value || (typeof value === "boolean")) {
                rootState.headerFilter.filter[key] = value
              }
            })

            commit(SET_ENTITY, {
              module: "documentFlowModule",
              entity: "isHeaderFilterSaved",
              value: true
            }, { root: true });

            resolve()
          })
      })
    },
    /**/
    fetchInnerDocumentUpdate({commit, rootState}, payload) {
      return new Promise((resolve, reject) => {
        let model = {
          ...payload,
          doc_type: payload.doc_type.id,
          journal: payload.journal.id,
          author: payload.author.id,
          signer: payload.signer.id,
          department: payload.department.id,
          register_date: `${payload.register_date} ${new Date().getHours()}:${new Date().getMinutes()}`,
          documents: [],
          review_users: [],
          deadline: payload.deadline ? `${payload.deadline}T18:00:00+05:00` : null,
          correspondent: payload.correspondent && payload.correspondent.id
        }

        this.$axios.put(`/docflow/${rootState.auth.currentUser.filial}/inner_doc/${payload.id}/`, model)
          .then(({data}) => {
            resolve(data)
          })
      })
    },
    /**/
    fetchOutgoingDocumentById({commit, dispatch, rootState}, payload) {
      commit("SET_DETAIL_LOADER", true, { root: true });
      return new Promise((resolve, reject) => {
        this.$axios.get(`/docflow/${rootState.auth.currentUser.filial}/outgoing/${payload}/`)
          .then(({data}) => {
            resolve(data)
            let model = {
              ...data,
              register_date: new Date(data.register_date).toISOString().replace(/T.*$/, ''),
              deadline: data.deadline ? new Date(data.deadline).toISOString().replace(/T.*$/, '') : null,
              __deadline: data.deadline ? new Date(data.deadline) : null,
            }
            commit(SET_ENTITY, {
              module: "documentFlowModule",
              entity: "reviewGetOneOutgoing",
              value: model
            }, { root: true });

            dispatch(
              "treeView/fetchTreeUsersList",
              { treeId: data.tree_id ? data.tree_id : data.id },
              { root: true }
            );

            if (data && data.review_users && data.review_users.length){
              dispatch("documentFlowModule/fetchAssignmentList", { id: data.review_users[0].id }, { root: true })
            }

            commit("SET_DETAIL_LOADER", false, { root: true });
          })
      })
    },
    /**/
    fetchOutgoingDocumentUpdate({commit, rootState}, payload) {
      return new Promise((resolve, reject) => {
        let model = {
          ...payload,
          doc_type: payload.doc_type.id,
          journal: payload.journal.id,
          department: payload.department.id,
          author: payload.author.id,
          correspondent: payload.correspondent.id,
          signer: payload.signer.id,
          register_date: `${payload.register_date} ${new Date().getHours()}:${new Date().getMinutes()}`,
          deadline: payload.deadline ? `${payload.deadline}T18:00:00+05:00` : null,
          documents: [],
          review_users: []
        }

        this.$axios.put(`/docflow/${rootState.auth.currentUser.filial}/outgoing/${payload.id}/`, model)
          .then(({data}) => {
            resolve(data)
          })
      })
    },
    /**/
    fetchAppealDocumentById({commit, dispatch, rootState}, payload) {
      commit("SET_DETAIL_LOADER", true, { root: true });
      return new Promise((resolve, reject) => {
        this.$axios.get(`/docflow/${rootState.auth.currentUser.filial}/appeals/${payload}/`)
          .then(({data}) => {
            let model = {
              ...data,
              register_date: new Date(data.register_date).toISOString().replace(/T.*$/, ''),
              outgoing_date: new Date(data.outgoing_date).toISOString().replace(/T.*$/, ''),
              deadline: data.deadline ? new Date(data.deadline).toISOString().replace(/T.*$/, '') : null,
              organization_reg_date: data.organization_reg_date ? new Date(data.organization_reg_date).toISOString().replace(/T.*$/, '') : null,
              __deadline: data.deadline ? new Date(data.deadline) : null,
            }
            commit(SET_ENTITY, {
              module: "documentFlowModule",
              entity: "reviewGetOneAppeal",
              value: model
            },{ root: true });

            dispatch(
              "treeView/fetchTreeUsersList",
              { treeId: data.id },
              { root: true }
            );

            if (data && data.review_users && data.review_users.length){
              dispatch("documentFlowModule/fetchAssignmentList", { id: data.review_users[0].id }, { root: true })
            }

            commit("SET_DETAIL_LOADER", false, { root: true });

            resolve(data);
          })
      })
    },
    /**
     *
     * */
    fetchAppealCallCenterDocumentById({commit, dispatch, rootState}, payload) {
      commit("SET_DETAIL_LOADER", true, { root: true });
      return new Promise((resolve, reject) => {
        this.$axios.get(`/docflow/${rootState.auth.currentUser.filial}/appeals/${payload}/`, {
          params: {
            journal: 2
          }
        })
        .then(({data}) => {
          let model = {
            ...data,
            register_date: new Date(data.register_date).toISOString().replace(/T.*$/, ''),
            outgoing_date: new Date(data.outgoing_date).toISOString().replace(/T.*$/, ''),
            deadline: data.deadline ? new Date(data.deadline).toISOString().replace(/T.*$/, '') : null,
            organization_reg_date: data.organization_reg_date ? new Date(data.organization_reg_date).toISOString().replace(/T.*$/, '') : null,
            __deadline: data.deadline ? new Date(data.deadline) : null,
          }
          commit(SET_ENTITY, {
            module: "documentFlowModule",
            entity: "reviewGetOneAppealCallCenter",
            value: model
          },{ root: true });

          dispatch(
            "treeView/fetchTreeUsersList",
            { treeId: data.id },
            { root: true }
          );

          if (data && data.review_users && data.review_users.length){
            dispatch("documentFlowModule/fetchAssignmentList", { id: data.review_users[0].id }, { root: true })
          }

          commit("SET_DETAIL_LOADER", false, { root: true });

          resolve(data);
        })
      })
    },
    /**/
    fetchAppealDocumentUpdate({commit, rootState}, payload) {
      return new Promise((resolve, reject) => {
        let model = {
          ...payload,
          doc_type: payload.doc_type.id,
          correspondent: payload.correspondent.id,
          region: payload.region.id,
          district: payload.district.id,
          delivery_type: payload.delivery_type.id,
          state: payload.state && payload.state.id,
          theme: payload.theme && payload.theme.id,
          closed_type: payload.closed_type && payload.closed_type.id,
          complaint_type: payload.complaint_type.id,
          journal: payload.journal.id,
          register_date: `${payload.register_date} ${new Date().getHours()}:${new Date().getMinutes()}`,
          outgoing_date: `${payload.outgoing_date} ${new Date().getHours()}:${new Date().getMinutes()}`,
          deadline: payload.deadline ? `${payload.deadline}T18:00:00+05:00` : null,
          documents: [],
          review_users: [],
          gender: payload.applicant === 'physics' ? payload.gender : null,
          status: payload.status.id,
          general_theme: payload.general_theme && payload.general_theme.id,
          bxm: payload.bxm && payload.bxm.id,
          organization: payload.organization && payload.organization.id,
          organization_reg_date: payload.organization_reg_date ? `${payload.organization_reg_date} ${new Date().getHours()}:${new Date().getMinutes()}` : null
        }

        this.$axios.put(`/docflow/${rootState.auth.currentUser.filial}/appeals/${payload.id}/`, model)
          .then(({data}) => {
            resolve(data)
          })
      })
    },
    /** **/
    fetchCallCenterAppealDocumentUpdate({commit, rootState}, payload) {
      return new Promise((resolve, reject) => {
        let model = {
          ...payload,
          region: payload.region.id,
          district: payload.district.id,
          journal: payload.journal.id,
          register_date: `${payload.register_date} ${new Date().getHours()}:${new Date().getMinutes()}`,
          outgoing_date: `${payload.outgoing_date} ${new Date().getHours()}:${new Date().getMinutes()}`,
          deadline: payload.deadline ? `${payload.deadline}T18:00:00+05:00` : null,
          documents: [],
          review_users: [],
          gender: payload.applicant === 'physics' ? payload.gender : null,
          status: payload.status.id,
          organization_reg_date: payload.organization_reg_date ? `${payload.organization_reg_date} ${new Date().getHours()}:${new Date().getMinutes()}` : null
        }

        this.$axios.put(`/docflow/${rootState.auth.currentUser.filial}/appeals/${payload.id}/`, model)
          .then(({data}) => {
            resolve(data)
          })
      })
    },
    /** **/
    fetchRegisterOrdersProtocols({commit, rootState}, payload) {
      return new Promise((resolve) => {
        this.$axios.post(`/docflow/${rootState.auth.currentUser.filial}/orders_protocols/`, payload)
          .then(({data}) => {
            resolve(data)
          })
      })
    },
    /**/
    fetchOrderProtocolList({commit, dispatch, rootState, rootGetters}, payload) {
      commit(SET_ENTITY, { module: "documentFlowModule", entity: "isListLoading", value: true }, { root: true })
      CrudService.getList(`/docflow/${rootState.auth.currentUser.filial}/orders_protocols/`, {
        ...payload,
        journal: 6,
        page_size: rootState.paginationOptions.page_size,
      })
        .then((data) => {
          commit(SET_ENTITY, {
            module: "documentFlowModule",
            entity: "orderProtocolList",
            value: data.results
          }, {root: true});
          dispatch("fetchPagination", data.count, {root: true})
          commit(SET_ENTITY, { module: "documentFlowModule", entity: "isListLoading", value: false }, { root: true })
        })
    },
    /**/
    fetchFilialDocumentsList({ commit, dispatch, rootState }, payload) {
      commit(SET_ENTITY, { module: "documentFlowModule", entity: "isListLoading", value: true }, { root: true })
      return new Promise((resolve, reject) => {
        this.$axios.get(`/docflow/${rootState.auth.currentUser.filial}/for_registration/`, {
          params: {
            ...payload,
            page_size: rootState.paginationOptions.page_size,
          }
        })
          .then(({ data }) => {
            resolve(data)
            commit(SET_ENTITY, {
              module: "documentFlowModule",
              entity: "filialDocumentsList",
              value: data.results
            }, {root: true});
            dispatch("fetchPagination", data.count, {root: true})
            commit(SET_ENTITY, { module: "documentFlowModule", entity: "isListLoading", value: false }, { root: true })
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    /**/
    fetchApplicationsList({ commit, dispatch, rootState }, payload){
      commit(SET_ENTITY, { module: "documentFlowModule", entity: "isListLoading", value: true }, { root: true })
      return new Promise((resolve, reject) => {
        this.$axios.get(`/docflow/${rootState.auth.currentUser.filial}/inner_doc/`, {
          params: {
            ...payload,
            journal: 7,
            page_size: rootState.paginationOptions.page_size,
          }
        })
          .then(({ data }) => {
            resolve(data);
            commit(SET_ENTITY, {
              module: "documentFlowModule",
              entity: "applicationsList",
              value: data.results
            }, {root: true});
            dispatch("fetchPagination", data.count, {root: true})
            commit(SET_ENTITY, { module: "documentFlowModule", entity: "isListLoading", value: false }, { root: true })
          })
          .catch(err => {
            reject(err);
          })
      })
    },
    /**/
    fetchApplicationById({ commit, rootState, dispatch }, payload){
      commit("SET_DETAIL_LOADER", true, { root: true });
      return new Promise((resolve, reject) => {
        this.$axios.get(`/docflow/${rootState.auth.currentUser.filial}/inner_doc/${payload}/`)
          .then(({ data }) => {
            resolve(data);

            let documentArray =[];

            let model = {
              ...data,
              register_date: new Date(data.register_date).toISOString().replace(/T.*$/, ''),
              deadline: data.deadline ? new Date(data.deadline).toISOString().replace(/T.*$/, '') : null,
              __deadline: data.deadline ? new Date(data.deadline) : null,
            }

            data.documents.forEach(item => {
              documentArray.push({
                document: item.document
              });

              // commit("SET_ATTACH_DOCUMENT_LIST_ID", {document: item.id});
            })

            commit(SET_ENTITY, {
              module: "documentFlowModule",
              entity: "attachDocumentsList",
              value: documentArray
            }, {root: true});

            commit(SET_ENTITY, {
              module: "documentFlowModule",
              entity: "reviewGetOneApplication",
              value: model
            }, { root: true });

            dispatch(
              "treeView/fetchTreeUsersList",
              { treeId: data.id },
              { root: true }
            );

            if (data && data.review_users && data.review_users.length){
              dispatch("documentFlowModule/fetchAssignmentList", { id: data.review_users[0].id }, { root: true })
            }

            commit("SET_DETAIL_LOADER", false, { root: true });
          })
          .catch(err => {
            reject(err);
          })
      })
    },
    /**/
    fetchApplicationUpdate({ commit, rootState }, payload){
      return new Promise((resolve, reject) => {
        let model = {
          ...payload,
          correspondent: payload.correspondent.id,
          doc_type: payload.doc_type.id,
          journal: payload.journal.id,
          register_date: `${payload.register_date} ${new Date().getHours()}:${new Date().getMinutes()}`,
          deadline: payload.deadline ? `${payload.deadline}T18:00:00+05:00` : null,
          author: payload.author.id,
          department: payload.department.id,
          signer: payload.signer.id,
          review_users: [],
          documents: []
        }

        this.$axios.put(`/docflow/${rootState.auth.currentUser.filial}/inner_doc/${payload.id}/`, model)
          .then(({ data }) => {
            resolve(data)
          })
      })
    },
    /**/
    fetchFilialDocumentsById({commit, rootState}, payload){
      return new Promise((resolve, reject) => {
        this.$axios.get(`/docflow/${rootState.auth.currentUser.filial}/for_registration/${payload}/`)
          .then(({ data }) => {
            resolve(data)

            let documentArray =[]

            let model = {
              ...data,
              receiver_type: 'for_reg_comment'  // for commenting in filial document registration
            }

            data.attachments.forEach(item => {
              documentArray.push({
                document: {...item}
              });

              commit("SET_ATTACH_DOCUMENT_LIST_ID", {document: item.id});
            })

            commit(SET_ENTITY, {
              module: "documentFlowModule",
              entity: "attachDocumentsList",
              value: documentArray
            }, {root: true});

            commit(SET_ENTITY, {
              module: "documentFlowModule",
              entity: "reviewGetOneFilialDocument",
              value: model
            }, { root: true });

          })
          .catch(err => {
            reject(err)
          })
      })
    },
    /**/
    fetchOrderProtocolById({commit, dispatch, rootState}, payload) {
      commit("SET_DETAIL_LOADER", true, { root: true });
      this.$axios.get(`/docflow/${rootState.auth.currentUser.filial}/orders_protocols/${payload}/`)
        .then(({data}) => {
          let model = {
            ...data,
            register_date: new Date(data.register_date).toISOString().replace(/T.*$/, ''),
            deadline: data.deadline ? new Date(data.deadline).toISOString().replace(/T.*$/, '') : null,
            __deadline: data.deadline ? new Date(data.deadline) : null,
          }
          commit(SET_ENTITY, {
            module: "documentFlowModule",
            entity: "reviewGetOneOrdersProtocols",
            value: model
          }, { root: true });

          dispatch(
            "treeView/fetchTreeUsersList",
            { treeId: data.id },
            { root: true }
          );

          if (data && data.review_users && data.review_users.length){
            dispatch("documentFlowModule/fetchAssignmentList", { id: data.review_users[0].id }, { root: true })
          }

          commit("SET_DETAIL_LOADER", false, { root: true });
        })
    },
    /**/
    fetchOrdersProtocolsUpdate({commit, rootState}, payload) {
      return new Promise((resolve, reject) => {
        let model = {
          ...payload,
          doc_type: payload.doc_type.id,
          journal: payload.journal.id,
          department: payload.department ? payload.department.id : null,
          register_date: `${payload.register_date} ${new Date().getHours()}:${new Date().getMinutes()}`,
          deadline: payload.deadline ? `${payload.deadline}T18:00:00+05:00` : null,
          status: payload.status.id,
          sub_journal: payload.sub_journal.id,
          delivery_type: payload.delivery_type ? payload.delivery_type.id : null,
          documents: [],
          review_users: [],
        }
        this.$axios.put(`/docflow/${rootState.auth.currentUser.filial}/orders_protocols/${payload.id}/`, model)
          .then(({data}) => {
            resolve(data)
          })
      })
    },

    // Mock-state actions
    /*
    *
    * */
    fetchBoxDropdownSelected({commit}, { module, entity, value }) {
      commit(SET_ENTITY, {
        module: module,
        entity: entity,
        value: value
      }, {root: true})
    },
    /**/
    deleteRow({commit}, payload) {
      return new Promise((resolve, reject) => {
        this.$axios.delete(payload.apiUrl)
          .then(() => {
            resolve();
          })
          .catch((data) => {
            reject(data)
          })
      })
    },
    /**/
    scanDocument() {
      alert("Для того, чтобы отсканировать документ, откройте приложение сканера!");
    },
    /**/
    fetchScannedDocument({commit}, payload) {
      commit("SET_ATTACH_DOCUMENT_LIST_ID", { document: payload.content.file.id, incoming_doc: undefined })
      commit("MERGE_ATTACHED_DOCUMENTS", payload.content.file)
    },
    /**/
    fetchClearFileIDs({ commit }){
      commit("CLEAR_ATTACHED_DOCUMENTS_ID")
    },
    /**/
    fetchClearRegisterStartDate({ state, rootState }){
      let setDateEnd = set(new Date(), { year: new Date().getFullYear(), month: new Date().getMonth() + 1, date: 1 })

      rootState.headerFilter.filter.register_start_date = null
      rootState.headerFilter.filter.register_end_date = format(setDateEnd, "yyyy-MM-dd")
    },
    /**/
    fetchClearHeaderFilter({ state }){
      state.headerFilters = {
        // Входящие
          priority: null,
          status: null,
          journal: null,
          doc_type: null,
          type: null,
          deadline_start: null,
          deadline_end: null,
          correspondent: null,
          // На рассмотрение
          register_start_date: new Date().getFullYear() + '-' + (Number(new Date().getMonth()) + 1) + '-01',
          register_end_date: null,
          register_number: null,
          outgoing_start_date: null,
          outgoing_end_date: null,
          outgoing_number: null,
          author: null,
          signer: null,
          department: null,
          condition: null,
          search: null,
          review_user: null,
          // Для Зам.пред
          // is_for_signature: "True",
          // is_verified: "False",
          __resolutionAssign: false,
          is_signed: null,
          compose__register_number: null,
          is_agreed: null,
          is_read: null
      }
    },
    /**/
    fetchDownloadDocument({ commit }, payload){
      saveAs(payload.url, payload.name)
    },
    /**/
    checkKeyDownAlphaNumeric({commit}, payload){
      if (!/^[0-9/-]+$/.test(payload.key)) {
        payload.preventDefault();
      }
    },
    /**/
    checkCopiedText( { commit }, payload ){
      const copiedText = payload.clipboardData.getData('text')
      if (!/^[0-9/-]+$/.test(copiedText)) {
        payload.preventDefault();
        return false
      }
    },
    /**/
    fetchFilialList({ commit }, payload){
      return new Promise((resolve, reject) => {
        this.$axios.get(`/filials/`, {
          params: {
            page_size: 25
          }
        })
          .then(({ data }) => {
            commit(SET_ENTITY, {
              module: "documentFlowModule",
              entity: "filialList",
              value: data.results
            }, { root: true })

            resolve(data)
          })
      })
    },
    /**/
    fetchChangeStatus({ commit, rootState }, payload){
      return new Promise((resolve, reject) => {
        this.$axios.put(`/docflow/${rootState.auth.currentUser.filial}/${payload.api}/${payload.id}/`, {
          status: payload.statusId
        })
          .then(({ data }) => {
            resolve(data)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    /**/
    fetchSearchedDocumentList({ commit, rootState, dispatch }, payload){
      commit(SET_ENTITY, { module: "documentFlowModule", entity: "isListLoading", value: true }, { root: true })
      return new Promise((resolve, reject) => {
        this.$axios.get(`/docflow/${rootState.auth.currentUser.filial}/document/search/`, {
          params: {
            ...payload,
            page_size: rootState.paginationOptions.page_size,
          }
        })
          .then(({ data }) => {
            resolve(data)
            commit(SET_ENTITY, {
              module: "documentFlowModule",
              entity: "searchedDocumentList",
              value: data.results
            }, { root: true });
            dispatch("fetchPagination", data.count, {root: true})
            commit(SET_ENTITY, { module: "documentFlowModule", entity: "isListLoading", value: false }, { root: true })
          })
      })
    },
    fetchClearSearchedDocumentList({ commit }){
      commit(SET_ENTITY, {
        module: "documentFlowModule",
        entity: "searchedDocumentList",
        value: []
      }, { root: true });
    },
    /**/
    fetchDepartmentSelected({ commit, rootState, dispatch }, payload){
      commit(SET_ENTITY, { module: "documentFlowModule", entity: "isListLoading", value: true }, { root: true })
      this.$axios.get(`/dashboard/${payload.mfo}/my-departament-users/${payload.code ? payload.code : '1'}/`, {
        params: {
          page_size: 150
        }
      })
        .then(({data}) => {
          let usersList = data.results
          this.$axios.get(`/docflow/assignment-performance-statistic/`, {
            params: {
              dept_code: payload.code,
              filial: payload.mfo,
              page_size: 50,
              start_date: payload.start_date ? payload.start_date : '2022-01-01',
              end_date: payload.end_date ? payload.end_date : new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getDate()
            }
          })
            .then(({ data }) => {
              const newArrIncoming = usersList.map(x => ({ ...x, ...data.results.find(x2 => x2.user_id === x.id) }))
              commit(SET_ENTITY, {
                module: "documentFlowModule",
                entity: "executionStatisticsListIncoming",
                value: newArrIncoming
              }, { root: true })

              this.$axios.get(`/docflow/review-performance-statistic/`, {
                params: {
                  dept_code: payload.code,
                  filial: payload.mfo,
                  page_size: 50,
                  start_date: payload.start_date ? payload.start_date : '2022-01-01',
                  end_date: payload.end_date ? payload.end_date : new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getDate()
                }
              })
                .then(({ data }) => {
                  const newArrReview = usersList.map(x => ({ ...x, ...data.results.find(x2 => x2.user_id === x.id) }))
                  commit(SET_ENTITY, {
                    module: "documentFlowModule",
                    entity: "executionStatisticsListReview",
                    value: newArrReview
                  }, { root: true })
                })
                .finally(() => {
                  commit(SET_ENTITY, { module: "documentFlowModule", entity: "isListLoading", value: false }, { root: true })
                })
            })
        })
    },
    /**/
    fetchSearchDocumentClearList({ commit }){
      commit(SET_ENTITY, {
        module: "documentFlowModule",
        entity: "searchedDocumentList",
        value: []
      }, { root: true });
    }
  }
}
