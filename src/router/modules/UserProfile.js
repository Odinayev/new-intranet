
const UserProfile = [
	{
		path: "profile",
		name: "MainUserProfile",
		meta: {
			isAuthRequired: true
		},
		component: () => import(/* webpackChunkName: "MainUserProfile" */ "@/modules/UserProfile/views/UserProfile"),
		redirect: { name: "UserProfile" },
		children: [
			{
				path: "show",
				name: "UserProfile",
				meta: {
					isAuthRequired: true
				},
				component: () => import("@/modules/UserProfile/views/UserProfileDetail")
			},
			{
				path: "techniques",
				name: "UserTechniques",
				meta: {
					isAuthRequired: true
				},
				component: () => import("@/modules/UserProfile/views/Technique")
			},
			{
				path: "password-and-security",
				name: "UserPasswordAndSecurity",
				meta: {
					isAuthRequired: true
				},
				component: () => import("@/modules/UserProfile/views/PasswordAndSecurity")
			},

			// Salary
			{
				path: "salary",
				name: "UserSalary",
				component: () => import("@/modules/UserProfile/views/Salary")
			},
			// /Salary

			// Personal Documents
			{
				path: "personal-documents",
				name: "UserPersonalDocuments",
				component: () => import("@/modules/UserProfile/views/PersonalDocuments")
			},
			// /Personal Documents
		]
	}
]

export default UserProfile
