
export default {
	namespaced: true,
	/*
	*
	* */
	state: {
		filesList: [],
		fileId: null,
	},
	/*
	*
	* */
	getters: {
		getFileId:(state) => state.fileId
	},
	/*
	*
	* */
	mutations: {
		/*
		*
		* */
	},
	/*
	*
	* */
	actions: {
		/*
		*
		* */
		fetchFiles({ commit }, { url, files }) {
			for (let i = 0; i < files.length; i++) {
				let formData = new FormData()
				formData.append("file", files[i])

				this.$axios
					.post(url, formData)
					.then(({ data }) => {
						console.log(data);
						// this.mixinFileList.push({
						// 	document: data.id
						// })
					})
			}
		},
		/**/
		uploadFile({ commit }, file){
			let formData = new FormData();
			formData.append("file", file);
			return new Promise((resolve) => {
				this.$axios.post(`/upload/`, formData)
					.then(({ data }) => {
						resolve(data)
					})
			})
		}
	},
}
