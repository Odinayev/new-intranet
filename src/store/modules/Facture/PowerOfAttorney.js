import { SET_ENTITY } from "@/store/mutation-types";
import {fetchCreatePowerOfAttorneyRoaming} from "../../../api/Facture/facture.api.service";

export default {
  namespaced: true,
  state: {
    powerOfAttorneyModel: {
      sender_info: {
        id: 1
      },
      approver_and_signers: {
        signer_director: {
          position: "",
          last_name: "",
          first_name: "",
          middle_name: "",
          inn: "",
          pinfl: ""
        },
        signer_accountant: {
          position: "",
          last_name: "",
          first_name: "",
          middle_name: "",
          inn: "",
          pinfl: ""
        },
        signer_warehouse: {
          position: "",
          last_name: "",
          first_name: "",
          middle_name: "",
          inn: "",
          pinfl: ""
        }
      },
      receiver: {
        receiver_info: {
          inn: ""
        }
      },
      document: {
        deliverer: "",
        deliverer_inn: "",
        deliverer_passport_serial_char: "",
        deliverer_passport_serial_number: "",
        position: "",
        issued_by: "",
        date_of_issue: "",
        document_number: "",
        document_date: "",
        document_values_for: "",
        document_valid_date: "",
        contract_number: "",
        contract_date: "",
        items: [
          {
            // temp
            productSelected: {},
            // /temp
            item_number: "1",
            title: "",
            measurement_unit: "",
            quantity: "",
            catalog: {
              code: "",
              name: ""
            },
            barcode: "",
            lgota_id: ""
          }
        ]
      }
    },
    powerOfAttorneyModelTemp: {
      sender_info: {
        id: 1
      },
      approver_and_signers: {
        signer_director: {
          position: "",
          last_name: "",
          first_name: "",
          middle_name: "",
          inn: "",
          pinfl: ""
        },
        signer_accountant: {
          position: "",
          last_name: "",
          first_name: "",
          middle_name: "",
          inn: "",
          pinfl: ""
        },
        signer_warehouse: {
          position: "",
          last_name: "",
          first_name: "",
          middle_name: "",
          inn: "",
          pinfl: ""
        }
      },
      receiver: {
        receiver_info: {
          inn: ""
        }
      },
      document: {
        deliverer: "",
        deliverer_inn: "",
        deliverer_passport_serial_char: "",
        deliverer_passport_serial_number: "",
        position: "",
        issued_by: "",
        date_of_issue: "",
        document_number: "",
        document_date: "",
        document_values_for: "",
        document_valid_date: "",
        contract_number: "",
        contract_date: "",
        items: [
          {
            // temp
            productSelected: {},
            // /temp
            item_number: "1",
            title: "",
            measurement_unit: "",
            quantity: "",
            catalog: {
              code: "",
              name: ""
            },
            barcode: "",
            lgota_id: ""
          }
        ]
      }
    }
  },
  actions: {
    actionCreateDocument({ commit }, model) {
      return new Promise((resolve, reject) => {
        if (model.document.items && model.document.items.length){
          model.document.items.forEach(item => {
            if (item.hasOwnProperty('productSelected')){
              delete item.productSelected
            }
            if (item.hasOwnProperty('measurementSelected')){
              delete item.measurementSelected
            }
          })
        }
        fetchCreatePowerOfAttorneyRoaming(model)
          .then(res => {
            resolve(res)
          })
          .catch(err => {
            reject(err)
          })
      })
    }
  }
}
