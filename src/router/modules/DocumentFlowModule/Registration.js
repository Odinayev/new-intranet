const Registration = [
  {
    path: "registration",
    name: "RegistrationIndex",
    meta: {
      breadcrumb: true,
      title: "Регистрация",
      isAuthRequired: true,
    },
    component: () => import(/* webpackChunkName: "BoxesIndex" */ "@/modules/DocumentFlow/views/RegistrationNew"),
    redirect: {name: "IncomingIndex"},
    children: [
      // Входящий
      {
        path: "incoming",
        name: "IncomingIndex",
        meta: {
          breadcrumb: true,
          title: "Входящие",
          isAuthRequired: true,
        },
        component: () => import(/* webpackChunkName: "ReviewIndex" */ "@/modules/DocumentFlow/views/RegistrationNew/Incoming"),
      },
      // Внутренние
      {
        path: "inner",
        name: "InnerIndex",
        meta: {
          breadcrumb: true,
          title: "Внутренние",
          isAuthRequired: true,
        },
        component: () => import(/* webpackChunkName: "ReviewIndex" */ "@/modules/DocumentFlow/views/RegistrationNew/Inner"),
      },
      // Исходящие
      {
        path: "outgoing",
        name: "OutgoingIndex",
        meta: {
          breadcrumb: true,
          title: "Исходящие",
          isAuthRequired: true,
        },
        component: () => import(/* webpackChunkName: "ReviewIndex" */ "@/modules/DocumentFlow/views/RegistrationNew/Outgoing"),
      },
      // Обращения
      {
        path: "appeal",
        name: "AppealIndex",
        meta: {
          breadcrumb: true,
          title: "Обращения",
          isAuthRequired: true,
        },
        component: () => import(/* webpackChunkName: "ReviewIndex" */ "@/modules/DocumentFlow/views/RegistrationNew/Appeal"),
      },
      // Call center обращения
      {
        path: "call-center-appeal",
        name: "CallCenterAppealIndex",
        meta: {
          breadcrumb: true,
          title: "Call-центр обращения",
          isAuthRequired: true,
        },
        component: () => import(/* webpackChunkName: "CallCenterAppealIndex" */ "@/modules/DocumentFlow/views/RegistrationNew/AppealCallCenter"),
      },
      // Приказы и распоряжения
      {
        path: "order-protocol",
        name: "OrderProtocolIndex",
        meta: {
          breadcrumb: true,
          title: "Приказы и распоряжения",
          isAuthRequired: true,
        },
        component: () => import(/* webpackChunkName: "ReviewIndex" */ "@/modules/DocumentFlow/views/RegistrationNew/OrderProtocol"),
      },
      // Входящие от филиалов
      {
        path: "filial-documents",
        name: "FilialDocumentsIndex",
        meta: {
          breadcrumb: true,
          title: "Входящие от филиалов",
          isAuthRequired: true,
        },
        component: () => import(/* webpackChunkName: "ReviewIndex" */ "@/modules/DocumentFlow/views/RegistrationNew/FilialDocuments"),
      },
      // Заявления
      {
        path: "application",
        name: "ApplicationIndex",
        meta: {
          breadcrumb: true,
          title: "Заявления",
          isAuthRequired: true,
        },
        component: () => import(/* webpackChunkName: "ReviewIndex" */ "@/modules/DocumentFlow/views/RegistrationNew/Application"),
      },
      // XDFU
      {
        path: "secret-documents",
        name: "SecretDocumentsIndex",
        meta: {
          breadcrumb: true,
          title: "XDFU",
          isAuthRequired: true,
        },
        component: () => import(/* webpackChunkName: "ReviewIndex" */ "@/modules/DocumentFlow/views/RegistrationNew/SecretDocuments"),
      },
    ]
  },
  {
    path: "registration/incoming-create",
    name: "IncomingRegistrationCreate",
    meta: {
      isAuthRequired: true,
    },
    component: () => import(/* webpackChunkName: "ReviewIndex" */ "@/modules/DocumentFlow/views/RegistrationNew/Incoming/Create"),
  },
  {
    path: "registration/incoming/:id",
    name: "IncomingRegistrationDetail",
    meta: {
      isAuthRequired: true,
    },
    component: () => import(/* webpackChunkName: "ReviewIndex" */ "@/modules/DocumentFlow/views/RegistrationNew/Incoming/Detail"),
  },
  {
    path: "registration/inner/:id",
    name: "InnerRegistrationDetail",
    meta: {
      isAuthRequired: true,
    },
    component: () => import(/* webpackChunkName: "ReviewIndex" */ "@/modules/DocumentFlow/views/RegistrationNew/Inner/Detail"),
  },
  {
    path: "registration/outgoing/:id",
    name: "OutgoingRegistrationDetail",
    meta: {
      isAuthRequired: true,
    },
    component: () => import(/* webpackChunkName: "ReviewIndex" */ "@/modules/DocumentFlow/views/RegistrationNew/Outgoing/Detail"),
  },
  {
    path: "registration/appeal/:id",
    name: "AppealRegistrationDetail",
    meta: {
      isAuthRequired: true,
    },
    component: () => import(/* webpackChunkName: "ReviewIndex" */ "@/modules/DocumentFlow/views/RegistrationNew/Appeal/Detail"),
  },
  {
    path: "registration/call-center-appeal/:id",
    name: "CallCenterAppealRegistrationDetail",
    meta: {
      isAuthRequired: true,
    },
    component: () => import(/* webpackChunkName: "ReviewIndex" */ "@/modules/DocumentFlow/views/RegistrationNew/AppealCallCenter/Detail"),
  },
  {
    path: "registration/order-protocol/:id",
    name: "OrderProtocolRegistrationDetail",
    meta: {
      isAuthRequired: true,
    },
    component: () => import(/* webpackChunkName: "ReviewIndex" */ "@/modules/DocumentFlow/views/RegistrationNew/OrderProtocol/Detail"),
  },
  {
    path: "registration/application/:id",
    name: "ApplicationRegistrationDetail",
    meta: {
      isAuthRequired: true,
    },
    component: () => import(/* webpackChunkName: "ReviewIndex" */ "@/modules/DocumentFlow/views/RegistrationNew/Application/Detail"),
  },
  {
    path: "registration/secret-documents-create",
    name: "SecretDocumentsRegistrationCreate",
    meta: {
      isAuthRequired: true,
    },
    component: () => import(/* webpackChunkName: "ReviewIndex" */ "@/modules/DocumentFlow/views/RegistrationNew/SecretDocuments/Create"),
  },
  {
    path: "registration/secret-documents/:id",
    name: "SecretDocumentsRegistrationDetail",
    meta: {
      isAuthRequired: true,
    },
    component: () => import(/* webpackChunkName: "ReviewIndex" */ "@/modules/DocumentFlow/views/RegistrationNew/SecretDocuments/Detail"),
  },
]

export default Registration
