
export const CreditMonitoring = [
	{
		path: "monitoring",
		name: "CreditMonitoring",
		component: () => import("@/modules/CreditMonitoring"),
		children: [
			{
				path: "head",
				name: "CreditMonitoringHead",
				meta: {
					isAuthRequired: true
				},
				component: () => import("@/modules/CreditMonitoring/Head")
			},
			{
				path: "head/branch/:id/:parentId",
				name: "ShowBranchFolder",
				meta: {
					isAuthRequired: true
				},
				component: () => import("@/modules/CreditMonitoring/Head/showBranchFolder")
			},
			{
				path: "head/sub_folder/:id/:parentId",
				name: "ShowBranchFolderNested",
				meta: {
					isAuthRequired: true
				},
				component: () => import("@/modules/CreditMonitoring/Head/showBranchFolderNested")
			},
			{
				path: "head/files/:folderId/:parentId",
				name: "ShowBranchFolderNestedFiles",
				meta: {
					isAuthRequired: true
				},
				component: () => import("@/modules/CreditMonitoring/Head/showBranchFolderNestedFiles")
			},

			// Branch
			{
				path: "branch",
				name: "CreditMonitoringBranch",
				meta: {
					isAuthRequired: true
				},
				component: () => import("@/modules/CreditMonitoring/Branch")
			},
			{
				path: "branch/:id/:parentId",
				name: "ShowFolderFiles",
				meta: {
					isAuthRequired: true
				},
				component: () => import("@/modules/CreditMonitoring/Branch/showFolderNested")
			},
			{
				path: "branch/files/:folderId/:parentId/",
				name: "ShowFolderNestedFiles",
				meta: {
					isAuthRequired: true
				},
				component: () => import("@/modules/CreditMonitoring/Branch/showFolderNestedFiles")
			},
		]
	}
]

export default CreditMonitoring
