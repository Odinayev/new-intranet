import { SET_ENTITY } from "@/store/mutation-types"
import CrudService from "@/services/crud.service"
import { getItemRow } from "@/common/helpers"
export default {
	namespaced: true,
	state: {
		commentsList: []
	},
	/*
	*
	* */
	actions: {
		/*
		* Получить список комментариев
		* */
		async fetchCommentsList({ commit, rootState }, payload) {
			await CrudService.getList(`action/comments/`, {
				obj: payload.obj,
				type: payload.type
			})
			.then((data) => {
				commit(SET_ENTITY, {
					module: "comment",
					entity: "commentsList",
					value: data.results
				}, { root: true })
			})
		},
		/**
		 * Добавить комментарий
		 * */
		async fetchSendComment({ commit, state }, payload) {
			await this.$axios.post(`action/comments/`, {
				obj: payload.obj,
				description: payload.description,
				type: payload.type
			})
			.then(({ data }) => {
				state.commentsList.push(data)
			})
		},
		/*
		* Изменить комментарий
		* */
		async fetchUpdateComment({ commit, state, dispatch }, payload) {
			await this.$axios.put(`action/comments/${payload.id}/`, {
				obj: payload.obj,
				description: payload.description,
				type: payload.type
			})
			.then(({ data }) => {
				let commentParent = getItemRow(payload.id, state.commentsList)
				commentParent.description = data.description
				commentParent.is_edited = data.is_edited
			})
		},
		/*
		* Ответить комментарий
		* */
		async fetchReplyComment({ commit, state }, payload) {
			await this.$axios.post(`action/comments/`, {
				obj: payload.obj,
				description: payload.description,
				type: payload.type,
				replied_to: payload.replied_to
			})
			.then(({ data }) => {
				let commentParent = getItemRow(payload.id, state.commentsList)
				commentParent.replies.push(data)
			})
		},
		/*
		* Загрузить файлы TODO: feature
		* */
		async fetchAttachFiles({ commit, state }, payload) {

		},
		/*
		* Открыть текстовое поле (изменить или ответить)
		* */
		openEditOrReplyArea({ state }, payload) {
			state.commentsList.forEach(comment => comment[payload.action] = false)
			payload.item[payload.action] = true
		},
		/*
		* Закрыть текстовое поле (изменить или ответить)
		* */
		closeEditOrReplyArea({ state }, payload) {
			payload.item[payload.action] = false
		},
	}
}
