import axios from "@/plugins/axios";

class CrudService {
	getList(url, params) {
		return new Promise((resolve, reject) => {
			axios.get(url, { params }).then(({ data }) => {
				resolve(data)
			}).catch((error) => {
				reject(error)
			})
		})
	}
	/*
	*
	* */
	post(url, data) {
		return new Promise((resolve, reject) => {
			axios.post(url, data).then(({ data }) => {
				resolve(data)
			}).catch((error) => {
				reject(error)
			})
		})
	}
	/*
	*
	* */
	put(url, data) {
		return new Promise((resolve, reject) => {
			axios.post(url, data).then(({ data }) => {
				resolve(data)
			}).catch((error) => {
				reject(error)
			})
		})
	}
	/*
	*
	* */
	delete(url, data) {
		return new Promise((resolve, reject) => {
			axios.delete(url, data).then(({ data }) => {
				resolve(data)
			}).catch((error) => {
				reject(error)
			})
		})
	}
}

export default new CrudService()
