import {SET_ENTITY} from "@/store/mutation-types";
import LoginWithERI from "../../views/Auth/LoginWithERI";
import socket from "./socket";

export default {
  namespaced: true,
  state: {
    isSearching: false,
    chatsSearchList: null,
    globallyUsersList: [],
    isConversationOpen: false,
    isUserDetailSidebarOpen: false,
    conversationUserSelected: {},
    conversationOpenWithUserSelect: {},
    conversationPersonalUsers: [],
    conversationUserMessages: [],
    chatAreaRef: null,
    scrollHeight: false,
    isScrollHeightChanged: false,
    conversationGroupList: [],
    chatText: "",
    selectedChat: {},
    isGroup: false,
    messageReadModel: {},
    callUnreadMessagesText: false,
    messageToBeEdited: "",
    messageToBeReplied: {},
    messageModelToBeEdited: {},
    groupChatMembers: [],
    isUserSelectForBotOpen: false,
    conversationBotList: [],
    isUsersLoading: false,
    isChatLoading: false,
  },
  /*
  *
  * */
  getters: {
    getIsSearching: (state) => state.isSearching,
    getIsGroup: (state) => state.isGroup,
    getChatsSearchList: (state) => state.chatsSearchList,
    getGloballyUsersList: (state) => state.globallyUsersList,
    getIsConversationOpen: (state) => state.isConversationOpen,
    getIsUserDetailSidebarOpen: (state) => state.isUserDetailSidebarOpen,
    getConversationUserSelected: (state) => state.conversationUserSelected,
    getConversationOpenWithUserSelect: (state) => state.conversationOpenWithUserSelect,
    getConversationPersonalUsers: (state) => state.conversationPersonalUsers,
    getConversationUserMessages: (state) => state.conversationUserMessages,
    getChatAreaRef: (state) => state.chatAreaRef,
    getScrollHeight: (state) => state.scrollHeight,
    getIsScrollHeightChanged: (state) => state.isScrollHeightChanged,
    getChatText: (state) => state.chatText,
    getConversationGroupList: (state) => state.conversationGroupList,
    getSelectedChat: (state) => state.selectedChat,
    getMessageReadModel: (state) => state.messageReadModel,
    getCallUnreadMessagesText: (state) => state.callUnreadMessagesText,
    getMessageToBeEdited: (state) => state.messageToBeEdited,
    getMessageToBeReplied: (state) => state.messageToBeReplied,
    getMessageModelToBeEdited: (state) => state.messageModelToBeEdited,
    getGroupChatMembers: (state) => state.groupChatMembers,
    getIsUserSelectForBotOpen: (state) => state.isUserSelectForBotOpen,
    getConversationBotList: (state) => state.conversationBotList,
    getIsUsersLoading: (state) => state.isUsersLoading,
    getIsChatLoading: (state) => state.isChatLoading,
  },
  /*
  *
  * */
  mutations: {
    'TOGGLE_SCROLL_HEIGHT_TO_FIND_BOTTOM'(state) {
      state.scrollHeight = !state.scrollHeight
    },
    'TOGGLE_SCROLL_HEIGHT'(state) {
      state.isScrollHeightChanged = !state.isScrollHeightChanged
    },
    'TOGGLE_SEARCH_OVERLAY'(state, payload) {
      state.isSearching = payload
    },
    /*
    *
    * */
    'SET_LIST_CHATS_SEARCH'(state, responses) {
      const [first, second] = responses

      state.chatsSearchList = {
        first: first.data,
        second: second.data
      }
    },
    /*
    *
    * */
    'UPDATE_IS_CONVERSATION_OPEN'(state, payload) {
      state.isConversationOpen = payload
    },
    /*
    *
    * */
    'TOGGLE_IS_USER_DETAIL_SIDEBAR_OPEN'(state, payload) {
      state.isUserDetailSidebarOpen = payload
    },
    /*
    *
    * */
    'SET_CONVERSATION_USER_SELECT'(state, payload) {
      state.conversationUserSelected = payload
    },
    /*
    *
    * */
    'SET_CONVERSATION_OPEN_WITH_USER_SELECT'(state, payload) {
      state.conversationOpenWithUserSelect = payload
    },
    /*
    *
    * */
    'SET_CONVERSATION_PERSONAL_USERS_LIST'(state, payload) {
      state.conversationPersonalUsers = payload
    },
    /*
    *
    * */
    'SET_CONVERSATION_USER_MESSAGES'(state, payload) {
      state.conversationUserMessages = [];
      setTimeout(() => {
        state.conversationUserMessages = payload;
        state.callUnreadMessagesText = !state.callUnreadMessagesText;
      }, 10)
    },
    /**/
    'CLEAR_CONVERSATION_USER_MESSAGES'(state) {
      state.conversationUserMessages = [];
    },
    /*
    *
    * */
    'MERGE_CONVERSATION_USER_MESSAGES'(state, payload) {
      state.conversationUserMessages.push(payload)
    },
    /*
    *
    * */
    'SET_CHAT_AREA_REF'(state, payload) {
      state.chatAreaRef = payload
    },
    /**/
    'CHANGE_UNREAD_MESSAGE'(state, payload) {
      state.conversationUserMessages.forEach(item => {
        if (item.id <= payload.message_id) {
          item.read_time = "read"
        }
      })
      state.conversationPersonalUsers.forEach(item => {
        if (item.id === payload.chat_id) {
          item.unread_message_count--
        }
      })
      state.conversationGroupList.forEach(item => {
        if (item.id === payload.chat_id) {
          item.unread_message_count--
        }
      })
      state.conversationBotList.forEach(item => {
        if (item.id === payload.chat_id) {
          item.unread_message_count--
        }
      })
    },
    /**/
    'SET_UNREAD_MESSAGE_COUNT'(state, payload) {
      state.conversationUserMessages.filter(item => item.read_time === null).length;
    },
    /**/
    'DELETE_MESSAGE'(state, payload) {
      const index = state.conversationUserMessages.findIndex(item => item.id === payload.message_id);

      if (index > 0) {
        state.conversationUserMessages.splice(index, 1)

      }
    },
    /**/
    'EDIT_MESSAGE'(state, payload) {
      const index = state.conversationUserMessages.findIndex(item => item.id === payload.message_id);

      if (index > 0) {
        state.conversationUserMessages[index].text = payload.text;
        state.conversationUserMessages[index].edited = true
      }
    },
  },
  /*
  *
  * */
  actions: {
    chatAreaInstance({commit}, payload) {
      commit("SET_CHAT_AREA_REF", payload)

      // payload.scrollTop = payload.scrollHeight
    },
    /*
    *
    * */
    fetchSearchUsers({commit}, payload) {
      const [writtenByUsers, generalSearchResult, foundMessages] = payload.api

      this.$axios.all([
        this.$axios.get(writtenByUsers, {
          params: {
            search: payload.value,
            page_size: 100
          }
        }),
        this.$axios.get(generalSearchResult, {
          params: {
            search: payload.value,
            page_size: 100
          }
        })
      ])
        .then(this.$axios.spread((...responses) => {
          commit("SET_LIST_CHATS_SEARCH", responses)
        }))
    },
    /*
    *
    * */
    startPrivateChat({commit, rootGetters, dispatch}, payload) {
      dispatch("fetchClearConversationUserMessages");
      this.$axios.post('/chats/private/', {
        members: [payload.id]
      })
        .then(({data}) => {
          if (rootGetters["socket/getChatSocket"]) {
            rootGetters["socket/getChatSocket"].onCloseChannel()
          }
          commit("SET_CONVERSATION_USER_SELECT", data)
          commit("SET_CONVERSATION_OPEN_WITH_USER_SELECT", data)

          dispatch(
            "socket/chatSocket",
            this.$socket.init(process.env.VUE_APP_SOCKET_URL, this.$jwt.getToken()),
            {root: true}
          )

          rootGetters["socket/getChatSocket"].onOpen({
            command: "chat_handshake",
            chat_id: data.id,
            chat_type: data.type
          })

          rootGetters["socket/getChatSocket"].onMessageReceive()

        })
        .finally(() => {
          commit("UPDATE_IS_CONVERSATION_OPEN", true)
        })
    },
    /*
    *
    * */
    openPrivateChat({commit, dispatch, rootGetters}, payload) {
      this.$axios.get(`/chats/${payload.type === 'private' ? 'private' : 'group'}/${payload.id}/`)
        .then(({data}) => {
          if (rootGetters["socket/getChatSocket"]) {
            rootGetters["socket/getChatSocket"].onCloseChannel()
          }

          commit("SET_CONVERSATION_USER_SELECT", data)
          commit("SET_CONVERSATION_OPEN_WITH_USER_SELECT", data)

          dispatch(
            "socket/chatSocket",
            this.$socket.init(process.env.VUE_APP_SOCKET_URL, this.$jwt.getToken()),
            {root: true}
          );

          rootGetters["socket/getChatSocket"].onOpen({
            command: "chat_handshake",
            chat_id: data.id,
            chat_type: data.type
          })

          rootGetters["socket/getChatSocket"].onMessageReceive()

          dispatch('fetchConversationUserMessages', payload)
        })
        .finally(() => {
          commit("UPDATE_IS_CONVERSATION_OPEN", true);
        })
    },

    fetchGetBot({ commit }, payload){
      return new Promise((resolve, reject) => {
        this.$axios.get(`/chats/bot/`).then(({data}) => {
          commit(SET_ENTITY, {
            module: "chatModule",
            entity: "conversationBotList",
            value: data.results
          }, { root: true })
          resolve(data);
        })
          .catch(err => {
            reject(err)
          })
      })
    },

    fetchStartBot({ commit, dispatch, rootGetters }, payload){
      dispatch("fetchClearConversationUserMessages");
      return new Promise((resolve, reject) => {
        this.$axios.post('/chats/bot/', {
          members: [9749]
        })
          .then(({data}) => {
            resolve(data)
            if (rootGetters["socket/getChatSocket"]) {
              rootGetters["socket/getChatSocket"].onCloseChannel()
            }
            commit("SET_CONVERSATION_USER_SELECT", data)
            commit("SET_CONVERSATION_OPEN_WITH_USER_SELECT", data)

            dispatch(
              "socket/chatSocket",
              this.$socket.init(process.env.VUE_APP_SOCKET_URL, this.$jwt.getToken()),
              {root: true}
            )

            rootGetters["socket/getChatSocket"].onOpen({
              command: "chat_handshake",
              chat_id: data.id,
              chat_type: data.type
            })

            rootGetters["socket/getChatSocket"].onMessageReceive()

          })
          .catch(err => {
            reject(err)
          })
          .finally(() => {
            commit("UPDATE_IS_CONVERSATION_OPEN", true);
            dispatch('fetchGetBot')
          })
      })
    },

    fetchOpenBot({ commit, rootGetters, dispatch, state }, payload){
      dispatch('fetchGetBot').then(data => {
        if (data.count > 0){
          this.$axios.get(`/chats/bot/${data.results[0].id}/`)
            .then(({data}) => {
              if (rootGetters["socket/getChatSocket"]) {
                rootGetters["socket/getChatSocket"].onCloseChannel()
              }

              commit("SET_CONVERSATION_USER_SELECT", data)
              commit("SET_CONVERSATION_OPEN_WITH_USER_SELECT", data)

              dispatch(
                "socket/chatSocket",
                this.$socket.init(process.env.VUE_APP_SOCKET_URL, this.$jwt.getToken()),
                {root: true}
              );

              rootGetters["socket/getChatSocket"].onOpen({
                command: "chat_handshake",
                chat_id: data.id,
                chat_type: data.type
              })

              rootGetters["socket/getChatSocket"].onMessageReceive()
              dispatch('fetchConversationUserMessages', { id: data.id })

            })
            .finally(() => {
              commit("UPDATE_IS_CONVERSATION_OPEN", true)
            })
        }else {
          dispatch('fetchStartBot')
        }
      })
    },

    fetchOpenGroupChat({ commit }, payload){
      this.$axios.get(`/chats/members/`, {
        params: {
          chat_id: payload.id,
          page_size: 100
        }
      })
        .then(({ data }) => {
          commit(SET_ENTITY, {
            module: "chatModule",
            entity: "groupChatMembers",
            value: data.results
          }, {root: true})
        })
    },
    /*
    *
    * */
    openUserDetailSidebar({commit}, payload) {
      commit("TOGGLE_IS_USER_DETAIL_SIDEBAR_OPEN", payload)
    },
    /*
    *
    * */
    fetchConversationUserMessages({commit, dispatch}, payload) {
      commit(SET_ENTITY, { module: "chatModule", entity: "isChatLoading", value: true }, { root: true })
      this.$axios.get('/messages/', {
        params: {
          chat: payload.id,
          page_size: 1000,
          ordering: "created_date"
        }
      })
        .then(({data}) => {

          let array = [];
          let is_new_date = false

          if (data.results.length){
            for (let i = 0; i < data.results.length; i++){
              if (i === 0){
                is_new_date = true
              }
              if (i > 0){
                is_new_date = new Date(data.results[i].created_date).getDate() !== new Date(data.results[i - 1].created_date).getDate();
              }
              array.push({
                ...data.results[i],
                is_new_date: is_new_date
              })
            }
          }
          commit("SET_CONVERSATION_USER_MESSAGES", array)
        })
        .finally(() => {
          commit(SET_ENTITY, { module: "chatModule", entity: "isChatLoading", value: false }, { root: true })
        })
    },
    /*
    *
    * */
    mergeConversationUserMessages({commit, state}, payload) {
      let name = "";
      let model = {};
      let is_new_date = false;

      if (state.conversationUserMessages.length && new Date(state.conversationUserMessages[state.conversationUserMessages.length - 1].created_date).getDate() !== new Date(payload.created_date).getDate()){
        is_new_date = true
      }else if (state.conversationUserMessages.length === 0){
        is_new_date = true
      }

      if (payload.replied_to) {
        name = payload.replied_to.sender.first_name + ' ' + payload.replied_to.sender.last_name;
        model = {
          ...payload,
          id: payload.message_id,
          chat: payload.id,
          read_time: null,
          is_new_date: is_new_date,
          replied_to: {
            ...payload.replied_to,
            sender: {
              ...payload.replied_to.sender,
              name: name
            }
          }
        }
      }else {
        model = {
          ...payload,
          id: payload.message_id,
          chat: payload.id,
          read_time: null,
          is_new_date: is_new_date,
        }
      }
      commit("MERGE_CONVERSATION_USER_MESSAGES", model)
    },
    /*
    *
    * */
    fetchConversationUsersList({commit}) {
      commit(SET_ENTITY, { module: "chatModule", entity: "isUsersLoading", value: true }, { root: true })
      this.$axios.get('/chats/private/', {
        params: {
          page_size: 100
        }
      })
        .then(({data}) => {
          commit("SET_CONVERSATION_PERSONAL_USERS_LIST", data.results);
        })
        .finally(() => {
          commit(SET_ENTITY, { module: "chatModule", entity: "isUsersLoading", value: false }, { root: true })
        })
    },
    /**/
    fetchConversationGroupList({commit}) {
      commit(SET_ENTITY, { module: "chatModule", entity: "isUsersLoading", value: true }, { root: true })
      this.$axios.get(`/chats/group/`)
        .then(({data}) => {
          commit(SET_ENTITY, {
            module: "chatModule",
            entity: "conversationGroupList",
            value: data.results
          }, {root: true})
        })
        .finally(() => {
          commit(SET_ENTITY, { module: "chatModule", entity: "isUsersLoading", value: false }, { root: true })
        })
    },
    /*
    *
    * */
    createChatGroup({commit}, payload) {
      return new Promise((resolve, reject) => {
        this.$axios.post(`/chats/group/`, payload)
          .then(({data}) => {
            resolve(data);
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    /**/
    fetchSetScrollHeight({commit}, payload) {
      commit(SET_ENTITY, {
        module: "chatModule",
        entity: "scrollHeight",
        value: payload
      }, {root: true})
    },
    fetchToggleScrollHeight({commit}) {
      commit('TOGGLE_SCROLL_HEIGHT')
    },
    /**/
    fetchToggleScrollHeightToFindBottom({commit}) {
      commit('TOGGLE_SCROLL_HEIGHT_TO_FIND_BOTTOM')
    },
    /**/
    fetchSetChatText({commit}, payload) {
      commit(SET_ENTITY, {
        module: "chatModule",
        entity: "chatText",
        value: payload
      }, {root: true})
    },
    /**/
    fetchClearConversationUserMessages({commit}) {
      commit("CLEAR_CONVERSATION_USER_MESSAGES")
    },
    fetchSetSelectedChat({commit}, payload) {
      commit(SET_ENTITY, {
        module: "chatModule",
        entity: "selectedChat",
        value: payload
      }, {root: true})
    },
    /**/
    fetchSetIsGroup({commit}, payload) {
      commit(SET_ENTITY, {
        module: "chatModule",
        entity: "isGroup",
        value: payload
      }, {root: true})
    },
    /**/
    fetchAddMemberToGroup({commit}, payload) {
      return new Promise((resolve, reject) => {
        this.$axios.post(`/chats/members/`, payload)
          .then(({data}) => {
            resolve(data)
          })
          .catch(err => {
            console.log(err);
            reject(err)
          })
      })
    },
    /**/
    fetchSetMessageReadModel({commit}, payload) {
      commit(SET_ENTITY, {
        module: "chatModule",
        entity: "messageReadModel",
        value: payload
      }, {root: true});

      commit("CHANGE_UNREAD_MESSAGE", payload)
    },
    /**/
    fetchChangeUnreadMessageCount({commit}, payload) {
      commit("SET_UNREAD_MESSAGE_COUNT", payload)
    },
    /**/
    fetchDeleteMessage({commit, dispatch}, payload) {
      this.$axios.delete(`/messages/${payload.id}/`)
        .then(({data}) => {
          dispatch("fetchConversationUsersList")
        })
    },
    /**/
    fetchDeleteMessageFromSocket({commit}, payload) {
      commit('DELETE_MESSAGE', payload)
    },
    /**/
    fetchEditMessageFromSocket({commit}, payload) {
      commit('EDIT_MESSAGE', payload)
    },
    /**/
    fetchSetMessageToBeEdited({commit}, payload) {
      commit(SET_ENTITY, {
        module: "chatModule",
        entity: "messageModelToBeEdited",
        value: payload
      }, {root: true});

      commit(SET_ENTITY, {
        module: "chatModule",
        entity: "messageToBeEdited",
        value: payload.text
      }, {root: true});
    },
    /**/
    fetchEditMessage({state}, payload) {
      return new Promise(resolve => {
        this.$axios.put(`/messages/${state.messageModelToBeEdited.id}/`, {text: payload})
          .then(({data}) => {
            resolve(data)
          })
      })
    },
    /**/
    fetchSetMessageToBeReplied({commit}, payload) {
      console.log(payload)
      commit(SET_ENTITY, {
        module: "chatModule",
        entity: "messageToBeReplied",
        value: payload
      }, {root: true});

      commit(SET_ENTITY, {
        module: "chatModule",
        entity: "messageModelToBeEdited",
        value: payload
      }, {root: true});
    },
    /**/
    fetchClearChangingMessages({ commit }){
      commit(SET_ENTITY, {
        module: "chatModule",
        entity: "messageToBeReplied",
        value: ""
      }, {root: true});
      commit(SET_ENTITY, {
        module: "chatModule",
        entity: "messageToBeEdited",
        value: ""
      }, {root: true});
    },
    /**/
    fetchToggleBotUserSelect({ commit }, payload){
      commit(SET_ENTITY, {
        module: "chatModule",
        entity: "isUserSelectForBotOpen",
        value: payload
      }, { root: true })
    }
  },
}
