// const TOKEN = 'Access';
const TOKEN = 'Authorization' // Access;
const REFRESH = 'Refresh';

export const getItem = (name) => {
	return window.localStorage.getItem(name)
}

export const setItem = (name, value) => {
	return window.localStorage.setItem(name, value)
}

export const removeItem = (name) => {
	window.localStorage.removeItem(name);
};

export const 	getToken = () => {
	return window.localStorage.getItem(TOKEN);
};

export const getRefreshToken = () => {
	return window.localStorage.getItem(REFRESH);
};

export const saveToken = (access, refresh) => {
	window.localStorage.setItem(TOKEN, access);
	window.localStorage.setItem(REFRESH, refresh);
};

export const saveAccessToken = access => {
	window.localStorage.setItem(TOKEN, access);
};

export const removeAccessToken = () => {
	window.localStorage.removeItem(TOKEN);
};

export const removeToken = () => {
	window.localStorage.removeItem(TOKEN);
	window.localStorage.removeItem(REFRESH);
};

export default { getItem, setItem, removeItem, getToken, saveToken, removeToken, getRefreshToken, saveAccessToken, removeAccessToken };
