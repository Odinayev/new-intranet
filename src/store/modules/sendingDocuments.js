import { SET_ENTITY } from "@/store/mutation-types"
import CrudService from "@/services/crud.service"

export default {
  namespaced: true,
  state: {
    documentsList: [],
    getOneDocument: {}
  },
  /*
  *
  * */
  getters: {
    getDocumentsList: (state) => state.documentsList,
    getOneDocument: (state) => state.getOneDocument,
  },
  mutations: {
    /*
    *
    * */
  },
  /*
  *
  * */
  actions: {
    /*
    *
    * */
    fetchDocumentsList({ commit, rootState, dispatch }, payload) {
      commit(SET_ENTITY, { entity: "isLoading", value: true }, { root: true })
      CrudService.getList(`/docflow/${rootState.auth.currentUser.filial}/compose/`, {
        page_size: rootState.paginationOptions.page_size,
        ...payload
      })
        .then((data) => {
          let model = data.results.map(item => {
            return {
              ...item,
              __text: item.text && item.text.replace(/(<([^>]+)>)/gi, "")
            }
          })

          commit(SET_ENTITY, {
            module: "sendingDocuments",
            entity: "documentsList",
            value: model
          }, {root: true})

          dispatch("fetchPagination", data.count, {root: true})
        })
      .finally(() => {
        commit(SET_ENTITY, { entity: "isLoading", value: false }, { root: true })
      })
    },
    /*
    *
    * */
    fetchGetOneDocument({ commit, rootState }, payload) {
      return new Promise((resolve => {
        CrudService.getList(`/docflow/${rootState.auth.currentUser.filial}/compose/${payload.id}/`, {
          ...payload
        })
          .then((data) => {
            resolve(data)
            commit(SET_ENTITY, {
              module: "sendingDocuments",
              entity: "getOneDocument",
              value: data
            }, {root: true})
          })
      }))
    },
    /**/
    fetchDeleteDocument({ commit, rootState }, payload){
      return new Promise((resolve, reject) => {
        this.$axios.delete(`/docflow/${rootState.auth.currentUser.filial}/compose/${payload.id}/`)
          .then(({data}) => {
            resolve(data)
          })
          .catch(err => {
            reject(err)
          })
      })
    }
  }
}
