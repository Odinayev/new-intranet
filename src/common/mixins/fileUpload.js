export default {
	data() {
		return {
			mixinFileList: []
		}
	},
	methods: {
		$uploadFiles(files) {
			for (let i = 0; i < files.length; i++) {
				let formData = new FormData()
				formData.append("file", files[i])

				this.$axios
					.post('/upload/', formData)
					.then(({ data }) => {
						this.mixinFileList.push({
							document: data.id
						})
					})
			}
		}
	}
}
