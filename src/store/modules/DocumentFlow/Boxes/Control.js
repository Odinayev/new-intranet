import { SET_ENTITY } from "@/store/mutation-types"
import CrudService from "@/services/crud.service";

export default {
	namespaced: true,
	state: {
		indexTableCols: [
			{
				text: "#",
				value: "index",
				width: "50",
				active: true,
				sortable: false,
				align: "center",
				detail: false
			},
			{
				text: "priority",
				value: "document.priority",
				width: "150",
				active: true,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: "ui-priority",
				detail: true,
				order: 6
			},
			{
				text: "document-flow.views.boxes.journal",
				value: "document.journal.name",
				width: "135",
				active: true,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: null,
				detail: true,
				order: 2
			},
			{
				text: "components.document-type",
				value: "document.doc_type.name",
				width: "200",
				active: true,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: null,
				detail: true,
				order: 1
			},
			{
				text: "components.registration-number",
				value: "document.register_number",
				active: true,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: null,
				detail: true,
				order: 4
			},
			{
				text: "components.outgoing-number",
				value: "document.outgoing_number",
				active: false,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: null,
				detail: true,
				order: 7
			},
			{
				text: "document-flow.components.registration-date",
				value: "document.register_date",
				active: true,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: "ui-date-text",
				detail: true,
				order: 5
			},
			{
				text: "document-flow.components.outgoing-date",
				value: "document.outgoing_date",
				active: false,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: "ui-date-text",
				detail: true,
				order: 8
			},
			{
				text: "document-flow.views.registration.deadline",
				value: "assignment.deadline",
				active: true,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: "ui-countdown",
				detail: true,
				order: 66
			},
			{
				text: "document-flow.components.correspondent",
				value: "document.correspondent.organization_name",
				width: "400",
				active: true,
				sortable: false,
				cols: {
					"md": "12"
				},
				component: null,
				detail: true,
				order: 77
			},
			// {
			// 	text: "document-flow.components.for-consideration",
			// 	value: "document.review_users",
			// 	width: "200",
			// 	active: true,
			// 	sortable: false,
			// 	cols: {
			// 		"md": "4"
			// 	},
			// 	component: null,
			// 	detail: true,
			// 	order: 88
			// },
			{
				text: "document-flow.views.boxes.content",
				value: "document.description",
				active: false,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: null,
				detail: true,
				order: 99
			},
			// {
			// 	text: "document-flow.views.registration.state",
			// 	value: "document.condition",
			// 	width: "100",
			// 	active: true,
			// 	sortable: false,
			// 	cols: {
			// 		"md": "4"
			// 	},
			// 	component: "ui-status",
			// 	detail: true,
			// 	order: 100
			// },
			{
				text: "components.status",
				value: "status.name",
				width: "140",
				sortable: false,
				active: true,
				cols: {
					"md": "4"
				},
				component: "ui-status",
				detail: true,
				order: 3
			},
		],
		indexTableFilters: {
			search: null,
			priority: null,
			condition: null,
			journal: null,
			register_number: null,
			register_start_date: null,
			register_end_date: null,
			__register_date: null,
			outgoing_number: null,
			outgoing_start_date: null,
			outgoing_end_date: null,
			__outgoing_date: null,
			correspondent: null,
			correspondent_name: null,
			__correspondent: null,
			review_user: null,
			review_user_name: null,
			__review_user: null,
			page: 1,
			page_size: 15
		},
		controlDetailModelLoader: true,
		controlDetailModel: null,
		removeFromControlModel: {
			file: null,
			performance_note: null
		}
	},
	/**
	 *
	 **/
	actions: {
		/*
		*
		* */
		async fetchControlById({ commit, rootState }, payload) {
			if(payload.loader) {
				commit(SET_ENTITY, {
					module: "documentFlowBoxesControl",
					entity: "controlDetailModelLoader",
					value: true
				}, {root: true})
			}

			await CrudService.getList(`v2/docflow/${rootState.auth.currentUser.filial}/my-control/${payload.id}/`)
			.then((data) => {
				commit(SET_ENTITY, {
					module: "documentFlowBoxesControl",
					entity: "controlDetailModel",
					value: data
				}, {root: true})

				setTimeout(() => {
					commit(SET_ENTITY, {
						module: "documentFlowBoxesControl",
						entity: "controlDetailModelLoader",
						value: false
					}, {root: true})
				}, 1000)
			})
		},
		/*
		* Изменить статус документа (На контроль)
		* */
		async fetchUpdateControlStatus({ commit, rootState, state, dispatch }, payload) {
			await this.$axios.put(`/v2/docflow/${rootState.auth.currentUser.filial}/my-control/${payload.documentId}/change-status/`, {
				status: payload.status,
				comment: payload.comment
			})
			.then(({ data }) => {
				state.controlDetailModel.status = data.status

				dispatch(
					"treeView/fetchTreeUsersList",
					{ treeId: state.controlDetailModel.document.id },
					{ root: true }
				)
			})
		},
		/*
		* Ознакомиться с документом (На контроль)
		* */
		async fetchControlAcquaintWithDocument({ commit, rootState, state, dispatch }, payload) {
			await this.$axios.put(`/v2/docflow/${rootState.auth.currentUser.filial}/my-control/${payload.documentId}/acquaint/`)
			.then(({ data }) => {
				state.controlDetailModel.read_time = data.read_time

				dispatch(
					"treeView/fetchTreeUsersList",
					{ treeId: state.controlDetailModel.document.id },
					{ root: true }
				)
			})
		},
		/*
		* Снять с контроля
		* */
		async fetchRemoveFromControl({ commit, rootState, state, dispatch }, payload) {
			await this.$axios.put(
				`v2/docflow/${rootState.auth.currentUser.filial}/my-control/${payload.id}/remove-from-control/`,
				state.removeFromControlModel
			)
			.then(({ data }) => {
				dispatch(
					"fetchControlById",
					{ id: state.controlDetailModel.id, loader: false }
				)

				dispatch(
					"treeView/fetchTreeUsersList",
					{ treeId: state.controlDetailModel.document.id },
					{ root: true }
				)
			})
		},
		/*
		*
		* */
		async fetchGetCreatedRemoveFromControl({ state, dispatch }) {
			dispatch(
				"fetchControlById",
				{ id: state.controlDetailModel.id, loader: false }
			)

			state.removeFromControlModel.file = state.controlDetailModel.file
			state.removeFromControlModel.performance_note = state.controlDetailModel.performance_note
		},
		/*
		* Заполняем нужные поля колонки таблицы если пользователь перезагрузил браузер
		* */
		setActiveIndexTableCols({ commit }, payload) {
			commit(SET_ENTITY, { module: "documentFlowBoxesControl", entity: "indexTableCols", value: payload }, { root: true })
		},
		/*
		*
		* */
		resetIndexTableCols({ state }) {
			state.indexTableCols = [
				{
					text: "#",
					value: "index",
					width: "50",
					active: true,
					sortable: false,
					align: "center",
					detail: false
				},
				{
					text: "priority",
					value: "document.priority",
					width: "150",
					active: true,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: "ui-priority",
					detail: true,
					order: 6
				},
				{
					text: "document-flow.views.boxes.journal",
					value: "document.journal.name",
					width: "135",
					active: true,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: null,
					detail: true,
					order: 2
				},
				{
					text: "components.document-type",
					value: "document.doc_type.name",
					width: "200",
					active: true,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: null,
					detail: true,
					order: 1
				},
				{
					text: "components.registration-number",
					value: "document.register_number",
					active: true,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: null,
					detail: true,
					order: 4
				},
				{
					text: "components.outgoing-number",
					value: "document.outgoing_number",
					active: false,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: null,
					detail: true,
					order: 7
				},
				{
					text: "document-flow.components.registration-date",
					value: "document.register_date",
					active: true,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: "ui-date-text",
					detail: true,
					order: 5
				},
				{
					text: "document-flow.components.outgoing-date",
					value: "document.outgoing_date",
					active: false,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: "ui-date-text",
					detail: true,
					order: 8
				},
				{
					text: "document-flow.views.registration.deadline",
					value: "assignment.deadline",
					active: true,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: null,
					detail: true,
					order: 66
				},
				{
					text: "document-flow.components.correspondent",
					value: "document.correspondent.organization_name",
					width: "400",
					active: true,
					sortable: false,
					cols: {
						"md": "12"
					},
					component: null,
					detail: true,
					order: 77
				},
				// {
				// 	text: "document-flow.components.for-consideration",
				// 	value: "document.review_users",
				// 	width: "200",
				// 	active: true,
				// 	sortable: false,
				// 	cols: {
				// 		"md": "4"
				// 	},
				// 	component: null,
				// 	detail: true,
				// 	order: 88
				// },
				{
					text: "document-flow.views.boxes.content",
					value: "document.description",
					active: false,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: null,
					detail: true,
					order: 99
				},
				// {
				// 	text: "document-flow.views.registration.state",
				// 	value: "document.condition",
				// 	width: "100",
				// 	active: true,
				// 	sortable: false,
				// 	cols: {
				// 		"md": "4"
				// 	},
				// 	component: "ui-status",
				// 	detail: true,
				// 	order: 100
				// },
				{
					text: "components.status",
					value: "status.name",
					width: "140",
					sortable: false,
					active: true,
					cols: {
						"md": "4"
					},
					component: "ui-status",
					detail: true,
					order: 3
				},
			]
		},
		/*
		*
		* */
		async resetIndexTableFilters({ state }) {
			state.indexTableFilters = {
				search: null,
				priority: null,
				condition: null,
				journal: null,
				register_number: null,
				register_start_date: null,
				register_end_date: null,
				outgoing_number: null,
				outgoing_start_date: null,
				outgoing_end_date: null,
				correspondent: null,
				correspondent_name: null,
				__correspondent: null,
				__register_date: null,
				__outgoing_date: null,
				review_user: null,
				review_user_name: null,
				__review_user: null,
				page: 1,
				page_size: 15
			}
		},
		/*
		* Заполняем нужные поля фильтра если пользователь перезагрузил браузер
		* */
		setActiveIndexTableFilters({ commit, state }, payload) {
			Object.entries(payload).forEach(item => {
				const [key, value] = item

				if(value && (typeof value === "boolean" || Array.isArray(value))) {
					state.indexTableFilters[key] = value
				}

				if(!(value && (typeof value === "boolean"))) {
					state.indexTableFilters[key] = value
				}

				if(value && (typeof value === "number" || typeof value === "string")) {
					state.indexTableFilters[key] = Number(value) ? Number(value) : value
				}

				if(value === "true" || value === "false") {
					state.indexTableFilters[key] = value === "true"
				}
			})

			Object.entries(state.indexTableFilters).forEach(item => {
				const [key, value] = item

				if(value && key === "register_start_date") {
					state.indexTableFilters.__register_date = [state.indexTableFilters.register_start_date, state.indexTableFilters.register_end_date]
				}

				if(value && key === "outgoing_start_date") {
					state.indexTableFilters.__outgoing_date = [state.indexTableFilters.outgoing_start_date, state.indexTableFilters.outgoing_end_date]
				}

				if(value && key === "correspondent_name") {
					state.indexTableFilters.__correspondent = {
						id: state.indexTableFilters.correspondent,
						organization_name: state.indexTableFilters.correspondent_name
					}
				}

				if(value && key === "review_user_name") {
					state.indexTableFilters.__review_user = {
						id: state.indexTableFilters.review_user,
						full_name: state.indexTableFilters.review_user_name
					}
				}
			})
		}
	},
}
