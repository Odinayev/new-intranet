
export default [
	{
		text: "#",
		value: "index",
		width: "50",
		active: true,
		sortable: false,
		align: "center",
	},
	{
		text: "document-flow.views.send-document.edit-date",
		value: "created_date",
		active: true,
		sortable: false
	},
	{
		text: "users",
		value: "created_by.full_name",
		active: true,
		sortable: false
	},
	{
		text: "document-flow.components.action",
		value: "description",
		active: true,
		sortable: false
	},
	{
		text: "document-flow.views.registration.initial-value",
		value: "original_value",
		active: true,
		sortable: false
	},
	{
		text: "document-flow.views.registration.new-value",
		value: "new_value",
		active: true,
		sortable: false
	},
	{
		text: "ip-address",
		value: "ip_addr",
		width: "200",
		active: true,
		sortable: false
	},
]
