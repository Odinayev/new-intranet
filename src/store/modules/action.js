import { SET_ENTITY } from "@/store/mutation-types"
import CrudService from "@/services/crud.service"
import { getItemRow } from "../../common/helpers";

export default {
	namespaced: true,
	state: {
		activitiesList: [],
		commentsList: [],
		loadActivityCount: 1,
		loadCommentsCount: 1,
		dataCount: null
	},
	/*
	*
	* */
	getters: {
		getActivitiesList: (state) => state.activitiesList,
		getCommentsList: (state) => state.commentsList,
		getLoadActivityCount: (state) => state.loadActivityCount,
		getLoadCommentsCount: (state) => state.loadCommentsCount,
		getDataCount: (state) => state.dataCount,
	},
	/*
	*
	* */
	mutations: {
		"LOAD_MORE_COMMENTS"(state, payload) {
			if (!payload.load_more) {
				state.commentsList = []
			}
			state.commentsList.push(...payload.results)
		},
		/*
		*
		* */
		"SET_NEW_COMMENT"(state, payload) {
			state.commentsList.unshift(payload)
		},
		/*
		*
		* */
		"COMMENT_COUNT"(state) {
			state.loadCommentsCount = state.loadCommentsCount + 1
		},
		/**/
		"COMMENT_COUNT_TO_ONE"(state) {
			state.loadCommentsCount = 1
		},
		/*
		*
		* */
		"SET_ACTIVITIES_LIST"(state, payload){
			state.activitiesList = []
			state.activitiesList = payload
		},
		/**/
		"LOAD_MORE_ACTIVITY"(state, payload) {

			state.activitiesList.push(...payload)
		},
		/*
		*
		* */
		"ACTIVITY_COUNT"(state) {
			state.loadActivityCount = state.loadActivityCount + 1
		},
		/*
		*
		* */
		"CLEAR_HISTORY"(state) {
			state.activitiesList = []
			state.commentsList = []
			state.loadActivityCount = 1
		},
		"UPDATE_COMMENTS_LIST"(state, payload) {
			state.commentsList = payload
		}
	},
	/*
	*
	* */
	actions: {
		/*
		*
		* */
		fetchActivitiesList({ commit, rootGetters }, { obj }) {
			CrudService.getList(`/action/activities/`, {
				obj,
				page: rootGetters["action/getLoadActivityCount"]
			})
				.then(( data) => {
					commit("SET_ACTIVITIES_LIST", data.results)
				})
		},
		/*
		*
		* */
		fetchLoadMoreActivityList({ commit, rootGetters, dispatch }, {obj}) {
			commit("ACTIVITY_COUNT")

			CrudService.getList(`/action/activities/`, {
				obj,
				page: rootGetters["action/getLoadActivityCount"]
			})
				.then(( data) => {
					commit("LOAD_MORE_ACTIVITY", data.results)
				})
		},
		/*
		*
		* */
		fetchNewComments({ commit, dispatch, state }, payload) {
			return new Promise((resolve) => {
				CrudService.post(`/action/comments/`, payload)
					.then((data) => {
						if (!payload.replied_to){
							commit("SET_NEW_COMMENT", data)
						}else {
							 getItemRow(payload.replied_to, state.commentsList).replies.push(data)
						}
						resolve()
					})
			})
		},
		/*
		*
		* */
		fetchCommentsList({ commit, rootGetters }, { obj, load_more, type }) {
			return new Promise((resolve) => {
				CrudService.getList(`/action/comments/`, {
					obj,
					type: type ? type : null,
					page: rootGetters["action/getLoadCommentsCount"]
				})
					.then((data) => {
						commit(SET_ENTITY, { module: "action", entity: "dataCount", value: data.count }, { root: true })
						if (load_more){
							commit("LOAD_MORE_COMMENTS", {
								results: data.results,
								load_more: load_more
							})
						}else {
							commit("LOAD_MORE_COMMENTS", {
								results: data.results
							})
						}

						resolve(data)
					})
			})
		},
		/*
		*
		* */
		fetchLoadMoreCommentsList({ commit, dispatch }, obj) {
			commit("COMMENT_COUNT")

			dispatch("fetchCommentsList", obj)
		},
		/*
		*
		* */
		fetchClearHistory({ commit }) {
			commit("CLEAR_HISTORY")
		},
		updateCommentsList({ commit }, commentsList) {
			commit('UPDATE_COMMENTS_LIST', commentsList)
		},
	},
}
