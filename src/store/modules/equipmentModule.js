import { SET_ENTITY } from "@/store/mutation-types"
import CrudService from "@/services/crud.service"

export default {
  namespaced: true,
  state: {
    departmentList: [],
    departmentUsersList: [],
    equipmentsList: [],
    isEquipmentsLoading: false,
    isDepartmentsLoading: false,
    correspondentList: [],
    resolutionContentList: [],
    addNewsList: [],
    groupStaffList: [],
    restartLoading: false,
    shortDescriptionList: []
  },
  /*
  *
  * */
  getters: {
    getDepartmentList: (state) => state.departmentList,
    departmentUsersList: (state) => state.departmentUsersList,
    getEquipmentsList: (state) => state.equipmentsList,
    getIsEquipmentsLoading: (state) => state.isEquipmentsLoading,
    getIsDepartmentsLoading: (state) => state.isDepartmentsLoading,
    getCorrespondentList: (state) => state.correspondentList,
    getResolutionContentList: (state) => state.resolutionContentList,
    getAddNewsList: (state) => state.addNewsList,
    getGroupStaffList: (state) => state.groupStaffList,
    getRestartLoading: (state) => state.restartLoading,
  },
  mutations: {
    "PUSH_LIST"(state, { entity, value }) {
      state[entity].push(...value)
    },
    /*
    *
    * */
    "CLEAR_LIST"(state, { entity }) {
      state[entity] = []
    },
    /**/
    SET_EQUIPMENTS_LOADER(state, payload){
      state.isEquipmentsLoading = payload
    },
    /**/
    SET_DEPARTMENTS_LOADER(state, payload){
      state.isDepartmentsLoading = payload
    }
  },
  /*
  *
  * */
  actions: {
    fetchEquipmentsList({ commit, rootState }, payload){
      commit("SET_EQUIPMENTS_LOADER", true)
      this.$axios.get(`/${rootState.auth.currentUser.filial}/user/equipments/`, {
        params: {
          search: payload,
          page_size: 2000
        }
      })
        .then(({ data }) => {
          commit(SET_ENTITY, {
            module: "equipmentModule",
            entity: "equipmentsList",
            value: data.results
          }, { root: true });
          commit("SET_EQUIPMENTS_LOADER", false)
        })
    },
    /*
    *
    * */
    fetchDepartmentList({ commit }, payload) {
      commit("SET_DEPARTMENTS_LOADER", true)
      return new Promise((resolve, reject) => {
        CrudService.getList(`/departments/`, {
          filial: payload,
          condition: "A",
          page_size: 50
        })
          .then((data) => {
            commit(SET_ENTITY, {
              module: "equipmentModule",
              entity: "departmentList",
              value: data.results.filter(item => item.condition === 'A'),
            }, { root: true });
            commit("SET_DEPARTMENTS_LOADER", false)
            resolve(data)
          })
      })
    },
    /*
    *
    * */
    fetchDepartmentSelected({ commit, dispatch }, payload) {
      // Отчистим массив перед тем как заполнить
      dispatch("clearDepartmentUserList")

      dispatch("fetchDepartmentUsers", payload)

      if(payload.sub_departments.length > 0) {
        payload.sub_departments.forEach(item => {
          setTimeout(() => {
            dispatch('fetchDepartmentUsers', item)
          }, 75)

          if(item.sub_departments.length > 0) {
            item.sub_departments.forEach(subItem => {
              setTimeout(() => {
                dispatch('fetchDepartmentUsers', subItem)
              }, 150)
            })
          }
        })
      }
    },
    /*
    *
    * */
    fetchDepartmentUsers({ commit, dispatch }, payload) {
      commit("SET_DEPARTMENTS_LOADER", true)
      CrudService.getList('/user_lists_for_equipment/', {
        dept_code: payload.code,
        filial: payload.mfo,
        search: payload.search,
        page_size: 50
      })
        .then((data) => {
          commit("PUSH_LIST", {
            entity: "departmentUsersList",
            value: data.results
          })
        })
        .then(() => {
          commit("SET_DEPARTMENTS_LOADER", false)
        })
    },
    /*
    *
    * */
    clearDepartmentUserList({ commit }) {
      commit("CLEAR_LIST", { entity: "departmentUsersList" })
    },
    /*
    *
    * */
    fetchCorrespondentList({ commit, rootState, dispatch }, payload ) {
      CrudService.getList(`/action/correspondent/`, {
        page_size: rootState.paginationOptions.page_size,
        ...payload
      })
        .then((data) => {
          commit(SET_ENTITY, {
            module: "equipmentModule",
            entity: "correspondentList",
            value: data.results,
          }, { root: true });

          dispatch("fetchPagination", data.count, {root: true})
        })
    },
    /*
    *
    * */
    fetchResolutionContentList({ commit, rootState, dispatch }, payload ) {
      CrudService.getList(`/docflow/${rootState.auth.currentUser.filial}/resolution_text/`, {
        page_size: rootState.paginationOptions.page_size,
        ...payload
      })
        .then((data) => {
          commit(SET_ENTITY, {
            module: "equipmentModule",
            entity: "resolutionContentList",
            value: data.results,
          }, { root: true });

          dispatch("fetchPagination", data.count, {root: true})
        })
    },
    /*
     *
     * */
    fetchAddNewsList({ commit, rootState, dispatch }, payload ) {
      CrudService.getList(`/action/news/`, {
        page_size: rootState.paginationOptions.page_size,
        ...payload
      })
        .then((data) => {
          commit(SET_ENTITY, {
            module: "equipmentModule",
            entity: "addNewsList",
            value: data.results,
          }, { root: true });

          dispatch("fetchPagination", data.count, {root: true})
        })
    },
    /*
    *
    * */
    fetchGetGroupStaffList({ commit, rootState }, payload) {
      return new Promise((resolve, reject) => {
        CrudService.getList(`/action/employee/groups/`, {
          page_size: 25,
          created_by: payload && payload.type === 'general' ? null : rootState.auth.currentUser.id,
          ...payload
        })
        .then((data) => {
          let model = data.results.map(item => {
            return {
              ...item,
              employees: item.employees.map(employee => ({ user: employee })),
              __employees: item.employees,
              __checked: false
            }
          })

          console.log('MODEL', model);

          commit(SET_ENTITY, {
            module: "equipmentModule",
            entity: "groupStaffList",
            value: model,
          }, { root: true });

          resolve()
        })
      })
    },
    /*
    *
    * */
    fetchGenerationFailedData({ state }, payload) {
      console.log("HI",payload)
    },
    /*
    *
    * */
    fetchGetShortDescriptions({ commit, rootState, dispatch }, payload) {
      CrudService.getList(`/action/short-descriptions/`, {
        ...payload.params
      })
      .then((data) => {
        commit(SET_ENTITY, {
          module: "equipmentModule",
          entity: "shortDescriptionList",
          value: data.results,
        }, { root: true })

        dispatch("fetchPagination", data.count, {root: true})
      })
    }
  }
}
