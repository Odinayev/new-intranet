
const PhoneBook = [
  {
    path: "phone-book",
    name: "MainPhoneBook",
    component: () => import(/* webpackChunkName: "MainPhoneBook" */ "@/modules/PhoneBook/views"),
    redirect: { name: "PhoneBookHome" },
    children: [
      {
        path: "home",
        name: "PhoneBookHome",
        meta: {
          isAuthRequired: true
        },
        component: () => import(/* webpackChunkName: "PhoneBookHome" */ "@/modules/PhoneBook/views/Home")
      }
    ]
  }
]

export default PhoneBook
