export default {
  namespaced: true,
  state: {
    listLoading: false,
    detailLoading: false,
    surveyList: [],
    surveyDetail: null,
    surveyStatuses: [
      {
        id: 1,
        label: 'current',
        icon: 'CheckCircleLinear',
        iconStrokeWidth: 2,
        count: null,
        status: 'current',
        active: false
      },
      {
        id: 2,
        label: 'history',
        icon: 'ClockCircleLinear',
        iconStrokeWidth: 2,
        count: null,
        status: 'history',
        active: false
      }
    ]
  },
  actions: {
    actionGetSurveyList({ state }, params) {
     return new Promise((resolve, reject) => {
       state.listLoading = true
       this.$axios.get(`/survey/list/`, params)
         .then(({ data }) => {
           state.surveyList = data.results
           resolve(data)
         })
         .catch(err => {
           reject(err)
         })
         .finally(() => {
           state.listLoading = false
         })
     })
    },
    /** **/
    actionGetSurveyQuestions({ state }, surveyId) {
      return new Promise((resolve, reject) => {
        state.detailLoading = true
        this.$axios.get(`/survey/list/${surveyId}/`)
          .then(({ data }) => {
            state.surveyDetail = {
              ...data,
              questions: data.questions.map(question => ({
                ...question,
                answer: {
                  selected_option_id: null,
                  other_answer: null
                },
                selected_option: {
                  id: null,
                  is_other: false
                }
              }))
            }
            resolve(data)
          })
          .catch(err => {
            reject(err)
          })
          .finally(() => {
            state.detailLoading = false
          })
      })
    },
    /** **/
    actionSendAnswers({ state }) {
      return new Promise((resolve, reject) => {
        let model = {
          survey: state.surveyDetail?.survey_id,
          answers: state.surveyDetail?.questions.flatMap(question => {
            if (question.input_type !== 'checkbox') {
              return {
                question: question.id,
                selected_option: question?.answer?.selected_option_id,
                other_answer: question?.answer?.other_answer
              }
            } else {
              // For checkbox type
              const baseAnswer = {
                question: question.id,
                selected_option: question?.answer?.selected_option_id,
                other_answer: null
              };

              // Check if "other_answer" exists
              if (question?.answer?.other_answer) {
                const otherAnswer = {
                  question: question.id,
                  selected_option: question?.options[question.options.length - 1]?.id, // Last option ID
                  other_answer: question?.answer?.other_answer
                }

                // Return both base and "other_answer"
                return [baseAnswer, otherAnswer]
              }

              // Return only the base answer if no "other_answer"
              return baseAnswer
            }
          })
        }

        this.$axios.post(`/survey/submission/`, model)
          .then((res) => {
            resolve(res)
          })
          .catch(err => {
            reject(err)
          })
      })
    }
  }
}