import {AUDIT_PERFORMER_TYPES} from "../../../common/constants"
import {findNewParticipant} from "../../../common/helpers"

export default {
  namespaced: true,
  state: {
    checkupTableCols: [
      {
        text: "№",
        value: "index",
        width: "50",
        active: true,
        sortable: false,
      },
      {
        text: "Аудитский номер",
        i18n: "audit-number",
        value: "audit_number",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Автор",
        i18n: "components.author",
        value: "created_by",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Название группы",
        i18n: "chat-messenger.components.group-name",
        value: "group_name",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Начальник группы",
        i18n: "group-head",
        value: "head_of_group",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Дата создана",
        i18n: "created-date",
        value: "created_date",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Срок (дедлайн)",
        i18n: "deadline",
        value: "deadline",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Кому",
        i18n: "document-flow.views.send-document.to-whom",
        value: "to_where",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Состояние",
        i18n: "components.state",
        value: "status",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Действие",
        i18n: "action",
        value: "action",
        width: "50",
        active: true,
        sortable: false,
      }
    ],
    checkupTableFilters: {
      page: 1,
      page_size: 15,
    },
    statusLoading: true,
    statusCardList: [
      {
        id: 1,
        label: 'statusText.pending',
        icon: 'QuestionCircleBold',
        iconColor: 'text-warning-500',
        value: 'pending',
        count: 15,
        active: true
      },
      {
        id: 2,
        label: 'components.in-progress',
        icon: 'HamburgerMenuBold',
        iconColor: 'text-info-500',
        value: 'in_progress',
        count: 72,
        active: false
      },
      {
        id: 3,
        label: 'accepted',
        icon: 'CheckCircleBold',
        iconColor: 'text-success-300',
        value: 'accepted',
        count: 29,
        active: false
      },
      {
        id: 4,
        label: 'statusText.declined',
        icon: 'CloseCircleBold',
        iconColor: 'text-critic-500',
        value: 'rejected',
        count: 8,
        active: false
      },
      {
        id: 5,
        label: 'history',
        icon: 'ClockCircleBold',
        iconColor: 'text-info-500',
        value: 'closed',
        count: 198,
        active: false
      }
    ]
  },
  actions: {
    actionCreateCheckup({ commit }, model) {
      return new Promise((resolve, reject) => {
        this.$axios.post(`/audit/check-ups/`, model)
          .then(res => {
            resolve(res)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    /** **/
    actionUpdateCheckup({ commit }, payload) {
      let model = {
        deadline: payload.body.deadline
      }
      return new Promise((resolve, reject) => {
        this.$axios.put(`/audit/check-ups/${payload.id}/change-deadline/`, model)
          .then(res => {
            resolve(res)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    /** **/
    actionDeleteCheckup({ commit }, id) {
      return new Promise((resolve, reject) => {
        this.$axios.delete(`/audit/check-ups/${id}/`)
          .then(res => {
            resolve(res)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    actionUpdateCheckupTasks({ commit }, { id, items }){
      return new Promise((resolve, reject) => {
        this.$axios.put(`/audit/check-ups/${id}/`, { tasks: items })
            .then(res => {
              resolve(res)
            })
            .catch(err => {
              reject(err)
            })
      })
    },
    /** **/
    actionGetCheckupDetail({ commit }, id){
      return new Promise((resolve, reject) => {
        this.$axios.get(`/audit/check-ups/${id}/`)
            .then(async res => {
              if (res && res.status === 200){
                const { data } = res
                let items = []
                if (data.is_started && data.tasks.length) {
                  const sortedList = data.tasks.sort((a, b) => {
                    if (a.performer_type === 'group_head' && b.performer_type !== 'group_head') {
                      return -1
                    }
                    if (b.performer_type === 'group_head' && a.performer_type !== 'group_head') {
                      return 1
                    }
                    return 0
                  })
                  const newParticipants =  await findNewParticipant(data.participants, data.tasks)
                  // const realParticipants = await findRealParticipants(data.participants, data.tasks)  // This function finds objects that are not available in participants and returns it
                  if (newParticipants) {
                    items = [...sortedList, ...newParticipants.map(item => ({ performer: item, performer_type: AUDIT_PERFORMER_TYPES.GROUP_MEMBER, directions: [], deadline_in_day: 1 }))]
                  }
                  // if (realParticipants) {
                  //   items = realParticipants
                  // }
                  else {
                    items = data.tasks
                  }
                }
                else {
                  const { head_of_group: groupHead } = data.group || {}
                  const members = data.participants || []
                  items = [
                    { performer: groupHead, performer_type: AUDIT_PERFORMER_TYPES.GROUP_HEAD, directions: [], deadline_in_day: 1 },
                    ...members.map(item => ({ performer: item, performer_type: AUDIT_PERFORMER_TYPES.GROUP_MEMBER, directions: [], deadline_in_day: 1 }))
                  ]
                }
                resolve({ model: res.data, items: items })
              }
            })
            .catch(err => {
              reject(err)
            })
      })
    },
    /** **/
    actionCreateTask({ commit }, model) {
      return new Promise((resolve, reject) => {
        this.$axios.post(`/audit/tasks/`, model)
            .then(res => {
              resolve(res)
            })
            .catch(err => {
              reject(err)
            })
      })
    },
    /** **/
    actionUpdateTask({ commit }, { id, items }){
      return new Promise((resolve, reject) => {
        this.$axios.put(`/audit/tasks/${id}/`, items)
            .then(res => {
              resolve(res)
            })
            .catch(err => {
              reject(err)
            })
      })
    },
    /** **/
    actionAcquaintCheckup({ commit }, id) {
      return new Promise((resolve, reject) => {
        this.$axios.put(`/audit/check-ups/${id}/acquaintance/`)
            .then(res => {
              resolve(res)
            })
            .catch(err => {
              reject(err)
            })
      })
    },
    /** **/
    actionAcquaintTask({ commit }, id) {
      return new Promise((resolve, reject) => {
        this.$axios.put(`/audit/tasks/${id}/acquaintance/`)
            .then(res => {
              resolve(res)
            })
            .catch(err => {
              reject(err)
            })
      })
    },
    /** **/
    actionCompleteCheckup({ commit }, id) {
      return new Promise((resolve, reject) => {
        this.$axios.put(`/audit/check-ups/${id}/close/`)
          .then(res => {
            resolve(res)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    /** **/
    actionGetCheckupStatusList({ state, commit }, params) {
      return new Promise((resolve, reject) => {
        this.$axios.get(`/audit/check-ups/counts/`, {
          params
        })
          .then(res => {
            if (res && res.status === 200) {
              state.statusCardList = state.statusCardList.map(item => {
                if (res.data[item.value] !== undefined) {
                  return {
                    ...item,
                    count: res.data[item.value]
                  }
                }
                return item
              })
            }
            state.statusLoading = false
            resolve(res)
          })
          .catch(err => {
            reject(err)
          })
      })
    }
  }
}
