const HR = [
  {
    path: "hr",
    name: "MainHR",
    meta: {
      isAuthRequired: true
    },
    component: () => import(/* webpackChunkName: "MainHR" */ "@/modules/HR"),
    children: [
      {
        path: "survey",
        name: "HRSurvey",
        component: () => import("@/modules/HR/Survey/List")
      },
      {
        path: "survey/:id/update/",
        name: "HRSurveyUpdate",
        component: () => import("@/modules/HR/Survey/Update")
      },
      {
        path: "survey/:id/read/",
        name: "HRSurveyRead",
        component: () => import("@/modules/HR/Survey/Show")
      },
      {
        path: "create",
        name: "HRSurveyCreate",
        component: () => import("@/modules/HR/Survey/Create")
      },
      {
        path: "completed-list",
        name: "HRCompletedList",
        component: () => import("@/modules/HR/Survey/CompletedList")
      },
      {
        path: "completed-list/:id",
        name: "HRCompletedID",
        component: () => import("@/modules/HR/Survey/CompletedRead")
      },

      // Hiring
      {
        path: "hiring",
        name: "HiringList",
        component: () => import("@/modules/HR/Hiring/List.vue")
      },
      {
        path: "hiring/:id",
        name: "HiringID",
        component: () => import("@/modules/HR/Hiring/Show/index.vue")
      },
      // / Hiring

      // Sign
      {
        path: "sign",
        name: "SignList",
        component: () => import("@/modules/HR/Sign/List.vue")
      },
      {
        path: "sign/:id",
        name: "SignID",
        component: () => import("@/modules/HR/Sign/Show")
      },
      // /Sign

      // Personal Documents
      {
        path: "personal-documents",
        name: "PersonalDocumentsList",
        component: () => import("@/modules/HR/PersonalDocuments")
      },
      // /Personal Documents
    ]
  }
]

export default HR
