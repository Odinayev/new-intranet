import "@/assets/css/main.css";
import "@/common/filters";
import "@/plugins/carousel";
import { i18n } from "@/plugins/i18n";
import "@/plugins/jquery";
import "@/plugins/perfect-scrollbar";
import "@/plugins/quill-editor";
import "@/plugins/toaster";
import "@/plugins/ui";
import "@/plugins/vue-apexchart";
import "@/plugins/vue-notification";
import "@/plugins/vue-observe-visibility";
import "@/plugins/vuelidate";
import "@/plugins/vueMask";
import "@/plugins/vuePlugins";
import vuetify from "@/plugins/vuetify";
import "hooper/dist/hooper.css";
import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
Vue.config.productionTip = false;
new Vue({
	router,
	store,
	vuetify,
	i18n,
	render: h => h(App),
}).$mount("#app");
