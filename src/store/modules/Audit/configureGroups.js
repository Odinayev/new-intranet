export default {
  namespaced: true,
  state: {
    configureGroupsTableCols: [
      {
        text: "№",
        value: "index",
        width: "50",
        active: true,
        sortable: false,
      },
      {
        text: "Название группы",
        i18n: "chat-messenger.components.group-name",
        value: "name",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Начальник группы",
        i18n: "group-head",
        value: "head_of_group",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Сотрудники",
        i18n: "equipments.views.group-staff.employees",
        value: "participants",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Дата создана",
        i18n: "created-date",
        value: "created_date",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Состояние",
        i18n: "components.state",
        value: "is_active",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Действие",
        i18n: "action",
        value: "action",
        width: "50",
        active: true,
        sortable: false,
      }
    ],
    groupTableFilters: {
      page: 1,
      page_size: 15,
    }
  },
  actions: {
    actionCreateGroup({ commit }, body){
      return new Promise((resolve, reject) => {
        this.$axios.post(`/audit/groups/`, body)
          .then(({ data }) => {
            resolve(data)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    /** **/
    actionGetGroupDetail({ commit }, id) {
      return new Promise((resolve, reject) => {
        this.$axios.get(`/audit/groups/${id}/`)
          .then((res) => {
            if (res && res.status === 200){
              resolve(res.data)
            }
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    /** **/
    actionUpdateGroup({ commit }, {id, body}){
      return new Promise((resolve, reject) => {
        this.$axios.put(`/audit/groups/${id}/`, body)
          .then(({ data }) => {
            resolve(data)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    /** **/
    actionGetGroupHistoryList({ commit }, { object_id, content_type }){
      return new Promise((resolve, reject) => {
        this.$axios.get(`/audit/work-history/`, {
          params: {
            object_id,
            content_type
          }
        })
          .then(res => {
            if (res && res.status === 200) {
              resolve(res.data.results)
            }
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    /** **/
    actionDeleteGroup({ commit }, { id }){
      return new Promise((resolve, reject) => {
        this.$axios.delete(`/audit/groups/${id}/`)
          .then(res => {
            resolve(res)
          })
          .catch(err => {
            reject(err)
          })
      })
    }
  }
}
