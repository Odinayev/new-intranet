import DocumentFlowModule from "./modules/DocumentFlowModule"
import ChatMessenger from "@/router/modules/ChatMessenger"
import ControlTasks from "@/router/modules/ControlTasks"
import CreditMonitoring from "@/router/modules/CreditMonitoring"
import Dashboard from "@/router/modules/Dashboard"
import DocumentFlow from "@/router/modules/DocumentFlow"
import Audit from "@/modulesV3/Audit/routes"
import Equipments from "@/router/modules/Equipments"
import EventCalendar from "@/router/modules/EventCalendar"
import ExchangeEmail from "@/router/modules/ExchangeEmail"
import Facture from "@/router/modules/Facture"
import HR from "@/router/modules/HR"
import PhoneBook from "@/router/modules/PhoneBook"
import UserProfile from "@/router/modules/UserProfile"
import BankStructure from '@/router/modules/BankStructure';
import Authorities from '@/router/modules/Authorites';
import SurveyClient from "@/router/modules/SurveyClient";
import SurveyRoutes from "../modulesV3/Survey/routes"
import store from "@/store"
import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)
const routes = [
	// Main Layout
	{
		path: "/",
		name: "MainLayout",
		component: () => import(/* webpackChunkName: "MainLayout" */ "@/layouts/MainLayout"),
		redirect: { name: "MainDashboard" },
		children: [
			// {
			// 	path: "/dashboard",
			// 	name: "MainDashboard",
			// 	meta: {
			// 		middleware: [
			// 			auth
			// 		],
			// 		isAuthRequired: true
			// 	},
			// 	component: () => import(/* webpackChunkName: "Home" */ "@/views/Home")
			// },
			...Dashboard,
			...EventCalendar,
			...DocumentFlowModule,
			...DocumentFlow,
			...Audit,
			...UserProfile,
			...ChatMessenger,
			...ExchangeEmail,
			...PhoneBook,
			...Equipments,
			...Facture,
			...HR,
			...ControlTasks,
			...CreditMonitoring,
			...BankStructure,
			...Authorities,
			...SurveyClient,
			...SurveyRoutes
		]
	},
	// End main Layout

	// Auth Layout
	{
		path: '/auth',
		name: 'AuthLayout',
		component: () => import(/* webpackChunkName: "AuthLayout" */ "@/layouts/AuthLayout"),
		redirect: { name: 'AuthLogin' },
		children: [
			{
				path: 'login',
				name: 'AuthLogin',
				component: () => import(/* webpackChunkName: "Auth" */ '@/views/Auth/index'),
				redirect: { name: 'AuthLoginWithPhone' },
				children: [
					{
						path: 'login-with-phone',
						name: 'AuthLoginWithPhone',
						component: () => import(/* webpackChunkName: "Auth" */ '@/views/Auth/LoginWithPhone'),
					},
					{
						path: 'login-with-active-directory',
						name: 'AuthLoginWithAD',
						component: () => import(/* webpackChunkName: "Auth" */ '@/views/Auth/LoginWIthAD'),
					},
					{
						path: 'login-with-eri',
						name: 'AuthLoginWithERI',
						component: () => import(/* webpackChunkName: "Auth" */ '@/views/Auth/LoginWithERI'),
					}
				]
			},
			{
				path: 'register',
				name: 'AuthRegister',
				component: () => import(/* webpackChunkName: "Auth" */ '@/views/Auth/Registration'),
				redirect: { name: 'AuthRegisterEnterPhone' },
				children: [
					{
						path: 'enter-your-phone-number',
						name: 'AuthRegisterEnterPhone',
						component: () => import(/* webpackChunkName: "Auth" */ '@/views/Auth/EnterPhoneNumber'),
					},
					{
						path: 'enter-verification-code/:phone',
						name: 'AuthRegisterVerificationCode',
						component: () => import(/* webpackChunkName: "Auth" */ '@/views/Auth/EnterVerificationCode'),
					},
					{
						path: 'reset-password/:phone',
						name: 'AuthRegisterResetPassword',
						component: () => import(/* webpackChunkName: "Auth" */ '@/views/Auth/ResetPassword'),
					}
				]
			},
		]
	},
	// End Auth Layout

	{
		path: "/base-phone-book",
		name: "BasePhoneBook",
		component: () => import(/* webpackChunkName: "MainPhoneBook" */ "@/modules/PhoneBook/views"),
		redirect: { name: "BasePhoneBookHome" },
		children: [
			{
				path: "home",
				name: "BasePhoneBookHome",
				component: () => import(/* webpackChunkName: "PhoneBookHome" */ "@/modules/PhoneBook/views/Home")
			}
		]
	},

	// Survey Layout
	{
		path: "/survey",
		name: "SurveyLayout",
		component: () => import("@/layouts/SurveyLayout"),
		redirect: { name: "SurveyIntro" },
		children: [
			{
				path: "intro",
				name: "SurveyIntro",
				component: () => import("@/modules/Survey")
			},
			{
				path: "questions/:id",
				name: "SurveyQuestions",
				component: () => import("@/modules/Survey/SurveyQuestionList")
			},
			{
				path: "finish",
				name: "SurveyQuestionsFinish",
				component: () => import("@/modules/Survey/Finish")
			}
		]
	},

	// NotFound Page
	{
		path: "*",
		name: "NotFoundPage",
		meta: {
			isAuthRequired: true,
		},
		component: () =>
			import(
				/* webpackChunkName: "NotFoundPage" */ "@/modules/NotFound/index"
			),
	},
]

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes,
})
// TODO uncomment before commit
router.beforeEach((to, from, next) => {
	const token = localStorage.getItem("Authorization")
	store.commit('SET_ROUTE', to);

	if (to.meta.isAuthRequired && !token) {
		next({ name: "AuthLogin" });
	}
	next();
})

// router.beforeEach(async (to, from, next) => {
// 	// console.log('store', store.getters["auth/getUserLoggedIn"])
// 	// const dashboardMiddleWare = to.matched[0].meta.middleware
// 	//
// 	// console.log(dashboardMiddleWare);
//
// 	// Для публичных маршрутов
// 	if (!to.meta.middleware) {
// 		console.log('not middleware')
// 		return next()
// 	}
//
// 	if(!store.getters["auth/getUserLoggedIn"]) {
// 		await store.dispatch("auth/fetchCurrentUser")
// 		console.log(2)
// 	}
// 	console.log('1')
// 	const middleware = to.meta.middleware
// 	const context = { to, from, next, store }
//
// 	return middleware[0]({
// 		...context
// 	})
// })

export default router
