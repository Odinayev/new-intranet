export default [
  {
    path: "send-documents",
    name: "SendDocumentNew",
    meta: {
      breadcrumb: true,
      title: "Отправка документов",
      isAuthRequired: true,
    },
    component: () => import("@/modules/DocumentFlow/views/SendDocumentNew/TableList.vue")
  }
]
