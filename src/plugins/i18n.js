import ru from '@/locales/ru.json';
import uz from '@/locales/uz.json';
import Vue from 'vue';
import VueI18n from 'vue-i18n';
Vue.use(VueI18n)
const locale = localStorage.getItem('lang') || 'uz';

export const i18n = new VueI18n({
	locale,
	fallbackLocale: locale,
	messages: { ru, uz }
})
export const changeLocale = locale => {
	i18n.locale = locale
	localStorage.setItem('lang', locale)
	document.querySelector('html').setAttribute('lang', locale)
}
export const getLocale = () => {
	return i18n.locale;
};
// const languages = ['uz-latin', 'uz-kiril', 'ru']
// export function navigatorCloseLanguage () {
// 	const userLanguage = navigator.language
// 	if (languagesAvailable.includes(userLanguage)) {
// 		return loadLanguageAsync(userLanguage)
// 	} else if (userLanguage.includes('-')) {
// 		const userLanguageSplit = userLanguage.split('-')
// 		if (languagesAvailable.includes(userLanguageSplit[0])) {
// 			return loadLanguageAsync(userLanguageSplit[0])
// 		}
// 	}
// 	return 'ru'
// }

// export function loadLanguageAsync (lang) {
// 	if (i18n.locale !== lang) {
// 		if (!loadedLanguages.includes(lang)) {
// 			return import(/* webpackChunkName: "lang-[request]" */ `@/locales/${lang}`).then(message => {
// 				i18n.setLocaleMessage(lang, message.default)
// 				loadedLanguages.push(lang)
// 				return setI18nLanguage(lang)
// 			})
// 		}
// 		return Promise.resolve(setI18nLanguage(lang))
// 	}
// 	return Promise.resolve(lang)
// }
