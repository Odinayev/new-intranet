const Facture = [
  {
    path: "facture",
    name: "MainFacture",
    meta: {
      isAuthRequired: true
    },
    component: () => import(/* webpackChunkName: "DocumentFlow" */ "@/modules/Facture"),
    children: [
      {
        path: "documents",
        name: "FactureDocuments",
        meta: {
          isAuthRequired: true
        },
        redirect: { name: "FactureIncomingDocumentsList" },
        component: () => import(/* webpackChunkName: "DocumentFlow" */ "@/modules/Facture/Documents"),
        children: [
          {
            path: "incoming",
            name: "FactureIncomingDocumentsList",
            meta: {
              isAuthRequired: true,
              pathName: "incoming"
            },
            component: () => import(/* webpackChunkName: "DocumentFlow" */ "@/modules/Facture/Documents/Incoming/List"),
          },
          {
            path: "outgoing",
            name: "FactureOutgoingDocumentsList",
            meta: {
              isAuthRequired: true,
              pathName: "outgoing"
            },
            component: () => import(/* webpackChunkName: "DocumentFlow" */ "@/modules/Facture/Documents/Outgoing/List"),
          },
          {
            path: "draft",
            name: "FactureDraftDocumentsList",
            meta: {
              isAuthRequired: true,
              pathName: "draft"
            },
            component: () => import(/* webpackChunkName: "DocumentFlow" */ "@/modules/Facture/Documents/Draft/List"),
          },
          {
            path: "rejected",
            name: "FactureRejectedDocumentsList",
            meta: {
              isAuthRequired: true,
              pathName: "rejected"
            },
            component: () => import(/* webpackChunkName: "DocumentFlow" */ "@/modules/Facture/Documents/Rejected/List"),
          },
          {
            path: "deleted",
            name: "FactureDeletedDocumentsList",
            meta: {
              isAuthRequired: true,
              pathName: "deleted"
            },
            component: () => import(/* webpackChunkName: "DocumentFlow" */ "@/modules/Facture/Documents/Deleted/List"),
          },
          {
            path: "archive",
            name: "FactureArchiveDocumentsList",
            meta: {
              isAuthRequired: true,
              pathName: "archive"
            },
            component: () => import(/* webpackChunkName: "DocumentFlow" */ "@/modules/Facture/Documents/Archive/List"),
          },
          {
            path: "annulled",
            name: "FactureAnnulledDocumentsList",
            meta: {
              isAuthRequired: true,
              pathName: "annulled"
            },
            component: () => import(/* webpackChunkName: "DocumentFlow" */ "@/modules/Facture/Documents/Annulled/List"),
          },
          {
            path: "committent",
            name: "FactureCommittentDocumentsList",
            meta: {
              isAuthRequired: true,
              pathName: "committent"
            },
            component: () => import(/* webpackChunkName: "DocumentFlow" */ "@/modules/Facture/Documents/Committent/List"),
          },
          {
            path: "approval-in-processing",
            name: "FactureApprovalInProcessingDocumentsList",
            meta: {
              isAuthRequired: true,
              pathName: "approval-in-processing"
            },
            component: () => import(/* webpackChunkName: "DocumentFlow" */ "@/modules/Facture/Documents/ApprovalInProcessing/List"),
          },
          {
            path: "approval-processed",
            name: "FactureApprovalProcessedDocumentsList",
            meta: {
              isAuthRequired: true,
              pathName: "approval-processed"
            },
            component: () => import(/* webpackChunkName: "DocumentFlow" */ "@/modules/Facture/Documents/ApprovalProcessed/List"),
          },
        ]
      },
      // FOR CREATING DOCUMENT
      {
        path: "documents/create/:type",
        name: "FactureDocumentCreate",
        meta: {
          isAuthRequired: true
        },
        component: () => import(/* webpackChunkName: "DocumentFlow" */ "@/modules/Facture/Documents/CreateDocument/CreateForm"),
      },

      /* DETAIL */
      {
        path: "documents/detail/:id",
        name: "FactureDocumentDetailShow",
        meta: {
          isAuthRequired: true
        },
        component: () => import(/* webpackChunkName: "DocumentFlow" */ "@/modules/Facture/Documents/DetailShow"),
      }
    ]
  }
]

export default Facture
