
const exchangeEmail = [
	{
		path: "exchange-email",
		name: "MainExchangeEmail",
		component: () => import(/* webpackChunkName: "MainExchangeEmail" */ "@/modules/ExchangeEmail/views"),
		redirect: { name: "ExchangeEmailInbox" },
		children: [
			{
				path: "inbox",
				name: "ExchangeEmailInbox",
				component: () => import(/* webpackChunkName: "ExchangeEmailInbox" */ "@/modules/ExchangeEmail/views/Folders/Inbox/List")
			},
			{
				path: "inbox/:id",
				name: "ExchangeEmailInboxDetail",
				component: () => import("@/modules/ExchangeEmail/views/Folders/Inbox/Detail")
			},
			{
				path: "send",
				name: "ExchangeEmailSend",
				component: () => import(/* webpackChunkName: "ExchangeEmailSend" */ "@/modules/ExchangeEmail/views/Folders/Sent/List")
			},
			{
				path: "trash",
				name: "ExchangeEmailTrash",
				component: () => import(/* webpackChunkName: "ExchangeEmailTrash" */ "@/modules/ExchangeEmail/views/Folders/Trash/List")
			}
		]
	}
]

export default exchangeEmail
