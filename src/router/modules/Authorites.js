const Authorities = [
  {
    path: "authority",
    name: "MainAuthority",
    component: () => import(/* webpackChunkName: "MainChat" */ "@/modules/Authorities/views"),
    // redirect: { name: 'AuthorityLeadership' },
    children: [
    	{
    		path: "leadership",
    		name: "AuthorityLeadership",
    		component: () => import(/* webpackChunkName: "ChatAreaWrapper" */ "@/modules/Authorities/views/Leadership")
    	}
    ]
  },
  {
    path: "authority-admin",
    name: "MainAuthorityAdmin",
    component: () => import(/* webpackChunkName: "MainChat" */ "@/modules/Authorities/views/Admin"),
  },
  {
    path: "authority-user-create",
    name: "AuthorityUserCreate",
    component: () => import(/* webpackChunkName: "MainChat" */ "@/modules/Authorities/views/Admin/CreateUser"),
  },
  {
    path: "authority-user-update/:id",
    name: "AuthorityUserUpdate",
    component: () => import(/* webpackChunkName: "MainChat" */ "@/modules/Authorities/views/Admin/UpdateUser"),
  },
  {
    path: "authority-user-affirmative/:id",
    name: "AuthorityUserAffirmative",
    component: () => import(/* webpackChunkName: "MainChat" */ "@/modules/Authorities/views/Admin/UserAffirmative"),
  },
  {
    path: "authority-affidavit-create",
    name: "AuthorityAffidavitCreate",
    component: () => import(/* webpackChunkName: "MainChat" */ "@/modules/Authorities/views/Admin/CreateAffidavit"),
  },
  {
    path: "authority-affidavit-update/:id",
    name: "AuthorityAffidavitUpdate",
    component: () => import(/* webpackChunkName: "MainChat" */ "@/modules/Authorities/views/Admin/UpdateAffidavit"),
  },
  {
    path: "authority-affidavit-detail-create",
    name: "AuthorityAffidavitDetailCreate",
    component: () => import(/* webpackChunkName: "MainChat" */ "@/modules/Authorities/views/Admin/CreateAffidavitDetail"),
  },
  {
    path: "authority-affidavit-detail-update/:id",
    name: "AuthorityAffidavitDetailUpdate",
    component: () => import(/* webpackChunkName: "MainChat" */ "@/modules/Authorities/views/Admin/UpdateAffidavitDetail"),
  },

]

export default Authorities
