import { SET_ENTITY } from "@/store/mutation-types"

export default {
  namespaced: true,
  state: {
    factureCurrentUser: {},
    isListLoading: false,
    isDocumentDetailLoading: false,
    documentList: [],
    documentDetail: {},
    documentPreview: "",
    documentCreateTypeList: [
      {
        id: 1,
        name: "Акт",
        value: "act",
        children: [
          {
            name: "Акт и счет-фактура (ПКМ №489)",
            routeName: "FactureDocumentCreate",
            paramsType: "universal_act_invoice",
            query: null,
          },
          {
            name: "Акт (Роуминг)",
            routeName: "FactureDocumentCreate",
            paramsType: "standard_act",
            query: null,
          },
          {
            name: "Акт и счет-фактура акциз 2020",
            routeName: "FactureDocumentCreate",
            paramsType: "universal_act_invoice",
            query: "tax"
          },
        ]
      },
      {
        id: 2,
        name: "Счет-фактура",
        value: "invoice",
        children: [
          {
            name: "Cчет-фактура (ПКМ №489)",
            routeName: "FactureDocumentCreate",
            paramsType: "universal_vat_invoice",
            query: null,
          }
        ]
      },
      {
        id: 3,
        name: "Доверенность",
        value: "power_of_attorney",
        children: [
          {
            name: "Доверенность (Роуминг)",
            routeName: "FactureDocumentCreate",
            paramsType: "power_of_attorney_roaming",
            query: null,
          }
        ]
      },
    ],
    invoiceTypeList: [
      {
        id: 1,
        name: "Стандартный",
        value: "0"
      },
      {
        id: 2,
        name: "Дополнительный",
        value: "1"
      },
      {
        id: 3,
        name: "Возмещения расходов",
        value: "2"
      },
      {
        id: 4,
        name: "Без оплаты",
        value: "3"
      },
      {
        id: 5,
        name: "Исправленный",
        value: "4"
      },
      {
        id: 6,
        name: "Исправленный (возмещение затрат)",
        value: "5"
      },
      {
        id: 7,
        name: "Дополнительный (возмещение затрат)",
        value: "6"
      }
    ],
    invoiceReceiverDetails: {
      Accounts: [],
      Branches: [],
      CompanyAddress: null,
      CompanyInn: null,
      CompanyName: null,
      Email: null,
      Oked: null,
      PhoneNumber: null,
      Pinfl: null,
      SpecialAccount: null,
      TaxGap: null,
      TaxPayerTypeName: null,
      VatCode: null
    },
    asakabankAccountList: [
      "29802000900000873003"
    ],
    vatPercentageList: [
      {
        vat_rate_temp: "Без НДС",
        vat_value_temp: null,
      },
      {
        vat_rate_temp: "0",
        vat_value_temp: 0,
      },
      {
        vat_rate_temp: "12%",
        vat_value_temp: 0.12,
      }
    ],
    universalVatIncomeFactureModel: {
      document: {
        document_number: "",
        document_date: "",
        contract_number: "",
        contract_date: "",
        factura_type: "0",
        document_description: "",
        doverennost_number: "",
        doverennost_date: null,
        doverennost_agent_name: "",
        doverennost_agent_inn: "",
        doverennost_agent_pinfl: "",
        korrektirovoch_document_uid: "",
        korrektirovoch_document_number: "",
        korrektirovoch_document_date: "",
        items: [
          {
            // temporary objects
            vat_temp: {
              vat_rate_temp: "12%",
              vat_value_temp: 0.12,
            },
            productSelected: {},
            measurementSelected: {},
            // end temporary objects
            item_number: "1",
            description: "",
            volume: "",
            unit_price: "",
            subtotal: "",
            vat: {
              vat_rate: "",
              vat_value: ""
            },
            subtotal_with_taxes: "",
            measurement_unit: "",
            excise: {
              excise_rate: "",
              excise_value: ""
            },
            catalog: {
              code: "",
              name: ""
            },
            lgota_id: "",
            barcode: ""
          }
        ],
        column_summary_values: {
          column_subtotal: "",
          column_subtotal_uzs: "",
          column_vat_value: "",
          column_vat_value_uzs: "",
          column_subtotal_with_taxes: "",
          column_subtotal_with_taxes_total: "",
          column_subtotal_with_taxes_uzs: ""
        }
      },
      sender_info: {
        id: ""
      },
      approver_and_signers: {
        approver: {
          position: "",
          last_name: "",
          first_name: "",
          middle_name: "",
          inn: "",
          pinfl: ""
        },
        signer: {
          position: "",
          last_name: "",
          first_name: "",
          middle_name: "",
          inn: "",
          pinfl: ""
        },
        signer_director: {
          position: "Руководитель ГО",
          last_name: "Tulyaganov",
          first_name: "Kudratilla",
          middle_name: "Asatullayevich",
          inn: "",
          pinfl: ""
        },
        signer_accountant: {
          position: "Главный бухгалтер филиала",
          last_name: "Абдуллаев",
          first_name: "Роман",
          middle_name: "Матназарович",
          inn: "",
          pinfl: ""
        },
        signer_warehouse: {
          position: null,
          last_name: null,
          first_name: null,
          middle_name: null,
          inn: null,
          pinfl: null
        }
      },
      receiver: {
        receiver_info: {
          inn: ""
        }
      }
    },

    universalVatIncomeFactureModelTemp: {
      document: {
        document_number: "",
        document_date: "",
        contract_number: "",
        contract_date: "",
        factura_type: "0",
        document_description: "",
        doverennost_number: "",
        doverennost_date: null,
        doverennost_agent_name: "",
        doverennost_agent_inn: "",
        doverennost_agent_pinfl: "",
        korrektirovoch_document_uid: "",
        korrektirovoch_document_number: "",
        korrektirovoch_document_date: "",
        items: [
          {
            // temporary objects
            vat_temp: {
              vat_rate_temp: "12%",
              vat_value_temp: 0.12,
            },
            productSelected: {},
            measurementSelected: {},
            // end temporary objects
            item_number: "1",
            description: "",
            volume: "",
            unit_price: "",
            subtotal: "",
            vat: {
              vat_rate: "",
              vat_value: ""
            },
            subtotal_with_taxes: "",
            measurement_unit: "",
            excise: {
              excise_rate: "",
              excise_value: ""
            },
            catalog: {
              code: "",
              name: ""
            },
            lgota_id: "",
            barcode: ""
          }
        ],
        column_summary_values: {
          column_subtotal: "",
          column_subtotal_uzs: "",
          column_vat_value: "",
          column_vat_value_uzs: "",
          column_subtotal_with_taxes: "",
          column_subtotal_with_taxes_total: "",
          column_subtotal_with_taxes_uzs: ""
        }
      },
      sender_info: {
        id: ""
      },
      approver_and_signers: {
        approver: {
          position: "",
          last_name: "",
          first_name: "",
          middle_name: "",
          inn: "",
          pinfl: ""
        },
        signer: {
          position: "",
          last_name: "",
          first_name: "",
          middle_name: "",
          inn: "",
          pinfl: ""
        },
        signer_director: {
          position: "Руководитель ГО",
          last_name: "Tulyaganov",
          first_name: "Kudratilla",
          middle_name: "Asatullayevich",
          inn: "",
          pinfl: ""
        },
        signer_accountant: {
          position: "Главный бухгалтер филиала",
          last_name: "Абдуллаев",
          first_name: "Роман",
          middle_name: "Матназарович",
          inn: "",
          pinfl: ""
        },
        signer_warehouse: {
          position: null,
          last_name: null,
          first_name: null,
          middle_name: null,
          inn: null,
          pinfl: null
        }
      },
      receiver: {
        receiver_info: {
          inn: ""
        }
      }
    },

    universalActInvoiceModel: {
      document: {
        document_number: "",
        document_date: "",
        contract_number: "",
        contract_date: "",
        factura_type: "0",
        document_description: "",
        doverennost_number: "",
        doverennost_date: null,
        doverennost_agent_name: "",
        doverennost_agent_inn: "",
        doverennost_agent_pinfl: "",
        korrektirovoch_document_uid: "",
        korrektirovoch_document_number: "",
        korrektirovoch_document_date: "",
        items: [
          {
            // temporary objects
            vat_temp: {
              vat_rate_temp: "12%",
              vat_value_temp: 0.12,
            },
            productSelected: {},
            measurementSelected: {},
            // end temporary objects
            item_number: "1",
            description: "",
            volume: "",
            unit_price: "",
            subtotal: "",
            vat: {
              vat_rate: "",
              vat_value: ""
            },
            subtotal_with_taxes: "",
            measurement_unit: "",
            excise: {
              excise_rate: "",
              excise_value: ""
            },
            catalog: {
              code: "",
              name: ""
            },
            lgota_id: "",
            barcode: ""
          }
        ],
        column_summary_values: {
          column_subtotal: "",
          column_subtotal_uzs: "",
          column_vat_value: "",
          column_vat_value_uzs: "",
          column_subtotal_with_taxes: "",
          column_subtotal_with_taxes_total: "",
          column_subtotal_with_taxes_uzs: ""
        }
      },
      sender_info: {
        id: 1
      },
      approver_and_signers: {
        approver: {
          position: "",
          last_name: "",
          first_name: "",
          middle_name: "",
          inn: "",
          pinfl: ""
        },
        signer: {
          position: "",
          last_name: "",
          first_name: "",
          middle_name: "",
          inn: "",
          pinfl: ""
        },
        signer_director: {
          position: "Руководитель ГО",
          last_name: "Tulyaganov",
          first_name: "Kudratilla",
          middle_name: "Asatullayevich",
          inn: "",
          pinfl: ""
        },
        signer_accountant: {
          position: "Главный бухгалтер филиала",
          last_name: "Абдуллаев",
          first_name: "Роман",
          middle_name: "Матназарович",
          inn: "",
          pinfl: ""
        },
        signer_warehouse: {
          position: null,
          last_name: null,
          first_name: null,
          middle_name: null,
          inn: null,
          pinfl: null
        }
      },
      receiver: {
        receiver_info: {
          inn: ""
        }
      }
    },

    standardActModel: {
      document: {
        document_number: "",
        document_date: "",
        contract_number: "",
        contract_date: "",
        factura_type: "0",
        document_description: "",
        doverennost_number: "",
        doverennost_date: null,
        doverennost_agent_name: "",
        doverennost_agent_inn: "",
        doverennost_agent_pinfl: "",
        korrektirovoch_document_uid: "",
        korrektirovoch_document_number: "",
        korrektirovoch_document_date: "",
        items: [
          {
            // temporary objects
            vat_temp: {
              vat_rate_temp: "12%",
              vat_value_temp: 0.12,
            },
            productSelected: {},
            measurementSelected: {},
            // end temporary objects
            item_number: "1",
            description: "",
            volume: "",
            unit_price: "",
            subtotal: "",
            vat: {
              vat_rate: "",
              vat_value: ""
            },
            subtotal_with_taxes: "",
            measurement_unit: "",
            excise: {
              excise_rate: "",
              excise_value: ""
            },
            catalog: {
              code: "",
              name: ""
            },
            lgota_id: "",
            barcode: ""
          }
        ],
        column_summary_values: {
          column_subtotal: "",
          column_subtotal_uzs: "",
          column_vat_value: "",
          column_vat_value_uzs: "",
          column_subtotal_with_taxes: "",
          column_subtotal_with_taxes_total: "",
          column_subtotal_with_taxes_uzs: ""
        }
      },
      sender_info: {
        id: 1
      },
      approver_and_signers: {
        approver: {
          position: "",
          last_name: "",
          first_name: "",
          middle_name: "",
          inn: "",
          pinfl: ""
        },
        signer: {
          position: "",
          last_name: "",
          first_name: "",
          middle_name: "",
          inn: "",
          pinfl: ""
        },
        signer_director: {
          position: "Руководитель ГО",
          last_name: "Tulyaganov",
          first_name: "Kudratilla",
          middle_name: "Asatullayevich",
          inn: "",
          pinfl: ""
        },
        signer_accountant: {
          position: "Главный бухгалтер филиала",
          last_name: "Абдуллаев",
          first_name: "Роман",
          middle_name: "Матназарович",
          inn: "",
          pinfl: ""
        },
        signer_warehouse: {
          position: null,
          last_name: null,
          first_name: null,
          middle_name: null,
          inn: null,
          pinfl: null
        }
      },
      receiver: {
        receiver_info: {
          inn: ""
        }
      }
    }
  },
  getters: {
    getDocumentList: (state) => state.documentList,
    getIsListLoading: (state) => state.isListLoading,
    getIsDocumentDetailLoading: (state) => state.isDocumentDetailLoading,
    getDocumentDetail: (state) => state.documentDetail,
    getDocumentPreview: (state) => state.documentPreview,
    getDocumentCreateTypeList: (state) => state.documentCreateTypeList,
    getInvoiceTypeList: (state) => state.invoiceTypeList,
    getInvoiceReceiverDetails: (state) => state.invoiceReceiverDetails,
    getAsakabankAccountList: (state) => state.asakabankAccountList,
    getVatPercentageList: (state) => state.vatPercentageList,
  },
  mutations: {

  },
  actions: {
    fetchGetDocumentList({ commit, dispatch }, payload){
      commit(SET_ENTITY, { module: "facture", entity: "isListLoading", value: true}, { root: true })
      this.$axios.get(`/facture/get_docs/`, {
        params: {
          ...payload,
          page_size: 15
        }
      })
        .then(({data}) => {
          commit(SET_ENTITY, {
            module: "facture",
            entity: "documentList",
            value: data.documents
          }, { root: true });
          dispatch("fetchPagination", data.totalCount, {root: true})
        })
        .catch((err) => {

        })
        .finally(() => {
          commit(SET_ENTITY, { module: "facture", entity: "isListLoading", value: false}, { root: true })
        })
    },
    /**/
    fetchGetDocumentDetail({ commit, dispatch }, payload){
      commit(SET_ENTITY, { module: "facture", entity: "isDocumentDetailLoading", value: true}, { root: true })
      this.$axios.get(`/facture/get_docs/detail/${payload.id}/`)
        .then(({ data }) => {
          commit(SET_ENTITY, {
            module: "facture",
            entity: "documentDetail",
            value: data
          }, { root: true });

          dispatch('fetchGetDocumentPreview', payload)
        })
        .catch(err => {

        })
        .finally(() => {

        })
    },
    /**/
    fetchGetDocumentContent({ commit }, payload){
      return new Promise((resolve, reject) => {
        this.$axios.post(`/facture/get_docs/document_content/`, {
          document_ids: [
            payload.id
          ]
        })
          .then(({ data }) => {
            resolve(data)
          })
      })
    },
    /**/
    fetchGetDocumentPreview({ commit }, payload){
      return new Promise((resolve, reject) => {
        this.$axios.get(`/facture/get_docs/detail/preview_doc/${payload.id}/`, {
          params: {
            preview_type: "html"
          }
        })
          .then(({ data }) => {
            commit(SET_ENTITY, {
              module: "facture",
              entity: "documentPreview",
              value: data
            }, { root: true });
          })
          .catch(err => {

          })
          .finally(() => {
            commit(SET_ENTITY, { module: "facture", entity: "isDocumentDetailLoading", value: false}, { root: true })
          })
      })
    },
    /**/
    fetchGetInvoiceReceiverDetails({commit}, payload) {
      return new Promise((resolve, reject) => {
        this.$axios.get(`facture/company_basic_details/`, {
          params: {
            company_inn: payload
          }
        })
          .then(({ data }) => {
            resolve(data);
            commit(SET_ENTITY, {
              module: "facture",
              entity: "invoiceReceiverDetails",
              value: data
            }, { root: true })
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    /**/
    fetchDownloadPDFDocument({ commit }, payload) {
      commit(SET_ENTITY, { module: "facture", entity: "isDocumentDetailLoading", value: true}, { root: true })
      return new Promise((resolve, reject) => {
        this.$axios.get(`/facture/get_docs/detail/preview_doc/${payload.id}/`, {
          params: {
            preview_type: "pdf",
          },
          responseType: "blob"
        })
          .then(response => {
            const url = window.URL.createObjectURL(new Blob([response.data], {type: 'application/pdf'}));
            const fileName = `${payload.title}.pdf`;
            const link = document.createElement("a");
            link.href = url;
            link.setAttribute("download", fileName);
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
          })
          .catch(err => {

        }).finally(() => {
          commit(SET_ENTITY, { module: "facture", entity: "isDocumentDetailLoading", value: false}, { root: true })
        })

      })
    }
  },

}
