import { colorsList } from "@/common/enums/notificationTypes"
import { currentDate } from "@/common/helpers"
import CrudService from "@/services/crud.service"
import { PUSH_ENTITY, SET_ENTITY, UNSHIFT_ENTITY } from "@/store/mutation-types"
import { format, isValid } from "date-fns"


export default {
	namespaced: true,
	/*
	*
	* */
	state: {
		calendar: {
			ref: ""
		},
		eventMenuActivator: {
			absolute: false
		},
		eventTypeState: null,
		calendarTypeList: [
			{
				title: "День",
				value: "day",
        code: "D"
			},
			{
				title: "Неделя",
				value: "week",
        code: "W"
			},
			{
				title: "Месяц",
				value: "month",
        code: "M"
			}
		],
		calendarType: {
			title: "Месяц",
			value: "month"
		},
		changedDate: currentDate,
		isEventModalVisible: false,
		isEventDrawerVisible: false,
		activatorMenuType: null,
		selectedEvent: {},
		selectedElement: null,
		eventState: false,
		eventsList: [],
		eventCreateModel: {
			start_date: null,
			end_date: null,
			attachments: [],
			participants: null,
			room: null,
			description: null,
			notify_via: null,
			organizer: null,
			meeting_type: "offline",
			link: null,
			__zoom_link: null,
			__cisco_link: null,
			__meeting_type: true,
			__hours: {
				start: "09:00",
				end: "18:00",
				calendar_format: null
			},
			__participants: [],
			__organizer: [],
			__files: []
		},
		tasksList: [],
		controlTasksList: [],
		taskCreateModel: {
			start_date: null,
			description: null,
			__start_hour: null
		},
		mergedEventsList: [],
		myCalendarList: []
	},
	/*
	*
	* */
	getters: {
		getCalendar: (state) => state.calendar,
		getEventMenuActivator: (state) => state.eventMenuActivator,
		getEventTypeState: (state) => state.eventTypeState,
		getCalendarTypeList: (state) => state.calendarTypeList,
		getCalendarType: (state) => state.calendarType,
		getChangedDate: (state) => state.changedDate,
		isEventModalVisible: (state) => state.isEventModalVisible,
		getIsEventDrawerVisible: (state) => state.isEventDrawerVisible,
		getActivatorMenuType: (state) => state.activatorMenuType,
		getSelectedEvent: (state) => state.selectedEvent,
		getSelectedElement: (state) => state.selectedElement,
		getEventState: (state) => state.eventState,
		getTasksList: (state) => state.tasksList,
		getEventsList: (state) => state.eventsList,
		getMergedEventsList: (state) => state.mergedEventsList,
		getMyCalendarList: (state) => state.myCalendarList,
		getControlTasksList: (state) => state.controlTasksList
	},
	/*
	*
	* */
	mutations: {
		/*
		*
		* */
		"SET_CALENDAR_REF"(state, payload) {
			state.calendar.ref = payload
		},
		/*
		 *
		 * */
		UPDATE_CHANGED_DATE(state, payload) {
			state.changedDate = payload;
		},
		/*
		*
		* */
		"CLEAR_EVENT"(state) {
			state.eventsList = []
		},
		/*
		*
		* */
		"SET_EVENT_MENU_ACTIVATOR"(state, payload) {
			state.eventMenuActivator[payload.entity] = payload.value
		},
		/*
		*
		* */
		"SET_CALENDAR_LIST"(state, payload) {
			state.myCalendarList = payload
		}
	},
	/*
	*
	* */
	actions: {
		eventTypeState({ commit, state }, payload) {
			commit(SET_ENTITY, { module: "gridCalendar", entity: "eventTypeState", value: payload }, { root: true })

			if(state.mergedEventsList.length) {
				let mockEvent = state.mergedEventsList[state.mergedEventsList.length - 1]

				switch (payload) {
					case "EventView":
						mockEvent.color = colorsList.PRIMARY
						break;
					case "TaskView":
						mockEvent.color = colorsList.WARNING
						break;
					default:
				}
			}
		},
		/*
		*
		* */
		toggleEventDescriptionModal({ commit }, payload) {
			commit(SET_ENTITY, { module: "gridCalendar", entity: "isEventModalVisible", value: payload }, { root: true })
		},
		/*
		*
		* */
		changeCalendarType({ commit }, payload) {
			commit(SET_ENTITY, { module: "gridCalendar", entity: "calendarType", value: payload }, { root: true })
		},
		changeCalendarTypeList({ commit }, payload) {
			commit(SET_ENTITY, { module: "gridCalendar", entity: "calendarTypeList", value: payload }, { root: true })
		},
		/*
		*
		* */
		toggleEventDrawer({ commit }, payload) {
			commit(SET_ENTITY, { module: "gridCalendar", entity: "isEventDrawerVisible", value: payload }, { root: true })
		},
		/*
		*
		* */
		toggleEventState({ commit }, payload) {
			commit(SET_ENTITY, { module: "gridCalendar", entity: "eventState", value: payload }, { root: true })
		},
		/*
		*
		* */
		activeCurrentAction({ commit, state }, payload) {
			// False state
			if(!payload.state) {
				payload.__actions.forEach(item => {
					let index = state.mergedEventsList.findIndex(i => i.id === item.id)

					if(index >= 0) {
						return state.mergedEventsList.splice(index, 1)
					}
				})
			}

			// True state
			if(payload.state) {
				commit(SET_ENTITY, {
					module: "gridCalendar",
					entity: "mergedEventsList",
					value: [...state.mergedEventsList, ...payload.__actions]
				}, { root: true }
				)
			}

			// // Show only event
			// if(payload.includes("EVENT") && !payload.includes("TASK")) {
			// 	commit(SET_ENTITY, { module: "gridCalendar", entity: "mergedEventsList", value: state.eventsList }, { root: true })
			// }
			// // Show only tasks
			// if(!payload.includes("EVENT") && payload.includes("TASK")) {
			// 	commit(SET_ENTITY, { module: "gridCalendar", entity: "mergedEventsList", value: state.tasksList }, { root: true })
			// }
			// // Show both (event & tasks)
			// if(payload.includes("EVENT") && payload.includes("TASK")) {
			// 	commit(SET_ENTITY, { module: "gridCalendar", entity: "mergedEventsList", value: [...state.eventsList, ...state.tasksList] }, { root: true })
			// }
		},
		/*
		*
		* */
		absoluteMenuActivator({ commit }, payload) {
			commit("SET_EVENT_MENU_ACTIVATOR", {
				entity: "absolute",
				value: payload
			})
		},
		/*
		*
		* */
		async showMockEvent({ commit, state, dispatch }, payload) {
			state.eventCreateModel.__hours.calendar_format = payload.date

			let eventModel = {
				name: payload.title,
				start: payload.date,
				color: state.eventTypeState === "TaskView" ? colorsList.WARNING : colorsList.PRIMARY,
				textColor: "#FFF",
				mock: true
			}

			commit(PUSH_ENTITY, {
				module: "gridCalendar",
				entity: "mergedEventsList",
				value: eventModel
			}, { root: true })

			await payload.nextTick.then(() => {
				let mockElement = document.querySelector(`[data-date="${payload.date}"]`)

				dispatch("triggerMenuActivator", {
					event: {},
					activator: mockElement,
					type: "MockEvent"
				})
			})
		},
		/*
		*
		* */
		deleteShowedMockEvent({ state }) {
			let index = state.mergedEventsList.findIndex(item => item.mock)

			if(index >= 0) {
				state.mergedEventsList.splice(index, 1)
			}
		},
		/*
		*
		* */
		clearEvents({ commit }) {
			commit("CLEAR_EVENT")
		},
		/*
		*
		* */
		clearActionForm({ state }) {
			if(state.eventTypeState === 'EventView') {
				state.eventCreateModel.start_date = null
				state.eventCreateModel.end_date = null
				state.eventCreateModel.attachments = []
				state.eventCreateModel.participants = null
				state.eventCreateModel.room = null
				state.eventCreateModel.description = null
				state.eventCreateModel.notify_via = null
				state.eventCreateModel.organizer = null
				state.eventCreateModel.meeting_type = "offline"
				state.eventCreateModel.link = null
				state.eventCreateModel.__zoom_link = null
				state.eventCreateModel.__cisco_link = null
				state.eventCreateModel.__meeting_type = true
				state.eventCreateModel.__hours.start = "09:00"
				state.eventCreateModel.__hours.end = "18:00"
				state.eventCreateModel.__participants = []
				state.eventCreateModel.__organizer = []
				state.eventCreateModel.__files = []
			}
			else {
				state.taskCreateModel.start_date = null
				state.taskCreateModel.description = null
				state.taskCreateModel.__start_hour = null
			}
		},
		/*
		*
		* */
		triggerMenuActivator({ commit, state, dispatch }, payload) {
			const open = () => {
				state.selectedEvent = payload.event
				state.selectedElement = payload.activator
				state.activatorMenuType = payload.type
				requestAnimationFrame(() => requestAnimationFrame(() => dispatch("toggleEventDescriptionModal", true)))
			}

			requestAnimationFrame(() => requestAnimationFrame(() => open()))
		},
    /**
     * @param {*} param0
     * @param {*} payload
     * @returns
     */
    setAllDay({ state }, payload) {
      if(payload) {
        state.eventCreateModel.__hours.start = "09:00"
        state.eventCreateModel.__hours.end = "18:00"
      }
      else {
        state.eventCreateModel.__hours.start = "09:00"
        state.eventCreateModel.__hours.end = "18:00"
      }
    },
		/*
		*
		* */
		fetchCreateEvent({ commit, state }, payload) {
			return new Promise((resolve, reject) => {
				// Если выбран оффлайн в чекбокс
				if(state.eventCreateModel.__meeting_type) {
					state.eventCreateModel.meeting_type = "offline"
					state.eventCreateModel.link = null
					state.eventCreateModel.__zoom_link = null
					state.eventCreateModel.__cisco_link = null
				}
				// Если выбран онлайн в чекбокс
				else {
					state.eventCreateModel.room = null

					if(state.eventCreateModel.meeting_type === "zoom") {
						state.eventCreateModel.link = state.eventCreateModel.__zoom_link
					}

					if(state.eventCreateModel.meeting_type === "cisco") {
						state.eventCreateModel.link = state.eventCreateModel.__cisco_link
					}
				}

				state.eventCreateModel.start_date = `${state.changedDate}T${state.eventCreateModel.__hours.start}:00`
				state.eventCreateModel.end_date = `${state.changedDate}T${state.eventCreateModel.__hours.end}:00`
				state.eventCreateModel.participants = state.eventCreateModel.__participants.map(user => {
					return {
						user: user.id
					}
				})
				state.eventCreateModel.organizer = state.eventCreateModel.__organizer.id
				state.eventCreateModel.attachments = state.eventCreateModel.__files.map(file => {
					return {
						id: file.id
					}
				})

				let model = {
					...payload,
					...state.eventCreateModel
				}

				CrudService.post(`/calendar/event/`, model)
					.then((data) => {
						let model = {
							...data,
							name: data.title,
							start: isValid(new Date(data.start_date)) ? format(new Date(data.start_date), "yyyy-MM-dd HH:mm") : "0000-00-00 00:00",
							end: isValid(new Date(data.end_date)) ? format(new Date(data.end_date), "yyyy-MM-dd HH:mm") : "0000-00-00 00:00",
							color: colorsList.PRIMARY,
							textColor: "#FFF",
							__hours: {
								start: isValid(new Date(data.start_date)) ? format(new Date(data.start_date), "HH:mm") : "0000-00-00 00:00",
								end: isValid(new Date(data.end_date)) ? format(new Date(data.end_date), "HH:mm") : "0000-00-00 00:00",
								calendar_format: isValid(new Date(data.start_date)) ? format(new Date(data.start_date), "yyyy-MM-dd") : "0000-00-00"
							},
							__meeting_type: data.meeting_type === "offline",
							__files: data.attachments,
							__participants: data.participants,
							__organizer: data.organizer,
							__zoom_link: data.link,
							__cisco_link: data.link,
						}

						commit(UNSHIFT_ENTITY, {
							module: "gridCalendar",
							entity: "eventsList",
							value: model
						}, { root: true })

						commit(UNSHIFT_ENTITY, {
							module: "gridCalendar",
							entity: "mergedEventsList",
							value: model
						}, { root: true })

						resolve()
					})
			})
		},
		/*
		*
		* */
		fetchGetEventsList({ commit }, payload) {
			return new Promise((resolve, reject) => {
				CrudService.getList(`/calendar/event/`, {
					page_size: 50,
					...payload
				})
					.then((data) => {
						let model = data.results.map(item => {
							return {
								...item,
								name: item.title,
								start: isValid(new Date(item.start_date)) ? format(new Date(item.start_date), "yyyy-MM-dd HH:mm") : "0000-00-00 00:00",
								end: isValid(new Date(item.end_date)) ? format(new Date(item.end_date), "yyyy-MM-dd HH:mm") : "0000-00-00 00:00",
								color: colorsList.PRIMARY,
								textColor: "#FFF",
								__meeting_type: item.meeting_type === "offline",
								__hours: {
									start: isValid(new Date(item.start_date)) ? format(new Date(item.start_date), "HH:mm") : "0000-00-00 00:00",
									end: isValid(new Date(item.end_date)) ? format(new Date(item.end_date), "HH:mm") : "0000-00-00 00:00",
									calendar_format: isValid(new Date(item.start_date)) ? format(new Date(item.start_date), "yyyy-MM-dd") : "0000-00-00"
								},
								__files: item.attachments,
								__participants: item.participants,
								__organizer: item.organizer,
							}
						})

						commit(SET_ENTITY, { module: "gridCalendar", entity: "eventsList", value: model }, { root: true })

						resolve(model)
					})
			})
		},
		fetchControlTasksList({commit}) {
			const year = new Date().getFullYear();
			return new Promise((resolve, reject) => {
				CrudService.getList(`/calendar/control_tasks/?deadline_start_date=${year}-01-01&deadline_end_date=${year}-12-31`, {
					page_size: 500
				})
					.then((data) => {
						const model = data.results.map(item => {
							return {
								...item,
								name: item.description,
								start: isValid(new Date(item.deadline)) ? format(new Date(item.deadline), "yyyy-MM-dd HH:mm") : "0000-00-00 00:00",
								end: isValid(new Date(item.deadline)) ? format(new Date(item.deadline), "yyyy-MM-dd HH:mm") : "0000-00-00 00:00",
								color: colorsList.SUCCESS,
								textColor: "#FFF",
								type: 'control-event',
								__hours: {
									start: isValid(new Date(item.deadline)) ? format(new Date(item.deadline), "HH:mm") : "0000-00-00 00:00",
									end: isValid(new Date(item.deadline)) ? format(new Date(item.deadline), "HH:mm") : "0000-00-00 00:00",
									calendar_format: isValid(new Date(item.deadline)) ? format(new Date(item.deadline), "yyyy-MM-dd") : "0000-00-00"
								},
							}
						})
						commit(SET_ENTITY, { module: "gridCalendar", entity: "controlTasksList", value: model }, { root: true })
						resolve(model)
					}).catch(() => {
						resolve([])
					})
			})
		},
		/*
		*
		* */
		fetchEventRelation({ commit, state }, payload) {
			return new Promise((resolve, reject) => {
				CrudService.getList(`/calendar/event/`, {
					page_size: 50,
					member: payload.user.map(item => item.id).join(),
					...payload.params
				})
					.then((data) => {
						state.mergedEventsList = [...state.eventsList, ...state.tasksList]

						let filteredList = payload.user.map(item => {
							let actions = data.results.filter(action => action.created_by.id === item.id)

							let model = actions.map(action => {
								return {
									...action,
									name: action.title,
									start: isValid(new Date(action.start_date)) ? format(new Date(action.start_date), "yyyy-MM-dd HH:mm") : "0000-00-00 00:00",
									end: isValid(new Date(action.end_date)) ? format(new Date(action.end_date), "yyyy-MM-dd HH:mm") : "0000-00-00 00:00",
									color: item.color,
									textColor: "#FFF",
									__meeting_type: action.meeting_type === "offline",
									__hours: {
										start: isValid(new Date(action.start_date)) ? format(new Date(action.start_date), "HH:mm") : "0000-00-00 00:00",
										end: isValid(new Date(action.end_date)) ? format(new Date(action.end_date), "HH:mm") : "0000-00-00 00:00",
										calendar_format: isValid(new Date(action.start_date)) ? format(new Date(action.start_date), "yyyy-MM-dd") : "0000-00-00"
									},
									__files: action.attachments,
									__participants: action.participants,
									__organizer: action.organizer,
								}
							})

							return {
								...item,
								__actions: model
							}
						})

						// commit(SET_ENTITY, {
						// 	module: "gridCalendar",
						// 	entity: "myCalendarList",
						// 	value: [
						// 		...state.myCalendarList.slice(0, 2),
						// 		...filteredList,
						// 	]
						// }, { root: true })

						filteredList.forEach(item => {
							item.__actions.forEach(action => {
								commit(PUSH_ENTITY, {
										module: "gridCalendar",
										entity: "mergedEventsList",
										value: action
									}, { root: true }
								)
							})
						})
					})
			})
		},
		/*
		*
		* */
		fetchGetEventById({ commit, state }, payload) {
			return new Promise((resolve, reject) => {
				CrudService.getList(`/calendar/event/${payload.id}/`)
					.then((data) => {
						let model = {
							...data,
							__meeting_type: data.meeting_type === "offline",
							__hours: {
								start: format(new Date(data.start_date), "HH:mm"),
								end: format(new Date(data.end_date), "HH:mm"),
								calendar_format: isValid(new Date(data.start_date)) ? format(new Date(data.start_date), "yyyy-MM-dd") : "0000-00-00"
							},
							__files: data.attachments,
							__participants: data.participants,
							__organizer: data.organizer,
							__zoom_link: data.link,
							__cisco_link: data.link,
						}

						commit(SET_ENTITY, { module: "gridCalendar", entity: "eventCreateModel", value: model }, { root: true })

						resolve()
					})
			})
		},
		/*
		*
		* */
		fetchUpdateEventById({ commit, state, dispatch }, payload) {

			let model = {
				...payload.model,
				start_date: `${payload.model.__hours.calendar_format}T${payload.model.__hours.start}:00`,
				end_date: `${payload.model.__hours.calendar_format}T${payload.model.__hours.end}:00`,
				status: payload.status,
				organizer: payload.model.__organizer.id,
				participants: payload.model.__participants.map(user => {
					return {
						user: user.user ? user.user.id : user.id
					}
				}),
				attachments: payload.model.__files.map(file => {
					return {
						id: file.id
					}
				}),
			}

			// Если выбран оффлайн в чекбокс
			if(model.__meeting_type) {
				model.meeting_type = "offline"
				model.link = null
				model.__zoom_link = null
				model.__cisco_link = null
			}
			// Если выбран онлайн в чекбокс
			else {
				model.room = null

				if(model.meeting_type === "zoom") {
					model.link = model.__zoom_link
				}

				if(model.meeting_type === "cisco") {
					model.link = model.__cisco_link
				}
			}


			return new Promise((resolve, reject) => {
				this.$axios.put(`/calendar/event/${payload.id}/`, model)
					.then(({ data }) => {

						// dispatch("mergeEventsList")
						let object = state.eventsList.find(item => item.id === payload.id)

						// object.name = data.title
						object.status = data.status
						// object.__files = data.attachments


						resolve()
					})
					.catch((error) => {
						reject(error)
					})
			})
		},
		/*
		*
		* */
		fetchDeleteEvent({ commit }, payload) {
			return new Promise((resolve, reject) => {
				CrudService.delete(`/calendar/event/${payload.id}/`)
					.then(() => {
						resolve()
					})
					.catch(() => {
						reject()
					})
			})
		},
		/*
		*
		* */
		fetchGetTasksList({ commit, rootState }, payload) {
			return new Promise((resolve, reject) => {
				CrudService.getList(`/calendar/task/`, {
					page_size: 50,
					...payload
				})
					.then((data) => {
						let model = data.results.map(item => {
							return {
								...item,
								name: item.title,
								start: isValid(new Date(item.start_date)) ? format(new Date(item.start_date), "yyyy-MM-dd HH:mm") : "0000-00-00 00:00",
								__start_hour: isValid(new Date(item.start_date)) ? format(new Date(item.start_date), "HH:mm") : "0000-00-00 00:00",
								color: colorsList.WARNING,
								textColor: "#FFF",
								__calendar_format: isValid(new Date(item.start_date)) ? format(new Date(item.start_date), "yyyy-MM-dd") : "0000-00-00",
								__is_completed: item.status === "finished"
							}
						})

						commit(SET_ENTITY, {
							module: "gridCalendar",
							entity: "tasksList",
							value: model
						}, { root: true })

						resolve(model)
					})
			})
		},
		/*
		*
		* */
		fetchCreateTask({ commit, state }, payload) {
			// state.taskCreateModel.start_date = `${state.changedDate}T${state.taskCreateModel.__start_hour ? state.taskCreateModel.__start_hour : '00:00'}:00`
			state.taskCreateModel.start_date = state.changedDate

			let model = {
				...payload,
				...state.taskCreateModel
			}

			return new Promise((resolve, reject) => {
				CrudService.post(`/calendar/task/`, model)
					.then((data) => {
						let model = {
							...data,
							name: data.title,
							start: isValid(new Date(data.start_date)) ? format(new Date(data.start_date), "yyyy-MM-dd HH:mm") : "0000-00-00 00:00",
							__start_hour: isValid(new Date(data.start_date)) ? format(new Date(data.start_date), "HH:mm") : "0000-00-00 00:00",
							color: colorsList.WARNING,
							textColor: "#FFF",
							__calendar_format: isValid(new Date(data.start_date)) ? format(new Date(data.start_date), "yyyy-MM-dd") : "0000-00-00"
						}

						commit(UNSHIFT_ENTITY, {
							module: "gridCalendar",
							entity: "tasksList",
							value: model
						}, { root: true })

						commit(UNSHIFT_ENTITY, {
							module: "gridCalendar",
							entity: "mergedEventsList",
							value: model
						}, { root: true })

						resolve()
					})
					.catch(() => {
						reject()
					})
			})
		},
		/*
		* TODO: Удалить функцию fetchAddTask
		* */
		fetchAddTask({ commit }, payload) {
			return new Promise((resolve, reject) => {
				CrudService.post(`/calendar/task/`, payload.model)
					.then((data) => {
						commit(PUSH_ENTITY, {
							module: "gridCalendar",
							entity: "tasksList",
							value: data
						}, { root: true })

						resolve()
					})
					.catch(() => {
						reject()
					})
			})
		},
		/*
		*
		* */
		fetchDeleteTask({ commit }, payload) {
			return new Promise((resolve, reject) => {
				CrudService.delete(`/calendar/task/${payload.id}/`)
					.then(() => {
						resolve()
					})
					.catch(() => {
						reject()
					})
			})
		},
		/*
		*
		* */
		fetchUpdateTask({ commit }, payload ) {
			return new Promise((resolve, reject) => {
				this.$axios.put(`/calendar/task/${payload.id}/`, payload.model)
					.then(() => {
						resolve()
					})
					.catch(() => {
						reject()
					})
			})
		},
		/*
		*
		* */
		fetchUpdateTaskStatus({ commit }, payload ) {
			return new Promise((resolve, reject) => {
				this.$axios.patch(`/calendar/task/${payload.id}/`, payload.model)
					.then(({ data }) => {
						resolve(data)
					})
					.catch(() => {
						reject()
					})
			})
		},
		/*
		*
		* */
		fetchSetNewEntityTask({ commit, state, dispatch }, payload) {
			let model = {
				...payload.model,
				status: payload.status,
				start_date: `${payload.model.__calendar_format}T${payload.model.__start_hour ? payload.model.__start_hour : '00:00'}:00`
			}

			return new Promise((resolve, reject) => {
				dispatch("fetchUpdateTaskStatus", {
					id: payload.id,
					model: model
				})
					.then((data) => {
						let object = state.tasksList.find(item => item.id === payload.id)

						object.name = data.title
						object.status = data.status
						object.start = isValid(new Date(data.start_date)) ? format(new Date(data.start_date), "yyyy-MM-dd HH:mm") : "0000-00-00 00:00"
						object.__start_hour = isValid(new Date(data.start_date)) ? format(new Date(data.start_date), "HH:mm") : "0000-00-00 00:00"
						object.__calendar_format = isValid(new Date(data.start_date)) ? format(new Date(data.start_date), "yyyy-MM-dd") : "0000-00-00"
						object.__is_completed = data.status === "finished"

						resolve()
					})
					.catch(() => {
						reject()
					})
			})
		},
		/*
		*
		* */
		deleteActionFromMergedEventsList({ state, dispatch }, { id, name }) {
			return new Promise((resolve, reject) => {
				dispatch(name, { id })
					.then(() => {
						let index = state.mergedEventsList.findIndex(item => item.id === id)

						state.mergedEventsList.splice(index, 1)
						resolve()
					})
					.catch(() => {
						reject()
					})
			})
		},
		/*
		*
		* */
		mergeEventsList({ commit, state, dispatch, rootState }) {
			setTimeout(() => {
				return Promise.all([
					dispatch("fetchGetEventsList"),
					dispatch("fetchGetTasksList"),
					dispatch("fetchControlTasksList")
				])
					.then((data) => {
						commit(SET_ENTITY, { module: "gridCalendar", entity: "mergedEventsList", value: data.flat(1) }, { root: true })

						commit(PUSH_ENTITY, {
							module: "gridCalendar",
							entity: "myCalendarList",
							value: {
								title: "Задача",
								id: 1,
								color: colorsList.WARNING,
								__actions: state.tasksList
							}
						}, { root: true })
						commit(PUSH_ENTITY, {
							module: "gridCalendar",
							entity: "myCalendarList",
							value: {
								title: "Контрольные задания",
								id: 0,
								color: colorsList.SUCCESS,
								__actions: state.controlTasksList
							}
						}, { root: true })

						let model = state.eventsList.filter(item => item.created_by.id === rootState.auth.currentUser.id)

						commit(PUSH_ENTITY, {
							module: "gridCalendar",
							entity: "myCalendarList",
							value: {
								...rootState.auth.currentUser,
								__actions: state.eventsList
							}
						}, { root: true })

					})
			}, 500)
		},
		/*
		*
		* */
		clearMyCalendarList({ state }) {
			state.myCalendarList = []
		}
	},
}
