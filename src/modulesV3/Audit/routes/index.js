const Audit = [
  {
    path: 'audit',
    name: 'AuditModule',
    meta: {
      isAuthRequired: true
    },
    component: () => import('@/modulesV3/Audit/views'),
    redirect: { name: 'AuditConfigureGroupsList' },
    children: [
      // Настроить группы
      {
        path: 'configure-groups-list',
        name: 'AuditConfigureGroupsList',
        meta: {
          isAuthRequired: true
        },
        component: () => import('@/modulesV3/Audit/views/ConfigureGroups'),
      },
      {
        path: 'configure-groups/detail/:id',
        name: 'AuditConfigureGroupsDetail',
        meta: {
          isAuthRequired: true
        },
        component: () => import('@/modulesV3/Audit/views/ConfigureGroups/Detail'),
      },
      // /Настроить группы

      // Справочник
      {
        path: 'reference',
        name: 'AuditReferenceIndex',
        meta: {
          isAuthRequired: true
        },
        component: () => import('@/modulesV3/Audit/views/Reference'),
        redirect: { name: 'AuditReferenceDirectionsList' },
        children: [
          {
            path: 'directions',
            name: 'AuditReferenceDirectionsList',
            meta: {
              isAuthRequired: true
            },
            component: () => import('@/modulesV3/Audit/views/Reference/Directions'),
          }
        ]
      },
      // /Справочник

      // Проверка
      {
        path: 'checkup',
        name: 'AuditCheckupIndex',
        meta: {
          isAuthRequired: true
        },
        component: () => import('@/modulesV3/Audit/views/Checkup'),
        redirect: { name: 'AuditCheckupDepartmentList' },
        children: [
          {
            path: 'department/list',
            name: 'AuditCheckupDepartmentList',
            meta: {
              isAuthRequired: true
            },
            component: () => import('@/modulesV3/Audit/views/Checkup/Department'),
          },
          {
            path: 'filial/list',
            name: 'AuditCheckupFilialList',
            meta: {
              isAuthRequired: true
            },
            component: () => import('@/modulesV3/Audit/views/Checkup/Filial'),
          },
          {
            path: 'process/list',
            name: 'AuditCheckupProcessList',
            meta: {
              isAuthRequired: true
            },
            component: () => import('@/modulesV3/Audit/views/Checkup/Process'),
          },
          {
            path: 'assign-directions/:id',
            name: 'AuditCheckupAssignDirections',
            meta: {
              isAuthRequired: true
            },
            component: () => import('@/modulesV3/Audit/views/Checkup/AssignDirections'),
          },
          {
            path: 'detail/:id/:audit_type/:audit_number/:status',
            name: 'AuditCheckupDetail',
            meta: {
              isAuthRequired: true
            },
            component: () => import('@/modulesV3/Audit/views/Checkup/Detail'),
          }
        ]
      }
      // /Проверка
    ]
  }
]

export default Audit
