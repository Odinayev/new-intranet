import Vue from "vue";
import Vuex from "vuex";
import modules from "./modules";
import mockStates from "@/store/mock-states"
import { SET_ENTITY, PUSH_ENTITY, UNSHIFT_ENTITY } from "@/store/mutation-types";
import CrudService from "@/services/crud.service"
import router from "@/router"
import { i18n } from "@/plugins/i18n";

import vuexPlugins from "@/plugins/vuexPlugins";

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		breakpointName: "xl",
		currentUserBirthday: false,
		documentStateList: [
			{
				title: i18n.t('components.completed'),
				value: "performed",
				color: "var(--primary-2)"
			},
			{
				title: i18n.t('components.today'),
				value: "today",
				color: "var(--primary-3)"
			},
			{
				title: i18n.t('components.less-three-days'),
				value: "three_days_left",
				color: "var(--primary-3)"
			},
			{
				title: i18n.t('components.done-late'),
				value: "overdue_performed",
				color: "var(--primary-4)"
			},
			{
				title: i18n.t('components.overdue'),
				value: "overdue",
				color: "var(--primary-6)"
			}
		],
		journalList: [],

		route: null,
		page_number: 1,
		notificationList: [],
		listCount: null,
		notificationListCount: 0,
		mockStates,
		isDynamicModuleLoaded: false,
		isLoading: false,
		isNotificationsLoading: false,
		isAttachedDocumentsLoading: false,
		isReviewListLoading: false,
		isDetailLoading: false,
		usersListChat: [],
		formType: {
			create: "create",
			read: "read",
			update: "update"
		},
		priorityList: [
			// {
			// 	title: "Критический",
			// 	value: "high"
			// },
			{
				title: i18n.t('emergent'),
				value: "high"
			},
			{
				title: i18n.t('with-deadline'),
				value: "medium"
			},
			{
				title: i18n.t('for-information'),
				value: "low"
			},
		],
		riskLevelList: [
			{
				title: 'Высокий',
				value: 'high'
			},
			{
				title: 'Средний',
				value: 'medium'
			},
			{
				title: 'Низкий',
				value: 'low'
			}
		],
		priorityListForCompose: [
			{
				title: i18n.t('reply-required'),
				value: "high"
			},
			{
				title: i18n.t('for-information'),
				value: "low"
			}
		],
		sendType: [
			{
				title: i18n.t('viaChancellery'),
				value: "chancellery"
			},
			{
				title: i18n.t('viaHybridPost'),
				value: "hybrid_mail"
			}
		],
		statusList: [
			{
				id: 1,
				title: "TO DO",
				translated: i18n.t('components.do'),
				name: "Сделать",
				disabled: false,
				description: "TO DO"
			},
			{
				id: 2,
				title: "IN PROGRESS",
				translated: i18n.t('components.in-progress'),
				name: "В прогресс",
				disabled: false,
				description: "В данный момент по этой проблеме ведется активная работа назначенным лицом."
			},
			{
				id: 3,
				title: "ON HOLD",
				translated: i18n.t('components.in-expectation'),
				name: "В ожидание",
				disabled: false,
				description: "ON HOLD"
			},
			{
				id: 4,
				title: "DONE",
				translated: i18n.t('components.done'),
				name: "Выполнено",
				disabled: false,
				description: "DONE"
			},
			{
				id: 5,
				title: "FOR SIGNATURE",
				translated: i18n.t('components.for-signature'),
				name: "На подпись",
				disabled: false,
				description: "На подпись"
			},
			{
				id: 6,
				title: "CANCEL",
				translated: i18n.t('cancels'),
				name: "Отменить",
				disabled: false,
				description: "Отменить"
			},
			{
				id: 7,
				title: "ON REVIEW",
				translated: i18n.t('components.on-review'),
				name: "На рассмотрении",
				disabled: false,
				description: "Исполнитель сделал"
			},
			{
				id: 8,
				title: "OVERDUE PERFORMANCE",
				translated: i18n.t('overdue-performance'),
				name: "Просроченное исполнение",
				disabled: false,
				description: "Просроченное исполнение"
			},
			{
				id: 9,
				title: "NOT PERFORMED",
				translated: i18n.t('components.task-not-done'),
				name: "Не выполнено",
				disabled: false,
				description: "Не выполнено"
			}
		],
		usersList: [],
		paginationOptions: {
			page: 1,
			page_size: 15,
			totalVisible: 7,
			page_length: null,
			list_length: null,
			total_visible: 7
		},
		unreadCountList: {},
		filterMenuBoxes: [
			{
				title: "На подпись",
				value: "sign",
				link: "BoxesSign",
				count: null,
				role: []
			},
			{
				title: "На согласование",
				value: "approval",
				link: "BoxesApproval",
				count: null,
				role: []
			},
			{
				title: "На рассмотрение",
				value: "review",
				link: "BoxesReview",
				count: null,
				role: [
					"admin",
					null,
					"chairman",
					"deputy_chair",
					"filial_manager",
					"assistant",
					"department_director",
					"department_deputy_director",
					"filial_deputy_manager",
					"filial_employee",
					"chancellery_admin",
					"network_operator",
					"head_of_group",
					"hr_manager",
					"filial_hr_manager",
					"filial_credit_monitoring",
					"credit_monitoring"
				]
			},
			{
				title: "Входящие",
				value: "inbox",
				link: "InboxTableType",
				count: null,
				role: [
					"admin",
					null,
					"chairman",
					"deputy_chair",
					"filial_manager",
					"filial_chancellery",
					"department_employee",
					"department_director",
					"department_deputy_director",
					"filial_deputy_manager",
					"filial_employee",
					"hr_manager",
					"filial_hr_manager",
					"chancellery_admin",
					"call_center",
					"network_operator",
					"head_of_group",
					"filial_credit_monitoring",
					"credit_monitoring"
				]
			},
			{
				title: "Созданные поручение",
				value: "created-resolutions",
				link: "CreatedResolutionsTableType",
				count: null,
				role: []
			},
			{
				title: "На контроль",
				value: "control",
				link: "ControlTableType",
				count: null,
				role: [
					"admin",
					"chancellery_admin"
				]
			}
		],
		uploadPercentage: 0,
		employeeConditionList: [
			'sick',
			'unpaid_leave',
			'business_trip',
			'vacation'
		],
		/** **/
		documentTypeList: [
			{
				id: 1,
				name: i18n.t('document-flow.views.send-document.interior'),
				journal_id: 3,
				type: 'service_letter'
			},
			{
				id: 2,
				name: i18n.t('document-flow.views.send-document.notification'),
				journal_id: 3,
				type: 'notice'
			},
			{
				id: 3,
				name: i18n.t('document-flow.views.send-document.outgoing'),
				journal_id: 4,
				type: 'service_letter'
			},
			{
				id: 4,
				name: i18n.t('document-flow.views.send-document.board-protocol'),
				journal_id: 6,
				type: 'protocol'
			},
			{
				id: 5,
				name: i18n.t('document-flow.views.send-document.committee-protocol'),
				journal_id: 6,
				type: 'credit_committee'
			},
			{
				id: 6,
				name: i18n.t('document-flow.views.send-document.statement'),
				journal_id: 7,
				type: 'hr_application'
			},
			{
				id: 7,
				name: i18n.t('document-flow.views.send-document.orders'),
				journal_id: 6,
				type: 'decree'
			},
			{
				id: 8,
				name: i18n.t('document-flow.views.send-document.act'),
				journal_id: 15,
				type: 'act'
			},
			{
				id: 9,
				name: i18n.t('document-flow.views.send-document.created-orders'),
				journal_id: 6,
				type: 'hr_order'
			},
			{
				id: 10,
				name: i18n.t('document-flow.views.send-document.other-organizations'),
				journal_id: 4,
				type: 'organization_letter'
			},
			{
				id: 11,
				name: i18n.t('trainingSpecialists'),
				journal_id: 6,
				type: 'compliance_regulation'
			}
		],
		/** **/
	},
	/*
	 *
	 * */
	getters: {
		getBreakpointName: (state) => state.breakpointName,
		getLoading: (state) => state.isLoading,
		getIsNotificationsLoading: (state) => state.isNotificationsLoading,
		getListCount: (state) => state.listCount,
		getAttachedDocumentsLoading: (state) => state.isAttachedDocumentsLoading,
		getReviewListLoading: (state) => state.isReviewListLoading,
		getDetailLoading: (state) => state.isDetailLoading,
		getUsersList: (state) => state.usersList,
		getUsersListChat: (state) => state.usersListChat,
		getPaginationOptions: (state) => state.paginationOptions,
		getUnreadCountList: (state) => state.unreadCountList,
		getFilterMenuBoxes: (state) => state.filterMenuBoxes,
		getNotificationList: (state) => state.notificationList,
		getNotificationListCount: (state) => state.notificationListCount,
		getDisableItems: () => (item) => item.condition_name !== 'Рабочие' &&
			item.condition_name !== 'Командировка' &&
			item.condition_name !== 'Рабочие (не штат.)' &&
			item.condition_name !== 'Трудавой отп.',
		getAllowedDates: () => val => val >= new Date().toISOString().substr(0, 10),
		getUploadPercentage: (state) => state.uploadPercentage,
		getEmployeeConditionList: (state) => state.employeeConditionList,
		getDocumentTypeList: (state) => state.documentTypeList,
	},
	/*
	 *
	 * */
	mutations: {
		[SET_ENTITY](state, { module, entity, value }) {
			module ? state[module][entity] = value : state[entity] = value
		},
		/*
		*
		* */
		[PUSH_ENTITY](state, {module, entity, value}) {
			module
				? state[module][entity].push(value)
				: state[entity].push(value)
		},
		/*
		*
		* */
		[UNSHIFT_ENTITY](state, {module, entity, value}) {
			module
				? state[module][entity].unshift(value)
				: state[entity].unshift(value)
		},
		/*
		*
		* */
		LOADED_DYNAMIC_MODULE(state, payload) {
			state.isDynamicModuleLoaded = payload;
		},
		/**/
		SET_ROUTE(state, route) {
			state.route = route;
		},
		/*
		*
		* */
		'LOADING'(state, payload) {
			state.isLoading = payload
		},
		/*
		*
		* */
		SET_PAGINATION(state, payload) {
			state.paginationOptions.page_length = Math.ceil(payload / state.paginationOptions.page_size);
			state.paginationOptions.list_length = payload
		},
		SET_NOTIFICATION_CONTENT(state, payload){
			state.notificationContent = payload;
			state.isNotificationCome = !state.isNotificationCome
		},
		SET_RU_LOADER(state, payload) {
			state.isReviewListLoading = payload;
		},
		SET_PDF_LOADER(state, payload) {
			state.isAttachedDocumentsLoading = payload;
		},
		SET_DETAIL_LOADER(state, payload) {
			state.isDetailLoading = payload;
		},
		MERGE_NOTIFICATION(state, payload){
			state.notificationList.push(...payload.results);
			state.listCount = payload.count;
		},
	},
	/*
	 *
	 * */
	actions: {
		initBaseRequest({ commit, dispatch }, payload) {
			dispatch("fetchJournalList")
		},
		/*
		* Получать лист журналов
		* */
		fetchJournalList({ commit, rootState }) {
			CrudService.getList(`/docflow/${rootState.auth.currentUser.filial}/journal/`)
			.then((data) => {
				commit(SET_ENTITY, {
					module: null,
					entity: "journalList",
					value: data.results
				}, {root: true})
			})
		},
		/*
		*
		* */
		setBreakpointName({ commit }, payload) {
			commit(SET_ENTITY, { module: null, entity: "breakpointName", value: payload.breakpoint })
		},
		/*
		*
		* */
		fetchCurrentUserBirthday({ commit }, payload) {
			commit(SET_ENTITY, { module: null, entity: "currentUserBirthday", value: payload })
		},
		/*
		*
		* */
		fetchUsersList({ commit }, { search, pageSize }) {
			this.$axios.get("/user/search/", {
				params: {
					search: search,
					page_size: pageSize
				}
			})
				.then(({ data }) => {
					commit(SET_ENTITY, { module: null, entity: "usersList", value: data.results })
				})
		},
		/*
		*
		* */
		fetchUsersListChat({ commit }, { search }) {
			this.$axios.get("/user/search/for_chat/", {
				params: {
					search: search
				}
			})
				.then(({ data }) => {
					commit(SET_ENTITY, { module: null, entity: "usersListChat", value: data.results })
				})
		},
		/*
		*
		* */
		clearUserList({ commit, dispatch }) {
			dispatch("fetchUsersList", { search: "" })
		},
		/*
		*
		* */
		fetchPagination({ commit }, payload) {
			commit("SET_PAGINATION", payload)
		},
		/*
		*
		* */
		fetchUnreadCountList({ commit, rootState, state }) {
			CrudService.getList(`/docflow/${rootState.auth.currentUser.filial}/unread_count/`)
				.then((data) => {
					rootState.filterMenuBoxes[0].count = data.unsigned_count
					rootState.filterMenuBoxes[1].count = data.agreement_count
					rootState.filterMenuBoxes[2].count = data.review_count
					rootState.filterMenuBoxes[3].count = data.assignment_count
					rootState.documentFlowModule.newJournalList[5].count = data.unregistered_count
					commit(SET_ENTITY, { module: null, entity: "unreadCountList", value: data })
				})
		},
		/** **/
		fetchNotificationList({ commit }, payload) {
			commit(SET_ENTITY, { entity: "isNotificationsLoading", value: true }, { root: true })
			CrudService.getList(`/alert/notifications/`, {
				page_size: 15,
				page: payload ? payload.page : 1
			})
				.then((data) => {
					if (payload && payload.loadMore){
						commit("MERGE_NOTIFICATION", { results: data.results, count: data.count })
					}else {
						commit(SET_ENTITY, { entity: "notificationList", value: data.results }, { root: true });
						commit(SET_ENTITY, { entity: "listCount", value: data.count }, { root: true });
					}
					commit(SET_ENTITY, { entity: "isNotificationsLoading", value: false }, { root: true })
				}).catch(err => {
				commit(SET_ENTITY, { entity: "isNotificationsLoading", value: false }, { root: true })
			})
		},
		/**/
		fetchNotification({ dispatch, state }, payload){
			if (payload.type === 'new_message' && state.route.name !== 'MainChat'){
				let shortenedText = payload.content.message.substring(0, 30)
				Vue.notify({
					group: 'notification',
					title: payload.content.sender.first_name + ' ' + payload.content.sender.last_name,
					text: shortenedText.replace(/<\/?[^>]+(>|$)/g, ""),
					duration: 5000,
					speed: 1000
				});
			}else {
				if (payload.type === 'new_message'){
					return;
				}
				Vue.notify({
					group: 'notification',
					title: payload.content.title,
					text: payload.content && payload.content.content ? payload.content.content.replace(/<\/?[^>]+(>|$)/g, "") : null,
					duration: 5000,
					speed: 1000
				});
				setTimeout(() => {
					dispatch("fetchNotificationList")
				}, 1000)
			}

		},
		/**/
		fetchNotificationDelete( { commit, state }, payload ){
			return new Promise((resolve, reject) => {
				this.$axios.delete(`/alert/notifications/${ payload }/`)
					.then(({data}) => {
						const index = state.notificationList.findIndex(item => item.id === payload);
						state.notificationList.splice(index, 1);
						resolve(payload);
					})
					.catch((err) => {
						reject(err);
					})
			})
		},
		/**/
		fetchNotificationMarsAsRead({ commit, state }, payload) {
			return new Promise((resolve, reject) => {
				this.$axios.put(`/alert/notifications/${payload}/mark_as_read/`)
					.then(({data}) => {
						state.notificationList.find(item => item.id === payload).is_read = true;
						resolve(data);
					})
					.catch((err) => {
						reject(err);
					})
			})
		},
		/**/
		fetchNotificationMarkAsAllRead({ commit }) {
			return new Promise((resolve, reject) => {
				this.$axios.put(`/alert/notifications/mark_all_as_read/`)
					.then(() => {
						resolve()
					})
					.catch(err => {
						reject(err)
					})
			})
		},
		/**/
		fetchNotificationListUnreadCount({ commit, dispatch }) {
			CrudService.getList(`/alert/notifications/unread_count/`)
				.then((data) => {
					commit(
						SET_ENTITY, {
							module: null,
							entity: "notificationListCount",
							value: data.count
						}
					)

					dispatch("fetchUnreadCountList")
				})
		},
		/**/
		fetchNotificationListUnreadCountFromSocket({ commit }, payload) {
			commit(
				SET_ENTITY, {
					module: null,
					entity: "notificationListCount",
					value: payload.count
				}
			)
		},
		/*
		*
		* */
		setDateSelect({ commit }, { value, state, start, end }) {
			state[start] = value[0]
			state[end] = value[1]
		},
		/*
		*
		* */
		clearDateSelect({ commit }, { state, start, end }) {
			state[start] = null
			state[end] = null
		},
		/** **/
		fetchNewMessageNotification({ commit }, payload) {
			if (!isTabActive()) {
				if (Notification.permission === "granted") {
					createNotification(payload);
				} else if (Notification.permission !== "denied") {
					Notification.requestPermission().then(permission => {
						if (permission === "granted") {
							createNotification(payload);
						}
					});
				}
			}
		},
	},
	/*
	 *
	 * */
	modules,
	/*
	*
	* */
	plugins: [vuexPlugins]
});

let notification;

const createNotification = (payload) => {

	notification = new Notification(`Новое сообщение от ${payload?.content?.sender?.full_name}`, {
		body: payload?.content?.message,
		icon: '/img/logo_asaka_4.png'
	});

	// Handle click event on the notification
	notification.onclick = () => {
		// Handle click action
		console.log("Notification clicked!");
	};
}

// Check if the tab is visible
function handleVisibilityChange() {
	if (document.visibilityState === 'visible') {
		// Tab is now visible, close the notification if it exists
		if (notification) {
			notification.close();
		}
	}
}

// Listen for visibility change events
document.addEventListener('visibilitychange', handleVisibilityChange);

// Function to check if the tab is currently active
function isTabActive() {
	return document.visibilityState === 'visible';
}

