import {format} from "date-fns";
import {SET_ENTITY} from "../../../mutation-types";

let day = new Date()
export default {
  namespaced: true,
  state: {
    docTypeList: [
      {
        id: 1,
        title: "document-flow.views.send-document.interior",
        journal_id: 3,
        type: "service_letter",
        role: null,
        active: true,
        doc_active: true,
        disabled: true
      },
      {
        id: 2,
        title: "document-flow.views.send-document.notification",
        journal_id: 3,
        type: "notice",
        role: null,
        active: true,
        doc_active: false,
        disabled: false
      },
      {
        id: 3,
        title: "hr-notice",
        journal_id: 3,
        type: "hr_notice",
        role: null,
        active: false,
        doc_active: false,
        disabled: false
      },
      {
        id: 4,
        title: "hr-rotation",
        journal_id: 3,
        type: "hr_rotation",
        role: null,
        active: false,
        doc_active: false,
        disabled: false
      },
      {
        id: 5,
        title: "summary",
        journal_id: 3,
        type: "summary",
        role: null,
        active: false,
        doc_active: false,
        disabled: false
      },
      {
        id: 6,
        title: "document-flow.views.send-document.currency_summary",
        journal_id: 3,
        type: "currency_summary",
        role: null,
        active: false,
        doc_active: false,
        disabled: false
      },
      {
        id: 7,
        title: "document-flow.views.send-document.outgoing",
        journal_id: 4,
        type: "service_letter",
        role: null,
        active: true,
        doc_active: false,
        disabled: false
      },
      {
        id: 8,
        title: "document-flow.views.send-document.board-protocol",
        journal_id: 6,
        type: "protocol",
        role: null,
        active: true,
        doc_active: false,
        disabled: false
      },
      {
        id: 9,
        title: "document-flow.views.send-document.committee-protocol",
        journal_id: 6,
        type: "credit_committee",
        role: null,
        active: true,
        doc_active: false,
        disabled: false
      },
      {
        id: 10,
        title: "document-flow.views.send-document.statement",
        journal_id: 7,
        type: "hr_application",
        role: null,
        active: true,
        doc_active: false,
        disabled: false
      },
      {
        id: 11,
        title: "document-flow.views.send-document.orders",
        journal_id: 6,
        type: "decree",
        role: null,
        active: true,
        doc_active: false,
        disabled: false
      },
      {
        id: 12,
        title: "document-flow.views.send-document.act",
        journal_id: 15,
        type: "act",
        role: null,
        active: false,
        doc_active: false,
        disabled: false
      },
      {
        id: 13,
        title: "document-flow.views.send-document.created-orders",
        journal_id: 6,
        type: "hr_order",
        role: null,
        active: false,
        doc_active: false,
        disabled: false
      },
      {
        id: 14,
        title: "document-flow.views.send-document.other-organizations",
        journal_id: 4,
        type: "organization_letter",
        role: null,
        active: true,
        doc_active: false,
        disabled: false
      },
      {
        id: 15,
        title: "trainingSpecialists",
        journal_id: 6,
        type: "compliance_regulation",
        role: null,
        active: false,
        doc_active: false,
        disabled: false
      },
      {
        id: 16,
        title: "hr_contract",
        journal_id: 3,
        type: "hr_contract",
        role: null,
        active: false,
        doc_active: false,
        disabled: false
      },
      {
        id: 17,
        title: "businessTrip",
        journal_id: 6,
        type: "trip_license",
        role: null,
        active: true,
        doc_active: false,
        disabled: false
      },
      {
        id: 18,
        title: "actCompletedServices",
        journal_id: 3,
        type: "retail_act",
        role: null,
        active: false,
        doc_active: false,
        disabled: false
      },
      {
        id: 18,
        title: "irr",
        journal_id: 3,
        type: "irr",
        role: null,
        active: false,
        doc_active: false,
        disabled: false
      },
      {
        id: 19,
        title: "expenseNotice",
        journal_id: 3,
        type: "expense_notice",
        role: null,
        active: false,
        doc_active: false,
        disabled: false
      },
      {
        id: 20,
        title: "techniqueTeaching",
        journal_id: 3,
        type: "technic_teaching",
        role: null,
        active: false,
        doc_active: false,
        disabled: false
      },
      {
        id: 21,
        title: "attorney",
        journal_id: 3,
        type: "attorney",
        role: null,
        active: false,
        doc_active: false,
        disabled: false
      },
      {
        id: 22,
        title: "reference-information",
        journal_id: 10,
        type: "reference",
        role: null,
        active: false,
        doc_active: false,
        disabled: false
      }
    ],
    sendDocumentTableCols: [
      {
        text: "#",
        value: "index",
        width: "50",
        active: true,
        sortable: false,
        align: "center",
      },
      {
        text: "document-flow.views.send-document.type-letter",
        value: "doc_type",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "equipments.views.add-news.created-date",
        value: "created_date",
        width: "130",
        active: true,
        sort: true,
        sortable: false,
      },
      {
        text: "components.change-date",
        value: "modified_date",
        width: "130",
        active: true,
        sort: true,
        sortable: false,
      },
      {
        text: "document-flow.views.send-document.state",
        value: "status",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "document-flow.views.send-document.registration-number",
        value: "register_number",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "document-flow.views.send-document.author",
        value: "author",
        width: "130",
        active: false,
        sortable: false,
      },
      {
        text: "receiver",
        value: "receiver",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "document-flow.views.send-document.agreements",
        value: "approver",
        width: "130",
        active: false,
        sortable: false,
      },
      {
        text: "document-flow.views.send-document.signatory",
        value: "signer",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "sent_status",
        value: "sent_status",
        width: "130",
        active: false,
        sortable: false,
      },
      {
        text: "sendType",
        value: "send_by",
        width: "130",
        active: false,
        sortable: false,
      },
      {
        text: "document-flow.views.send-document.action",
        value: "action",
        width: "130",
        active: true,
        sortable: false,
      }
    ],
    sendDocumentTableFilters: {
      approver: null,
      author: null,
      department: null,
      is_sent: null,
      register_number: null,
      register_start_date: null,
      register_end_date: null,
      signer: null,
      compose_status: null,
      __outgoing_date: null,
      page: 1,
      page_size: 15,
    }
  },
  actions: {
    setActiveIndexTableCols({ commit }, payload) {
      commit(SET_ENTITY, { module: "documentFlowSendDocumentBase", entity: "sendDocumentTableCols", value: payload }, { root: true })
    },
    /** **/
    actionCreateCompose({ commit, rootState }, body) {
      return new Promise((resolve, reject) => {
        this.$axios.post(`/docflow/${rootState.auth.currentUser.filial}/compose/`, body)
          .then(res => {
            resolve(res)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    /** **/
    actionUpdateCompose({ rootState }, { id, body }) {
      return new Promise((resolve, reject) => {
        this.$axios.put(`/docflow/${rootState.auth.currentUser.filial}/compose/${id}/`, body)
          .then(res => {
            resolve(res)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    /** **/
    actionGetComposeDetail({ rootState }, id) {
      return new Promise((resolve, reject) => {
        this.$axios.get(`/docflow/${rootState.auth.currentUser.filial}/compose/${id}/`)
          .then(({ data }) => {
            resolve(data)
          })
          .catch(err => {
            reject(err)
          })
      })
    }
  }
}
