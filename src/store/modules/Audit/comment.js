export default {
  namespaced: true,
  state: {
    commentListLoading: true,
    commentList: [],
    comment: null,
    replyComment: null,
    repliedToId: null,
    isReplying: false
  },
  actions: {
    actionGetCommentList({ state, commit }, params) {
      return new Promise((resolve, reject) => {
        state.commentListLoading = true
        this.$axios.get(`/audit/result-comments/`, {
          params: {
            ...params,
            page_size: 100
          }
        })
          .then(res => {
            if (res && res.status === 200){
              state.commentList = res.data.results
              resolve(res)
            }
          })
          .catch(err => {
            reject(err)
          })
          .finally(() => {
            state.commentListLoading = false
          })
      })
    },
    actionCreateComment({ state, commit, dispatch }, body) {
      return new Promise((resolve, reject) => {
        this.$axios.post(`/audit/result-comments/`, body)
          .then(res => {
            if (res && res.status === 201) {
              state.comment = null
              state.replyComment = null
              state.repliedToId = null
              state.isReplying = false

              dispatch('actionGetCommentList', { result: body.result })
                .then(response => {
                  resolve(res)
                })

            }
          })
          .catch(err => {
            reject(err)
          })
      })
    }
  }
}