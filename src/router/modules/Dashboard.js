
const Dashboard = [
  {
    path: "dashboard-v3",
    name: "MainDashboard",
    component: () => import(/* webpackChunkName: "MainDashboard" */ "@/modules/DashboardV2/views"),
    redirect: { name: "DashboardHome" },
    children: [
      {
        path: "home",
        name: "DashboardHome",
        meta: {
          isAuthRequired: true
        },
        component: () => import(/* webpackChunkName: "DashboardHome" */ "@/modules/DashboardV2/views/Home")
      }
    ]
  },
  {
    path: "dashboard",
    redirect: { name: "MainDashboard" },
    children: [
      {
        path: "home",
        redirect: { name: "MainDashboard" }
      }
    ]
  },
  {
    path: "dashboard-v2",
    name: "MainDashboardOld",
    component: () => import(/* webpackChunkName: "MainDashboard" */ "@/modules/Dashboard/views"),
    redirect: { name: "DashboardHomeOld" },
    children: [
      {
        path: "home",
        name: "DashboardHomeOld",
        meta: {
          isAuthRequired: true
        },
        component: () => import(/* webpackChunkName: "DashboardHome" */ "@/modules/Dashboard/views/Home")
      }
    ]
  }
]

export default Dashboard
