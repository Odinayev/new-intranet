import { SET_ENTITY } from "@/store/mutation-types"
import CrudService from "@/services/crud.service"
import { MONTH_LIST } from "@/common/constants"

export default {
  namespaced: true,
  state: {
    statisticCardsLoading: true,
    statisticCards: null,
    documentWidgetLoading: true,
    documentWidget: null,
    documentWidgetReviewLoading: true,
    documentWidgetReview: null,
    statisticChartLoading: true,
    statisticChart: null,
    chartSeriesList: [
      {
        name: "Входящий",
        data: [],
        value: "incoming"
      },
      {
        name: "Внутренний",
        data: [],
        value: "inner"
      },
      {
        name: 'Исходящий',
        data: [],
        value: "outgoing"
      },
      {
        name: 'Приказы и распоряжения',
        data: [],
        value: "order_protocol"
      },
      {
        name: 'Обращение',
        data: [],
        value: "appeal"
      }
    ],
    chartRef: null,
    executionReviewList: [],
    executionInboxList: [],
    isLoading: false,
    composeTypeList: [
      { title: 'Внутренний', value: 3, type: "service_letter", role: null },
      { title: 'Исходящий', value: 4, type: "service_letter", role: null  },
      // { title: 'Протокол правления', value: 6, type: "protocol", role: null  },
      { title: 'Протокол комитета', value: 6, type: "credit_committee", role: null  },
      // { title: 'Заявление', value: 7, type: "hr_application" },
      { title: 'Распоряжения', value: 8, type: "decree", },
      // { title: 'Акт', value: 15, type: "act", role: "technique_officer"  },
      // { title: "Созданные приказы", value: 100, type: "hr_order"  },
    ],
    sendingDocumentStatistics: []
  },
  /*
  *
  * */
  getters: {
    getExecutionReviewList: (state) => state.executionReviewList,
    getExecutionInboxList: (state) => state.executionInboxList,
    getIsLoading: (state) => state.isLoading,
    getComposeTypeList: (state) => state.composeTypeList,
    getSendingDocumentStatistics: (state) => state.sendingDocumentStatistics,
  },
  mutations: {

  },
  /*
  *
  * */
  actions: {
    setChartRef({ commit }, payload) {
      commit(SET_ENTITY, {
        module: "documentStatistic",
        entity: "chartRef",
        value: payload
      }, { root: true })
    },
    /*
    *
    * */
    resetChartSeriesList({ commit }) {
      commit(SET_ENTITY, {
        module: "documentStatistic",
        entity: "chartSeriesList",
        value: [
          {
            name: "Входящий",
            data: [],
            value: "incoming"
          },
          {
            name: "Внутренний",
            data: [],
            value: "inner"
          },
          {
            name: 'Исходящий',
            data: [],
            value: "outgoing"
          },
          {
            name: 'Приказы и распоряжения',
            data: [],
            value: "order_protocol"
          },
          {
            name: 'Обращение',
            data: [],
            value: "appeal"
          }
        ]
      }, { root: true })
    },
    /*
    *
    * */
    fetchStatisticCardsList({ commit }, payload) {
      commit(SET_ENTITY, {
        module: "documentStatistic",
        entity: "statisticCardsLoading",
        value: true
      }, { root: true })

      CrudService.getList(`/docflow/journal-statistic/`, {
        ...payload
      })
      .then((data) => {
        commit(SET_ENTITY, {
          module: "documentStatistic",
          entity: "statisticCards",
          value: data
        }, { root: true })

        commit(SET_ENTITY, {
          module: "documentStatistic",
          entity: "statisticCardsLoading",
          value: false
        }, { root: true })
      })
    },
    /*
    *
    * */
    fetchDocumentWidgetList({ commit }, payload) {
      commit(SET_ENTITY, {
        module: "documentStatistic",
        entity: "documentWidgetLoading",
        value: true
      }, { root: true })

      CrudService.getList(`/docflow/assignment-statistic/`, {
        ...payload
      })
      .then((data) => {
        commit(SET_ENTITY, {
          module: "documentStatistic",
          entity: "documentWidget",
          value: data
        }, { root: true })

        commit(SET_ENTITY, {
          module: "documentStatistic",
          entity: "documentWidgetLoading",
          value: false
        }, { root: true })
      })
    },
    /*
    *
    * */
    fetchDocumentWidgetReviewList({ commit }, payload) {
      commit(SET_ENTITY, {
        module: "documentStatistic",
        entity: "documentWidgetReviewLoading",
        value: true
      }, { root: true })

      CrudService.getList(`/docflow/review-statistic/`, {
        ...payload
      })
      .then((data) => {
        commit(SET_ENTITY, {
          module: "documentStatistic",
          entity: "documentWidgetReview",
          value: data
        }, { root: true })

        commit(SET_ENTITY, {
          module: "documentStatistic",
          entity: "documentWidgetReviewLoading",
          value: false
        }, { root: true })
      })
    },
    /*
    *
    * */
    fetchStatisticChart({ commit, state }, payload) {
      // commit(SET_ENTITY, {
      //   module: "documentStatistic",
      //   entity: "statisticChartLoading",
      //   value: true
      // }, { root: true })

      return new Promise((resolve, reject) => {
        CrudService.getList(`/docflow/yearly-statistic/`, {
          ...payload
        })
        .then((data) => {
          let results = data.results

          MONTH_LIST.forEach((month, index) => {
            if(results.hasOwnProperty(index + 1)) {
              state.chartSeriesList.forEach(series => {
                series.data.push(results[index + 1][series.value])
              })
            } else {
              state.chartSeriesList.forEach(series => {
                series.data.push(0)
              })
            }
          })

          commit(SET_ENTITY, {
            module: "documentStatistic",
            entity: "statisticChartLoading",
            value: false
          }, { root: true })

          resolve()
        })
      })
    },
    fetchExecutionReviewList({ commit, rootState, dispatch }, payload){
      commit(SET_ENTITY, { module: "documentStatistic", entity: "isLoading", value: true }, { root: true });
      this.$axios.get(`/docflow/${rootState.auth.currentUser.filial}/user_reviews_for_stat/`, {
        params: {
          ...payload,
          page_size: 15
        }
      })
        .then(({ data }) => {
          commit(SET_ENTITY, {
            module: "documentStatistic",
            entity: "executionReviewList",
            value: data.results
          }, { root: true });

          dispatch("fetchPagination", data.count, {root: true})
        })
        .finally(() => {
          commit(SET_ENTITY, { module: "documentStatistic", entity: "isLoading", value: false }, { root: true });
        })
    },
    /**/
    fetchExecutionInboxList({ commit, rootState, dispatch }, payload) {
      commit(SET_ENTITY, { module: "documentStatistic", entity: "isLoading", value: true }, { root: true });
      this.$axios.get(`/docflow/${rootState.auth.currentUser.filial}/user_assignment_for_stat/`, {
        params: {
          ...payload,
          page_size: 15
        }
      })
        .then(({ data }) => {
          commit(SET_ENTITY, {
            module: "documentStatistic",
            entity: "executionInboxList",
            value: data.results
          }, { root: true });

          dispatch("fetchPagination", data.count, {root: true})
        })
        .finally(() => {
          commit(SET_ENTITY, { module: "documentStatistic", entity: "isLoading", value: false }, { root: true });
        })
    },
    /**/
    fetchSendingDocumentStatistics({ commit }, payload){
      commit(SET_ENTITY, { module: "documentStatistic", entity: "isLoading", value: true }, { root: true });
      this.$axios.get(`/docflow/compose-statistic/`, {
        params: payload
      })
        .then(({data}) => {
          commit(SET_ENTITY, {
            module: "documentStatistic",
            entity: "sendingDocumentStatistics",
            value: data.results
          }, { root: true })
        })
        .finally(() => {
          commit(SET_ENTITY, { module: "documentStatistic", entity: "isLoading", value: false }, { root: true });
        })
    }
  }
}
