import { SET_ENTITY } from "@/store/mutation-types"
import CrudService from "@/services/crud.service"

export default {
  namespaced: true,
  state: {
    filter: {
      search: null,
      // Входящие
      priority: null,
      status: null,
      journal: null,
      doc_type: null,
      type: null,
      deadline_start: null,
      deadline_end: null,
      correspondent: null,
      controller: null,
      // На рассмотрение
      register_start_date: null,
      // register_start_date: new Date().getFullYear() + '-' + (Number(new Date().getMonth()) + 1) + '-01',
      register_end_date: null,
      register_number: null,
      outgoing_start_date: null,
      outgoing_end_date: null,
      outgoing_number: null,
      author: null,
      signer: null,
      department: null,
      condition: null,
      review_user: null,
      has_deadline: null,
      is_signed: null,
      compose__register_number: null,
      is_agreed: null,
      is_read: null,
      // Для Зам.пред
      // is_for_signature: "True",
      // is_verified: "False",
      __resolutionAssign: null,
      filial: null,
      title: "",
      journal_id: null,
      has_resolution: null,
      compose_status: null,
      is_sent: null,
      branches: null,
      departments: null,
      organizations: null,
      curator: null,
      to_user: null,
      send_by: null,
      // page: 1,
    },
    searchInputs: {
      register_number: null,
      outgoing_number: null,
      text: null,
      register_start_date: null,
      register_end_date: null,
      correspondent: null
    },
    filteredState: {},
    filterFakeKeys: {
      correspondent: null,
      doc_type: null,
    },
    pagination: {
      page: 1,
      pageSize: 15,
      totalVisible: 7,
      pageLength: null,
      listLength: null
    }
  },
  /*
  *
  * */
  getters: {
    getSearchInputs: state => state.searchInputs
  },
  /*
  *
  * */
  mutations: {

  },
  /*
  *
  * */
  actions: {
    clearFilterDatePicker({ commit, state }, payload) {
      state.filter[payload.start] = null
      state.filter[payload.end] = null
    },
    /*
    *
    * */
    setFilteredState({ commit, state }) {
      let activeState = {}

      Object.entries(state.filter).forEach(item => {
        const [key, value] = item

        if(value || (typeof value === "boolean")) {
          activeState = {
            ...activeState,
            [key]: value
          }
        }
      })
      commit(SET_ENTITY, { module: "headerFilter", entity: "filteredState", value: activeState }, { root: true })
    },
    /*
    *
    * */
    setFiltersFromQueryParams({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        Object.entries(payload.query).forEach(item => {
          const [key, value] = item

          if(value || (typeof value === "boolean")) {
            state.filter[key] = value === "true" ? true : value === "false" ? false : Number(value) ? Number(value) : value
            state.filteredState[key] = value === "true" ? true : value === "false" ? false : Number(value) ? Number(value) : value
            state.pagination.page = +payload.query.page
          }
        })

        resolve(state.filter)
      })
    },
    /*
    *
    * */
    setClearFilters({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        state.pagination.page = 1
        // state.filter.register_start_date = new Date().getFullYear() + '-' + (Number(new Date().getMonth()) + 1) + '-01'

        Object.entries(payload.query).forEach(item => {
          const [key] = item

          // if(key === "register_start_date") {
          //   return;
          // }

          state.filter[key] = null

          if(state.filteredState.hasOwnProperty(key)) {
            state.filteredState[key] = null
          }
        })

        resolve()
      })
    }
  }
}
