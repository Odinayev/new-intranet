import { format } from "date-fns"
import { SET_ENTITY } from "@/store/mutation-types"

let day = new Date()
export default {
  namespaced: true,
  state: {
    indexTableCols: [
      {
        text: "#",
        value: "index",
        width: "50",
        active: true,
        sortable: false,
        align: "center",
      },
      {
        text: "components.status",
        value: "status.name",
        active: false,
        sortable: false,
        cols: {
          "md": "4"
        },
        component: "ui-status",
        detail: true,
        order: 2
      },
      {
        text: "components.document-type",
        value: "doc_type.name",
        active: true,
        sortable: false,
        cols: {
          "md": "4"
        },
        component: null,
        detail: true,
        order: 3
      },
      {
        text: "components.registration-number",
        value: "register_number",
        active: true,
        sortable: false,
        cols: {
          "md": "4"
        },
        component: null,
        detail: true,
        order: 4
      },
      {
        text: "document-flow.components.registration-date",
        value: "register_date",
        active: true,
        sortable: false,
        cols: {
          "md": "4"
        },
        component: "ui-date-text",
        detail: true,
        order: 5,
      },
      {
        text: "components.document-name",
        value: "title",
        active: true,
        sortable: false,
        cols: {
          "md": "4"
        },
        component: null,
        detail: true,
        order: 6
      },
      {
        text: "components.outgoing-number",
        value: "outgoing_number",
        active: false,
        sortable: false,
        cols: {
          "md": "4"
        },
        component: null,
        detail: true,
        order: 7
      },
      {
        text: "components.author",
        value: "created_by",
        active: true,
        sortable: false,
        cols: {
          "md": "4"
        },
        component: "ui-avatar",
        detail: true,
        order: 7
      },
      {
        text: "document-flow.components.for-consideration",
        value: "review_users",
        active: true,
        sortable: false,
        cols: {
          "md": "4"
        },
        component: "ui-avatar-group",
        detail: true,
        order: 7
      },
      {
        text: "document-flow.components.outgoing-date",
        value: "outgoing_date",
        active: false,
        sortable: false,
        cols: {
          "md": "4"
        },
        component: "ui-date-text",
        detail: true,
        order: 8
      },
      {
        text: "components.status",
        value: "status.name",
        active: false,
        sortable: false,
        cols: {
          "md": "4"
        },
        component: "ui-status",
        detail: true,
        order: 9
      },
      {
        text: "priority",
        value: "priority",
        width: "150",
        active: true,
        sortable: false,
        cols: {
          "md": "4"
        },
        component: "ui-priority",
        detail: true,
        order: 9
      },
      {
        text: "components.execution-duration",
        value: "__deadline",
        width: "150",
        active: false,
        sortable: false,
        cols: {
          "md": "4"
        },
        component: "ui-countdown",
        detail: true,
        order: 9
      },
      {
        text: "components.execution-duration",
        value: "deadline",
        active: true,
        sortable: false,
        cols: {
          "md": "4"
        },
        component: null,
        detail: false,
        order: 9
      },
      {
        text: "document-flow.components.correspondent",
        value: "correspondent.organization_name",
        width: "350",
        active: true,
        sortable: false,
        cols: {
          "md": "4"
        },
        component: null,
        detail: true,
        order: 10
      },
      {
        text: "document-flow.views.boxes.content",
        value: "text",
        active: false,
        sortable: false,
        width: "300",
        cols: {
          "md": "12"
        },
        component: null,
        detail: true,
        order: 11
      },
    ],
    indexTableFilters: {
      search: null,
      author: null,
      author_full_name: null,
      filial: null,
      priority: null,
      is_read: null,
      has_deadline: null,
      type: null,
      is_agreed: null,
      has_resolution: null,
      signer: null,
      status: null,
      statusList: null,
      condition: null,
      journal: null,
      department: null,
      doc_type: null,
      doc_type_name: null,
      register_number: null,
      register_start_date: format(Date.now() - (day.getDate() - 1) * 24 * 60 * 60 * 1000, "yyyy-MM-dd"),
      register_end_date: format(day, "yyyy-MM-dd"),
      outgoing_number: null,
      outgoing_start_date: null,
      outgoing_end_date: null,
      correspondent: null,
      correspondent_name: null,
      review_user: null,
      full_name: null,
      __author: null,
      __correspondent: null,
      __doc_type: null,
      __register_date: format(Date.now() - (day.getDate() - 1) * 24 * 60 * 60 * 1000, "yyyy-MM-dd"),
      __outgoing_date: null,
      __review_user: null,
      __signer: null,
      page: 1,
      page_size: 15
    }
  },
  actions: {
    setActiveIndexTableCols({commit}, payload) {
      commit(SET_ENTITY, {
        module: "documentFlowRegistrationAppeal",
        entity: "indexTableCols",
        value: payload
      }, {root: true})
    },
    /**/
    resetIndexTableCols({ state }) {
      state.indexTableCols = [
        {
          text: "#",
          value: "index",
          width: "50",
          active: true,
          sortable: false,
          align: "center",
        },
        {
          text: "components.status",
          value: "status.name",
          active: false,
          sortable: false,
          cols: {
            "md": "4"
          },
          component: "ui-status",
          detail: true,
          order: 2
        },
        {
          text: "components.document-type",
          value: "doc_type.name",
          active: true,
          sortable: false,
          cols: {
            "md": "4"
          },
          component: null,
          detail: true,
          order: 3
        },
        {
          text: "components.registration-number",
          value: "register_number",
          active: true,
          sortable: false,
          cols: {
            "md": "4"
          },
          component: null,
          detail: true,
          order: 4
        },
        {
          text: "document-flow.components.registration-date",
          value: "register_date",
          active: true,
          sortable: false,
          cols: {
            "md": "4"
          },
          component: "ui-date-text",
          detail: true,
          order: 5,
        },
        {
          text: "components.document-name",
          value: "title",
          active: true,
          sortable: false,
          cols: {
            "md": "4"
          },
          component: null,
          detail: true,
          order: 6
        },
        {
          text: "components.outgoing-number",
          value: "outgoing_number",
          active: false,
          sortable: false,
          cols: {
            "md": "4"
          },
          component: null,
          detail: true,
          order: 7
        },
        {
          text: "components.author",
          value: "created_by",
          active: true,
          sortable: false,
          cols: {
            "md": "4"
          },
          component: "ui-avatar",
          detail: true,
          order: 7
        },
        {
          text: "document-flow.components.for-consideration",
          value: "review_users",
          active: true,
          sortable: false,
          cols: {
            "md": "4"
          },
          component: "ui-avatar-group",
          detail: true,
          order: 7
        },
        {
          text: "document-flow.components.outgoing-date",
          value: "outgoing_date",
          active: false,
          sortable: false,
          cols: {
            "md": "4"
          },
          component: "ui-date-text",
          detail: true,
          order: 8
        },
        {
          text: "components.status",
          value: "status.name",
          active: false,
          sortable: false,
          cols: {
            "md": "4"
          },
          component: "ui-status",
          detail: true,
          order: 9
        },
        {
          text: "priority",
          value: "priority",
          width: "150",
          active: true,
          sortable: false,
          cols: {
            "md": "4"
          },
          component: "ui-priority",
          detail: true,
          order: 9
        },
        {
          text: "components.execution-duration",
          value: "__deadline",
          width: "150",
          active: false,
          sortable: false,
          cols: {
            "md": "4"
          },
          component: "ui-countdown",
          detail: true,
          order: 9
        },
        {
          text: "components.execution-duration",
          value: "deadline",
          active: true,
          sortable: false,
          cols: {
            "md": "4"
          },
          component: null,
          detail: false,
          order: 9
        },
        {
          text: "document-flow.components.correspondent",
          value: "correspondent.organization_name",
          width: "350",
          active: true,
          sortable: false,
          cols: {
            "md": "4"
          },
          component: null,
          detail: true,
          order: 10
        },
        {
          text: "document-flow.views.boxes.content",
          value: "text",
          active: false,
          sortable: false,
          width: "300",
          cols: {
            "md": "12"
          },
          component: null,
          detail: true,
          order: 11
        },
      ]
    },
    /**/
    async resetIndexTableFilters({ state, dispatch }) {
      state.indexTableFilters = {
        search: null,
        author: null,
        author_full_name: null,
        filial: null,
        priority: null,
        is_read: null,
        has_deadline: null,
        type: null,
        is_agreed: null,
        has_resolution: null,
        signer: null,
        signer_full_name: null,
        status: null,
        statusList: null,
        condition: null,
        journal: null,
        department: null,
        doc_type: null,
        doc_type_name: null,
        register_number: null,
        register_start_date: format(Date.now() - (day.getDate() - 1) * 24 * 60 * 60 * 1000, "yyyy-MM-dd"),
        register_end_date: format(day, "yyyy-MM-dd"),
        outgoing_number: null,
        outgoing_start_date: null,
        outgoing_end_date: null,
        correspondent: null,
        correspondent_name: null,
        review_user: null,
        full_name: null,
        __author: null,
        __correspondent: null,
        __doc_type: null,
        __register_date: format(Date.now() - (day.getDate() - 1) * 24 * 60 * 60 * 1000, "yyyy-MM-dd"),
        __outgoing_date: null,
        __review_user: null,
        __signer: null,
        page: 1,
        page_size: 15
      }

      dispatch("setActiveIndexTableFilters", state.indexTableFilters)
    },
    /**/
    setActiveIndexTableFilters({commit, state}, payload) {
      Object.entries(payload).forEach(item => {
        const [key, value] = item

        if (value && (typeof value === "boolean" || Array.isArray(value))) {
          state.indexTableFilters[key] = value
        }

        if (value && (typeof value === "number" || typeof value === "string")) {
          state.indexTableFilters[key] = Number(value) ? Number(value) : value
        }

        if (value === "true" || value === "false") {
          state.indexTableFilters[key] = value === "true"
        }
      })

      Object.entries(state.indexTableFilters).forEach(item => {
        const [key, value] = item

        if (value && value.length && key === "statusList") {
          state.indexTableFilters.statusList = state.indexTableFilters.statusList.map(item => parseInt(item))
        }

        if (value && key === "doc_type_name") {
          state.indexTableFilters.__doc_type = {
            id: state.indexTableFilters.doc_type,
            name: state.indexTableFilters.doc_type_name,
            type: "doc_type"
          }
        }

        if (value && key === "register_start_date") {
          state.indexTableFilters.__register_date = [state.indexTableFilters.register_start_date, state.indexTableFilters.register_end_date]
        }

        if (value && key === "outgoing_start_date") {
          state.indexTableFilters.__outgoing_date = [state.indexTableFilters.outgoing_start_date, state.indexTableFilters.outgoing_end_date]
        }

        if (value && key === "correspondent_name") {
          state.indexTableFilters.__correspondent = {
            id: state.indexTableFilters.correspondent,
            organization_name: state.indexTableFilters.correspondent_name
          }
        }

        if (value && key === "review_user") {
          state.indexTableFilters.__review_user = {
            id: state.indexTableFilters.review_user,
            full_name: state.indexTableFilters.full_name
          }
        }

        if (value && key === "author") {
          state.indexTableFilters.__author = {
            id: state.indexTableFilters.author,
            full_name: state.indexTableFilters.author_full_name
          }
        }

        if (value && key === "signer") {
          state.indexTableFilters.__signer = {
            id: state.indexTableFilters.signer,
            full_name: state.indexTableFilters.signer_full_name
          }
        }
      })
    }
  }
}
