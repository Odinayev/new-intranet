import Vue from "vue"
import { format, isValid, formatDistance, formatRFC3339 } from "date-fns";
import { ru } from "date-fns/locale";
import { capitalize } from "lodash";

Vue.filter("filterHHmm", (val) => {
	return isValid(new Date(val)) ? format(new Date(val), "HH:mm") : "Не верный формат даты"
})

Vue.filter("filterDateMonthYear", (val) => {
	return isValid(new Date(val)) ? format(new Date(val), "dd.MM.yyyy") : "Не верный формат даты"
})

Vue.filter("filterWeekDay", (val) => {
	return isValid(new Date(val)) ? format(new Date(val), "EEEE", { locale: ru }) : "Не верный формат даты"
})

Vue.filter("filterYearMonthDate", (val) => {
	return isValid(new Date(val)) ? format(new Date(val), "yyyy-MM-dd") : "Не верный формат даты"
})

Vue.filter("filterCalendarToTime", (val) => {
	return isValid(new Date(val)) ? format(new Date(val), "dd.MM.yyyy HH:mm") : "Не верный формат даты"
})

Vue.filter("filterDistance", (val) => {
	return isValid(new Date(val)) ? formatDistance(new Date(val), new Date(), { locale: ru, includeSeconds: true, addSuffix: true }) : "Не верный формат даты"
})

Vue.filter("filterLLLL", (val) => {
	return isValid(new Date(val)) ? format(new Date(val), "LLLL yyyy", { locale: ru }) : "Не верный формат даты"
})

Vue.filter("filterIIIIDDLLLL", (val) => {
	return isValid(new Date(val))
		? capitalize(format(new Date(val), "iiii, dd LLLL", { locale: ru }))
		: "Не верный формат даты"
})

Vue.filter("formatNumber", (val) => {
	if (val) {
		return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")
	}

	return val
})


Vue.filter("getFirstCharacter", (val) => {
	if (val){
		const firstTwoCharacters =  val.slice(0, 2).toLowerCase();
		if (firstTwoCharacters === 'sh' || firstTwoCharacters === 'ch' || firstTwoCharacters === "o'" || firstTwoCharacters === "g'"){
			return val.slice(0, 2) + '.'
		}else {
			return val.slice(0, 1) + '.'
		}
	}else {
		return ""
	}
})

Vue.filter("getFirstCharacterWithoutDot", (val) => {
	if (val){
		const firstTwoCharacters =  val.slice(0, 2).toLowerCase();
		if (firstTwoCharacters === "sh" || firstTwoCharacters === "ch" || firstTwoCharacters === "o'" || firstTwoCharacters === "g'"){
			return val.slice(0, 2)
		}else {
			return val.slice(0, 1)
		}
	}else {
		return ""
	}
})

Vue.filter("roundUp", (val, decimalPlaces) => {
	if (val){
		return parseFloat(val).toFixed(decimalPlaces)
	}
})

