import { SET_ENTITY } from "@/store/mutation-types"
import CrudService from "@/services/crud.service";

export default {
	namespaced: true,
	state: {
		inboxIndexTableCols: [
			{
				text: "#",
				value: "index",
				width: "50",
				active: true,
				sortable: false,
				align: "center",
			},
			{
				text: "components.execution-duration",
				value: "__deadline",
				width: "150",
				active: true,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: "ui-countdown",
				detail: true,
				order: 3
			},
			{
				text: "user-profile.views.name",
				value: "document.title",
				active: false,
				sortable: false,
				width: "125"
			},
			{
				text: "document-flow.views.registration.state",
				value: "is_read",
				width: "135",
				active: false,
				sortable: false
			},
			{
				text: "priority",
				value: "document.priority",
				width: "150",
				active: true,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: "ui-priority",
				detail: true,
				order: 6
			},
			{
				text: "components.registration-number",
				value: "document.register_number",
				active: true,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: null,
				detail: true,
				order: 4
			},
			{
				text: "components.document-type",
				value: "document.doc_type.name",
				active: true,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: null,
				detail: true,
				order: 1
			},
			{
				text: "document-flow.views.boxes.journal",
				value: "document.journal.name",
				active: true,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: null,
				detail: true,
				order: 12
			},
			{
				text: "document-flow.views.boxes.receipt-date",
				value: "assignment.receipt_date",
				active: true,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: "ui-date-text",
				detail: true,
				order: 13
			},
			{
				text: "components.outgoing-number",
				value: "document.outgoing_number",
				active: false,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: null,
				detail: true,
				order: 7
			},
			{
				text: "document-flow.components.registration-date",
				value: "document.register_date",
				active: true,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: "ui-date-text",
				detail: true,
				order: 5
			},
			{
				text: "document-flow.components.outgoing-date",
				value: "document.outgoing_date",
				active: false,
				sortable: false,
				cols: {
					"md": "4"
				},
				component: "ui-date-text",
				detail: true,
				order: 8
			},
			{
				text: "document-flow.views.registration.author",
				value: "document.created_by",
				active: true,
				sortable: false,
				width: "125",
				cols: {
					"md": "4"
				},
				component: "ui-avatar",
				detail: true,
				order: 2
			},
			{
				text: "document-flow.components.correspondent",
				value: "document.correspondent.organization_name",
				active: true,
				sortable: false,
				width: "300",
				cols: {
					"md": "12"
				},
				component: null,
				detail: true,
				order: 17
			},
			{
				text: "document-flow.views.boxes.content",
				value: "document.description",
				active: false,
				sortable: false,
				width: "300",
				cols: {
					"md": "12"
				},
				component: null,
				detail: true,
				order: 18
			},
			{
				text: "components.status",
				value: "status.name",
				width: "140",
				sortable: false,
				active: true,
				cols: {
					"md": "4"
				},
				component: "ui-status",
				detail: true,
				order: 3
			},
		],
		inboxIndexTableFilters: {
			search: null,
			priority: null,
			is_read: null,
			has_deadline: null,
			type: null,
			is_agreed: null,
			has_resolution: null,
			status: null,
			statusList: null,
			condition: null,
			journal: null,
			doc_type: null,
			doc_type_name: null,
			register_number: null,
			register_start_date: null,
			register_end_date: null,
			outgoing_number: null,
			outgoing_start_date: null,
			outgoing_end_date: null,
			correspondent: null,
			correspondent_name: null,
			controller: null,
			__correspondent: null,
			__doc_type: null,
			__register_date: null,
			__outgoing_date: null,
			review_user: null,
			review_user_name: null,
			__review_user: null,
			page: 1,
			page_size: 15
		},
		inboxDetailModelLoader: true,
		inboxDetailModel: {},
		resolutionPerformersList: [],
		executionMarkModel: {
			file: null,
			performance_note: null
		}
	},
	/**
	 *
	 **/
	actions: {
		/*
		*
		* */
		async fetchInboxById({ commit, rootState }, payload) {
			if(payload.loader) {
				commit(SET_ENTITY, {
					module: "documentFlowBoxesInbox",
					entity: "inboxDetailModelLoader",
					value: true
				}, {root: true})
			}

			await CrudService.getList(`v2/docflow/${rootState.auth.currentUser.filial}/my-assignment/${payload.id}/`, { is_secret: payload.is_secret })
			.then((data) => {
				let model = {
					...data,
					__deadline: data.assignment.deadline ?? data.document.deadline
				}

				commit(SET_ENTITY, {
					module: "documentFlowBoxesInbox",
					entity: "inboxDetailModel",
					value: model
				}, {root: true})

				setTimeout(() => {
					commit(SET_ENTITY, {
						module: "documentFlowBoxesInbox",
						entity: "inboxDetailModelLoader",
						value: false
					}, {root: true})
				}, 1000)
			})
		},
		/*
		* Изменить статус документа (Входящий)
		* */
		async fetchUpdateInboxStatus({ commit, rootState, state, dispatch }, payload) {
			await this.$axios.put(`/v2/docflow/${rootState.auth.currentUser.filial}/my-assignment/${payload.documentId}/change-status/`, {
				status: payload.status,
				comment: payload.comment
			})
			.then(({ data }) => {
				state.inboxDetailModel.status = data.status

				dispatch(
					"treeView/fetchTreeUsersList",
					{ treeId: state.inboxDetailModel.document.id },
					{ root: true }
				)
			})
		},
		/*
		* Ознакомиться с документом (Входящий)
		* */
		async fetchInboxAcquaintWithDocument({ commit, rootState, state, dispatch }, payload) {
			await this.$axios.put(`/v2/docflow/${rootState.auth.currentUser.filial}/my-assignment/${payload.documentId}/acquaint/?is_secret=${payload.is_secret}`)
			.then(({ data }) => {
				state.inboxDetailModel.read_time = data.read_time

				dispatch(
					"treeView/fetchTreeUsersList",
					{ treeId: state.inboxDetailModel.document.id },
					{ root: true }
				)
			})
		},
		/*
		* Создать резолюцию
		* */
		async fetchCreateInboxResolution({ commit, rootState, state, dispatch }, payload) {
			await this.$axios.post(`v2/docflow/${rootState.auth.currentUser.filial}/assignment/`, { ...payload.model })
			.then(({ data }) => {
				dispatch(
					"fetchInboxById",
					{ id: payload.documentId, loader: false, is_secret: payload.is_secret }
				)

				dispatch(
					"treeView/fetchTreeUsersList",
					{ treeId: payload.treeId },
					{ root: true }
				)
			})
		},
		/*
		* Получить созданную резолюцию
		* */
		async fetchGetCreatedInboxResolution({ commit, rootState, state }, payload) {
			await CrudService.getList(`v2/docflow/${rootState.auth.currentUser.filial}/assignment/${payload.id}/`)
			.then((data) => {
				state.inboxDetailModel.assignees = {
					assignment: data.assignment,
					check_id: data.check_id,
					content: data.content,
					deadline: data.deadline,
					id: data.id,
					parent: data.parent,
					review: data.review,
					is_verified: data.is_verified,
					type: data.type
				}
			})
		},
		/*
		* Изменить резолюцию
		* */
		async fetchUpdateInboxResolution({ commit, rootState, state, dispatch }, payload) {
			await this.$axios.put(
				`v2/docflow/${rootState.auth.currentUser.filial}/assignment/${payload.id}/`,
				{ ...payload.model }
			)
			.then(({ data }) => {
				dispatch(
					"fetchInboxById",
					{ id: payload.documentId, loader: false, is_secret: payload.is_secret }
				)

				dispatch(
					"treeView/fetchTreeUsersList",
					{ treeId: payload.treeId },
					{ root: true }
				)
			})
		},
		/*
		* Удалить резолюцию
		* */
		async fetchDeleteInboxResolution({ commit, rootState, state, dispatch }, payload) {
			await this.$axios.delete(`v2/docflow/${rootState.auth.currentUser.filial}/assignment/${payload.id}/`, {
				params: {
					comment: payload.comment
				}
			})
			.then(() => {
				dispatch(
					"fetchInboxById",
					{ id: payload.documentId, loader: false }
				)

				dispatch(
					"treeView/fetchTreeUsersList",
					{ treeId: payload.treeId },
					{ root: true }
				)
			})
		},
		/*
		* Получить список исполнителей созданную резолюцию
		* */
		async fetchGetResolutionPerformers({ commit, rootState, state }, payload) {
			await CrudService.getList(`v2/docflow/${rootState.auth.currentUser.filial}/assignment/${payload.id}/performers/`)
			.then((data) => {
				console.log(data)
				commit(SET_ENTITY, {
					module: "documentFlowBoxesInbox",
					entity: "resolutionPerformersList",
					value: data
				}, {root: true})
			})
		},
		/*
		* Отметка об исполнение
		* */
		async fetchExecutionMark({ commit, rootState, state, dispatch }, payload) {
			await this.$axios.put(
				`v2/docflow/${rootState.auth.currentUser.filial}/my-assignment/${payload.id}/perform/?is_secret=${payload.is_secret}`,
				state.executionMarkModel
			)
			.then(({ data }) => {
				dispatch(
					"fetchInboxById",
					{ id: state.inboxDetailModel.id, loader: false, is_secret: payload.is_secret }
				)

				dispatch(
					"treeView/fetchTreeUsersList",
					{ treeId: state.inboxDetailModel.document.id },
					{ root: true }
				)

				state.executionMarkModel.file = null
				state.executionMarkModel.performance_note = null
			})
		},
		/*
		*
		* */
		async fetchGetCreatedExecutionMark({ commit, rootState, state, dispatch }, payload) {
			dispatch(
				"fetchInboxById",
				{ id: state.inboxDetailModel.id, loader: false, is_secret: payload.is_secret }
			)

			dispatch(
				"treeView/fetchTreeUsersList",
				{ treeId: state.inboxDetailModel.document.id },
				{ root: true }
			)

			state.executionMarkModel.file = state.inboxDetailModel.file
			state.executionMarkModel.performance_note = state.inboxDetailModel.performance_note
		},
		/*
		*
		* */
		resetInboxModels({ state }) {
			state.inboxDetailModel = null
			state.executionMarkModel = {
				file: null,
				performance_note: null
			}
		},
		/*
		* Заполняем нужные поля колонки таблицы если пользователь перезагрузил браузер
		* */
		setActiveIndexTableCols({ commit }, payload) {
			commit(SET_ENTITY, { module: "documentFlowBoxesInbox", entity: "inboxIndexTableCols", value: payload }, { root: true })
		},
		/*
		*
		* */
		resetIndexTableCols({ state }) {
			state.inboxIndexTableCols = [
				{
					text: "#",
					value: "index",
					width: "50",
					active: true,
					sortable: false,
					align: "center",
				},
				{
					text: "components.execution-duration",
					value: "__deadline",
					width: "150",
					active: true,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: "ui-countdown",
					detail: true,
					order: 3
				},
				{
					text: "user-profile.views.name",
					value: "document.title",
					active: false,
					sortable: false,
					width: "125"
				},
				{
					text: "document-flow.views.registration.state",
					value: "is_read",
					width: "135",
					active: false,
					sortable: false
				},
				{
					text: "priority",
					value: "document.priority",
					width: "150",
					active: true,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: "ui-priority",
					detail: true,
					order: 6
				},
				{
					text: "components.registration-number",
					value: "document.register_number",
					active: true,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: null,
					detail: true,
					order: 4
				},
				{
					text: "components.document-type",
					value: "document.doc_type.name",
					active: true,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: null,
					detail: true,
					order: 1
				},
				{
					text: "document-flow.views.boxes.journal",
					value: "document.journal.name",
					active: true,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: null,
					detail: true,
					order: 12
				},
				{
					text: "document-flow.views.boxes.receipt-date",
					value: "assignment.receipt_date",
					active: true,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: "ui-date-text",
					detail: true,
					order: 13
				},
				{
					text: "components.outgoing-number",
					value: "document.outgoing_number",
					active: false,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: null,
					detail: true,
					order: 7
				},
				{
					text: "document-flow.components.registration-date",
					value: "document.register_date",
					active: true,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: "ui-date-text",
					detail: true,
					order: 5
				},
				{
					text: "document-flow.components.outgoing-date",
					value: "document.outgoing_date",
					active: false,
					sortable: false,
					cols: {
						"md": "4"
					},
					component: "ui-date-text",
					detail: true,
					order: 8
				},
				{
					text: "document-flow.views.registration.author",
					value: "document.created_by",
					active: true,
					sortable: false,
					width: "125",
					cols: {
						"md": "4"
					},
					component: "ui-avatar",
					detail: true,
					order: 2
				},
				{
					text: "document-flow.components.correspondent",
					value: "document.correspondent.organization_name",
					active: true,
					sortable: false,
					width: "300",
					cols: {
						"md": "12"
					},
					component: null,
					detail: true,
					order: 17
				},
				{
					text: "document-flow.views.boxes.content",
					value: "document.description",
					active: false,
					sortable: false,
					width: "300",
					cols: {
						"md": "12"
					},
					component: null,
					detail: true,
					order: 18
				},
				{
					text: "components.status",
					value: "status.name",
					width: "140",
					sortable: false,
					active: true,
					cols: {
						"md": "4"
					},
					component: "ui-status",
					detail: true,
					order: 3
				},
			]
		},
		/*
		*
		* */
		async resetIndexTableFilters({ state }) {
			state.inboxIndexTableFilters = {
				search: null,
				priority: null,
				is_read: null,
				has_deadline: null,
				type: null,
				is_agreed: null,
				has_resolution: null,
				status: null,
				statusList: null,
				condition: null,
				journal: null,
				doc_type: null,
				doc_type_name: null,
				register_number: null,
				register_start_date: null,
				register_end_date: null,
				outgoing_number: null,
				outgoing_start_date: null,
				outgoing_end_date: null,
				correspondent: null,
				correspondent_name: null,
				controller: null,
				__correspondent: null,
				__doc_type: null,
				__register_date: null,
				__outgoing_date: null,
				review_user: null,
				review_user_name: null,
				__review_user: null,
				page: 1,
				page_size: 15
			}
		},
		/*
		* Заполняем нужные поля фильтра если пользователь перезагрузил браузер
		* */
		setActiveIndexTableFilters({ commit, state }, payload) {
			Object.entries(payload).forEach(item => {
				const [key, value] = item

				if(value && (typeof value === "boolean" || Array.isArray(value))) {
					state.inboxIndexTableFilters[key] = value
				}

				if(!(value && (typeof value === "boolean"))) {
					state.inboxIndexTableFilters[key] = value
				}

				if(value && (typeof value === "number" || typeof value === "string")) {
					state.inboxIndexTableFilters[key] = Number(value) ? Number(value) : value
				}

				if(value === "true" || value === "false") {
					state.inboxIndexTableFilters[key] = value === "true"
				}
			})

			Object.entries(state.inboxIndexTableFilters).forEach(item => {
				const [key, value] = item

				if(value && value.length && key === "statusList") {
					state.inboxIndexTableFilters.statusList = state.inboxIndexTableFilters.statusList.map(item => parseInt(item))
				}

				if(value && key === "doc_type_name") {
					state.inboxIndexTableFilters.__doc_type = {
						id: state.inboxIndexTableFilters.doc_type,
						name: state.inboxIndexTableFilters.doc_type_name,
						type: "doc_type"
					}
				}

				if(value && key === "register_start_date") {
					state.inboxIndexTableFilters.__register_date = [state.inboxIndexTableFilters.register_start_date, state.inboxIndexTableFilters.register_end_date]
				}

				if(value && key === "outgoing_start_date") {
					state.inboxIndexTableFilters.__outgoing_date = [state.inboxIndexTableFilters.outgoing_start_date, state.inboxIndexTableFilters.outgoing_end_date]
				}

				if(value && key === "correspondent_name") {
					state.inboxIndexTableFilters.__correspondent = {
						id: state.inboxIndexTableFilters.correspondent,
						organization_name: state.inboxIndexTableFilters.correspondent_name
					}
				}

				if(value && key === "review_user_name") {
					state.indexTableFilters.__review_user = {
						id: state.indexTableFilters.review_user,
						full_name: state.indexTableFilters.review_user_name
					}
				}
			})
		}
	},
}
