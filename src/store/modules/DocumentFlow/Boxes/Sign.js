import { SET_ENTITY } from "@/store/mutation-types"

export default {
	namespaced: true,
	state: {
		indexTableCols: [
			{
				text: "#",
				value: "index",
				width: "50",
				active: true,
				sortable: false,
				align: "center",
			},
			{
				text: "document-flow.views.registration.state",
				value: "is_signed",
				width: "135",
				active: true,
				sortable: false
			},
			{
				text: "components.registration-number",
				value: "compose.register_number",
				active: true,
				sortable: false
			},
			{
				text: "document-flow.views.boxes.type-letter",
				value: "compose.type",
				active: true,
				sortable: false
			},
			{
				text: "document-flow.components.registration-date",
				value: "compose.created_date",
				active: true,
				sortable: false
			},
			{
				text: "receiver",
				value: "compose.receiver",
				active: true,
				sortable: false
			},
			// {
			// 	text: "document-flow.views.send-document.to-whom",
			// 	value: "compose.curator",
			// 	active: false,
			// 	sortable: false
			// },
			{
				text: "document-flow.views.boxes.agreement",
				value: "compose.agreed_by",
				active: true,
				sortable: false
			},
			{
				text: "document-flow.views.registration.signatory",
				value: "compose.signed_by",
				active: true,
				sortable: false
			},
			{
				text: "document-flow.views.registration.author",
				value: "compose.author",
				active: true,
				sortable: false,
				width: "125",
			},
			{
				text: "document-flow.views.boxes.short-description",
				value: "compose.short_description",
				active: true,
				sortable: false,
				width: "250",
			},
			{
				text: "document-flow.views.send-document.action",
				value: "action",
				width: "100",
				active: true,
				sortable: false,
			}
		],
		indexTableFilters: {
			search: null,
			is_signed: null,
			__type: null,
			type: null,
			journal_id: null,
			register_number: null,
			register_start_date: null,
			register_end_date: null,
			curator: null,
			curator_name: null,
			__curator: null,
			author: null,
			author_name: null,
			__author: null,
			branches: null,
			branches_name: null,
			__branches: null,
			departments: null,
			departments_name: null,
			__departments: null,
			organizations: null,
			organizations_name: null,
			__organizations: null,
			page: 1,
			page_size: 15
		}
	},
	/**
	 *
	 **/
	actions: {
		/*
		* Заполняем нужные поля колонки таблицы если пользователь перезагрузил браузер
		* */
		setActiveIndexTableCols({ commit }, payload) {
			commit(SET_ENTITY, { module: "documentFlowBoxesSign", entity: "indexTableCols", value: payload }, { root: true })
		},
		/*
		*
		* */
		resetIndexTableCols({ state }) {
			state.indexTableCols = [
				{
					text: "#",
					value: "index",
					width: "50",
					active: true,
					sortable: false,
					align: "center",
				},
				{
					text: "document-flow.views.registration.state",
					value: "is_signed",
					width: "135",
					active: true,
					sortable: false
				},
				{
					text: "components.registration-number",
					value: "compose.register_number",
					active: true,
					sortable: false
				},
				{
					text: "document-flow.views.boxes.type-letter",
					value: "compose.type",
					active: true,
					sortable: false
				},
				{
					text: "document-flow.components.registration-date",
					value: "compose.created_date",
					active: true,
					sortable: false
				},
				{
					text: "receiver",
					value: "compose.receiver",
					active: true,
					sortable: false
				},
				// {
				// 	text: "document-flow.views.send-document.to-whom",
				// 	value: "compose.curator",
				// 	active: false,
				// 	sortable: false
				// },
				{
					text: "document-flow.views.boxes.agreement",
					value: "compose.agreed_by",
					active: true,
					sortable: false
				},
				{
					text: "document-flow.views.registration.signatory",
					value: "compose.signed_by",
					active: true,
					sortable: false
				},
				{
					text: "document-flow.views.registration.author",
					value: "compose.author",
					active: true,
					sortable: false,
					width: "125",
				},
				{
					text: "document-flow.views.boxes.short-description",
					value: "compose.short_description",
					active: true,
					sortable: false,
					width: "300",
				},
				{
					text: "document-flow.views.send-document.action",
					value: "action",
					width: "100",
					active: true,
					sortable: false,
				}
			]
		},
		/*
		*
		* */
		async resetIndexTableFilters({ state }) {
			state.indexTableFilters = {
				search: null,
				is_signed: null,
				__type: null,
				type: null,
				journal_id: null,
				register_number: null,
				register_start_date: null,
				register_end_date: null,
				curator: null,
				curator_name: null,
				__curator: null,
				author: null,
				author_name: null,
				__author: null,
				branches: null,
				branches_name: null,
				__branches: null,
				departments: null,
				departments_name: null,
				__departments: null,
				organizations: null,
				organizations_name: null,
				__organizations: null,
				page: 1,
				page_size: 15
			}
		},
		/*
		* Заполняем нужные поля фильтра если пользователь перезагрузил браузер
		* */
		setActiveIndexTableFilters({ commit, state }, payload) {
			Object.entries(payload).forEach(item => {
				const [key, value] = item

				if(value && (typeof value === "boolean" || Array.isArray(value))) {
					state.indexTableFilters[key] = value
				}

				if(!(value && (typeof value === "boolean"))) {
					state.indexTableFilters[key] = value
				}

				if(value && (typeof value === "number" || typeof value === "string")) {
					state.indexTableFilters[key] = Number(value) ? Number(value) : value
				}

				if(value === "true" || value === "false") {
					state.indexTableFilters[key] = value === "true"
				}
			})

			Object.entries(state.indexTableFilters).forEach(item => {
				const [key, value] = item

				if(value && key === "register_start_date") {
					state.indexTableFilters.__register_date = [state.indexTableFilters.register_start_date, state.indexTableFilters.register_end_date]
				}

				if(value && key === "outgoing_start_date") {
					state.indexTableFilters.__outgoing_date = [state.indexTableFilters.outgoing_start_date, state.indexTableFilters.outgoing_end_date]
				}

				if(value && key === "curator_name") {
					state.indexTableFilters.__curator = {
						id: state.indexTableFilters.curator,
						full_name: state.indexTableFilters.curator_name
					}
				}

				if(value && key === "author_name") {
					state.indexTableFilters.__author = {
						id: state.indexTableFilters.author,
						full_name: state.indexTableFilters.author_name
					}
				}

				if(value && key === "branches_name") {
					state.indexTableFilters.__branches = {
						id: state.indexTableFilters.branches,
						name: state.indexTableFilters.branches_name
					}
				}

				if(value && key === "departments_name") {
					state.indexTableFilters.__departments = {
						id: state.indexTableFilters.departments,
						name: state.indexTableFilters.departments
					}
				}

				if(value && key === "organizations_name") {
					state.indexTableFilters.__organizations = {
						id: state.indexTableFilters.organizations,
						organization_name: state.indexTableFilters.organizations
					}
				}

				if(value && key === "type") {
					state.indexTableFilters.__type = {
						type: state.indexTableFilters.type,
						value: state.indexTableFilters.journal_id,
					}
				}
			})
		}
	},
}
