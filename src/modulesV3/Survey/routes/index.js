const SurveyRoutes = [
  // Client
  {
    path: "survey/client/list",
    name: "HRSurveyClientList",
    component: () => import("@/modulesV3/Survey/views/Client/List"),
  },
  {
    path: "survey/client/show/:id",
      name: "HRSurveyClientShow",
    component: () => import("@/modulesV3/Survey/views/Client/Show"),
  },
  // HR
  {
    path: "survey/hr/list",
    name: "HRSurveyList",
    component: () => import("@/modulesV3/Survey/views/HR/SurveyList"),
  },
  {
    path: "survey/hr/detail/:id",
    name: "HRSurveyDetail",
    component: () => import("@/modulesV3/Survey/views/HR/SurveyDetail"),
  },
  {
    path: "survey/hr/create",
    name: "HRCreateSurvey",
    component: () => import("@/modulesV3/Survey/views/HR/SurveyCreate"),
  },
  {
    path: "survey/hr/update/:id",
    name: "HRUpdateSurvey",
    component: () => import("@/modulesV3/Survey/views/HR/SurveyUpdate"),
  },
  {
    path: "survey/hr/statistics/:id",
    name: "HRSurveyStatistics",
    component: () => import("@/modulesV3/Survey/views/HR/SurveyStatistics"),
  },
  {
    path: "survey/hr/blocks",
    name: "HRSurveyBlocksList",
    component: () => import("@/modulesV3/Survey/views/HR/Blocks/List"),
  }
]

export default SurveyRoutes