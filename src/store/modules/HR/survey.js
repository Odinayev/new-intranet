import { Rows } from "../../../common/componentsV3/Icons"
import { mergeParticipants } from "../../../modulesV3/Survey/utils";
import { adjustObjectToArray } from "../../../common/helpers";
import { FORM_TYPES } from "../../../common/enums/notificationTypes";

export default {
  namespaced: true,
  state: {
    surveyTableCols: [
      {
        text: "№",
        value: "index",
        width: "50",
        active: true,
        sortable: false,
      },
      {
        text: "Названия",
        i18n: "components.base.name",
        value: "title",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Автор",
        i18n: "components.author",
        value: "created_by",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Дата создана",
        i18n: "created-date",
        value: "created_date",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Для кого",
        i18n: "for-whom",
        value: "for_whom",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Тип опросника",
        i18n: "survey-type",
        value: "type",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Опубликовано",
        i18n: "components.state",
        value: "is_published",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Действие",
        i18n: "action",
        value: "action",
        width: "50",
        active: true,
        sortable: false,
      }
    ],
    surveyTableFilters: {
      page: 1,
      page_size: 15
    },
    tabItems: [
      {
        id: 1,
        label: 'current',
        icon: 'CheckCircleLinear',
        iconStrokeWidth: 2,
        count: null,
        value: 'current',
        active: false
      },
      {
        id: 2,
        label: 'history',
        icon: 'ClockCircleLinear',
        iconStrokeWidth: 2,
        count: null,
        value: 'history',
        active: false
      },
      // {
      //   id: 3,
      //   label: 'blocks',
      //   icon: 'Rows',
      //   count: null,
      //   value: 'block',
      //   active: false
      // }
    ],
    buttonLoading: false,
    surveyDetailLoading: false,
    surveyDetail: null,
    surveyReport: null,
    questionsDetailLoading: false,
    surveyModel: {
      title: null,
      description: null,
      category: null,
      type: 'open',
      for_whom: 'for_all',
      start_date: null,
      end_date: null,
      is_active: false,
      is_published: false,
      participants: [
        {
          department: null,
          branch: null
        }
      ],
      __departments: [],
      __branches: [],
      __users: []
    },
    questions: [
      {
        title: null,
        input_type: 'radio',
        block: null,
        survey: null,
        is_required: true,
        options: [
          {
            name: null,
            rate: null,
            is_other: false
          }
        ],
        __text_answer: false,
        __loading: false
      }
    ]
  },
  actions: {
    actionAddQuestionObject({ state }) {
      state.questions.push({
        title: null,
        input_type: 'radio',
        block: null,
        survey: null,
        is_required: true,
        options: [
          {
            name: null,
            is_other: false
          }
        ],
        __text_answer: false
      })
    },
    /** **/
    async actionDeleteQuestionObject({ state }, { qIndex, formType, questionId }) {
      if (formType === FORM_TYPES.CREATE && qIndex) {
        state.questions.splice(qIndex, 1)
      } else if (questionId) {
        return new Promise((resolve, reject) => {
          this.$axios.delete(`/survey/question/${questionId}/`)
            .then(({ data }) => {
              resolve(data)
            }).catch(err => {
            reject(err)
          })
        })
      }
    },
    /** **/
    actionAddQuestionOptionObject({ state }, qIndex) {
      state.questions[qIndex].options.push({
        name: null,
        is_other: false
      })
    },
    /** **/
    actionDeleteQuestionOptionObject({ state }, { qIndex, oIndex }) {
      state.questions[qIndex].options.splice(oIndex, 1)
    },
    /** **/
    actionOnInputTypeChange({ state}, { qIndex, type }) {
      state.questions[qIndex].options = ['radio', 'checkbox'].includes(type) ? [{ name: null, is_other: false }] : [{ name: ".", is_other: false }]
      state.questions[qIndex].__text_answer = null
    },
    /** **/
    actionManage({ state, dispatch }) {
      return new Promise((resolve, reject) => {
        state.buttonLoading = true
        const surveyModel = {
          ...state.surveyModel,
          category: state.surveyModel.category?.id,
          participants: mergeParticipants(state.surveyModel.__departments, state.surveyModel.__branches, state.surveyModel.__users, state.surveyModel.for_whom)
        }

        const questions = state.questions.map((question) => ({
          ...question,
          block: question.block?.id ? question.block?.id : null,
          options: question.input_type === 'radio' ? question.options?.map((option, oIndex) => ({
            name: option.name,
            rate: Number(option.rate) || null,
            is_other: oIndex === question.options?.length - 1 && question.__text_answer
          })) :
            question.input_type === 'checkbox' ? question.options?.map((option, oIndex) => ({
              name: option.name,
              rate: null,
              is_other: oIndex === question.options?.length - 1
            })) : []
        }))

        dispatch('actionCreateSurvey', { surveyModel, questions }).then(({ survey, questions }) => {
          resolve({ survey, questions })
        }).catch(err => {
          reject(err)
        })
      })
    },
    /** **/
    actionCreateSurvey({ state }, { surveyModel, questions }) {
      return new Promise((resolve, reject) => {
        this.$axios.post(`/survey/`, surveyModel)
          .then(({ data }) => {
            const arrayOfQuestions = questions.map(q => ({
              ...q,
              survey: data.id
            }))
            this.$axios.post(`/survey/question/`, arrayOfQuestions)
              .then(res => {
                resolve({ survey: data, questions: res?.data })
              }).catch(err => {
                reject(err)
            })
          }).catch(err => {
            reject(err)
        }).finally(() => {
          state.buttonLoading = false
        })
      })
    },
    /** **/
    actionGetSurveyForUpdate({ state }, id) {
      return new Promise((resolve, reject) => {
        state.surveyDetailLoading = true
        this.$axios.get(`/survey/${id}/`).then(async ({data}) => {
          state.surveyModel = data
          const departments = data.participants.map(item => item.department).filter(Boolean)
          const branches = data.participants.map(item => item.branch).filter(Boolean)
          const users = data.participants.map(item => item.user).filter(Boolean)

          state.surveyModel.__departments = await adjustObjectToArray('departments/department_without_children', departments)
          state.surveyModel.__branches = await adjustObjectToArray('filials', branches)
          state.surveyModel.__users = users
          resolve(data)
          state.surveyDetailLoading = false
        }).catch(err => {
          reject(err)
        })
      })
    },
    /** **/
    actionUpdateSurvey({ state }, id) {
      return new Promise((resolve, reject) => {
        state.buttonLoading = true
        const surveyModel = {
          ...state.surveyModel,
          category: state.surveyModel.category?.id,
          participants: mergeParticipants(state.surveyModel.__departments, state.surveyModel.__branches, state.surveyModel.__users,  state.surveyModel.for_whom)
        }

        this.$axios.put(`/survey/${id}/`, surveyModel).then(({ data }) => {
          resolve(data)
        }).catch(err => {
          reject(err)
        }).finally(() => {
          state.buttonLoading = false
        })
      })
    },
    /** **/
    actionGetQuestionsForUpdate({ state }, surveyId) {
      return new Promise((resolve, reject) => {
        state.questionsDetailLoading = true
        this.$axios.get(`/survey/question/`, {
          params: {
            survey: surveyId,
            page_size: 500
          }
        }).then(({ data }) => {
          state.questions = data.results.map(item => ({
            ...item,
            survey: item?.survey?.id,
            __text_answer: item.options.some(opt => opt.is_other),
            __loading: false
          }))
          state.questionsDetailLoading = false
          resolve(data)
        }).catch(err=> {
          reject(err)
        })
      })
    },
    /** **/
    actionCreateUpdateQuestion({ state }, { qIndex, questionId, surveyId }) {
      return new Promise((resolve, reject) => {
        const question = state.questions[qIndex]
        question.__loading = true
        const model = {
          ...question,
          block: question.block?.id || null,
          survey: surveyId,
          options: question.input_type === 'radio' ? question.options?.map((option, oIndex) => ({
            ...(option.id && { id: option.id }),
            name: option.name,
            rate: Number(option.rate) || null,
            is_other: !!(oIndex === question.options?.length - 1 && question.__text_answer)
          })) : question.input_type === 'checkbox' ? question.options?.map((option, oIndex) => ({
            ...(option.id && { id: option.id }),
            name: option.name,
            rate: null,
            is_other: oIndex === question.options?.length - 1
          })) : []
        }

        if (questionId) {
          this.$axios.put(`/survey/question/${questionId}/`, model)
            .then(({ data }) => {
              resolve(data)
            })
            .catch(err => {
              reject(err)
            })
            .finally(() => {
              question.__loading = false
            })
        } else {
          this.$axios.post(`/survey/question/`, model)
            .then(({ data }) => {
              resolve(data)
            })
            .catch(err => {
              reject(err)
            })
            .finally(() => {
              question.__loading = false
            })
        }
      })
    },
    /** **/
    actionDeleteSurvey({ state }, surveyId) {
      return new Promise((resolve, reject) => {
        this.$axios.delete(`/survey/${surveyId}/`)
          .then((res) => {
            resolve(res)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    /** **/
    actionGetSurveyDetail({ state }, id) {
      return new Promise((resolve, reject) => {
        state.surveyDetailLoading = true
        this.$axios.get(`/survey/${id}/`)
          .then(({ data }) => {
            state.surveyDetail = data
            state.surveyDetailLoading = false
            resolve(data)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    /** **/
    actionGetSurveyReport({ state }, id) {
      return new Promise((resolve, reject) => {
        state.surveyDetailLoading = true
        this.$axios.get(`/survey/${id}/report/`)
          .then(({ data }) => {
            state.surveyReport = {
              ...data,
              questions: data.questions.map((question) => {
                if (question && question.statistics) {
                  return {
                    ...question,
                    series: [
                      {
                        data: question.statistics.map(stat => stat.response_count)
                      }
                    ],
                    chartOptions: {
                      chart: {
                        type: 'bar',
                        stacked: true,
                        toolbar: {
                          show: false
                        },
                      },
                      plotOptions: {
                        bar: {
                          borderRadius: 12,
                          columnWidth: '30%',
                        },
                      },
                      xaxis: {
                        type: 'category',
                        categories: question.statistics.map(stat => stat.text.substring(0, 35)),
                        labels: {
                          show: true,
                          trim: true,
                          rotate: 0,
                          style: {
                            fontSize: '14px',
                            fontFamily: 'Inter, sans-serif',
                            fontWeight: 600
                          },
                        },
                      },
                      legend: {
                        show: false
                      },
                      fill: {
                        opacity: 1,
                        colors: ['#5562E5', '#FFAF02']
                      },
                      tooltip: {
                        enabled: false,
                        followCursor: true,
                        style: {
                          fontSize: '12px',
                          fontFamily: 'Inter'
                        },
                        x: {
                          show: false
                        }
                      }
                    }
                  }
                } else {
                  return question
                }
              })
            }
            state.surveyDetailLoading = false
            console.log(state.surveyReport)
            resolve(data)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    /** **/
    actionResetSurveyQuestionsModel({ state }) {
      state.surveyModel = {
        title: null,
          description: null,
          category: null,
          type: 'open',
          for_whom: 'for_all',
          start_date: null,
          end_date: null,
          is_active: false,
          is_published: false,
          participants: [
          {
            department: null,
            branch: null
          }
        ],
        __departments: [],
        __branches: [],
        __users: []
      }
      state.questions = [
        {
          title: null,
          input_type: 'radio',
          block: null,
          survey: null,
          is_required: true,
          options: [
            {
              name: null,
              is_other: false
            }
          ],
          __text_answer: false,
          __loading: false
        }
      ]
    },
    /** **/
    actionDuplicateSurvey({ commit }, id) {
      return new Promise((resolve, reject) => {
        this.$axios.post(`/survey/${id}/copy/`, {})
          .then(res => {
            console.log(res)
            resolve(res)
          })
          .catch(err => {
            reject(err)
          })
      })
    }
  }
}