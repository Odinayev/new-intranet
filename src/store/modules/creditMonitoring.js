import { SET_ENTITY } from "@/store/mutation-types"
import CrudService from "@/services/crud.service"

export default {
	namespaced: true,
	state: {
		branchesList: [],
		branchesListLoading: true,
		branchById: {},
		subFoldersList: [],
		subFoldersLoading: true,
		uploadingFilesLoading: false,
		filesList: [],
		filesListLoading: true
	},
	/*
	*
	* */
	getters: {

	},
	/*
	*
	* */
	mutations: {
		"CLEAR_LIST"(state, payload) {
			state[payload] = []
		},
		/*
		*
		* */
		"CLEAR_LOADING"(state, payload) {
			state[payload.module] = payload.value
		},
	},
	/*
	*
	* */
	actions: {
		getCurrentBranchList({ commit, rootState }, payload) {
			commit(SET_ENTITY, {
				module: "creditMonitoring",
				entity: "branchesListLoading",
				value: true
			}, { root: true })

			CrudService.getList(`/quiz/${rootState.auth.currentUser.filial}/folder/`, {
				...payload,
				page_size: 30
			})
			.then((data) => {
				commit(SET_ENTITY, {
					module: "creditMonitoring",
					entity: "branchesList",
					value: data.results
				}, { root: true })
			})
			.finally(() => {
				commit(SET_ENTITY, {
					module: "creditMonitoring",
					entity: "branchesListLoading",
					value: false
				}, { root: true })
			})
		},
		/*
		*
		* */
		getBranchById({ commit, rootState }, payload) {
			commit(SET_ENTITY, {
				module: "creditMonitoring",
				entity: "branchesListLoading",
				value: true
			}, { root: true })

			CrudService.getList(`/quiz/${rootState.auth.currentUser.filial}/folder/${payload.id}/`, {
				...payload.params
			})
			.then((data) => {
				commit(SET_ENTITY, {
					module: "creditMonitoring",
					entity: "branchById",
					value: data
				}, { root: true })
			})
			.finally(() => {
				commit(SET_ENTITY, {
					module: "creditMonitoring",
					entity: "branchesListLoading",
					value: false
				}, { root: true })
			})
		},
		/*
		*
		* */
		fetchCreateFolder({ commit, dispatch, rootState, state }, payload) {
			return new Promise((resolve, reject) => {
				CrudService.post(`/quiz/${rootState.auth.currentUser.filial}/folder/`, payload)
				.then((data) => {
					dispatch("getSubFoldersList", { parentId: data.parent })
					resolve()
				})
			})
		},
		/*
		*
		* */
		fetchUpdateFolderName({ commit, dispatch, rootState, state }, payload) {
			return new Promise((resolve, reject) => {
				this.$axios.put(`/quiz/${rootState.auth.currentUser.filial}/folder/${payload.id}/`, payload.data)
				.then(({ data }) => {
					dispatch("getSubFoldersList", { parentId: data.parent })
					resolve()
				})
			})
		},
		/*
		*
		* */
		getSubFoldersList({ commit, rootState }, payload) {
			commit(SET_ENTITY, {
				module: "creditMonitoring",
				entity: "subFoldersLoading",
				value: true
			}, { root: true })

			CrudService.getList(`/quiz/${rootState.auth.currentUser.filial}/folder/${payload.parentId}/sub_folders/`, {
				...payload.params,
				page_size: 50
			})
			.then((data) => {
				commit(SET_ENTITY, {
					module: "creditMonitoring",
					entity: "subFoldersList",
					value: data.results
				}, { root: true })
			})
			.finally(() => {
				commit(SET_ENTITY, {
					module: "creditMonitoring",
					entity: "subFoldersLoading",
					value: false
				}, { root: true })
			})
		},
		/*
		*
		* */
		deleteSubFoldersList({ commit, rootState, dispatch }, payload) {
			return new Promise((resolve, reject) => {
				CrudService.delete(`/quiz/${rootState.auth.currentUser.filial}/folder/${payload.id}/`)
				.then((data) => {
					dispatch("getSubFoldersList", { parentId: payload.parentId })

					resolve()
				})
			})
		},
		/*
		*
		* */
		fetchUploadFiles({ commit, rootState, dispatch }, payload) {
			commit(SET_ENTITY, {
				module: "creditMonitoring",
				entity: "uploadingFilesLoading",
				value: true
			}, { root: true })

			let formData = new FormData()

			for (let i = 0; i < payload.files.length; i++) {
				formData.append("file", payload.files[i])
				formData.append("name", payload.files[i])
				formData.append("given_name", "credit_monitoring")
			}

			this.$axios
			.post('/multiple_upload/', formData)
			.then(({ data }) => {
				CrudService.post(`/quiz/${rootState.auth.currentUser.filial}/folder-documents/`, {
					files: data.map(item => ({ id: item.id })),
					folder: payload.folderId
				})
				.then((data) => {
					dispatch("getUploadFilesList", { params: { folder: payload.folderId } })
				})
				.finally(() => {
					commit(SET_ENTITY, {
						module: "creditMonitoring",
						entity: "uploadingFilesLoading",
						value: false
					}, { root: true })
				})
			})
		},
		/*
		*
		* */
		getUploadFilesList({ commit, rootState }, payload) {
			commit(SET_ENTITY, {
				module: "creditMonitoring",
				entity: "filesListLoading",
				value: true
			}, { root: true })

			CrudService.getList(`/quiz/${rootState.auth.currentUser.filial}/folder-documents/`, {
				...payload.params,
				page_size: 50
			})
			.then((data) => {
				commit(SET_ENTITY, {
					module: "creditMonitoring",
					entity: "filesList",
					value: data.results
				}, { root: true })
			})
			.finally(() => {
				commit(SET_ENTITY, {
					module: "creditMonitoring",
					entity: "filesListLoading",
					value: false
				}, { root: true })
			})
		},
		/*
		*
		* */
		deleteUploadFile({ commit, rootState, dispatch }, payload) {
			return new Promise((resolve, reject) => {
				this.$axios.put(`/quiz/${rootState.auth.currentUser.filial}/folder-documents/${payload.id}/`, {
					files: payload.model.map(item => ({ id: item.id })),
					folder: payload.folderId
				})
				.then(({ data }) => {
					dispatch("getUploadFilesList", { params: { folder: payload.folderId } })

					resolve()
				})
			})
		}
	},
}
