module.exports = {
	pluginOptions: {
		i18n: {
			enableBridge: undefined,
			enableInSFC: undefined,
			fallbackLocale: undefined,
			locale: undefined,
			localeDir: undefined,
		},
	},
	configureWebpack: {
		output: {
			filename: `js/[name].[hash:8]${Math.floor(Math.random() * 9999)}.js`,
			chunkFilename: `js/[name].[hash:8]${Math.floor(Math.random() * 9999)}.js`
		}
	},
	productionSourceMap: false,
	transpileDependencies: ['vuetify'],
};
