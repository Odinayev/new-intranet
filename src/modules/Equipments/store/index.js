import { SET_ENTITY } from "@/store/mutation-types"
import CrudService from "@/services/crud.service"

export default {
  namespaced: true,
  state: {
    departmentList: [],
    departmentUsersList: [],
    equipmentsList: [],
    isEquipmentsLoading: false
  },
  /*
  *
  * */
  getters: {
    getDepartmentList: (state) => state.departmentList,
    departmentUsersList: (state) => state.departmentUsersList,
    getEquipmentsList: (state) => state.equipmentsList,
    getIsEquipmentsLoading: (state) => state.isEquipmentsLoading,
  },
  mutations: {
    "PUSH_LIST"(state, { entity, value }) {
      state[entity].push(...value)
    },
    /*
    *
    * */
    "CLEAR_LIST"(state, { entity }) {
      state[entity] = []
    },
    /**/
    SET_EQUIPMENTS_LOADER(state, payload){
      state.isEquipmentsLoading = payload
    }
  },
  /*
  *
  * */
  actions: {
    fetchEquipmentsList({ commit, rootState }, payload){
      commit("SET_EQUIPMENTS_LOADER", true)
      this.$axios.get(`/${rootState.auth.currentUser.filial}/user/equipments/`, {
        params: {
          search: payload,
          page_size: 2000
        }
      })
        .then(({ data }) => {
          commit(SET_ENTITY, {
            module: "equipmentModule",
            entity: "equipmentsList",
            value: data.results
          }, { root: true });
          commit("SET_EQUIPMENTS_LOADER", false)
        })
    },
    /*
    *
    * */
    fetchDepartmentList({ commit }, payload) {
      commit("SET_EQUIPMENTS_LOADER", true)
      CrudService.getList(`/departments/`, {
        filial: payload,
        condition: "A",
        page_size: 50
      })
        .then((data) => {
          commit(SET_ENTITY, {
            module: "equipmentModule",
            entity: "departmentList",
            value: data.results.filter(item => item.condition === 'A'),
          }, { root: true })
        })
    },
    /*
    *
    * */
    fetchDepartmentSelected({ commit, dispatch }, payload) {
      // Отчистим массив перед тем как заполнить
      dispatch("clearDepartmentUserList")

      dispatch("fetchDepartmentUsers", payload)

      if(payload.sub_departments.length > 0) {
        payload.sub_departments.forEach(item => {
          setTimeout(() => {
            dispatch('fetchDepartmentUsers', item)
          }, 75)

          if(item.sub_departments.length > 0) {
            item.sub_departments.forEach(subItem => {
              setTimeout(() => {
                dispatch('fetchDepartmentUsers', subItem)
              }, 150)
            })
          }
        })
      }
    },
    /*
    *
    * */
    fetchDepartmentUsers({ commit, dispatch }, payload) {
      commit("SET_EQUIPMENTS_LOADER", true)
      CrudService.getList('/phonebook/', {
        dept_code: payload.code,
        filial: payload.mfo,
        search: payload.search,
        page_size: 50
      })
        .then((data) => {
          commit("PUSH_LIST", {
            entity: "departmentUsersList",
            value: data.results
          })
        })
        .then(() => {
          commit("SET_EQUIPMENTS_LOADER", false)
        })
    },
    /*
    *
    * */
    clearDepartmentUserList({ commit }) {
      commit("CLEAR_LIST", { entity: "departmentUsersList" })
    }
  }
}
