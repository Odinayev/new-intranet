export default {
  namespaced: true,
  state: {
    resultDetail: null,
    detailLoading: true,
    sidebar: false,
    checkupDetailTableCols: [
      {
        text: "№",
        value: "index",
        width: "50",
        active: true,
        sortable: false,
      },
      {
        text: "Аудитор",
        i18n: "auditor",
        value: "auditor",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Дата создания",
        i18n: "created-date",
        value: "created_date",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Направление",
        i18n: "directions",
        value: "direction",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Срок (дедлайн)",
        i18n: "deadline",
        value: "deadline",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Содержание",
        i18n: "document-flow.components.content",
        value: "content",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Уровень риска",
        i18n: "risk-level",
        value: "risk_level",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Статус вертикала",
        i18n: "components.status",
        value: "status",
        width: "130",
        active: true,
        sortable: false,
      },
      {
        text: "Действие",
        i18n: "action",
        value: "action",
        width: "50",
        active: true,
        sortable: false,
      }
    ],
    checkupDetailTableFilters: {
      page: 1,
      page_size: 15,
      created_by: null,
      __created_by: null,
    },
    resultTypeList: [
      {
        id: 1,
        label: 'analysis',
        icon: 'FileSearch',
        count: null,
        type: 'analysis',
        active: false
      },
      {
        id: 2,
        label: 'audit-success',
        icon: 'FolderCheck',
        count: null,
        type: 'success',
        active: false
      },
      {
        id: 3,
        label: 'flaw',
        icon: 'FolderClose',
        count: null,
        type: 'flaw',
        active: false
      }
    ]
  },
  actions: {
    actionCreateAuditResult({ commit }, body) {
      return new Promise((resolve, reject) => {
        this.$axios.post(`/audit/results/`, body)
          .then(res => {
            resolve(res)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    actionUpdateAuditResult({ commit }, payload) {
      return new Promise((resolve, reject) => {
        this.$axios.put(`/audit/results/${payload.id}/`, payload.body)
          .then(res => {
            resolve(res)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    actionGetResultDetail({ state }, id) {
      return new Promise((resolve, reject) => {
        state.detailLoading = true
        this.$axios.get(`/audit/results/${id}/`)
          .then(res => {
            if (res.status === 200) {
              state.resultDetail = res.data
              state.detailLoading = false
              resolve(res)
            }
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    actionDeleteAuditResult({ commit }, id) {
      return new Promise((resolve, reject) => {
        this.$axios.delete(`/audit/results/${id}/`)
          .then(res => {
            resolve(res)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    actionGetResultProcessList({ commit }, resultId) {
      return new Promise((resolve, reject) => {
        this.$axios.get(`/audit/result-processes/`, {
          params: {
            result: resultId
          }
        })
          .then(res => {
            resolve(res)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    actionDirectResult({ commit }, body ) {
      return new Promise((resolve, reject) => {
        this.$axios.post(`/audit/result-processes/assign/`, body)
          .then(res => {
            resolve(res)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    actionRejectResult({ commit }, { id, comment }) {
      return new Promise((resolve, reject) => {
        this.$axios.put(`/audit/result-processes/${id}/reject/`, {
          comment
        })
          .then(res => {
            resolve(res)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    actionAcceptResult({ commit }, id) {
      return new Promise((resolve, reject) => {
        this.$axios.put(`/audit/result-processes/${id}/accept/`)
          .then(res => {
            resolve(res)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    /** **/
    actionAcquaintResult({ commit }, id) {
      return new Promise((resolve, reject) => {
        this.$axios.put(`/audit/result-processes/${id}/acquaintance/`)
          .then(res => {
            resolve(res)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    /** **/
    actionGetResultCounts({ state }, { id }) {
      return new Promise((resolve, reject) => {
        this.$axios.get(`/audit/results/unread-counts/${id}/`)
          .then(res => {
            if (res && res.status === 200) {
              state.resultTypeList = state.resultTypeList.map(item => {
                if (res.data[item.type] !== undefined) {
                  return {
                    ...item,
                    count: res.data[item.type]
                  }
                }
                return item
              })
            }
          })
      })
    }
  }
}