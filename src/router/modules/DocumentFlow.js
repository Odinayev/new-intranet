const DocumentFlow = [
	{
		path: "document-flow",
		name: "MainDocumentFlow",
		meta: {
			isAuthRequired: true,
		},
		component: () =>
			import(
				/* webpackChunkName: "DocumentFlow" */ "@/modules/DocumentFlow/views"
			),
		children: [
			{
				path: "home",
				name: "DocumentFlowHome",
				meta: {
					isAuthRequired: true,
				},
				component: () =>
					import(
						/* webpackChunkName: "DocumentFlowHome" */ "@/modules/DocumentFlow/views/Home"
					),
			},
			// Document Registration
			{
				path: "document-registration",
				name: "DocumentFlowRegistration",
				meta: {
					isAuthRequired: true,
				},
				component: () =>
					import(
						/* webpackChunkName: "DocumentFlowRegistration" */ "@/modules/DocumentFlow/views/Registration"
					),
				redirect: { name: "IncomingDocumentTableListType" },
				children: [
					{
						path: "incoming-documents-list",
						name: "IncomingDocumentTableListType",
						redirect: { name: "IncomingIndex" },
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "IncomingDocumentTableListType" */ "@/modules/DocumentFlow/views/Registration/TableListType"
							),
					},
					{
						path: "inner-documents-list",
						name: "DocumentFlowInnerDocumentsList",
						redirect: { name: "InnerIndex" },
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "GridListType" */ "@/modules/DocumentFlow/views/Registration/components/InnerDocuments/List"
							),
					},
					{
						path: "outgoing-documents-list",
						name: "DocumentFlowOutgoingDocumentsList",
						redirect: { name: "OutgoingIndex" },
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "CalendarListType" */ "@/modules/DocumentFlow/views/Registration/components/OutgoingDocuments/List"
							),
					},
					{
						path: "appeals-list",
						name: "DocumentFlowAppealsList",
						redirect: { name: "AppealIndex" },
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "CalendarListType" */ "@/modules/DocumentFlow/views/Registration/components/Appeals/List"
							),
					},
					{
						path: "orders-and-protocols-list",
						name: "DocumentFlowOrdersProtocolsList",
						redirect: { name: "OrderProtocolIndex" },
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "CalendarListType" */ "@/modules/DocumentFlow/views/Registration/components/OrdersProtocols/List"
							),
					},
					{
						path: "filial-documents-list",
						name: "DocumentFlowFilialDocumentsList",
						redirect: { name: "FilialDocumentsIndex" },
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "CalendarListType" */ "@/modules/DocumentFlow/views/Registration/components/FilialDocuments/List"
							),
					},
					{
						path: "applications-list",
						name: "DocumentFlowApplicationsList",
						redirect: { name: "ApplicationIndex" },
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "CalendarListType" */ "@/modules/DocumentFlow/views/Registration/components/Applications/List"
							),
					},
				],
			},

			// Incoming documents Show
			{
				path: "document-registration/incoming-documents-list/show/:id",
				name: "IncomingDocumentRegistrationFormShow",
				meta: {
					isAuthRequired: true,
				},
				component: () =>
					import(
						/* webpackChunkName: "DocumentFlowRegistrationFormShow" */ "@/modules/DocumentFlow/views/Registration/components/IncomingDocuments/Show/IncomingFormShow"
					),
				redirect: { name: "IncomingFormShowView" },
				children: [
					{
						path: "view",
						name: "IncomingFormShowView",
						component: () =>
							import(
								/* webpackChunkName: "IncomingFormShowView" */ "@/modules/DocumentFlow/views/Registration/components/IncomingDocuments/Show/IncomingFormShowView"
							),
					},
					{
						path: "completion-note-finished/:assignmentId",
						name: "CompletionNoteFinishedIncoming",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "CompletionNoteFinishedIncoming" */ "@/modules/DocumentFlow/views/Boxes/Inbox/CompletionNoteFinished"
							),
					},
				],
			},

			// Inner documents Show
			{
				path: "document-registration/inner-documents-list/show/:id",
				name: "InnerDocumentRegistrationFormShow",
				meta: {
					isAuthRequired: true,
				},
				component: () =>
					import(
						/* webpackChunkName: "DocumentFlowRegistrationFormShow" */ "@/modules/DocumentFlow/views/Registration/components/InnerDocuments/Show/InnerFormShow"
					),
				redirect: { name: "InnerFormShowView" },
				children: [
					{
						path: "view",
						name: "InnerFormShowView",
						component: () =>
							import(
								/* webpackChunkName: "IncomingFormShowView" */ "@/modules/DocumentFlow/views/Registration/components/InnerDocuments/Show/InnerFormShowView"
							),
					},
					{
						path: "completion-note-finished/:assignmentId",
						name: "CompletionNoteFinishedInner",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "CompletionNoteFinishedInner" */ "@/modules/DocumentFlow/views/Boxes/Inbox/CompletionNoteFinished"
							),
					},
				],
			},

			// Outgoing documents Show
			{
				path: "document-registration/outgoing-documents-list/show/:id",
				name: "OutgoingDocumentRegistrationFormShow",
				meta: {
					isAuthRequired: true,
				},
				component: () =>
					import(
						/* webpackChunkName: "DocumentFlowRegistrationFormShow" */ "@/modules/DocumentFlow/views/Registration/components/OutgoingDocuments/Show/OutgoingFormShow"
					),
				redirect: { name: "OutgoingFormShowView" },
				children: [
					{
						path: "view",
						name: "OutgoingFormShowView",
						component: () =>
							import(
								/* webpackChunkName: "IncomingFormShowView" */ "@/modules/DocumentFlow/views/Registration/components/OutgoingDocuments/Show/OutgoingFormShowView"
							),
					},
				],
			},

			// Appeal documents Show
			{
				path: "document-registration/appeals-list/show/:id",
				name: "AppealDocumentRegistrationFormShow",
				meta: {
					isAuthRequired: true,
				},
				component: () =>
					import(
						/* webpackChunkName: "DocumentFlowRegistrationFormShow" */ "@/modules/DocumentFlow/views/Registration/components/Appeals/Show/AppealFormShow"
					),
				redirect: { name: "AppealFormShowView" },
				children: [
					{
						path: "view",
						name: "AppealFormShowView",
						component: () =>
							import(
								/* webpackChunkName: "IncomingFormShowView" */ "@/modules/DocumentFlow/views/Registration/components/Appeals/Show/AppealFormShowView"
							),
					},
				],
			},

			// Call Center documents Show
			{
				path: "document-registration/call-center-appeals-list/show/:id",
				name: "CallCenterDocumentRegistrationFormShow",
				meta: {
					isAuthRequired: true,
				},
				component: () =>
					import(
						/* webpackChunkName: "DocumentFlowRegistrationFormShow" */ "@/modules/DocumentFlow/views/Registration/components/AppealCallCenter/Show/AppealFormShow"
						),
				redirect: { name: "CallCenterAppealFormShowView" },
				children: [
					{
						path: "view",
						name: "CallCenterAppealFormShowView",
						component: () =>
							import(
								/* webpackChunkName: "IncomingFormShowView" */ "@/modules/DocumentFlow/views/Registration/components/AppealCallCenter/Show/AppealFormShowView"
								),
					},
				],
			},

			// Orders Protocols Show
			{
				path: "document-registration/orders-and-protocols-list/show/:id",
				name: "OrdersProtocolsDocumentRegistrationFormShow",
				meta: {
					isAuthRequired: true,
				},
				component: () =>
					import(
						/* webpackChunkName: "DocumentFlowRegistrationFormShow" */ "@/modules/DocumentFlow/views/Registration/components/OrdersProtocols/Show/OrdersProtocolsShow"
					),
				redirect: { name: "OrdersProtocolsShowView" },
				children: [
					{
						path: "view",
						name: "OrdersProtocolsShowView",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "IncomingFormShowView" */ "@/modules/DocumentFlow/views/Registration/components/OrdersProtocols/Show/OrdersProtocolsShowView"
							),
					},
					{
						path: "completion-note-finished/:assignmentId",
						name: "CompletionNoteFinishedProtocol",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "CompletionNoteFinishedProtocol" */ "@/modules/DocumentFlow/views/Boxes/Inbox/CompletionNoteFinished"
							),
					},
				],
			},

			// Application Show
			{
				path: "document-registration/application-list/show/:id",
				name: "ApplicationsRegistrationFormShow",
				meta: {
					isAuthRequired: true,
				},
				component: () =>
					import(
						/* webpackChunkName: "DocumentFlowRegistrationFormShow" */ "@/modules/DocumentFlow/views/Registration/components/Applications/Show/ApplicationFormShow"
					),
				redirect: { name: "ApplicationsShowView" },
				children: [
					{
						path: "view",
						name: "ApplicationsShowView",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "IncomingFormShowView" */ "@/modules/DocumentFlow/views/Registration/components/Applications/Show/ApplicationFormShowView"
							),
					},
				],
			},

			// XDFU documents Show
			{
				path: "document-registration/secret-documents-list/show/:id",
				name: "SecretDocumentsRegistrationFormShow",
				meta: {
					isAuthRequired: true,
				},
				component: () =>
					import(
						/* webpackChunkName: "DocumentFlowRegistrationFormShow" */ "@/modules/DocumentFlow/views/Registration/components/SecretDocuments/Show/SecretDocumentsShow"
						),
				redirect: { name: "SecretDocumentsFormShowView" },
				children: [
					{
						path: "view",
						name: "SecretDocumentsFormShowView",
						component: () =>
							import(
								/* webpackChunkName: "IncomingFormShowView" */ "@/modules/DocumentFlow/views/Registration/components/SecretDocuments/Show/SecretDocumentsShowView"
								),
					},
					{
						path: "completion-note-finished/:assignmentId",
						name: "CompletionNoteFinishedIncoming",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "CompletionNoteFinishedIncoming" */ "@/modules/DocumentFlow/views/Boxes/Inbox/CompletionNoteFinished"
								),
					},
				],
			},

			// Filial Documents Show
			{
				path: "document-registration/filial-documents-list/show/:id",
				name: "FilialDocumentsRegistrationFormShow",
				meta: {
					isAuthRequired: true,
				},
				component: () =>
					import(
						/* webpackChunkName: "DocumentFlowRegistrationFormShow" */ "@/modules/DocumentFlow/views/Registration/components/FilialDocuments/Show/FilialDocumentsShow"
					),
				redirect: { name: "FilialDocumentsShowView" },
				children: [
					{
						path: "view",
						name: "FilialDocumentsShowView",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "IncomingFormShowView" */ "@/modules/DocumentFlow/views/Registration/components/FilialDocuments/Show/FilialDocumentsShowView"
							),
					},
				],
			},

			//  Incoming documents Create
			{
				path: "incoming-document-registration/create/:id",
				name: "DocumentFlowIncomingRegistrationFormCreate",
				meta: {
					isAuthRequired: true,
				},
				component: () =>
					import(
						/* webpackChunkName: "DocumentFlowRegistrationFormCreate" */ "@/modules/DocumentFlow/views/Registration/components/IncomingDocuments/Create"
					),
			},

			// Inner documents Create
			{
				path: "inner-document-registration/create/:id",
				name: "DocumentFlowInnerRegistrationFormCreate",
				meta: {
					isAuthRequired: true,
				},
				component: () =>
					import(
						/* webpackChunkName: "DocumentFlowRegistrationFormCreate" */ "@/modules/DocumentFlow/views/Registration/components/InnerDocuments/Create"
					),
			},

			// Outgoing documents Create
			{
				path: "outgoing-document-registration/create/:id",
				name: "DocumentFlowOutgoingRegistrationFormCreate",
				meta: {
					isAuthRequired: true,
				},
				component: () =>
					import(
						/* webpackChunkName: "DocumentFlowRegistrationFormCreate" */ "@/modules/DocumentFlow/views/Registration/components/OutgoingDocuments/Create"
					),
			},

			// Appeals Create
			{
				path: "appeal-registration/create/:id",
				name: "DocumentFlowAppealRegistrationFormCreate",
				meta: {
					isAuthRequired: true,
				},
				component: () =>
					import(
						/* webpackChunkName: "DocumentFlowRegistrationFormCreate" */ "@/modules/DocumentFlow/views/Registration/components/Appeals/Create"
					),
			},

			// Call center appeals Create
			{
				path: "call-center-appeal-registration/create/:id",
				name: "DocumentFlowCallCenterAppealRegistrationFormCreate",
				meta: {
					isAuthRequired: true,
				},
				component: () =>
					import(
						/* webpackChunkName: "DocumentFlowRegistrationFormCreate" */ "@/modules/DocumentFlow/views/Registration/components/AppealCallCenter/Create"
						),
			},

			// Orders Protocols Create
			{
				path: "orders-protocols-registration/create/:id",
				name: "DocumentFlowOrdersProtocolsRegistrationFormCreate",
				meta: {
					isAuthRequired: true,
				},
				component: () =>
					import(
						/* webpackChunkName: "DocumentFlowRegistrationFormCreate" */ "@/modules/DocumentFlow/views/Registration/components/OrdersProtocols/Create"
					),
			},
			//  XDFU documents Create
			{
				path: "secret-documents-registration/create/:id",
				name: "DocumentFlowSecretDocumentsRegistrationFormCreate",
				meta: {
					isAuthRequired: true,
				},
				component: () =>
					import(
						/* webpackChunkName: "DocumentFlowRegistrationFormCreate" */ "@/modules/DocumentFlow/views/Registration/components/SecretDocuments/Create"
						),
			},
			// End Document Registration

			// Boxes
			{
				path: "boxes",
				name: "DocumentFlowBoxes",
				meta: {
					isAuthRequired: true,
				},
				component: () =>
					import(
						/* webpackChunkName: "DocumentFlowBoxes" */ "@/modules/DocumentFlow/views/Boxes"
					),
				// redirect: { name: "BoxesReview" },
				children: [
					//Sign
					{
						path: "sign",
						name: "BoxesSign",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "BoxesSign" */ "@/modules/DocumentFlow/views/Boxes/Sign"
							),
						redirect: { name: "SignTableType" },
						children: [
							{
								path: "table",
								name: "SignTableType",
								meta: {
									isAuthRequired: true,
								},
								component: () =>
									import(
										/* webpackChunkName: "SignTableType" */ "@/modules/DocumentFlow/views/Boxes/Sign/TableType"
									),
							},
						],
					},

					// Approval
					{
						path: "approval",
						name: "BoxesApproval",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "BoxesApproval" */ "@/modules/DocumentFlow/views/Boxes/Approval"
							),
						redirect: { name: "ApprovalTableType" },
						children: [
							{
								path: "table",
								name: "ApprovalTableType",
								meta: {
									isAuthRequired: true,
								},
								component: () =>
									import(
										/* webpackChunkName: "ApprovalTableType" */ "@/modules/DocumentFlow/views/Boxes/Approval/TableType"
									),
							},
						],
					},

					// Review
					{
						path: "review",
						name: "BoxesReview",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "BoxesReview" */ "@/modules/DocumentFlow/views/Boxes/Review"
							),
						redirect: { name: "BoxesTableType" },
						children: [
							{
								path: "table",
								name: "BoxesTableType",
								meta: {
									isAuthRequired: true,
								},
								component: () =>
									import(
										/* webpackChunkName: "BoxesTableType" */ "@/modules/DocumentFlow/views/Boxes/Review/TableType"
									),
							},
							{
								path: "grid",
								name: "BoxesGridType",
								meta: {
									isAuthRequired: true,
								},
								component: () =>
									import(
										/* webpackChunkName: "BoxesGridListType" */ "@/modules/DocumentFlow/views/Boxes/Review/GridType"
									),
							},
							{
								path: "calendar",
								name: "BoxesCalendarType",
								meta: {
									isAuthRequired: true,
								},
								component: () =>
									import(
										/* webpackChunkName: "BoxesCalendarListType" */ "@/modules/DocumentFlow/views/Boxes/Review/CalendarType"
									),
							},
						],
					},

					// Inbox
					{
						path: "inbox",
						name: "BoxesInbox",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "BoxesInbox" */ "@/modules/DocumentFlow/views/Boxes/Inbox"
							),
						redirect: { name: "InboxTableType" },
						children: [
							{
								path: "table",
								name: "InboxTableType",
								meta: {
									isAuthRequired: true,
								},
								component: () =>
									import(
										/* webpackChunkName: "InboxTableType" */ "@/modules/DocumentFlow/views/Boxes/Inbox/TableType"
									),
							},
							{
								path: "grid",
								name: "InboxGridType",
								meta: {
									isAuthRequired: true,
								},
								component: () =>
									import(
										/* webpackChunkName: "InboxGridType" */ "@/modules/DocumentFlow/views/Boxes/Inbox/GridType"
									),
							},
							{
								path: "calendar",
								name: "InboxCalendarType",
								meta: {
									isAuthRequired: true,
								},
								component: () =>
									import(
										/* webpackChunkName: "InboxCalendarType" */ "@/modules/DocumentFlow/views/Boxes/Inbox/CalendarType"
									),
							},
							{
								path: "created-resolutions-list",
								name: "InboxCreatedResolutionsList",
								meta: {
									isAuthRequired: true,
								},
								component: () =>
									import(
										/* webpackChunkName: "InboxCreatedResolutionsList" */ "@/modules/DocumentFlow/views/Boxes/Inbox/CreatedResolutionsList"
									),
							},
						],
					},

					// Created Resolutions
					{
						path: "created-resolutions",
						name: "CreatedResolutions",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "CreatedResolutions" */ "@/modules/DocumentFlow/views/Boxes/CreatedResolutions"
							),
						redirect: { name: "CreatedResolutionsTableType" },
						children: [
							{
								path: "table",
								name: "CreatedResolutionsTableType",
								meta: {
									isAuthRequired: true,
								},
								component: () =>
									import(
										/* webpackChunkName: "CreatedResolutionsTableType" */ "@/modules/DocumentFlow/views/Boxes/CreatedResolutions/TableType"
									),
							},
						],
					},

					// NA KONTROL
					{
						path: "control",
						name: "BoxesControl",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "BoxesInbox" */ "@/modules/DocumentFlow/views/Boxes/Control"
							),
						redirect: { name: "ControlTableType" },
						children: [
							{
								path: "table-control",
								name: "ControlTableType",
								meta: {
									isAuthRequired: true,
								},
								component: () =>
									import(
										/* webpackChunkName: "InboxTableType" */ "@/modules/DocumentFlow/views/Boxes/Control/TableType"
									),
							},
						],
					},
				],
			},

			// Send document
			{
				path: "sending-documents",
				name: "SendingDocuments",
				redirect: { name: "SendDocumentNew" },
				component: () =>
					import(
						/* webpackChunkName: "SendDocument" */ "@/modules/DocumentFlow/views/SendDocument"
					),
			},

			{
				path: "sending-documents/:id",
				name: "SendingDocumentsShow",
				component: () =>
					import(
						/* webpackChunkName: "SendingDocumentsShow" */ "@/modules/DocumentFlow/views/SendDocument/show"
					),
			},
			{
				path: "sending-documents/:id/show",
				name: "SendingDocumentsOpen",
				component: () =>
					import(
						/* webpackChunkName: "SendingDocumentsOpen" */ "@/modules/DocumentFlow/views/SendDocument/CreateForm"
					),
			},
			{
				path: "sending-documents/:id/inner",
				name: "SendingDocumentsInnerUpdate",
				component: () =>
					import(
						/* webpackChunkName: "SendingDocumentsInnerUpdate" */ "@/modules/DocumentFlow/views/SendDocument/InnerFormUpdate"
					),
			},
			{
				path: "sending-documents/:id/leadership",
				name: "SendingDocumentsLeadershipUpdate",
				component: () =>
					import(
						/* webpackChunkName: "SendingDocumentsLeadershipUpdate" */ "@/modules/DocumentFlow/views/SendDocument/LeadershipUpdate"
					),
			},
			{
				path: "sending-documents/:id/reference",
				name: "SendingDocumentsReferenceUpdate",
				component: () =>
					import(
						/* webpackChunkName: "SendingDocumentsLeadershipUpdate" */ "@/modules/DocumentFlow/views/SendDocumentNew/DocumentTypes/ReferenceUpdate"
						),
			},
			{
				path: "sending-documents/:id/leadership/show",
				name: "SendingDocumentsLeadershipShow",
				component: () =>
					import(
						/* webpackChunkName: "SendingDocumentsLeadershipUpdate" */ "@/modules/DocumentFlow/views/SendDocument/LeadershipShow"
					),
			},
			{
				path: "sending-documents/:id/decree",
				name: "SendingDocumentsDecreeUpdate",
				component: () =>
					import(
						/* webpackChunkName: "SendingDocumentsLeadershipUpdate" */ "@/modules/DocumentFlow/views/SendDocument/DecreeUpdate"
					),
			},
			{
				path: "sending-documents/:id/outgoing",
				name: "SendingDocumentsInnerOutgoing",
				component: () =>
					import(
						/* webpackChunkName: "SendingDocumentsInnerOutgoing" */ "@/modules/DocumentFlow/views/SendDocument/OutgoingFormUpdate"
					),
			},
			{
				path: "sending-documents/:id/protocol",
				name: "SendingDocumentsProtocolUpdate",
				component: () =>
					import(
						/* webpackChunkName: "SendingDocumentsInnerOutgoing" */ "@/modules/DocumentFlow/views/SendDocument/ProtocolFormUpdate"
					),
			},
			{
				path: "sending-documents/:id/committee-protocol",
				name: "SendingDocumentsCommitteeProtocolUpdate",
				component: () =>
					import(
						/* webpackChunkName: "SendingDocumentsInnerOutgoing" */ "@/modules/DocumentFlow/views/SendDocument/CommitteeProtocolUpdate"
						),
			},
			{
				path: "sending-documents-create",
				name: "SendingDocumentsCreate",
				component: () =>
					import(
						/* webpackChunkName: "SendingDocumentsCreate" */ "@/modules/DocumentFlow/views/SendDocument/CreateForm"
					),
			},
			{
				path: "create-application",
				name: "SendingDocumentsCreateApplication",
				component: () =>
					import(
						"@/modules/DocumentFlow/views/SendDocument/CreateApplicationCreate"
					),
			},
			{
				path: "create-application/:id",
				name: "SendingDocumentsCreateApplicationUpdate",
				component: () =>
					import(
						"@/modules/DocumentFlow/views/SendDocument/CreateApplicationUpdate"
					),
			},
			{
				path: "create-application-show/:id",
				name: "SendingDocumentsCreateApplicationShow",
				component: () =>
					import(
						"@/modules/DocumentFlow/views/SendDocument/CreateApplicationShow"
					),
			},
			// Рапорт для приема на работу
			{
				path: "sending-documents/:id/job",
				name: "SendingDocumentsJobUpdate",
				component: () =>
					import(
						/* webpackChunkName: "SendingDocumentsJobUpdate" */ "@/modules/DocumentFlow/views/SendDocument/JobApplicationReportUpdate"
					),
			},
			// /Рапорт для приема на работу

			// Создать приказ
			{
				path: "sending-documents/:id/hr_order",
				name: "SendingDocumentsOrderUpdateUpdate",
				component: () =>
					import(
						/* webpackChunkName: "CreateOrderUpdate" */ "@/modules/DocumentFlow/views/Boxes/Inbox/CreateOrderUpdate"
					),
			},
			// /Создать приказ

			// Изменить произвольный приказ
			{
				path: "sending-documents/:id/arbitrary_order",
				name: "ArbitraryOrderUpdate",
				component: () =>
					import(
						/* webpackChunkName: "CreateOrderUpdate" */ "@/modules/DocumentFlow/views/SendDocument/ArbitraryOrderUpdate.vue"
						),
			},
			// Изменить произвольный приказ

			{
				path: "sending-documents/:id/compliance_regulation",
				name: "ComplianceRegulationUpdate",
				component: () =>
					import(
						/* webpackChunkName: "CreateOrderUpdate" */ "@/modules/DocumentFlow/views/SendDocument/TrainingSpecialistsUpdate.vue"
						),
			},
			// Trip license update
			{
				path: "sending-documents/:id/trip-license",
				name: "TripLicenseUpdate",
				component: () =>
					import(
						/* webpackChunkName: "CreateOrderUpdate" */ "@/modules/DocumentFlow/views/SendDocument/TripLicenseUpdate.vue"
						),
			},

			// Board Protocol
			{
				path: "board-protocol-create",
				name: "BoardProtocolCreate",
				component: () =>
					import(
						/* webpackChunkName: "SendingDocumentsCreate" */ "@/modules/DocumentFlow/views/SendDocument/CreateForm"
					),
			},
			// End Board Protocol

			// Review Show
			{
				path: "boxes/review/show/:id/:type",
				name: "ReviewFormShow",
				meta: {
					isAuthRequired: true,
				},
				component: () =>
					import(
						"@/modules/DocumentFlow/views/Boxes/Review/ReviewFormShow"
					),
				redirect: { name: "ReviewFormView" },
				children: [
					{
						path: "view",
						name: "ReviewFormView",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								"@/modules/DocumentFlow/views/Boxes/Review/ReviewFormShowView"
							),
					},
					{
						path: "resolution",
						name: "ReviewFormResolution",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "ReviewFormResolution" */ "@/modules/DocumentFlow/components/Resolution"
							),
					},
					{
						path: "completion-note",
						name: "ReviewFormCompletionNote",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "ReviewFormCompletionNote" */ "@/modules/DocumentFlow/components/CompletionNote"
							),
					},
					{
						path: "project-resolution",
						name: "ReviewFormProjectResolution",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "ProjectResolutionCreate" */ "@/modules/DocumentFlow/views/Boxes/Review/ProjectResolutionCreate"
							),
					},
					{
						path: "project-resolution-edit",
						name: "ReviewFormProjectResolutionEdit",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "ReviewFormProjectResolutionEdit" */ "@/modules/DocumentFlow/views/Boxes/Review/ProjectResolutionEdit"
							),
					},
					{
						path: "remove-users-assignment",
						name: "RemoveUsersAssignment",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "RemoveUsersAssignment" */ "@/modules/DocumentFlow/views/Boxes/Review/RemoveUsersAssignment"
								),
					},
					{
						path: "completion-note-finished/:assignmentId",
						name: "CompletionNoteFinishedReview",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "CompletionNoteFinishedReview" */ "@/modules/DocumentFlow/views/Boxes/Inbox/CompletionNoteFinished"
							),
					},
					{
						path: "create-resolution",
						name: "CreateResolution",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "CreateResolution" */ "@/modules/DocumentFlow/views/Boxes/Review/CreateResolution"
							),
					},
				],
			},

			// Inbox Show
			{
				path: "boxes/inbox/show/:id/:type",
				name: "InboxFormShow",
				meta: {
					isAuthRequired: true,
				},
				component: () =>
					import("@/modules/DocumentFlow/views/Boxes/Inbox/InboxFormShow"),
				redirect: { name: "InboxFormShowView" },
				children: [
					{
						path: "view",
						name: "InboxFormShowView",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								"@/modules/DocumentFlow/views/Boxes/Inbox/InboxFormShowView"
							),
					},
					{
						path: "create-order",
						name: "InboxCreateOrder",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								"@/modules/DocumentFlow/views/Boxes/Inbox/CreateOrder"
							),
					},
					{
						path: "resolution",
						name: "InboxResolutionView",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "InboxResolutionView" */ "@/modules/DocumentFlow/views/Boxes/Inbox/ResolutionView"
							),
					},
					{
						path: "resolution-update",
						name: "InboxResolutionUpdate",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "InboxResolutionUpdate" */ "@/modules/DocumentFlow/views/Boxes/Inbox/ResolutionUpdate"
							),
					},
					{
						path: "control-point-update",
						name: "InboxControlPointUpdate",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "InboxControlPointUpdate" */ "@/modules/DocumentFlow/views/Boxes/Inbox/ControlPointUpdate"
							),
					},
					{
						path: "for-information-update",
						name: "InboxForInformationUpdate",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "InboxForInformationUpdate" */ "@/modules/DocumentFlow/views/Boxes/Inbox/ForInformationUpdate"
							),
					},
					{
						path: "completion-note",
						name: "CompletionNote",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "CompletionNote" */ "@/modules/DocumentFlow/views/Boxes/Inbox/CompletionNote"
							),
					},
					{
						path: "completion-note-edit",
						name: "CompletionNoteEdit",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "CompletionNote" */ "@/modules/DocumentFlow/views/Boxes/Inbox/CompletionNoteEdit"
							),
					},
					{
						path: "completion-note-finished/:assignmentId",
						name: "CompletionNoteFinished",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "CompletionNoteFinished" */ "@/modules/DocumentFlow/views/Boxes/Inbox/CompletionNoteFinished"
							),
					},
					{
						path: "remove-from-control",
						name: "RemoveFromControl",
						meta: {
							isAuthRequired: true,
						},
						component: () =>
							import(
								/* webpackChunkName: "CompletionNoteFinished" */ "@/modules/DocumentFlow/views/Boxes/Inbox/RemoveFromControl"
							),
					},
				],
			},

			// Approval Show
			{
				path: "boxes/approval/show/:id",
				name: "ApprovalFormShow",
				meta: {
					isAuthRequired: true,
				},
				component: () =>
					import("@/modules/DocumentFlow/views/Boxes/Approval/show"),
			},

			// Sign Show
			{
				path: "boxes/sign/show/:id",
				name: "SignFormShow",
				meta: {
					isAuthRequired: true,
				},
				component: () =>
					import("@/modules/DocumentFlow/views/Boxes/Sign/show"),
			},

			// Statistic
			{
				path: "statistic",
				name: "Statistic",
				component: () => import("@/modules/DocumentFlow/views/Statistic"),
			},

			// Execution Statistic
			{
				path: "execution-statistics",
				name: "ExecutionStatistics",
				component: () =>
					import(
						"@/modules/DocumentFlow/views/Statistic/ExecutionStatistics/"
					),
			},
			// End Execution Statistic

			// Sending Document Statistic
			{
				path: "sending-document-statistics",
				name: "SendingDocumentStatistics",
				component: () =>
					import(
						"@/modules/DocumentFlow/views/Statistic/SendingDocumentStatistics/"
						),
			},
			// End Sending Document Statistic

			// KPI
			{
				path: "kpi",
				name: "StatisticKPI",
				component: () => import("@/modules/DocumentFlow/views/Statistic/KPI")
			},
			{
				path: "kpi-departments",
				name: "StatisticKPIDepartment",
				component: () => import("@/modules/DocumentFlow/views/Statistic/KPI/department.vue")
			},
			{
				path: "kpi-users",
				name: "StatisticKPIUsers",
				component: () => import("@/modules/DocumentFlow/views/Statistic/KPI/users.vue")
			},
			{
				path: "kpi-detail",
				name: "StatisticKPIDetail",
				component: () => import("@/modules/DocumentFlow/views/Statistic/KPI/show.vue")
			},
			{
				path: "board-protocol-show",
				name: "BoardProtocolShow",
				component: () => import("@/modules/DocumentFlow/views/Statistic/KPI/boardProtocolShow.vue")
			},
			// /KPI

			{
				path: "review-list",
				name: "ExecutionStatisticsReviewList",
				component: () =>
					import(
						"@/modules/DocumentFlow/views/Statistic/ExecutionStatistics/ReviewList"
					),
			},

			{
				path: "inbox-list",
				name: "ExecutionStatisticsInboxList",
				component: () =>
					import(
						"@/modules/DocumentFlow/views/Statistic/ExecutionStatistics/InboxList"
					),
			},

			// Search
			{
				path: "search",
				name: "Search",
				component: () => import("@/modules/DocumentFlow/views/Search"),
			},

			// Экспорт
			{
				path: "export",
				name: "Export",
				component: () => import("@/modules/DocumentFlow/views/Export"),
			},
		],
	},
];

export default DocumentFlow;
