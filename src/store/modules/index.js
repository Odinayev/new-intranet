import { camelCase } from "lodash";

const requireModule = require.context(
	'.', true, /\.js$/,
);
const modules = {};

requireModule.keys().forEach((fileName) => {
	if (fileName === './index.js') {
		return;
	}

	const moduleConfig = requireModule(fileName);
	const moduleName = camelCase(fileName.replace(/(\.\/|\.js)/g, ''));
	modules[moduleName] = moduleConfig.default || moduleConfig;
});

console.log("MODULES", modules)

export default modules;
