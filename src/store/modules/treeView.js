import { SET_ENTITY } from "@/store/mutation-types"
import CrudService from "@/services/crud.service"
import { getItemRow, findUserById } from "@/common/helpers"

export default {
	namespaced: true,
	state: {
		treeList: []
	},
	/*
	*
	* */
	actions: {
		/*
		* Получить дерево пользователей
		* */
		async fetchTreeUsersList({ commit, rootState, state }, payload) {
			await CrudService.getList(`v2/docflow/${rootState.auth.currentUser.filial}/resolution-tree/${payload.treeId}/`)
			.then((data) => {
				// let findUser = null
				//
				// for (const review of data.review_users) {
				// 	let assignees = review.assignees[0]
				//
				// 	findUser = findUserById(assignees.assignment, 9512)
				// }
				//
				// const treeUp = (user) => {
				// 	let current = user
				//
				// 	while (current.parent) {
				// 		current = current.parent
				// 	}
				//
				// 	return current
				// }
				//
				// let asd = treeUp(findUser)
				//
				// console.log("111", findUser)
				// console.log("222", asd)

				state.treeList = data.review_users
			})
		}
	}
}
