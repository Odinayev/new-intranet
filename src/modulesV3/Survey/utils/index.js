export const mergeParticipants = (departments = [], branches = [], users = [], for_whom) => {
  if (for_whom !== 'for_all') {
    const maxLength = Math.max(departments.length, branches.length, users.length)

    return Array.from({length: maxLength}, (_, i) => ({
      department: departments[i]?.id || null,
      branch: branches[i]?.id || null,
      user: users[i]?.id || null
    }))
  } else {
    return [
      {
        department: null,
        branch: 25,
        user: null
      }
    ]
  }
}
